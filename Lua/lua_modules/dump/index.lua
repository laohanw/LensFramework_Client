
local function dump_value_(v)
  if type(v) == "string" then
      v = "\"" .. v .. "\""
  end
  return tostring(v)
end

function dump(value, desciption, nesting)
  if type(nesting) ~= "number" then nesting = 3 end

  local lookupTable = {}
  local result = {}

  local traceback = string.split(debug.traceback("", 2), "\n")
  --print("dump from: " .. string.trim(traceback[3]))

  local function dump_(value, desciption, indent, nest, keylen)
      desciption = desciption or "<var>"
      local spc = ""
      if type(keylen) == "number" then
          spc = string.rep(" ", keylen - string.len(dump_value_(desciption)))
      end
      if type(value) ~= "table" then
          result[#result +1 ] = string.format("%s%s%s = %s", indent, dump_value_(desciption), spc, dump_value_(value))
      elseif lookupTable[tostring(value)] then
          result[#result +1 ] = string.format("%s%s%s = *REF*", indent, dump_value_(desciption), spc)
      else
          lookupTable[tostring(value)] = true
          if nest > nesting then
              result[#result +1 ] = string.format("%s%s = *MAX NESTING*", indent, dump_value_(desciption))
          else
              result[#result +1 ] = string.format("%s%s = {", indent, dump_value_(desciption))
              local indent2 = indent.."    "
              local keys = {}
              local keylen = 0
              local values = {}
              for k, v in pairs(value) do
                  keys[#keys + 1] = k
                  local vk = dump_value_(k)
                  local vkl = string.len(vk)
                  if vkl > keylen then keylen = vkl end
                  values[k] = v
              end
              table.sort(keys, function(a, b)
                  if type(a) == "number" and type(b) == "number" then
                      return a < b
                  else
                      return tostring(a) < tostring(b)
                  end
              end)
              for i, k in ipairs(keys) do
                  dump_(values[k], k, indent2, nest + 1, keylen)
              end
              result[#result +1] = string.format("%s}", indent)
          end
      end
  end
  dump_(value, desciption, "- ", 1)

  for i, line in ipairs(result) do
      print(line)
  end
end

function dumptree(obj, width)

  -- 递归打印函数
  local dump_obj;
  local end_flag = {};

  local function make_indent(layer, is_end)
      local indent = "";
      end_flag[layer] = is_end;
      local subIndent = string.rep("  ", width)
      for index = 1, layer - 1 do
          if end_flag[index] then
              indent = indent.." "..subIndent
          else
              indent = indent.."|"..subIndent
          end
      end

      if is_end then
          return indent.."└"..string.rep("─", width).." "
      else
          return indent.."├"..string.rep("─", width).." "
      end
  end

  local function make_quote(str)
      str = string.gsub(str, "[%c\\\"]", {
          ["\t"] = "\\t",
          ["\r"] = "\\r",
          ["\n"] = "\\n",
          ["\""] = "\\\"",
          ["\\"] = "\\\\",
      })
      return "\""..str.."\""
  end

  local function dump_key(key)
      if type(key) == "number" then
          return key .. "] "
      elseif type(key) == "string" then
          return tostring(key).. ": "
      end
  end

  local function dump_val(val, layer)
      if type(val) == "table" then
          return dump_obj(val, layer)
      elseif type(val) == "string" then
          return make_quote(val)
      else
          return tostring(val)
      end
  end

  local function count_elements(obj)
      local count = 0
      for k, v in pairs(obj) do
          count = count + 1
      end
      return count
  end

  dump_obj = function(obj, layer)
      if type(obj) ~= "table" then
          return count_elements(obj)
      end

      layer = layer + 1
      local tokens = {}
      local max_count = count_elements(obj)
      local cur_count = 1
      for k, v in pairs(obj) do
          local key_name = dump_key(k)
          if type(v) == "table" then
              key_name = key_name.."\n"
          end
          table.insert(tokens, make_indent(layer, cur_count == max_count)
                  .. key_name .. dump_val(v, layer))
          cur_count = cur_count + 1
      end

      -- 处理空table
      if max_count == 0 then
          table.insert(tokens, make_indent(layer, true) .. "{ }")
      end

      return table.concat(tokens, "\n")
  end

  if type(obj) ~= "table" then
      return "the params you input is "..type(obj)..
              ", not a table, the value is --> "..tostring(obj)
  end

  width = width or 2
  return "root-->"..tostring(obj).."\n"..dump_obj(obj, 0)
end