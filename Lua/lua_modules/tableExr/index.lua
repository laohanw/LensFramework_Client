function table.nums(t)
    local count = 0
    for k, v in pairs(t) do
        count = count + 1
    end
    return count
end

function table.keys(hashtable)
    local keys = {}
    for k, v in pairs(hashtable) do
        keys[#keys + 1] = k
    end
    return keys
end

function table.values(hashtable)
    local values = {}
    for k, v in pairs(hashtable) do
        values[#values + 1] = v
    end
    return values
end

function table.merge(dest, src)
    for k, v in pairs(src) do
        dest[k] = v
    end
end

function table.insertto(dest, src, begin)
    begin = checkint(begin)
    if begin <= 0 then
        begin = #dest + 1
    end

    local len = #src
    for i = 0, len - 1 do
        dest[i + begin] = src[i + 1]
    end
end

function table.indexof(array, value, begin)
    for i = begin or 1, #array do
        if array[i] == value then
            return i
        end
    end
    return false
end

function table.keyof(hashtable, value)
    for k, v in pairs(hashtable) do
        if v == value then
            return k
        end
    end
    return nil
end
function table.tryGetValue(hashtable, key)
    for k, v in pairs(hashtable) do
        if k == key then
            return v
        end
    end
    return false
end
function table.containsKey(hashtable, key)
    for k, v in pairs(hashtable) do
        if k == key then
            return true
        end
    end
    return false
end

function table.removebyvalue(array, value, removeall)
    local c, i, max = 0, 1, #array
    while i <= max do
        if array[i] == value then
            table.remove(array, i)
            c = c + 1
            i = i - 1
            max = max - 1
            if not removeall then
                break
            end
        end
        i = i + 1
    end
    return c
end

function table.map(t, fn)
    for k, v in pairs(t) do
        t[k] = fn(v, k)
    end
end

function table.walk(t, fn)
    for k, v in pairs(t) do
        fn(v, k)
    end
end

function table.filter(t, fn)
    for k, v in pairs(t) do
        if not fn(v, k) then
            t[k] = nil
        end
    end
end

function table.unique(t, bArray)
    local check = {}
    local n = {}
    local idx = 1
    for k, v in pairs(t) do
        if not check[v] then
            if bArray then
                n[idx] = v
                idx = idx + 1
            else
                n[k] = v
            end
            check[v] = true
        end
    end
    return n
end

--sort_table 被排序的表，rule_list排序规则表，比如 rule={key="type_id",revert=true},
--revert代表正序还是反序，rule可以自定义多个key排序，rule表里面待排序的key前面的优先后面的。
function table.common_sort(sort_table, rule_list)
    local function sort_func(first_info, second_info)
        for index, v in ipairs(rule_list) do
            if first_info[v.key] < second_info[v.key] then
                if not v.revert then --正序
                    return true
                else --逆序
                    return false
                end
            end

            if first_info[v.key] > second_info[v.key] then
                if not v.revert then
                    return false
                else
                    return true
                end
            end
        end

        return false
    end

    table.sort(sort_table, sort_func)
end

function table.print(tabl)
    local stri = {}

    local function internal(tab, str, indent)
        for k, v in pairs(tab) do
            if type(v) == "table" then
                table.insert(str, indent .. tostring(k) .. ":\n")
                internal(v, str, indent .. " ")
            else
                table.insert(str, indent .. tostring(k) .. ": " .. tostring(v) .. "\n")
            end
        end
    end

    internal(tabl, stri, "")
    return table.concat(stri, "")
end
