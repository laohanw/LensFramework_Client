function handler(obj, method)
    return function(...)
        return method(obj, ...)
    end
end
