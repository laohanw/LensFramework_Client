function class(classTable,...)
    local cls = {}
    if classTable then
        for key,value in pairs(classTable) do
            cls[key]=value
        end
        if classTable.extends then
            cls.super=class(classTable.extends,...)
        end
    end
    cls.__index = cls
    setmetatable(cls,classTable)
    if cls.ctor then
        cls:ctor(...)
    end
    return cls
end