local __rt_1 = {
}
local System = 
{
	{
		help = "@348174",
		systemName = "@466450"
	},
	{
		help = "@96088",
		systemName = "@494937"
	},
	{
		openCondition = {
			levelLimit = -1,
			rankLimit = 25,
			vipLimit = -1
		},
		openRewards = {
			{
				count = 10,
				id = 100001,
				type = 1
			}
		}
	},
	{
		help = "@415778",
		openCondition = {
			levelLimit = -1,
			rankLimit = 15,
			vipLimit = -1
		},
		openRewards = {
			{
				count = 5,
				id = 100001,
				type = 1
			}
		},
		systemName = "@83240"
	}
}
local __default_values = {
	help = "@133037",
	openCondition = __rt_1,
	openRewards = __rt_1,
	systemName = "@384241"
}
do
	local base = { __index = __default_values, __newindex = function() error( "Attempt to modify read-only table" ) end }
	for k, v in pairs( System ) do
		setmetatable( v, base )
	end
	base.__metatable = false
end

return System
