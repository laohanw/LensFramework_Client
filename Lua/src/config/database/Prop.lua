local Prop = 
{
	[100001] = {
		desc = "@418604",
		sellPrice = 500,
		type = 1
	},
	[100002] = {
		desc = "@491923",
		name = "@170376",
		quality = 2,
		type = 1
	},
	[200001] = {
		desc = "@312800",
		name = "@312800"
	},
	[200002] = {
		desc = "@326314",
		name = "@326314"
	},
	[200003] = {
		desc = "@475820",
		name = "@475820"
	},
	[200004] = {
		desc = "@464497",
		name = "@464497"
	},
	[200005] = {
		desc = "@78503",
		name = "@78503"
	},
	[200006] = {
		desc = "@342783",
		name = "@342783"
	},
	[200007] = {
		desc = "@306369",
		name = "@306369"
	},
	[200008] = {
		name = "@203553"
	}
}
local __default_values = {
	desc = "@203553",
	icon = "item1",
	name = "@121397",
	quality = 1,
	sellPrice = 800,
	subType = -1,
	type = 2
}
do
	local base = { __index = __default_values, __newindex = function() error( "Attempt to modify read-only table" ) end }
	for k, v in pairs( Prop ) do
		setmetatable( v, base )
	end
	base.__metatable = false
end

return Prop
