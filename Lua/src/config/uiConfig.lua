return {
  defaultUI=nil,
  layer={
    "UI_LAYER_BROADCAST",
    "UI_LAYER_ACTIVE",
    "UI_LAYER_POP",
    "UI_LAYER_BATTLE",
    "UI_UPDATE_EFFECT",
    "UI_SPINE",
    "UI_AUTO_HIDE_ARELT",
    "UI_LAYER_GUIDE",
    "UI_NET_PANEL",
    "UI_VOICE_PANEL"
  },
  extensions={
    -- {
    --   pkgName="",
    --   resName="",
    --   luaPath=""
    -- }
  },
  generalUI={
    --普通UI
    --{ uiName luaPath showAndAni showAndMaskAni useOutClick isBlurScene }
    --uiName 窗口命名
    --moduleName 模块命名
    --level 窗口层级
    --pkgName  包名
    --resName  资源名
    --luaPath lua代码文件路径
    ---------------------------------------------- 主界面-------------------------------------
    ["MainUIView"]={moduleName="Main",pkgName="Guide",resName="Main",luaPath="view/MainUIView",layer="UI_LAYER_POP"}
  },
  orderUI={
    -- 有排序功能的UI
    ["upgrade"]={uiName ="upgrade",pkgName="Guide",resName="Main",luaPath="view/modal/dialog",layer="UI_LAYER_POP"}
  },
  modal={
    ["dialog"]={uiName="dialog",pkgName="Guide",resName="Main",luaPath="view/modal/dialog",layer="UI_LAYER_POP"}
  },
  banner={

  }
}