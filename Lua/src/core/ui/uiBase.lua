return {
    extends = G_LuaBehavior,
    pkg = nil,
     --{pkgName="",resName="",layer=""}
    view = nil,
    ctor = function(self)
        if self.pkg ~= nil then
            self.view = G_UI:loadUI(self.pkg.pkgName,self.pkg.resName,self.pkg.layer)
            self:created()
            self:bindEvent()
        end
    end,
    reconnected=function(self,data)
    end,
    created = function(self)
    end,
    bind = function(self)
    end,
    unbind=function( self)
    end,
    visible=function(self)
        self.view.visible=true
    end,
    invisible=function(self)
        self.view.visible=false
    end,
    dispose=function(self)
    end
}
