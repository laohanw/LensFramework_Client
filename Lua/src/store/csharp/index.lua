local state = require("store/csharp/state")
local getters = require("store/csharp/getters")
local mutations = require("store/csharp/mutations")
local actions = require("store/csharp/actions")
return {
    tag = "store_global",
    ctor = function(self)
        self.cls.state = class(state)
        self.cls.getters = class(getters)
        self.cls.mutations = class(mutations)
        self.cls.actions = class(actions)
    end,
    cls = {},
    data = {},
    dispatch = function(self, act, params)
        for k, v in pairs(self.cls) do
            if k == "actions" then
                for kc, vc in pairs(v) do
                    if kc == act then
                        vc(self, params)
                    end
                end
            end
        end
    end,
    commit = function(self, com, params)
        for k, v in pairs(self.cls) do
            if k == "mutations" then
                for kc, vc in pairs(v) do
                    if kc == com then
                        vc(state, params)
                    end
                end
            end
        end
    end,
    getter = function(self, get)
        for k, v in pairs(self.cls) do
            if k == "getters" then
                for kc, vc in pairs(v) do
                    if kc == get then
                        return vc(state)
                    end
                end
            end
        end
    end
}
