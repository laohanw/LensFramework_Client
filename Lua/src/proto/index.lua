
local pb = require('pb')
return {
    tag="L_Proto",
    ctor=function( self )
        local bytes=C_LuaManager.LoadFileBytes("proto/protocal.pb")
        pb.load(bytes)
        self.data.protoCSCmd=class(require('proto/protoCSCmd'))
        self.data.protoSCCmd=class(require('proto/protoSCCmd'))
    end,
    data={
        protoCSCmd=nil,
        protoSCCmd=nil
    },
    encode=function(self,type,cmd,data)
        if type=="cs" then
            local enum=self.data.protoCSCmd[cmd]
            if enum==nil then
                error("serialize cmd is null > "..cmd)
            else
                return pb.encode(enum.value,data),enum.id
            end
        elseif type=="sc" then
            local enum=self.data.protoSCCmd[cmd]
            if enum==nil then
                error("serialize cmd is null > "..cmd)
            else
                return pb.encode(enum.value,data),enum.id
            end
        else
            error("serialize type is error > "..type)
            return nil
        end
    end,
    decode=function(self,type,cmd,data)
        if type=="cs" then
            local name=self.data.protoCSCmd[cmd]
            if name==nil then
                error("serialize cmd is null > "..cmd)
            else
                return pb.decode(self.data.protoCSCmd[cmd].value,data)
            end
        elseif type=="sc" then
            local name=self.data.protoSCCmd[cmd]
            if name==nil then
                error("serialize cmd is null > "..cmd)
            else
                return pb.decode(self.data.protoSCCmd[cmd].value,data)
            end
        else
            error("serialize type is error > "..type)
            return nil
        end
    end,
    getCSCmd=function(self, id )
        return self.data.protoSCCmd:parse(id)
    end,
    getSCCmd=function( self,id )
        return self.data.protoSCCmd:parse(id)
    end
}