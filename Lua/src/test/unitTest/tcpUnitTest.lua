require("string")
require("printf")
require("dump")
require('math')
require('import')
require('check')
require('clone')
require('io')
require('table')
require('class')
require("handler")
require("coroutine")
require("async")
require("xluaLib")

require("alias")
L_Net=require("core/net/index")
class(
    {
        tag = "tcpUnitTest",
        ctor = function(self)
            L_Net:initialize()
            self:_checkProject()
            self:toTest()
        end,
        _checkProject = function()
            setmetatable(
                _G,
                {
                    __newindex = function(v1, v2, v3)
                        if (v2 ~= "i" and v2 ~= "j" and v2 ~= "socket" and v2 ~= "ltn12" and v2 ~= "reload") then
                            error("不能添加全局变量 " .. v2, 2)
                        else
                            rawset(v1, v2, v3)
                        end
                    end,
                    __index = function(t, k)
                        if (k ~= "jit") then
                            error("未注册的全局变量" .. k, 2)
                        end
                    end
                }
            )
        end,
        toTest=function(self)
            local cal=function( data )
                print(type(data))
                printf.log('wocao')
            end
            -- L_Net:addListener("SC_LoginRequest",cal)
            L_Net:connectedHandler(function()
                local data={
                    account="aaa",
                    serverid="wasd.asd",
                    timestamp=2,
                    digest="swfas"
                }
                L_Net:sendProto("CS_LoginRequest",data,cal)
                -- L_Net:removeListener("SC_LoginRequest",cal)
            end,true)
            L_Net:disconnectedHandler(function()
                printf.log("disconnectedHandler")
            end,true)
        end
    }
)
