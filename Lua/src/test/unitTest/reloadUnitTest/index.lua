-- require("core/system/reload")
-- require("../../Assets/CSharp/Tools/LuaReload/Lua/reload")
local mymod = require "test/unitTest/reloadUnitTest/mymod"

mymod.foobar(42)

local tmp = {}
local foo = mymod.foo2()
tmp[foo] = foo
print("FOO beforasde", foo)

local obj = mymod.new()

obj:show()

-- function test()
-- 	print("BEFORE update foo", foo)
-- 	reload.reload { "test/unitTest/reloadUnitTest/mymod" }
-- 	print("AFTER update foo", foo)
-- end

-- test()

coroutine.start(function()
	print('coroutine a started')
	while true do
		coroutine.yield(coroutine.waitForSeconds(3))
		foo()
		mymod.foo()
		print("F24353534543 ", foo)
		assert(tmp[foo] == foo)
		obj:show()
	end
end)
