@setlocal
@echo off
set unityPath=""
for /f "tokens=1,2 delims==" %%a in (config.ini) do (
	echo %%a
	if "x%%a"=="xwinUnityPath" (
		set unityPath=%%b
	)
)
echo %unityPath%
set branch=%~1
set channel=%~2
set platform=%~3
call :getProjectPath
if /i %branch%=="develop" goto:localtest
if /i %branch%=="master"  goto:localtest
if /i %branch%=="alpha"  goto:build
if /i %branch%=="beta" goto:build
if /i %branch%=="release" goto:build

:localtest
call cmd/build.bat %unityPath% %branch% %channel% %projectPath% %platform% %branch%
goto:eof

:standard
call cmd/build.bat %unityPath% %branch% %channel% %projectPath% %platform% %branch%
call cmd/test.bat %unityPath%
goto:eof

:getProjectPath
set ori=%cd%
cd ..
set projectPath=%cd%
cd %ori%
goto:eof

endlocal