@setlocal enableextensions enabledelayedexpansion
@echo off
set unityPath=""
set versionUrl=""
for /f "tokens=1,2 delims==" %%a in (config.ini) do (
	echo %%a
	if "x%%a"=="xwinUnityPath" (
		set unityPath=%%b
	)
	if "x%%a"=="xversionUrl" (
		set versionUrl=%%b
	)
)
set branch=%~1
set platform=%~2
set versionMode=%~3
set channel=%~4
call :getProjectPath
if /i %branch%=="develop" goto:localtest
if /i %branch%=="master"  goto:localtest
if /i %branch%=="alpha"  goto:build
if /i %branch%=="beta" goto:build
if /i %branch%=="release" goto:build

:localtest
call cmd/update.bat %unityPath% %projectPath% %platform% %branch% %versionMode% %channel% %versionUrl%
goto:eof

:standard
call cmd/check.bat %unityPath% %projectPath%
call cmd/update.bat %unityPath% %projectPath% %platform% %branch% %versionMode% %channel% %versionUrl%
call cmd/test.bat %unityPath%
goto:eof

:getProjectPath
set ori=!cd!
cd ..
set projectPath=!cd!
cd !ori!
goto:eof


endlocal