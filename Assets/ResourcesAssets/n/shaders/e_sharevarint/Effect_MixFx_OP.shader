﻿Shader "Mobile/Effect/MixFx_OP" {
Properties {
    [HDR]_TintColor("Main Color", Color) = (1,1,1,1)
	_MainTex("MainTex", 2D) = "white" {}
	_MMultiplier("Layer Multiplier", Float) = 1.0

	_MaskTex("MaskTex", 2D) = "white" {}
	
	[HideInInspector] _IsTwoSided("_IsTwoSided", Float) = 0.0
	[HideInInspector] _CullMode("_CullingMode", Float) = 0.0
	[HideInInspector] _Mode("__mode", Float) = 0.0
	[HideInInspector] _SrcBlend("__src", Float) = 1.0
	[HideInInspector] _DstBlend("__dst", Float) = 0.0
	[HideInInspector] _ZWrite("__zw", Float) = 1.0
}

Category{

		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		Blend[_SrcBlend][_DstBlend]
		Cull[_CullMode]
		ZWrite[_ZWrite]
		Lighting Off

	SubShader {

		LOD 100

	Pass {

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest

		#pragma shader_feature _MASK

		//#pragma exclude_renderers opengl d3d11_9x xbox360 xboxone ps3 ps4 psp2 
		#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal
		#pragma shader_feature _ALPHABLEND_ON
		#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
            float4 texcoord: TEXCOORD0;
		};

		struct v2f {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float4 uvmain : TEXCOORD0;
			float4 uvsec : TEXCOORD1;

		};

		sampler2D _MainTex;

        float4 _MainTex_ST;
        
        fixed4 _TintColor;
        half _MMultiplier;

#ifdef _MASK
		sampler2D _MaskTex;
		float4 _MaskTex_ST;
#endif

		v2f vert(appdata_t v)
		{
			v2f o = (v2f)0;

			o.vertex = UnityObjectToClipPos(v.vertex);
            o.color = v.color * _TintColor * _MMultiplier;

			o.uvmain.xy = v.texcoord.xy;

			o.uvmain.xy = TRANSFORM_TEX(o.uvmain.xy, _MainTex);

#if defined (_MASK)
			o.uvsec.xy = TRANSFORM_TEX(v.texcoord, _MaskTex);
#endif

			float4 worldPos = mul(unity_ObjectToWorld, v.vertex);

			return o;
		}

		fixed4 frag(v2f i) : COLOR
		{
			fixed4 col = 1;

			fixed4 col2 = tex2D(_MainTex, i.uvmain.xy);
			col2.rgb *= i.color.rgb;

#if defined(_ALPHABLEND_ON)
			col2.a *= i.color.a;
			col2.a = saturate(col2.a);
#endif
            col *=col2;

#if defined (_MASK)
			fixed4 cMask = tex2D(_MaskTex, i.uvsec.xy);
			col *= cMask;
#endif
			return col;
		}
		ENDCG
				}
		}
	}

	Fallback Off
	CustomEditor "MixOPEffectEditor"
}
