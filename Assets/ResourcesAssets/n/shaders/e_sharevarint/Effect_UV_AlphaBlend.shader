 
Shader "Effect/UV_AlphaBlend" {
    Properties {
        _color ("color", Color) = (0.5,0.5,0.5,0.5)
         _MainTex ("MainTex", 2D) = "white" {}
      
        _SpeedU ("SpeedU", Float ) = 0.1
        _SpeedV ("SpeedV", Float ) = 0.1 
        _Power("Power",float) = 2
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
           
            Blend SrcAlpha OneMinusSrcAlpha
            // Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
            #include "UnityCG.cginc"
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal  
           uniform sampler2D _MainTex; uniform fixed4 _MainTex_ST; 
            uniform fixed4 _color; 
            uniform half _SpeedU;
            uniform half _SpeedV;
       
            half _Power;
             struct VertexInput {
                float4 vertex : POSITION;
                half2 texcoord0 : TEXCOORD0;
                half4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                fixed4 vertexColor : COLOR;
           
                float2 movingUV :TEXCOORD1;
                half4 color:TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
             
                o.vertexColor = v.vertexColor;
                float4 time = _Time;
                o.movingUV = float2(((_SpeedU*time.g)+v.texcoord0.r),(v.texcoord0.g+(time.g*_SpeedV)));
                o.movingUV =TRANSFORM_TEX(o.movingUV, _MainTex);
             
                o.color = _color*v.vertexColor;
 
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            fixed4 frag(VertexOutput i) : SV_Target {
                // half isFrontFace = ( facing >= 0 ? 1 : 0 );
                // half faceSign = ( facing >= 0 ? 1 : -1 );
  
                fixed4 _MainTexVar = tex2D(_MainTex,i.movingUV);
               
              
                fixed3 emissive = i.color.rgb * _MainTexVar.rgb   ;
                fixed3 finalColor =  _Power *emissive; 
                return fixed4(finalColor,(i.color.a  *_MainTexVar.a  ));
            }
            ENDCG
        }
        
    }
}
