// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Mobile/Model/Shadow" {
Properties {
	 
	 _ShadowColor("ShadowColor",Color) = (0,0,0,0.3) 
	  _ShadowOffset("ShadowOffset (xy 按高度拍扁方向，zw 整体偏移)",vector) = (0,-0.61,0,0)
	  _ShadowHeight("ShadowHeight",float) = 0.1
	 

}
SubShader {
	//pass to render object 
    LOD 800 
		Pass {   
			Tags { 
				// "Queue"				= "AlphaTest"
				"Queue"				= "Transparent+7"
				"RenderType"		= "Opaque"

				
			 }
			Blend One OneMinusSrcAlpha
			ZWrite On

			Stencil
			{
				Ref 2
				Comp NotEqual
				Pass Replace
			}
	  
		// offset 1, 1
			CGPROGRAM
		
			#pragma vertex vert
			#pragma fragment frag
          
			#include "UnityCG.cginc"
			//#include "Lighting.cginc"
			half4 _ShadowOffset;
			half _ShadowHeight; 
  
			uniform fixed4 _ShadowColor; 
			struct v2f
			{
				float4 pos : POSITION;
				// float4 texcoord : TEXCOORD0;
			};

			struct appdata_t {
				float4 vertex : POSITION;
				 
			};


			v2f vert(appdata_t v)
			{
				v2f o;
				float4x4 _RotMatrix = unity_ObjectToWorld;
				_RotMatrix[0][3] = 0;
				_RotMatrix[1][3] = 0;
				_RotMatrix[2][3] = 0;
				float3 tempPos = float3(unity_ObjectToWorld[0][3],unity_ObjectToWorld[1][3],unity_ObjectToWorld[2][3]);

				float4 vertexPos = mul(_RotMatrix, v.vertex);
				vertexPos.xyz += tempPos;
				vertexPos.x +=  _ShadowOffset.x * vertexPos.y + _ShadowOffset.z ;
				vertexPos.z += _ShadowOffset.y * vertexPos.y  + _ShadowOffset.w;
			
				vertexPos.y = _ShadowHeight;

				o.pos = mul(UNITY_MATRIX_VP, vertexPos);
				// o.texcoord = v.texcoord;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{ 
  
				return _ShadowColor;
			}
			ENDCG
		} 
	}

}
