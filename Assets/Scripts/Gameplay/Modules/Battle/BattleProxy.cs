﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Gameplay.Modules.Battle
{
    using Framework.Core;
    public class BattleProxy:Singleton<BattleProxy>
    {
        public override void Initialize(Options options)
        {
        }
        public override void Dispose()
        {
            base.Dispose();
        }
    }
}