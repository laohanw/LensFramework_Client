﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Core
{
    public class Uniforms
    {
        public static readonly int _MainTex = Shader.PropertyToID("_MainTex");
        public static readonly int _MaskTex = Shader.PropertyToID("_MaskTex");
        public static readonly int _MaskColor = Shader.PropertyToID("_MaskColor");
        public static readonly int _Opacity = Shader.PropertyToID("_Opacity");
        public static readonly int _RimColor = Shader.PropertyToID("_RimColor");
        public static readonly int _RimIntensity = Shader.PropertyToID("_RimIntensity");
        public static readonly int _FrameIndex = Shader.PropertyToID("_FrameIndex");
        public static readonly int _AnimationTex = Shader.PropertyToID("_AnimationTex");
        public static readonly int _AnimationTexSize = Shader.PropertyToID("_AnimationTexSize");
        public static readonly int _FrameIndexFusion = Shader.PropertyToID("_FrameIndexFusion");
        public static readonly int _FusionProgress = Shader.PropertyToID("_FusionProgress");

    }
}
