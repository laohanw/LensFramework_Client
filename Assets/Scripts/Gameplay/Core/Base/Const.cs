﻿using System;
using System.Collections.Generic;


namespace Lens.Gameplay.Core
{
    public class Const
    {
        /// <summary>
        /// 资源bundle父文件夹名字
        /// </summary>
        public const string sourceBundleFolder = "Bundle";
        /// <summary>
        /// 场景bundle父文件夹名字
        /// </summary>
        public const string sceneBundleFolder = "Scene";
        /// <summary>
        /// lua文件夫文件夹名字
        /// </summary>
        public const string luaBundleFolder = "Lua";
        /// <summary>
        /// 资源文件表的名字
        /// </summary>
        public const string sourceManifestName = "manifest.bytes";
        /// <summary>
        /// 整包zip的包名
        /// </summary>
        public const string entireZipName = "entirePack.7z";
        /// <summary>
        /// 差异包的zip包名
        /// </summary>
        public const string diffZipName = "diffPack.7z";
        /// <summary>
        /// 远程资源zip包的列表信息
        /// </summary>
        public const string packageInfoFileName = "info";
        /// <summary>
        /// 版本文件名
        /// </summary>
        public const string versionFileName = "version";

    }
}
