﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Gameplay.Core
{
    using Framework.Core;
    public class Define
    {
        public static readonly int platform =
#if UNITY_EDITOR
        (int)UnityEditor.EditorUserBuildSettings.activeBuildTarget;
#else
        (int)Application.platform;
#endif
        public static readonly EProjectType projectType =
#if RELEASE
        EProjectType.Release;
#elif DEVELOPMENT
        EProjectType.Development;
#elif DEBUGGER
        EProjectType.Debug;
#else
        EProjectType.Debug;
#endif
        public static readonly string localSourceUrl= Application.dataPath+"/ResourcesAssets";
        public static readonly string tempUrl = Application.persistentDataPath + "/Temp/";

        public static readonly string sourceUrl =
#if RELEASE
        persistentUrl+"/" + platform ;
#elif DEVELOPMENT
        localSourceUrl ;
#else
        System.IO.Path.Combine( Application.dataPath,"../Dist",platform.ToString()) ;
#endif
        public static readonly string bundleUrl = sourceUrl
#if DEVELOPMENT
        ;
#else
        + "/" + Const.sourceBundleFolder;
#endif
        public static readonly string bundleManifestUrl = bundleUrl+"/" + Const.sourceManifestName;
        public static readonly string luaUrl= sourceUrl
#if DEVELOPMENT
        ;
#else
        + "/" + Const.luaBundleFolder + "/";
#endif
    }
}
