﻿using System;
using System.Collections.Generic;

namespace Lens.Gameplay.Managers
{
    public class TimelineStoreData
    {
        public int index;
        public string path;
        public int count;
    }
}
