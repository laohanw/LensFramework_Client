﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Lens.Gameplay.Managers
{
    using Framework.Libs;
    public enum ECoroutineType
    {
        None = 0,
        UI = 1,
        Battle = 2
    }
    public class CoroutineManager
    {
        private Coroutine m_coroutine;
        private static CoroutineManager m_instance;
        public static CoroutineManager GetInstance()
        {
            if (m_instance != null) return null;
            return m_instance = new CoroutineManager();
        }
        CoroutineManager()
        {
            m_coroutine = new Coroutine();
        }
        ~CoroutineManager()
        {
            //Debug.Log("Dispose Coroutine");
        }
        public void Dispose()
        {
            m_instance = null;
        }
        public void LateUpdate()
        {
            if (m_instance == null) return;
            m_coroutine.Update();
        }
        public static void Start(IEnumerator enumerator)
        {
            if (enumerator == null) return;
            m_instance.m_coroutine.Start(enumerator);
        }
        public static void Stop(IEnumerator enumerator)
        {
            if (enumerator == null) return;
            m_instance.m_coroutine.Stop(enumerator);

        }
        public static void StopAll()
        {
            m_instance.m_coroutine.StopAll();
        }
    }
}