﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lens.Framework.Core;

namespace Lens.Gameplay.Managers
{
    using FairyGUI;
    using Core;
    public class UIManager :Singleton<UIManager>
    {
        #region public member
        public const float safetyZone = 1504.0f;
        #endregion

        #region private member
        private Dictionary<string, AssetBundle> m_loadedBundles = new Dictionary<string, AssetBundle>();
        #endregion

        #region public methods
        public override void Initialize(Options options)
        {
            base.Initialize();
            UIObjectFactory.SetLoaderExtension(typeof(GLoaderExtensions));
            UIConfig.defaultFont = "sourcehansanscnregular";
            BaseFont defaultFont = FontManager.GetFont(UIConfig.defaultFont);
            FontManager.RegisterFont(defaultFont, "Source Han Sans CN");
            FontManager.RegisterFont(defaultFont, "Adobe Heiti Std R");// 这一条是防止有些UI字体没来得急换
            FontManager.RegisterFont(FontManager.GetFont("FZDaBiaoSongJianTi"), "FZDaBiaoSong-B06S");

            GRoot root = GRoot.inst;
            root.displayObject.parent.gameObject.name = "FairyGUI";
            root.displayObject.gameObject.name = "UIRoot";

            OnStageSizeChange();
            Stage.inst.onStageResized.Add((EventCallback0)OnStageSizeChange);
            GLoaderExtensions.OnUrlTextureLoadFinishEvent += PassUrlTextureLoadFinish;
        }
        public override void Dispose()
        {
            ColseAll();
            base.Dispose();
        }
        #endregion

        #region public static methods
        public static void Load(string pkgName)
        {
            FairyGUI.UIPackage uiPackage = FairyGUI.UIPackage.GetByName(pkgName);
            if (uiPackage == null)
            {
                uiPackage = FairyGUI.UIPackage.GetByName(pkgName);
                if (uiPackage!=null)
                {
                    return;
                }
            }
            else
            {
                return;
            }
            if (Define.projectType==EProjectType.Development)
            {
                FairyGUI.UIPackage.AddPackage("Assets/ResourcesAssets/GUI/"+pkgName);
            }
            else
            {
                var bundle = p_instance.LoadAssetBundle(pkgName);
                FairyGUI.UIPackage.AddPackage(bundle);
            }
        }
        public static void UnLoad(string pkgName)
        {
            if (Define.projectType == EProjectType.Development)
            {
                FairyGUI.UIPackage.RemovePackage(pkgName);
            }
            else
            {
                AssetBundle bundle;
                if (p_instance.m_loadedBundles.TryGetValue(pkgName, out bundle))
                {
                    FairyGUI.UIPackage.RemovePackage(pkgName);
                    bundle.Unload(true);
                    p_instance.m_loadedBundles.Remove(pkgName);
                }
                else
                {
                    UnityEngine.Debug.LogError("Unload failure: Dont has this pkg:  " + pkgName);
                }
            }
        }
        public static void FitWindow(Window window)
        {
            FairyGUI.GRoot gRoot = FairyGUI.GRoot.inst;
            window.SetSize(gRoot.width, gRoot.height);
            FairyGUI.GObject anchorsPoint = window.contentPane.GetChild("anchorsPoint");
            if (null != anchorsPoint)
            {
                anchorsPoint.visible = false;
                anchorsPoint.touchable = false;
                float viewW = UnityEngine.Mathf.Min(gRoot.width, safetyZone);
                viewW = UnityEngine.Mathf.Min(gRoot.width, viewW);
                float viewH = gRoot.height;
                anchorsPoint.SetSize(viewW, viewH);

                window.contentPane.SetSize(gRoot.width, viewH);
                window.contentPane.x = (gRoot.width - window.contentPane.width) * 0.5f;
                anchorsPoint.SetXY((gRoot.width - viewW) * 0.5f, 0);
            }
            else
            {
                window.contentPane.SetSize(gRoot.width, gRoot.height);
            }
        }
        public static void SetUIMultiTouchEnabled(bool boo)
        {
            Stage.inst.multiTouchEnabled = boo;
        }
        public static void ColseAll()
        {
            Stage.inst.onClick.Clear();
            Stage.inst.onTouchBegin.Clear();
            Stage.inst.onTouchEnd.Clear();
            foreach (var item in p_instance.m_loadedBundles)
            {
                var pkgName = item.Key;
                AssetBundle bundle;
                if (p_instance.m_loadedBundles.TryGetValue(pkgName, out bundle))
                {
                    FairyGUI.UIPackage.RemovePackage(pkgName);
                    bundle.Unload(true);
                }
                else
                {
                    UnityEngine.Debug.LogError("Unload failure: Dont has this pkg:  " + pkgName);
                }
            }
            p_instance.m_loadedBundles.Clear();
            //LuaManager.CallMethod("CLOSE_LUA_UI");
            GRoot.inst.touchable = true;
            Stage.inst.touchable = true;
        }
        #endregion

        #region private methods
        private AssetBundle LoadAssetBundle(string path)
        {
            AssetBundle bundle;
            string bundleName = path.ToLower();
            if (m_loadedBundles.TryGetValue(bundleName, out bundle))
            {
                
            }
            else
            {
                bundle = AssetBundle.LoadFromFile(Define.sourceUrl+"/"+ bundleName);
                m_loadedBundles.Add(bundleName, bundle);
            }
            return bundle;
        }
        private void PassUrlTextureLoadFinish(string url)
        {
            //Tool.Util.CallMethod(EventSystem.LuaNotification.luaEventDispatcher,
            //    EventSystem.LuaEventConst.URL_TEXTURE_LOAD_FINISH, url);
        }
        private void OnStageSizeChange()
        {
            GRoot root = GRoot.inst;
            root.ApplyContentScaleFactor();
            int designResolutionX = 1334;
            int designResolutionY = 750;
            float layerWidth = Screen.width;
            float layerHeight = Screen.height;
            float orgRootH = layerHeight;
            float layerY = 0;
            if (layerHeight / layerWidth > 750.0f / 1334.0f)
            {
                layerHeight = layerWidth * (750.0f / 1334.0f);
                layerY = (orgRootH - layerHeight) * 0.5f;
            }
            root.SetXY(0, layerY);
            if ((root.width / root.height) > ((float)designResolutionX / designResolutionY))//-- 屏幕过于长 导致高度不够
            {
                root.SetContentScaleFactor(designResolutionX, designResolutionY, UIContentScaler.ScreenMatchMode.MatchHeight);
            }
            else
            {
                root.SetContentScaleFactor(designResolutionX, designResolutionY, UIContentScaler.ScreenMatchMode.MatchWidth);
            }
        }
        #endregion
    }
}