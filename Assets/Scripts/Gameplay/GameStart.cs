﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Lens.Gameplay
{
    using Framework.Managers;
    using Framework.Libs;
    using Framework.Core;
    public class GameStart : MonoBehaviour
    {
        private class UpdateManagerListener :UpdateManager.IListener{
            /// <summary>
            /// 大版本号不一致，需要下载最新版app
            /// </summary>
            void UpdateManager.IListener.ToInstallNew(){

            }
            /// <summary>
            /// 首包解压缩
            /// </summary>
            void UpdateManager.IListener.OnDecompressing(UpdateManager.DecompressingInfo info){

            }
            /// <summary>
            /// 差异包下载
            /// </summary>
            void UpdateManager.IListener.OnDownloading(UpdateManager.DownloadingInfo info){
                
            }
            /// <summary>
            /// 热更状态
            /// </summary>
            void UpdateManager.IListener.OnStatus(UpdateManager.EStatus status){
                
            }
            /// <summary>
            /// 热更错误
            /// </summary>
            void UpdateManager.IListener.OnError(UpdateManager.EErrorType error, string msg){
                
            }
            /// <summary>
            /// 下载确认框
            /// </summary>
            void UpdateManager.IListener.ToDownloadDialog(Action ok,Action cancel){
                ok();
            }
        }
        public bool fps=true;

        private float m_timeLeft = 1;
        private int m_frameCount = 0;
        private int m_fps = 0;
        private bool m_isStarted=false;
        private ResourcesManager m_resourcesManager;
        private UpdateManager m_updateManager;
        private TimeManager m_timeManager;
        private LuaManager m_luaManager;
        private TcpManager m_tcpManager;
        void Awake()
        {
            UnityEngine.Object.DontDestroyOnLoad(gameObject);
            Construction();
        }
        void Start(){
            Initialize();
        }
        void Update()
        {
            Tick();
        }
        void OnGUI(){
            GUILayout.Label(m_fps.ToString());
        }
        void OnDestroy()
        {
            Dispose();
        }
        protected virtual void Construction(){
            m_tcpManager=TcpManager.GetInstance();
            m_timeManager=TimeManager.GetInstance();
            m_updateManager=UpdateManager.GetInstance();
            m_resourcesManager = ResourcesManager.GetInstance();
            m_luaManager=LuaManager.GetInstance();
        }
        protected virtual void Initialize(){
            if(Gameplay.Core.Define.projectType==EProjectType.Development){
                ToStart();
            }
            else {
                m_updateManager.AddListener(new UpdateManagerListener());
                m_updateManager.Initialize(new UpdateManager.MyOptions(){
                    sourceUrl=Gameplay.Core.Define.sourceUrl,
                    tempUrl=Gameplay.Core.Define.tempUrl
                });
                m_updateManager.Start(ToStart);
            }
        }
        protected virtual void ToStart(){
            m_resourcesManager.Initialize(new Framework.Managers.SourceOptions(){
                bundleManifestUrl=Gameplay.Core.Define.bundleManifestUrl,
                bundleUrl=Gameplay.Core.Define.bundleUrl,
                localSourceUrl=Gameplay.Core.Define.localSourceUrl,
                sourceUrl=Gameplay.Core.Define.sourceUrl
            });
            m_luaManager.Initialize(new Framework.Managers.LuaManager.MyOptions(){
                luaUrl=Gameplay.Core.Define.luaUrl
            });
            m_luaManager.Preload();
            m_luaManager.Start("main");
            m_tcpManager.Initialize();
            var luaNetTable = LuaManager.GetTable<L_Net>("L_Net");
            TcpManager.AddListener(ENetState.Connected, luaNetTable.C_OnConnected);
            TcpManager.AddListener(ENetState.Disconnected, luaNetTable.C_OnDisconnect);
            TcpManager.AddListenAll(EPackType.LuaProto, luaNetTable.C_OnReceive);
            TcpManager.AddListenAll(EPackType.Bytes, luaNetTable.C_OnReceive);
            m_isStarted=true;
        }
        protected virtual void Tick(){
            m_timeManager.Update();
            CalculateFPS(Time.deltaTime);
            m_updateManager.Update();
            if(m_isStarted){
                m_luaManager.Update();
                m_tcpManager.Update();
                m_resourcesManager.Update();
            }
        }
        protected virtual void Dispose(){
            m_resourcesManager.Dispose();
            m_luaManager.Dispose();
            m_updateManager.Dispose();
            m_tcpManager.Dispose();
        }
        private void CalculateFPS(float deltaTime)
        {
            m_timeLeft -= deltaTime;
            if (m_timeLeft < 0)
            {
                m_fps = m_frameCount;
                m_frameCount = 0;
                m_timeLeft = 1;
                return;
            }
            ++m_frameCount;
        }
    }
}