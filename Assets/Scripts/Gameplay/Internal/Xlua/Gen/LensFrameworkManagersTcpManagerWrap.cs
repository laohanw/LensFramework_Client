﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class LensFrameworkManagersTcpManagerWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			System.Type type = typeof(Lens.Framework.Managers.TcpManager);
			Utils.BeginObjectRegister(type, L, translator, 0, 3, 0, 0);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Initialize", _m_Initialize);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Dispose", _m_Dispose);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Update", _m_Update);
			
			
			
			
			
			Utils.EndObjectRegister(type, L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(type, L, __CreateInstance, 9, 0, 0);
			Utils.RegisterFunc(L, Utils.CLS_IDX, "Connect", _m_Connect_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "Reconnect", _m_Reconnect_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "Disconnect", _m_Disconnect_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "Send", _m_Send_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "AddListener", _m_AddListener_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "RemoveListener", _m_RemoveListener_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "AddListenAll", _m_AddListenAll_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "RemoveListenAll", _m_RemoveListenAll_xlua_st_);
            
			
            
			
			
			
			Utils.EndClassRegister(type, L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
			try {
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					Lens.Framework.Managers.TcpManager gen_ret = new Lens.Framework.Managers.TcpManager();
					translator.Push(L, gen_ret);
                    
					return 1;
				}
				
			}
			catch(System.Exception gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to Lens.Framework.Managers.TcpManager constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Initialize(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
                Lens.Framework.Managers.TcpManager gen_to_be_invoked = (Lens.Framework.Managers.TcpManager)translator.FastGetCSObj(L, 1);
            
            
                
                {
                    
                    gen_to_be_invoked.Initialize(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Dispose(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
                Lens.Framework.Managers.TcpManager gen_to_be_invoked = (Lens.Framework.Managers.TcpManager)translator.FastGetCSObj(L, 1);
            
            
                
                {
                    
                    gen_to_be_invoked.Dispose(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Update(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
                Lens.Framework.Managers.TcpManager gen_to_be_invoked = (Lens.Framework.Managers.TcpManager)translator.FastGetCSObj(L, 1);
            
            
                
                {
                    
                    gen_to_be_invoked.Update(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Connect_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
                
                {
                    System.Net.IPEndPoint _point = (System.Net.IPEndPoint)translator.GetObject(L, 1, typeof(System.Net.IPEndPoint));
                    
                    Lens.Framework.Managers.TcpManager.Connect( _point );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Reconnect_xlua_st_(RealStatePtr L)
        {
		    try {
            
            
            
                
                {
                    
                    Lens.Framework.Managers.TcpManager.Reconnect(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Disconnect_xlua_st_(RealStatePtr L)
        {
		    try {
            
            
            
                
                {
                    
                    Lens.Framework.Managers.TcpManager.Disconnect(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Send_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 1)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)) 
                {
                    int _msgId = LuaAPI.xlua_tointeger(L, 1);
                    byte[] _bytes = LuaAPI.lua_tobytes(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.Send( _msgId, _bytes );
                    
                    
                    
                    return 0;
                }
                if(gen_param_count == 2&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 1)&& translator.Assignable<System.IO.MemoryStream>(L, 2)) 
                {
                    int _msgId = LuaAPI.xlua_tointeger(L, 1);
                    System.IO.MemoryStream _mm = (System.IO.MemoryStream)translator.GetObject(L, 2, typeof(System.IO.MemoryStream));
                    
                    Lens.Framework.Managers.TcpManager.Send( _msgId, _mm );
                    
                    
                    
                    return 0;
                }
                if(gen_param_count == 3&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 1)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)&& translator.Assignable<System.Action<ProtoBuf.IExtensible>>(L, 3)) 
                {
                    int _msgId = LuaAPI.xlua_tointeger(L, 1);
                    byte[] _bytes = LuaAPI.lua_tobytes(L, 2);
                    System.Action<ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<ProtoBuf.IExtensible>>(L, 3);
                    
                    Lens.Framework.Managers.TcpManager.Send( _msgId, _bytes, _callback );
                    
                    
                    
                    return 0;
                }
                if(gen_param_count == 3&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 1)&& translator.Assignable<System.IO.MemoryStream>(L, 2)&& translator.Assignable<System.Action<ProtoBuf.IExtensible>>(L, 3)) 
                {
                    int _msgId = LuaAPI.xlua_tointeger(L, 1);
                    System.IO.MemoryStream _mm = (System.IO.MemoryStream)translator.GetObject(L, 2, typeof(System.IO.MemoryStream));
                    System.Action<ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<ProtoBuf.IExtensible>>(L, 3);
                    
                    Lens.Framework.Managers.TcpManager.Send( _msgId, _mm, _callback );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to Lens.Framework.Managers.TcpManager.Send!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_AddListener_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& translator.Assignable<Lens.Framework.Managers.EMsgType>(L, 1)&& translator.Assignable<System.Action<ProtoBuf.IExtensible>>(L, 2)) 
                {
                    Lens.Framework.Managers.EMsgType _msgType;translator.Get(L, 1, out _msgType);
                    System.Action<ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<ProtoBuf.IExtensible>>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.AddListener( _msgType, _callback );
                    
                    
                    
                    return 0;
                }
                if(gen_param_count == 2&& translator.Assignable<Lens.Framework.Managers.ENetState>(L, 1)&& translator.Assignable<System.Action>(L, 2)) 
                {
                    Lens.Framework.Managers.ENetState _netState;translator.Get(L, 1, out _netState);
                    System.Action _callback = translator.GetDelegate<System.Action>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.AddListener( _netState, _callback );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to Lens.Framework.Managers.TcpManager.AddListener!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_RemoveListener_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& translator.Assignable<Lens.Framework.Managers.EMsgType>(L, 1)&& translator.Assignable<System.Action<ProtoBuf.IExtensible>>(L, 2)) 
                {
                    Lens.Framework.Managers.EMsgType _msgType;translator.Get(L, 1, out _msgType);
                    System.Action<ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<ProtoBuf.IExtensible>>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.RemoveListener( _msgType, _callback );
                    
                    
                    
                    return 0;
                }
                if(gen_param_count == 2&& translator.Assignable<Lens.Framework.Managers.ENetState>(L, 1)&& translator.Assignable<System.Action>(L, 2)) 
                {
                    Lens.Framework.Managers.ENetState _netState;translator.Get(L, 1, out _netState);
                    System.Action _callback = translator.GetDelegate<System.Action>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.RemoveListener( _netState, _callback );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to Lens.Framework.Managers.TcpManager.RemoveListener!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_AddListenAll_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
                
                {
                    Lens.Framework.Managers.EPackType _packType;translator.Get(L, 1, out _packType);
                    System.Action<int, ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<int, ProtoBuf.IExtensible>>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.AddListenAll( _packType, _callback );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_RemoveListenAll_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
                
                {
                    Lens.Framework.Managers.EPackType _packType;translator.Get(L, 1, out _packType);
                    System.Action<int, ProtoBuf.IExtensible> _callback = translator.GetDelegate<System.Action<int, ProtoBuf.IExtensible>>(L, 2);
                    
                    Lens.Framework.Managers.TcpManager.RemoveListenAll( _packType, _callback );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        
        
        
        
        
		
		
		
		
    }
}
