﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Lens.Tools.TempMenu
{
    public class TempMenu
    {
        [MenuItem("Tools/TempMenu/CreateScriptableObject")]
        public static void CreateScriptableObject()
        {
            var s=ScriptableObject.CreateInstance<Tools.ProtoGenerate.GenerateConfig>();
            AssetDatabase.CreateAsset(s, "Assets/Settings.asset");
        }
        [MenuItem("Tools/TempMenu/ReladLua")]
        public static void CReloadLua()
        {
            //var f=Framework.Managers.LuaManager.GetTable<Framework.Managers.L_Delegate_S>("toReload");
            //f(Path.Combine(Application.dataPath,"../Lua/src/view/pages/login"));
        }
    }
}