﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

namespace Lens.Tools.ProtoGenerate
{
    public class ProtoGenerateWindow : EditorWindow
    {
        private string configPath = "Assets/CSharp/Tools/ProtoGenerate/Config/Settings.asset";
        private GenerateConfig m_config;
        private static ProtoGenerateWindow m_instance;
        [MenuItem("Tools/ProtoGenerate")]
        static void Init()
        {
            m_instance = EditorWindow.GetWindow<ProtoGenerateWindow>("ProtoGenerateWindow");
            m_instance.Initialize();
            m_instance.Show();
        }
        void Initialize()
        {
            var files = Directory.GetFiles(Application.dataPath, "ProtoGenSettings.asset", SearchOption.AllDirectories);
            if (files.Length <= 0)
            {
                UnityEngine.Debug.LogError("ProtoGenSettings.asset is not exsts");
            }
            else
            {
                string file = files[0];
                file = file.Replace("\\", "/");
                string configPath = "Assets" + file.Replace(Application.dataPath, "");
                m_config = (GenerateConfig)AssetDatabase.LoadAssetAtPath(configPath, typeof(GenerateConfig));
            }
        }
        void OnDestroy()
        {
            m_instance = null;
        }
        void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                {
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("Settings",EditorStyles.toolbarButton))
                    {
                        PingObject(m_config);
                    }
                }EditorGUILayout.EndHorizontal();
                GUILayout.Space(8);
                EditorGUILayout.HelpBox("proto分两种:lua的proto，和C#的proto\n1、lua的proto\n2、C#的proto", MessageType.Info);
                GUILayout.Space(8);
                if (GUILayout.Button("Generate Lua Protobuf"))
                {
                    new ProtoGenerate(m_config).GenerateLua(LogError);
                }
                GUILayout.Space(8);
                if (GUILayout.Button("Generate C# Protobuf"))
                {
                    new ProtoGenerate(m_config).GenerateCSharp(LogError);
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Generate All Protobuf"))
                {
                    new ProtoGenerate(m_config).GenerateAll(LogError);
                }
            }
            EditorGUILayout.EndVertical();
        }
        void PingObject(UnityEngine.Object obj)
        {
            int instanceID = obj.GetInstanceID();
            EditorGUIUtility.PingObject(instanceID);
            Selection.activeObject = obj;
        }
        void LogError(string error)
        {
            AssetDatabase.Refresh();
            if (!string.IsNullOrEmpty(error))
            {
                EditorUtility.DisplayDialog("Information", "导出失败！\n"+ error, "Ok");
                UnityEngine.Debug.LogError(error);
            }
            else
            {
                EditorUtility.DisplayDialog("Information", "导出成功！\n", "Ok");
            }
        }
    }
}