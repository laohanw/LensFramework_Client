﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Tools.ProtoGenerate
{
    public class GenerateConfig:ScriptableObject
    {
        public string packageName = "com.lh.demo.protocol";
        public string csCmdEnumName = "ECSCmd";
        public string scCmdEnumName = "ESCCmd";
        public string scCmdPreExtension = "SC_";
        public string csCmdPreExtension = "CS_";
        public int csharpMaxValue = 10000;
        public int luaMaxValue = 50000;
        public string sourceProtoRelatePath= "../../Proto";
        public string csharpDistRelatePath= "../Protobuf/CSharpProto/protogen/protos/protocal.proto";
        public string luaDistRelatePath= "../Protobuf/LuaProto/protocal.proto";
        public string luaCSCmdReplatePath = "../Lua/src/proto/protoCSCmd.lua";
        public string luaSCCmdReplatePath = "../Lua/src/proto/protoSCCmd.lua";
        public string csCompilePath = "../Protobuf/CSharpProto/generate.bat";
        public string luaCompilePath = "../Protobuf/LuaProto/generate.bat";
        public string luaCompileArguments = "../../Lua/src/proto/";
        public string csCompileArguments = "../../Assets/CSharp/Plugins/Protocol";
    }
}