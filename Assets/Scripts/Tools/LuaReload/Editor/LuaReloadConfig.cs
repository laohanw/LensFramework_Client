﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LuaReload
{
    public class LuaReloadConfig : ScriptableObject
    {
        [Tooltip("是否自动判断文件变化自动reload")]
        /// <summary>
        /// 是否自动判断文件变化自动reload
        /// </summary>
        public bool autoReload = true;
        [Tooltip("lua的root文件夹目录，相对于Application.data")]
        /// <summary>
        /// lua的root文件夹目录，相对于Application.data
        /// </summary>
        public string luaFolder = "GameFramework/Lua";
        [Tooltip("识别的lua文件后缀")]
        /// <summary>
        /// 识别的lua文件后缀
        /// </summary>
        public string extension = ".lua";
        [Tooltip("lua的版本号")]
        /// <summary>
        /// lua的版本号
        /// </summary>
        public string luaVersion = "5.1";
    }
}