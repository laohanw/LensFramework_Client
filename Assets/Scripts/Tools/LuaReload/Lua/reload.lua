reload = {}

function reload.FailNotify(...)
	error(...)
end

function reload.DebugNofity(...)
	print(...)
	-- if reload.DebugNofityFunc then reload.DebugNofityFunc(...) end
end
function reload.SetEnv(obj,env)
	if reload.version=="5.1" then
		debug.setfenv(obj,env)
	else
		for i = 1, math.huge do
			local s,k=debug.getupvalue( obj, i )
			if s=='_ENV' then k=env end
			if not s then break; end
		end
	end
end
function reload.GetENV(obj)
	if reload.version=="5.1" then
		return debug.getfenv(obj)
	else
		for i = 1, math.huge do
			local s,k=debug.getupvalue( obj, i )
			if s=='_ENV' then return k end
			if not s then break; end
		end
		return obj
	end
end
function reload.InitFakeTable()
	local meta = {}
	reload.Meta = meta
	local function FakeT() return setmetatable({}, meta) end
	local function EmptyFunc() end
	local function pairs() return EmptyFunc end  
	local function setmetatable(t, metaT)
		reload.MetaMap[t] = metaT 
		return t
	end
	local function getmetatable(t, metaT)
		return setmetatable({}, t)
	end
	local function require(LuaPath)
		if not reload.RequireMap[LuaPath] then
			local FakeTable = FakeT()
			reload.RequireMap[LuaPath] = FakeTable
		end
		return reload.RequireMap[LuaPath]
	end
	function meta.__index(t, k)
		if k == "setmetatable" then
			return setmetatable
		elseif k == "pairs" or k == "ipairs" then
			return pairs
		elseif k == "next" then
			return EmptyFunc
		elseif k == "require" then
			return require
		else
			local FakeTable = FakeT()
			rawset(t, k, FakeTable)
			return FakeTable 
		end
	end
	function meta.__newindex(t, k, v) rawset(t, k, v) end
	function meta.__call() return FakeT(), FakeT(), FakeT() end
	function meta.__add() return meta.__call() end
	function meta.__sub() return meta.__call() end
	function meta.__mul() return meta.__call() end
	function meta.__div() return meta.__call() end
	function meta.__mod() return meta.__call() end
	function meta.__pow() return meta.__call() end
	function meta.__unm() return meta.__call() end
	function meta.__concat() return meta.__call() end
	function meta.__eq() return meta.__call() end
	function meta.__lt() return meta.__call() end
	function meta.__le() return meta.__call() end
	function meta.__len() return meta.__call() end
	return FakeT
end

function reload.InitProtection()
	reload.Protection = {}
	reload.Protection[setmetatable] = true
	reload.Protection[pairs] = true
	reload.Protection[ipairs] = true
	reload.Protection[next] = true
	reload.Protection[require] = true
	reload.Protection[reload] = true
	reload.Protection[reload.Meta] = true
	reload.Protection[math] = true
	reload.Protection[string] = true
	reload.Protection[table] = true
end

function reload.ErrorHandle(e)
	reload.FailNotify("HotUpdate Error\n"..tostring(e))
	reload.ErrorHappen = true
end

function reload.BuildNewCode(SysPath, LuaPath)
	io.input(SysPath)
	local NewCode = io.read("*all")
	if reload.OldCode[SysPath] == NewCode then
		io.input():close()
		return false
	end
	reload.DebugNofity(SysPath)
	io.input(SysPath)  
	local chunk = "--[["..LuaPath.."]] "
	chunk = chunk..NewCode
	io.input():close()
	local NewFunction
	if reload.version=="5.1" then
		NewFunction = loadstring(chunk)
	else
		NewFunction = load(chunk)
	end
	if not NewFunction then 
  		reload.FailNotify(SysPath.." has syntax error.")  	
  		collectgarbage("collect")
  		return false
	else
		reload.FakeENV = reload.FakeT()
		reload.MetaMap = {}
		reload.RequireMap = {}
		reload.SetEnv(NewFunction, reload.FakeENV)
		local NewObject
		reload.ErrorHappen = false
		xpcall(function () NewObject = NewFunction() end, reload.ErrorHandle)
		if not reload.ErrorHappen then 
			reload.OldCode[SysPath] = NewCode
			return true, NewObject
		else
	  		collectgarbage("collect")
			return false
		end
	end
end

function reload.Travel_G()
	local visited = {}
	visited[reload] = true
	local function f(t)
		if (type(t) ~= "function" and type(t) ~= "table") or visited[t] or reload.Protection[t] then return end
		visited[t] = true
		if type(t) == "function" then
		  	for i = 1, math.huge do
				local name, value = debug.getupvalue(t, i)
				if not name then break end
				if type(value) == "function" then
					for _, funcs in ipairs(reload.ChangedFuncList) do
						if value == funcs[1] then
							debug.setupvalue(t, i, funcs[2])
						end
					end
				end
				f(value)
			end
		elseif type(t) == "table" then
			f(debug.getmetatable(t))
			local changeIndexs = {}
			for k,v in pairs(t) do
				f(k); f(v);
				if type(v) == "function" then
					for _, funcs in ipairs(reload.ChangedFuncList) do
						if v == funcs[1] then t[k] = funcs[2] end
					end
				end
				if type(k) == "function" then
					for index, funcs in ipairs(reload.ChangedFuncList) do
						if k == funcs[1] then changeIndexs[#changeIndexs+1] = index end
					end
				end
			end
			for _, index in ipairs(changeIndexs) do
				local funcs = reload.ChangedFuncList[index]
				t[funcs[2]] = t[funcs[1]] 
				t[funcs[1]] = nil
			end
		end
	end
	
	f(_G)
	local registryTable = debug.getregistry()
	f(registryTable)
	
	for _, funcs in ipairs(reload.ChangedFuncList) do
		if funcs[3] == "HUDebug" then funcs[4]:HUDebug() end
	end
end

function reload.ReplaceOld(OldObject, NewObject, LuaPath, From, Deepth)
	if type(OldObject) == type(NewObject) then
		if type(NewObject) == "table" then
			reload.UpdateAllFunction(OldObject, NewObject, LuaPath, From, "") 
		elseif type(NewObject) == "function" then
			reload.UpdateOneFunction(OldObject, NewObject, LuaPath, nil, From, "")
		end
	end
end

function reload.HotUpdateCode(LuaPath, SysPath)
	local OldObject 
	OldObject=package.loaded[LuaPath]
	if not OldObject then
		OldObject=package.loaded[string.gsub(LuaPath,"%/",".")]
	end
	if OldObject ~= nil then
		reload.VisitedSig = {}
		reload.ChangedFuncList = {}
		local Success, NewObject = reload.BuildNewCode(SysPath, LuaPath)
		if Success then
			reload.ReplaceOld(OldObject, NewObject, LuaPath, "Main", "")
			for LuaPath, NewObject in pairs(reload.RequireMap) do
				local OldObject = package.loaded[LuaPath]
				if not OldObject then
					OldObject=package.loaded[string.gsub(LuaPath,"%/",".")]
				end
				reload.ReplaceOld(OldObject, NewObject, LuaPath, "Main_require", "")
			end
			setmetatable(reload.FakeENV, nil)
			reload.UpdateAllFunction(reload.ENV, reload.FakeENV, " ENV ", "Main", "")
			if #reload.ChangedFuncList > 0 then
				reload.Travel_G()
			end
			collectgarbage("collect")
		end
	elseif reload.OldCode[SysPath] == nil then 
		io.input(SysPath)
		reload.OldCode[SysPath] = io.read("*all")
		io.input():close()
	end
end

function reload.ResetENV(object, name, From, Deepth)
	local visited = {}
	local function f(object, name)
		if not object or visited[object] then return end
		visited[object] = true
		if type(object) == "function" then
			reload.DebugNofity(Deepth.."reload.ResetENV", name, "  from:"..From)
			xpcall(function () reload.SetEnv(object, reload.ENV) end, reload.FailNotify)
		elseif type(object) == "table" then
			reload.DebugNofity(Deepth.."reload.ResetENV", name, "  from:"..From)
			for k, v in pairs(object) do
				f(k, tostring(k).."__key", " reload.ResetENV ", Deepth.."    " )
				f(v, tostring(k), " reload.ResetENV ", Deepth.."    ")
			end
		end
	end
	f(object, name)
end

function reload.UpdateUpvalue(OldFunction, NewFunction, Name, From, Deepth)
	reload.DebugNofity(Deepth.."reload.UpdateUpvalue", Name, "  from:"..From)
	local OldUpvalueMap = {}
	local OldExistName = {}
	for i = 1, math.huge do
		local name, value = debug.getupvalue(OldFunction, i)
		if not name then break end
		OldUpvalueMap[name] = value
		OldExistName[name] = true
	end
	for i = 1, math.huge do
		local name, value = debug.getupvalue(NewFunction, i)
		if not name then break end
		if OldExistName[name] then
			local OldValue = OldUpvalueMap[name]
			if type(OldValue) ~= type(value) then
				debug.setupvalue(NewFunction, i, OldValue)
			elseif type(OldValue) == "function" then
				reload.UpdateOneFunction(OldValue, value, name, nil, "reload.UpdateUpvalue", Deepth.."    ")
			elseif type(OldValue) == "table" then
				reload.UpdateAllFunction(OldValue, value, name, "reload.UpdateUpvalue", Deepth.."    ")
				debug.setupvalue(NewFunction, i, OldValue)
			else
				debug.setupvalue(NewFunction, i, OldValue)
			end
		else
			reload.ResetENV(value, name, "reload.UpdateUpvalue", Deepth.."    ")
		end
	end
end

function reload.UpdateOneFunction(OldObject, NewObject, FuncName, OldTable, From, Deepth)
	if reload.Protection[OldObject] or reload.Protection[NewObject] then return end
	if OldObject == NewObject then return end
	local oldStr=tostring(OldObject)
	local newStr=tostring(NewObject)
	if oldStr==nil then return end
	if newStr==nil then return end
	local signature = oldStr..newStr
	if reload.VisitedSig[signature] then return end
	reload.VisitedSig[signature] = true
	reload.DebugNofity(Deepth.."reload.UpdateOneFunction "..FuncName.."  from:"..From)
	
	if pcall(reload.SetEnv, NewObject, reload.GetENV(OldObject)) then
		reload.UpdateUpvalue(OldObject, NewObject, FuncName, "reload.UpdateOneFunction", Deepth.."    ")
		reload.ChangedFuncList[#reload.ChangedFuncList + 1] = {OldObject, NewObject, FuncName, OldTable}
	end
end

function reload.UpdateAllFunction(OldTable, NewTable, Name, From, Deepth)
	if reload.Protection[OldTable] or reload.Protection[NewTable] then return ErrorHandle end
	if OldTable == NewTable then return end
	local oldStr=tostring(OldTable)
	local newStr=tostring(NewTable)
	if oldStr==nil then return end
	if newStr==nil then return end
	local signature = oldStr..newStr
	if reload.VisitedSig[signature] then return end
	reload.VisitedSig[signature] = true
	reload.DebugNofity(Deepth.."reload.UpdateAllFunction "..Name.."  from:"..From)
	for ElementName, Element in pairs(NewTable) do
		local OldElement = OldTable[ElementName]
		if type(Element) == type(OldElement) then
			if type(Element) == "function" then
				reload.UpdateOneFunction(OldElement, Element, ElementName, OldTable, "reload.UpdateAllFunction", Deepth.."    ")
			elseif type(Element) == "table" then
				print("wocao")
				print(ElementName)
				print(OldElement,Element)
				reload.UpdateAllFunction(OldElement, Element, ElementName, "reload.UpdateAllFunction", Deepth.."    ")
			end
		elseif OldElement == nil and type(Element) == "function" then
			if pcall(reload.SetEnv, Element, reload.ENV) then
				OldTable[ElementName] = Element
			end
		end
	end
	local OldMeta = debug.getmetatable(OldTable)  
	local NewMeta = reload.MetaMap[NewTable]
	if type(OldMeta) == "table" and type(NewMeta) == "table" then
		reload.UpdateAllFunction(OldMeta, NewMeta, Name.."'s Meta", "reload.UpdateAllFunction", Deepth.."    ")
	end
end

function reload.Init( ENV)
	reload.OldCode = {}
	reload.ChangedFuncList = {}
	reload.VisitedSig = {}
	reload.FakeENV = nil
	reload.ENV = ENV or _G
	reload.FakeT = reload.InitFakeTable()
	reload.InitProtection()
end

function reload.Reload(luaFolder,FileList,version)
	reload.version=version or "5.3"
	local list={}
	if type(FileList)=="table" then
		list=FileList
	elseif type(FileList)=="userdata" then
		for i=0,FileList.Count-1 do
			table.insert(list, i+1, FileList[i])
		end
	else
		error(type(FileList).."is dont support")
	end
	for _, file in pairs(list) do
		reload.HotUpdateCode(file, luaFolder.."/"..file..".lua")
	end
end
reload.Init()