﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace LuaReload
{
    public interface IInternal
    {
        /// <summary>
        /// 判断lua虚拟机是否启动，用于注入reload.lua代码的时机判断
        /// </summary>
        /// <returns></returns>
        bool IsLuaStarted();
        /// <summary>
        /// 执行一次reload.lua
        /// </summary>
        void Inject();
        /// <summary>
        /// 修改后的代码reload
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="funcName"></param>
        /// <param name="luaFolder"></param>
        /// <param name="modifys"></param>
        /// <param name="luaVersion"></param>
        void ToReload(string tableName, string funcName,string luaFolder, List<string> modifys, string luaVersion);
    }
}

/*
/// <summary>
/// Example
/// </summary>
public class LuaReloadInternal : IInternal
{
    void IInternal.Inject()
    {
        byte[] bytes = null;
        using (FileStream fs = new FileStream(Path.Combine(Application.dataPath, "CSharp/Tools/LuaReload/Lua/reload.lua"), FileMode.Open, FileAccess.Read))
        {
            bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
        }
        Lens.Framework.Managers.LuaManager.DoString(System.Text.Encoding.UTF8.GetString(bytes));
    }

    bool IInternal.IsLuaStarted()
    {
        return Lens.Framework.Managers.LuaManager.isStarted();
    }

    void IInternal.ToReload(string tableName, string funcName, List<string> modifys, string postFix)
    {
        var f = Lens.Framework.Managers.LuaManager.GetTable<XLua.LuaTable>(funcName);
        var t = f.Get<MyGenConfig.L_Delegate_LS>(tableName);
        t(modifys, postFix);
    }
}
*/