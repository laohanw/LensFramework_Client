# 需求现状
使用lua进行业务层开发时，经常需要对界面进行微调操作，如果每次微调都需要重新运行游戏，并进行相关流程操作，会非常的费时费力，调试成本很高，为了能够提高开发效率，降低调试成本，考虑是否能够在不重新运行游戏的状态下使lua修改直接生效
# 功能作用
在游戏运行中，修改lua代码直接生效到游戏运行时，可以做到lua修改实时重载，并且不影响游戏运行逻辑
# 接入方法
修改LuaReloadSettings.asset配置

AutoReload：文件保存后是否自动reload，若关闭则可以手动reload，菜单栏Tools/LuaToReload，快捷键：Ctrl+Q

LuaFolder:lua配置文件的目录，相对于Application.data

Extension:lua文件的后缀

LuaVersion：Lua运行的版本

项目组需要新建一个代码继承IInternal，例子如下

``` c#
public class LuaReloadInternal : IInternal
{
    void IInternal.Inject()
    {
        byte[] bytes = null;
        using (FileStream fs = new FileStream(Path.Combine(Application.dataPath, "External/LuaReload/Lua/reload.lua"), FileMode.Open, FileAccess.Read))
        {
            bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
        }
        Framework.Instance.GetManager<LuaManager>(ManagerType.Lua).DoString(System.Text.Encoding.UTF8.GetString(bytes));
    }

    bool IInternal.IsLuaStarted()
    {
        return Framework.Instance.GetManager<LuaManager>(ManagerType.Lua).IsInit;
    }

    void IInternal.ToReload(string tableName, string funcName, string luaFolder, List<string> modifys, string luaVersion)
    {
        var f = Framework.Instance.GetManager<LuaManager>(ManagerType.Lua).lua.GetTable(tableName);
        var t = f.GetLuaFunction(funcName);
        t.Call(luaFolder,modifys, luaVersion);
    }
}
```
> void IInternal.Inject()

此方法是注入reload.lua到lua虚拟机中，需要项目组获取插件中reload.lua文件并通过DOString(bytes)执行reload代码

> bool IInternal.IsLuaStarted()

此方法是用来判断reload.lua注入的时机吗，虚拟机启动后才能进行reload.lua代码的注入

>  void IInternal.ToReload(string tableName, string funcName, string luaFolder, List<string> modifys, string luaVersion)

此方法是执行reload热重载的方法，对应需要通过C#执行lua代码，并传入参数

tableName：reload代码的table名字

funcName:reload代码的执行reload操作的函数名

luaFolder:lua的相对路径，需要传入到执行代码中

modifys:修改的文件路径

luaVersion:lua的运行时版本

注意：只需要继承就可以了，插件会自动识别

接入完成后，游戏运行，修改lua，等待1s，unity出现loading进度条，完成后reload就生效了

# 热重载生效内容
1、function修改有效

2、常量值修改无效

3、新增变量有效

4、删除变量无效

3、游戏启动后只执行一次，并且已经执行过了的function，reload无效

# 实现原理
lua运行中，通过沙盒环境loader新的修改后的lua文件，通过debug库获取全局注册表识别到旧的lua table结构，使用debug.getlocalvalue和debug.getUpValue获取table结构中对应的局部变量，非局部变量，然后使用debug.setLocalValue和debug.setUpValue将新的lua table结构merge到旧的table中。这样就实现了在不改变引用的情况下改变了全局注册表中的table结构

# 注意事项

1、List<string> 需要进行wrap，reload在传入参数时需要用到

2、如果是使用tolua，需要修改tolua的Print方法：修改print在reload后的filename文本识别

``` C#


        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int Print(IntPtr L)
        {
            //lua中的print方法转Unity中的Debugger输出
            if (Debugger.useLog)
            {
                try
                {
                    int n = LuaDLL.lua_gettop(L);
                    StringBuilder sb = LuaInterface.StringBuilderCache.Acquire();
#if UNITY_EDITOR
                    int line = LuaDLL.tolua_where(L, 1);
                    string filename = LuaDLL.lua_tostring(L, -1);
                    LuaDLL.lua_settop(L, n);

//----------------TODO:修改print在reload后的filename文本识别
                    if (filename.StartsWith("--[["))
                    {
                        filename = filename.Substring(4, filename.IndexOf("]"));
                    }
//------------------------------------------------------
                    if (!filename.Contains("."))
                    {
                        sb.AppendFormat("[{0}.lua:{1}]:", filename, line);
                    }
                    else
                    {
                        sb.AppendFormat("[{0}:{1}]:", filename, line);
                    }
#endif

                    for (int i = 1; i <= n; i++)
                    {
                        if (i > 1) sb.Append("    ");

                        if (LuaDLL.lua_isstring(L, i) == 1)
                        {
                            sb.Append(LuaDLL.lua_tostring(L, i));
                        }
                        else if (LuaDLL.lua_isnil(L, i))
                        {
                            sb.Append("nil");
                        }
                        else if (LuaDLL.lua_isboolean(L, i))
                        {
                            sb.Append(LuaDLL.lua_toboolean(L, i) ? "true" : "false");
                        }
                        else
                        {
                            IntPtr p = LuaDLL.lua_topointer(L, i);

                            if (p == IntPtr.Zero)
                            {
                                sb.Append("nil");
                            }
                            else
                            {
                                sb.AppendFormat("{0}:0x{1}", LuaDLL.luaL_typename(L, i), p.ToString("X"));
                            }
                        }
                    }

                    if (Debugger.useLog)
                    {
                        Debugger.Log(LuaInterface.StringBuilderCache.GetStringAndRelease(sb));
                    }
                    return 0;
                }
                catch (Exception e)
                {
                    return LuaDLL.toluaL_exception(L, e);
                }
            }
            else
            {
                return 0;
            }
        }

```

3、reload.lua是通过全局注入的方式执行，对项目组是无感知的，但如果项目组在代码初始化是针对全局变量做的强制判断的话，需要注意下流程，或者取消reload的全局判断的强制判断，如：

``` lua

 setmetatable(_G,
    {
       __newindex=function(v1,v2,v3)
       -- 添加reload的过滤
           if (v2~="i" and v2~="j" and v2~="socket" and v2 ~= "ltn12" and v2~="reload" ) then
               error("不能添加全局变量 " .. v2, 2)
           else
               rawset(v1,v2,v3)
           end
       end,
       __index = function(t,k)
           if (k ~= "jit") then
               error("未注册的全局变量" .. k, 2)
           end
       end
    });

```