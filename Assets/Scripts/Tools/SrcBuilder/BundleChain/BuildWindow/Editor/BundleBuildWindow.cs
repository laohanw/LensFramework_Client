﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace SrcBuilder.Bundle
{
    public class BundleBuildWindow : EditorWindow
    {

        private static BundleBuildWindow m_instance;
        private Vector2 m_filesScrollPos;
        private Vector2 m_tableScrollPos;
        private Vector2 m_paramsScrollPos;
        private BundleBuildManager m_bundleBuildManager;
        private string m_selectedCategory;
        private string m_selectedVariant;
        private string m_selectedFileName;
        private string m_selectedFolderName;
        private string m_selectedEntireName;
        private float m_paramMaxWidth = 350;
        private float m_categoryMaxWidth = 10;
        private float m_table_idMaxWidth = 150;
        private float m_table_categoryMaxWidth = 10;
        private float m_table_fileNameMaxWidth = 10;
        private float m_table_variantMaxWidth = 100;
        private float m_table_dependenciedMaxWidth = 150;
        private float m_table_bundlePathMaxWidth = 10;
        private float m_table_maxHeight = 600;
        private string m_searchValue;
        private string m_searchOldValue;
        private ESearchType m_searchType = ESearchType.Id;

        private float m_dependenciedCountWidth = 20;
        private float m_sourceHeight = 20;
        private float m_categoryWidth = 250;
        private float m_categoryHeight = 20;
        private float m_variantHeight = 20;
        private float m_textLengthAccer = 8;
        private float m_singleIntervalWidth = 12;
        [MenuItem("Tools/SrcBuilder/Bundle/BuildWindow")]
        static void Init()
        {
            m_instance = BundleBuildWindow.GetWindow<BundleBuildWindow>("BundleBuildWindow");
            m_instance.Initialize();
            m_instance.minSize = new Vector2(1300, 700);
            m_instance.Show();
        }
        [MenuItem("Tools/SrcBuilder/Bundle/Build Table")]
        static void BuildTable()
        {
            if (m_instance!=null)
            {
                EditorUtility.DisplayDialog("Error", "BundleWindow is open ,you can use BuildTable button","Ok");
                return;
            }
            BundleBuildManager manager = new BundleBuildManager();
            manager.BuildTable();
            manager = null;
            EditorUtility.DisplayDialog("Info", "Bundle table success!", "Ok");
        }
        [MenuItem("Tools/SrcBuilder/Bundle/Build Package")]
        static void BuildPackage()
        {
            if (m_instance != null)
            {
                EditorUtility.DisplayDialog("Error", "BundleWindow is open ,you can use BuildTable button", "Ok");
                return;
            }
            BundleBuildManager manager = new BundleBuildManager();
            manager.BuildPackage();
            manager = null;
            EditorUtility.DisplayDialog("Info", "Bundle Package success!", "Ok");
        }
        void Initialize()
        {
            EditorUtility.DisplayProgressBar("build table", "building", 0.8f);
            m_bundleBuildManager = new BundleBuildManager();
            if (m_bundleBuildManager.categoryData.Count <= 0)
            {
                m_selectedCategory = "";
                m_selectedFileName = "";
                m_selectedFolderName = "";
                m_selectedEntireName = "";
                m_selectedVariant = "";
            }
            EditorUtility.ClearProgressBar();
        }
        void OnEnable()
        {
        }
        void OnDestroy()
        {
        }
        void OnGUI()
        {
            if (m_instance == null)
            {
                m_bundleBuildManager = null;
                Close();
                Init();
                return;
            }
            if (m_bundleBuildManager == null) return;
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                //EPackType packType = EPackType.BuildPackage;
                //packType = (EPackType)EditorGUILayout.EnumPopup(packType, EditorStyles.toolbarDropDown,GUILayout.Width(90));
                if (GUILayout.Button("Build Package", EditorStyles.toolbarButton))
                {
                    //m_bundleBuildManager.packType = packType;
                    m_bundleBuildManager.BuildPackage();
                    EditorGUILayout.EndHorizontal();
                    Repaint();
                    EditorUtility.DisplayDialog("Info", "Bundle Package success!", "Ok");
                    return;
                }
                if (GUILayout.Button("Build Manifest", EditorStyles.toolbarButton))
                {
                    EditorUtility.DisplayProgressBar("build table", "building", 0.8f);
                    m_bundleBuildManager.BuildTable();
                    EditorUtility.ClearProgressBar();
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Refresh", EditorStyles.toolbarButton))
                {
                    m_bundleBuildManager = null;
                    Close();
                    Init();
                    EditorGUILayout.EndHorizontal();
                    return;
                }
                if (GUILayout.Button("Clear", EditorStyles.toolbarButton))
                {
                    if (EditorUtility.DisplayDialog("Information","are you sure clear all bundleName?","Ok","Cancel"))
                    {
                        m_bundleBuildManager.Clear();
                        m_bundleBuildManager = null;
                        Close();
                        Init();
                        EditorGUILayout.EndHorizontal();
                        return;
                    }
                }
                if (GUILayout.Button(m_bundleBuildManager.showTable ? "▤" : "▮", EditorStyles.toolbarButton, GUILayout.Width(25)))
                {
                    m_bundleBuildManager.showTable = !m_bundleBuildManager.showTable;
                }
                GUILayout.Space(8);
                if (GUILayout.Button("Settings", EditorStyles.toolbarButton))
                {
                    PingObject(m_bundleBuildManager.config);
                }
            }
            EditorGUILayout.EndHorizontal();
            try
            {
                EditorGUILayout.BeginHorizontal();
                {
                    if (m_bundleBuildManager.showTable)
                    {
                        RenderTable();
                    }
                    else
                    {
                        RenderHorizontal();
                    }
                    RenderParam();
                }
                EditorGUILayout.EndHorizontal();
            }
            catch{ }
        }
        void PingObject(string assetPath)
        {
            UnityEngine.Object[] obj = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            bool has = false;
            FileInfo info = new FileInfo(assetPath);
            string name = info.Name.Replace(info.Extension,"").ToLower();
            for (int i = 0; i < obj.Length; i++)
            {
                if (obj[i].name.ToLower().Contains(name))
                {
                    has = true;
                    int instanceID = obj[i].GetInstanceID();
                    EditorGUIUtility.PingObject(instanceID);
                    Selection.activeObject = obj[i];
                    break;
                }
            }
            if(!has)
            {
                if (obj.Length>0)
                {
                    int instanceID = obj[0].GetInstanceID();
                    EditorGUIUtility.PingObject(instanceID);
                    Selection.activeObject = obj[0];
                }
            }
        }
        void PingObject(UnityEngine.Object obj)
        {
            int instanceID = obj.GetInstanceID();
            EditorGUIUtility.PingObject(instanceID);
            Selection.activeObject = obj;
        }
        void RenderHorizontal()
        {
            float windowWidth = position.width;
            float windowHeight = position.height;
            EditorGUILayout.BeginVertical();
            {
                m_filesScrollPos = EditorGUILayout.BeginScrollView(m_filesScrollPos, true, false, GUILayout.Height(windowHeight - m_sourceHeight));
                {
                    if (m_bundleBuildManager.categoryData.Count > 0)
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            var categoryEnumer = m_bundleBuildManager.categoryData.GetEnumerator();
                            while (categoryEnumer.MoveNext())
                            {
                                if (categoryEnumer.Current.Value.scrollWidth==0)
                                {
                                    categoryEnumer.Current.Value.scrollWidth = m_categoryWidth;
                                }
                                var r = EditorGUILayout.BeginVertical();
                                {
                                    Color c = GUI.color;
                                    if (!string.IsNullOrEmpty(m_selectedCategory) && m_selectedCategory == categoryEnumer.Current.Key && string.IsNullOrEmpty(m_selectedFileName) && string.IsNullOrEmpty(m_selectedEntireName) && string.IsNullOrEmpty(m_selectedFolderName) && string.IsNullOrEmpty(m_selectedVariant))
                                    {
                                        GUI.color = Color.yellow;
                                    }
                                    else
                                    {
                                        if (Event.current.type == EventType.MouseUp)
                                        {
                                            if (new Rect(r.x, 0, m_categoryWidth, m_categoryHeight).Contains(Event.current.mousePosition))
                                            {
                                                m_selectedCategory = categoryEnumer.Current.Key;
                                                m_selectedFileName = "";
                                                m_selectedFolderName = "";
                                                m_selectedEntireName = "";
                                                m_selectedVariant = "";
                                                Repaint();
                                            }
                                        }
                                        GUI.color = Color.cyan;
                                    }

                                    GUILayout.Box(categoryEnumer.Current.Value.category, EditorStyles.helpBox, GUILayout.Width(m_categoryWidth));
                                    string value = (categoryEnumer.Current.Value.baseId).ToString();
                                    GUI.Label(new Rect(r.x + m_categoryWidth - value.Length * m_textLengthAccer - 10, 4, value.Length * m_textLengthAccer, m_categoryHeight), value, EditorStyles.miniLabel);
                                    GUI.color = c;
                                    categoryEnumer.Current.Value.scrollPos = EditorGUILayout.BeginScrollView(categoryEnumer.Current.Value.scrollPos, EditorStyles.textField, GUILayout.Width(m_categoryWidth), GUILayout.ExpandWidth(true));
                                    {
                                        int iFile = 0;
                                        EditorGUILayout.BeginVertical(GUILayout.Height(categoryEnumer.Current.Value.scrollHeight), GUILayout.Width(categoryEnumer.Current.Value.scrollWidth));
                                        {
                                            var variantEnumer = categoryEnumer.Current.Value.variantDic.GetEnumerator();
                                            while (variantEnumer.MoveNext())
                                            {
                                                var re = EditorGUILayout.BeginHorizontal();
                                                {
                                                    if (Event.current.type == EventType.MouseUp)
                                                    {
                                                        if (new Rect(re.x, re.y + iFile * m_variantHeight, categoryEnumer.Current.Value.scrollWidth, m_variantHeight).Contains(Event.current.mousePosition))
                                                        {
                                                            variantEnumer.Current.Value.isOpen = !variantEnumer.Current.Value.isOpen;
                                                            Repaint();
                                                        }
                                                    }
                                                    Color color = GUI.color;
                                                    GUI.color = Color.white;
                                                    GUI.Label(new Rect(re.x , re.y + iFile * m_variantHeight, 12, m_variantHeight), variantEnumer.Current.Value.isOpen ? "▼" : "▶");
                                                    GUI.Label(new Rect(re.x+ categoryEnumer.Current.Value.scrollWidth /2- variantEnumer.Current.Key.Length*m_textLengthAccer, re.y + iFile *m_variantHeight, categoryEnumer.Current.Value.scrollWidth, m_variantHeight), variantEnumer.Current.Key);
                                                    GUI.Box(new Rect(re.x, re.y + iFile * m_variantHeight, categoryEnumer.Current.Value.scrollWidth, m_variantHeight), "");
                                                    GUI.color = color;
                                                }
                                                iFile++;
                                                EditorGUILayout.EndHorizontal();
                                                if (variantEnumer.Current.Value.isOpen)
                                                {
                                                    foreach (var item in variantEnumer.Current.Value.entiresDic)
                                                    {
                                                        var rect = EditorGUILayout.BeginHorizontal();
                                                        {
                                                            if (Event.current.type == EventType.MouseUp)
                                                            {
                                                                if (new Rect(rect.x + m_dependenciedCountWidth + 6, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                {
                                                                    m_selectedCategory = categoryEnumer.Current.Key;
                                                                    m_selectedFileName = "";
                                                                    m_selectedFolderName = "";
                                                                    m_selectedEntireName = item.Key;
                                                                    m_selectedVariant = variantEnumer.Current.Key;
                                                                    Repaint();
                                                                }
                                                            }
                                                            if (Event.current.type == EventType.MouseUp)
                                                            {
                                                                if (new Rect(0, iFile * m_sourceHeight, m_dependenciedCountWidth+6, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                {
                                                                    item.Value.isOpen = !item.Value.isOpen;
                                                                    Repaint();
                                                                }
                                                            }
                                                            Color color = GUI.color;
                                                            if (item.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                            {
                                                                GUI.color = Color.yellow;
                                                                GUI.Label(new Rect(rect.x, rect.y + (iFile * m_dependenciedCountWidth), m_dependenciedCountWidth, m_sourceHeight), item.Value.dependenciedChainList.Count.ToString());
                                                                GUI.color = color;
                                                            }
                                                            if (item.Value.fileState == ESourceState.MainBundle)
                                                            {
                                                                if (!item.Value.dontDamage)
                                                                {
                                                                    GUI.color = Color.green;
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                }
                                                                GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                            }
                                                            else if (item.Value.fileState == ESourceState.SharedBundle)
                                                            {
                                                                if (!item.Value.dontDamage)
                                                                {
                                                                    GUI.color = Color.green;
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                }
                                                                GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                            }
                                                            else if (item.Value.fileState == ESourceState.PrivateBundle)
                                                            {
                                                                GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                            }
                                                            else if (item.Value.fileState == ESourceState.Damage)
                                                            {
                                                                GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                            }
                                                            if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedEntireName == item.Key && m_selectedVariant == variantEnumer.Current.Key && string.IsNullOrEmpty(m_selectedFileName) && string.IsNullOrEmpty(m_selectedFolderName))
                                                            {
                                                                GUI.color = Color.yellow;
                                                                GUI.Box(new Rect(rect.x, rect.y + iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                            }
                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), item.Value.isOpen ? "▼" : "▶");
                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + iFile * m_sourceHeight, item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                            GUI.color = color;
                                                        }
                                                        EditorGUILayout.EndHorizontal();
                                                        iFile++;
                                                        if (item.Value.isOpen)
                                                        {
                                                            foreach (var it in item.Value.filesDic)
                                                            {
                                                                Color color = GUI.color;
                                                                categoryEnumer.Current.Value.scrollWidth = categoryEnumer.Current.Value.scrollWidth > it.Value.fileName.Length * m_textLengthAccer + m_singleIntervalWidth*2 ? categoryEnumer.Current.Value.scrollWidth : it.Value.fileName.Length*m_textLengthAccer+ m_singleIntervalWidth * 2;
                                                                if (it.Value.dontDamage)
                                                                {
                                                                    if (Event.current.type == EventType.MouseUp)
                                                                    {
                                                                        if (new Rect(0, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                        {
                                                                            m_selectedCategory = categoryEnumer.Current.Key;
                                                                            m_selectedFileName = it.Key;
                                                                            m_selectedFolderName = "";
                                                                            m_selectedEntireName = item.Key;
                                                                            m_selectedVariant = variantEnumer.Current.Key;
                                                                            Repaint();
                                                                        }
                                                                    }
                                                                    if (it.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                                    {
                                                                        GUI.color = Color.yellow;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth , rect.y + iFile * m_sourceHeight, m_singleIntervalWidth , m_sourceHeight), it.Value.dependenciedChainList.Count.ToString());
                                                                        GUI.color = color;
                                                                    }
                                                                    if (it.Value.fileState == ESourceState.MainBundle)
                                                                    {
                                                                        if (!item.Value.dontDamage)
                                                                        {
                                                                            GUI.color = Color.green;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth , rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                        }
                                                                        GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                                    }
                                                                    else if (it.Value.fileState == ESourceState.SharedBundle)
                                                                    {
                                                                        if (!item.Value.dontDamage)
                                                                        {
                                                                            GUI.color = Color.green;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth , rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                        }
                                                                        GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                                    }
                                                                    else if (it.Value.fileState == ESourceState.Damage)
                                                                    {
                                                                        GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth , rect.y + iFile *m_sourceHeight, m_singleIntervalWidth , m_sourceHeight), "x");
                                                                    }
                                                                    else if (it.Value.fileState == ESourceState.PrivateBundle)
                                                                    {
                                                                        GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                                    }
                                                                    if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedFileName == it.Key && m_selectedVariant == variantEnumer.Current.Key && string.IsNullOrEmpty(m_selectedFolderName))
                                                                    {
                                                                        GUI.color = Color.yellow;
                                                                        GUI.Box(new Rect(rect.x, rect.y + iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                                    }
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 3, rect.y + iFile * m_sourceHeight, it.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), it.Value.fileName);
                                                                }
                                                                else
                                                                {
                                                                    GUI.color = Color.red;
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2 , rect.y + iFile * m_sourceHeight, m_singleIntervalWidth , m_sourceHeight), "x");
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 3, rect.y + iFile * m_sourceHeight, it.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), it.Value.fileName);
                                                                }
                                                                GUI.color = color;
                                                                iFile++;
                                                            }
                                                        }
                                                    }
                                                    //if (variantEnumer.Current.Value.entiresDic.Count > 0)
                                                    //{
                                                    //    GUI.TextField(new Rect(8, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, 1), "", EditorStyles.textField);
                                                    //    GUI.TextField(new Rect(8, iFile * m_sourceHeight + m_singleIntervalWidth, categoryEnumer.Current.Value.scrollWidth, 1), "", EditorStyles.textField);
                                                    //    iFile++;
                                                    //}
                                                    foreach (var item in variantEnumer.Current.Value.filesDic)
                                                    {
                                                        var rect = EditorGUILayout.BeginHorizontal();
                                                        {
                                                            Color color = GUI.color;
                                                            categoryEnumer.Current.Value.scrollWidth = categoryEnumer.Current.Value.scrollWidth > item.Value.fileName.Length*8+24 ? categoryEnumer.Current.Value.scrollWidth : item.Value.fileName.Length*8+24;
                                                            if (item.Value.dontDamage)
                                                            {
                                                                if (Event.current.type == EventType.MouseUp)
                                                                {
                                                                    if (new Rect(0, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                    {
                                                                        m_selectedCategory = categoryEnumer.Current.Key;
                                                                        m_selectedFileName = item.Key;
                                                                        m_selectedFolderName = "";
                                                                        m_selectedEntireName = "";
                                                                        m_selectedVariant = variantEnumer.Current.Key;
                                                                        Repaint();
                                                                    }
                                                                }
                                                                if (item.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                                {
                                                                    GUI.color = Color.yellow;
                                                                    GUI.Label(new Rect(rect.x, rect.y + (iFile * m_dependenciedCountWidth), m_dependenciedCountWidth, m_sourceHeight), item.Value.dependenciedChainList.Count.ToString());
                                                                    GUI.color = color;
                                                                }
                                                                if (item.Value.fileState == ESourceState.MainBundle)
                                                                {
                                                                    if (!item.Value.dontDamage)
                                                                    {
                                                                        GUI.color = Color.green;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + (iFile * m_sourceHeight) + m_singleIntervalWidth, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                    }
                                                                    GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                                }
                                                                else if (item.Value.fileState == ESourceState.SharedBundle)
                                                                {
                                                                    if (!item.Value.dontDamage)
                                                                    {
                                                                        GUI.color = Color.green;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + (iFile * m_sourceHeight) + m_singleIntervalWidth, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                    }
                                                                    GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                                }
                                                                else if (item.Value.fileState == ESourceState.PrivateBundle)
                                                                {
                                                                    GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                                }
                                                                else if (item.Value.fileState == ESourceState.Damage)
                                                                {
                                                                    GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                                }
                                                                if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedFileName == item.Key && m_selectedVariant == variantEnumer.Current.Key && string.IsNullOrEmpty(m_selectedFolderName))
                                                                {
                                                                    GUI.color = Color.yellow;
                                                                    GUI.Box(new Rect(rect.x, rect.y * (iFile * m_sourceHeight), categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                }
                                                                else
                                                                {
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                GUI.color = Color.red;
                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                            }
                                                            GUI.color = color;
                                                        }
                                                        EditorGUILayout.EndHorizontal();
                                                        iFile++;
                                                    }
                                                    foreach (var itse in variantEnumer.Current.Value.foldersDic)
                                                    {
                                                        var res = EditorGUILayout.BeginHorizontal();
                                                        {
                                                            if (Event.current.type == EventType.MouseUp)
                                                            {
                                                                if (new Rect(res.x + m_dependenciedCountWidth + 6, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                {
                                                                    Repaint();
                                                                }
                                                            }
                                                            if (Event.current.type == EventType.MouseUp)
                                                            {
                                                                if (new Rect(0, iFile * m_sourceHeight, m_dependenciedCountWidth + 6, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                {
                                                                    itse.Value.isOpen = !itse.Value.isOpen;
                                                                    Repaint();
                                                                }
                                                            }
                                                            Color color = GUI.color;
                                                            GUI.color = m_bundleBuildManager.config.folderColor;
                                                            GUI.Label(new Rect(res.x + m_singleIntervalWidth, res.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), itse.Value.isOpen ? "▼" : "▶");
                                                            GUI.Label(new Rect(res.x + m_singleIntervalWidth * 2, res.y + iFile * m_sourceHeight, itse.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), itse.Value.fileName);
                                                            GUI.color = color;
                                                        }
                                                        EditorGUILayout.EndHorizontal();
                                                        iFile++;
                                                        if (itse.Value.isOpen)
                                                        {
                                                            foreach (var item in itse.Value.entiresDic)
                                                            {
                                                                var rect = EditorGUILayout.BeginHorizontal();
                                                                {
                                                                    if (Event.current.type == EventType.MouseUp)
                                                                    {
                                                                        if (new Rect(rect.x + m_dependenciedCountWidth + 6, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                        {
                                                                            m_selectedCategory = categoryEnumer.Current.Key;
                                                                            m_selectedFileName = "";
                                                                            m_selectedFolderName = itse.Key;
                                                                            m_selectedEntireName = item.Key;
                                                                            m_selectedVariant = variantEnumer.Current.Key;
                                                                            Repaint();
                                                                        }
                                                                    }
                                                                    if (Event.current.type == EventType.MouseUp)
                                                                    {
                                                                        if (new Rect(rect.x+ m_singleIntervalWidth, iFile * m_sourceHeight, m_dependenciedCountWidth + 6, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                        {
                                                                            item.Value.isOpen = !item.Value.isOpen;
                                                                            Repaint();
                                                                        }
                                                                    }
                                                                    Color color = GUI.color;
                                                                    if (item.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                                    {
                                                                        GUI.color = Color.yellow;
                                                                        GUI.Label(new Rect(rect.x, rect.y + (iFile * m_dependenciedCountWidth), m_dependenciedCountWidth, m_sourceHeight), item.Value.dependenciedChainList.Count.ToString());
                                                                        GUI.color = color;
                                                                    }
                                                                    if (item.Value.fileState == ESourceState.MainBundle)
                                                                    {
                                                                        if (!item.Value.dontDamage)
                                                                        {
                                                                            GUI.color = Color.green;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                        }
                                                                        GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                                    }
                                                                    else if (item.Value.fileState == ESourceState.SharedBundle)
                                                                    {
                                                                        if (!item.Value.dontDamage)
                                                                        {
                                                                            GUI.color = Color.green;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                        }
                                                                        GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                                    }
                                                                    else if (item.Value.fileState == ESourceState.PrivateBundle)
                                                                    {
                                                                        GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                                    }
                                                                    else if (item.Value.fileState == ESourceState.Damage)
                                                                    {
                                                                        GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                                    }
                                                                    if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedEntireName == item.Key && m_selectedVariant == variantEnumer.Current.Key && string.IsNullOrEmpty(m_selectedFileName) && m_selectedFolderName==itse.Key)
                                                                    {
                                                                        GUI.color = Color.yellow;
                                                                        GUI.Box(new Rect(rect.x, rect.y + iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                                    }
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth*2, rect.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), item.Value.isOpen ? "▼" : "▶");
                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 3, rect.y + iFile * m_sourceHeight, item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                    GUI.color = color;
                                                                }
                                                                EditorGUILayout.EndHorizontal();
                                                                iFile++;
                                                                if (item.Value.isOpen)
                                                                {
                                                                    foreach (var it in item.Value.filesDic)
                                                                    {
                                                                        Color color = GUI.color;
                                                                        categoryEnumer.Current.Value.scrollWidth = categoryEnumer.Current.Value.scrollWidth > it.Value.fileName.Length * m_textLengthAccer + m_singleIntervalWidth * 2 ? categoryEnumer.Current.Value.scrollWidth : it.Value.fileName.Length * m_textLengthAccer + m_singleIntervalWidth * 2;
                                                                        if (it.Value.dontDamage)
                                                                        {
                                                                            if (Event.current.type == EventType.MouseUp)
                                                                            {
                                                                                if (new Rect(0, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                                {
                                                                                    m_selectedCategory = categoryEnumer.Current.Key;
                                                                                    m_selectedFileName = it.Key;
                                                                                    m_selectedFolderName = itse.Key;
                                                                                    m_selectedEntireName = item.Key;
                                                                                    m_selectedVariant = variantEnumer.Current.Key;
                                                                                    Repaint();
                                                                                }
                                                                            }
                                                                            if (it.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                                            {
                                                                                GUI.color = Color.yellow;
                                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), it.Value.dependenciedChainList.Count.ToString());
                                                                                GUI.color = color;
                                                                            }
                                                                            if (it.Value.fileState == ESourceState.MainBundle)
                                                                            {
                                                                                if (!item.Value.dontDamage)
                                                                                {
                                                                                    GUI.color = Color.green;
                                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                                }
                                                                                GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                                            }
                                                                            else if (it.Value.fileState == ESourceState.SharedBundle)
                                                                            {
                                                                                if (!item.Value.dontDamage)
                                                                                {
                                                                                    GUI.color = Color.green;
                                                                                    GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + (iFile * m_sourceHeight) + 15, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                                }
                                                                                GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                                            }
                                                                            else if (it.Value.fileState == ESourceState.Damage)
                                                                            {
                                                                                GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 2, rect.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), "x");
                                                                            }
                                                                            else if (it.Value.fileState == ESourceState.PrivateBundle)
                                                                            {
                                                                                GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                                            }
                                                                            if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedFileName == it.Key && m_selectedVariant == variantEnumer.Current.Key && m_selectedFolderName == itse.Key)
                                                                            {
                                                                                GUI.color = Color.yellow;
                                                                                GUI.Box(new Rect(rect.x, rect.y + iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                                            }
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + iFile * m_sourceHeight, it.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), it.Value.fileName);
                                                                        }
                                                                        else
                                                                        {
                                                                            GUI.color = Color.red;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 3, rect.y + iFile * m_sourceHeight, m_singleIntervalWidth, m_sourceHeight), "x");
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + iFile * m_sourceHeight, it.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), it.Value.fileName);
                                                                        }
                                                                        GUI.color = color;
                                                                        iFile++;
                                                                    }
                                                                }
                                                            }
                                                            //if (itse.Value.entiresDic.Count > 0)
                                                            //{
                                                            //    GUI.TextField(new Rect(8, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, 1), "", EditorStyles.textField);
                                                            //    GUI.TextField(new Rect(8, iFile * m_sourceHeight + m_singleIntervalWidth, categoryEnumer.Current.Value.scrollWidth, 1), "", EditorStyles.textField);
                                                            //    iFile++;
                                                            //}
                                                            foreach (var item in itse.Value.filesDic)
                                                            {
                                                                var rect = EditorGUILayout.BeginHorizontal();
                                                                {
                                                                    Color color = GUI.color;
                                                                    categoryEnumer.Current.Value.scrollWidth = categoryEnumer.Current.Value.scrollWidth > item.Value.fileName.Length * 8 + 24 ? categoryEnumer.Current.Value.scrollWidth : item.Value.fileName.Length * 8 + 24;
                                                                    if (item.Value.dontDamage)
                                                                    {
                                                                        if (Event.current.type == EventType.MouseUp)
                                                                        {
                                                                            if (new Rect(rect.x, iFile * m_sourceHeight, categoryEnumer.Current.Value.scrollWidth, m_sourceHeight).Contains(Event.current.mousePosition))
                                                                            {
                                                                                m_selectedCategory = categoryEnumer.Current.Key;
                                                                                m_selectedFileName = item.Key;
                                                                                m_selectedFolderName = itse.Key;
                                                                                m_selectedEntireName = "";
                                                                                m_selectedVariant = variantEnumer.Current.Key;
                                                                                Repaint();
                                                                            }
                                                                        }
                                                                        if (item.Value.dependenciedChainList.Count > m_bundleBuildManager.config.minBundleDependencyCount)
                                                                        {
                                                                            GUI.color = Color.yellow;
                                                                            GUI.Label(new Rect(rect.x+m_singleIntervalWidth*2, rect.y + (iFile * m_dependenciedCountWidth), m_dependenciedCountWidth, m_sourceHeight), item.Value.dependenciedChainList.Count.ToString());
                                                                            GUI.color = color;
                                                                        }
                                                                        if (item.Value.fileState == ESourceState.MainBundle)
                                                                        {
                                                                            if (!item.Value.dontDamage)
                                                                            {
                                                                                GUI.color = Color.green;
                                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + (iFile * m_sourceHeight) + m_singleIntervalWidth, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                            }
                                                                            GUI.color = m_bundleBuildManager.config.mainBundleColor;
                                                                        }
                                                                        else if (item.Value.fileState == ESourceState.SharedBundle)
                                                                        {
                                                                            if (!item.Value.dontDamage)
                                                                            {
                                                                                GUI.color = Color.green;
                                                                                GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + (iFile * m_sourceHeight) + m_singleIntervalWidth, item.Value.fileName.Length * m_textLengthAccer, 0.2f), "", EditorStyles.textField);
                                                                            }
                                                                            GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                                                                        }
                                                                        else if (item.Value.fileState == ESourceState.PrivateBundle)
                                                                        {
                                                                            GUI.color = m_bundleBuildManager.config.privateBundleColor;
                                                                        }
                                                                        else if (item.Value.fileState == ESourceState.Damage)
                                                                        {
                                                                            GUI.color = m_bundleBuildManager.config.damageBundleColor;
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth*3, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                                        }
                                                                        if (m_selectedCategory == categoryEnumer.Current.Key && m_selectedFileName == item.Key && m_selectedVariant == variantEnumer.Current.Key && m_selectedFolderName==itse.Key)
                                                                        {
                                                                            GUI.color = Color.yellow;
                                                                            GUI.Box(new Rect(rect.x, rect.y * (iFile * m_sourceHeight), categoryEnumer.Current.Value.scrollWidth, m_sourceHeight), "");
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                        }
                                                                        else
                                                                        {
                                                                            GUI.Label(new Rect(rect.x + m_singleIntervalWidth*3, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        GUI.color = Color.red;
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth*3, rect.y + (iFile * m_sourceHeight), m_singleIntervalWidth, m_sourceHeight), "x");
                                                                        GUI.Label(new Rect(rect.x + m_singleIntervalWidth * 4, rect.y + (iFile * m_sourceHeight), item.Value.fileName.Length * m_textLengthAccer, m_sourceHeight), item.Value.fileName);
                                                                    }
                                                                    GUI.color = color;
                                                                }
                                                                EditorGUILayout.EndHorizontal();
                                                                iFile++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            categoryEnumer.Current.Value.scrollHeight = iFile * m_sourceHeight;
                                        }
                                        EditorGUILayout.EndVertical();
                                    }
                                    EditorGUILayout.EndScrollView();
                                    GUILayout.Space(7);
                                    GUILayout.TextField("", GUILayout.Height(1));
                                    GUILayout.Space(7);
                                }
                                EditorGUILayout.EndVertical();
                            }
                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    else
                    {
                        GUILayout.Label("1、设置资源根目录RootName");
                        GUILayout.Label("2、设置好对应的平台BuildTarget");
                        GUILayout.Label("3、按下Refresh");
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.TextField("", GUILayout.Height(windowHeight), GUILayout.Width(0.5f));
        }
        void RenderTable()
        {
            float windowWidth = position.width;
            float windowHeight = position.height;
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                GUILayout.FlexibleSpace();
                m_searchOldValue = EditorGUILayout.TextField(m_searchOldValue);
                m_searchType = (ESearchType)EditorGUILayout.EnumPopup(m_searchType, GUILayout.Width(100));
                if (m_searchOldValue != m_searchValue && string.IsNullOrEmpty(m_searchOldValue))
                {
                    m_searchValue = m_searchOldValue;
                    m_bundleBuildManager.ResetSearch();
                }
                if (GUILayout.Button("Search", EditorStyles.miniButton))
                {
                    if (!string.IsNullOrEmpty(m_searchOldValue))
                    {
                        m_searchValue = m_searchOldValue;
                        m_bundleBuildManager.Search(m_searchValue, m_searchType);
                    }
                }
                EditorGUILayout.EndHorizontal();
                m_tableScrollPos = EditorGUILayout.BeginScrollView(m_tableScrollPos, true, false, GUILayout.Height(windowHeight - m_sourceHeight));
                {
                    EditorGUILayout.BeginVertical(GUILayout.MinHeight(m_table_maxHeight), GUILayout.ExpandHeight(true));
                    {
                        GUILayout.Label("");
                        int y = 0;
                        RenderTitle(y);
                        y++;
                        if (string.IsNullOrEmpty(m_searchValue))
                        {
                            foreach (var categoryData in m_bundleBuildManager.categoryData)
                            {
                                foreach (var variantData in categoryData.Value.variantDic)
                                {
                                    foreach (var directories in variantData.Value.entiresDic)
                                    {
                                        int dep = 0;
                                        foreach (var it in directories.Value.filesDic)
                                        {
                                            dep += it.Value.fileDependencied.Count;
                                        }
                                        bool selected = false;
                                        if (m_selectedCategory == categoryData.Key && m_selectedEntireName == directories.Key && string.IsNullOrEmpty(m_selectedFileName))
                                        {
                                            selected = true;
                                        }
                                        RenderRow(y, directories.Value.assetId, categoryData.Value.category, variantData.Key,  directories.Value.fileName, dep, directories.Value.bundleName, selected, directories.Value.fileState);
                                        if (Event.current.type == EventType.MouseUp && directories.Value.dontDamage)
                                        {
                                            if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                            {
                                                GUI.FocusControl("");
                                                m_selectedCategory = categoryData.Key;
                                                m_selectedFileName = "";
                                                m_selectedEntireName = directories.Key;
                                                m_selectedVariant = variantData.Key;
                                                Repaint();
                                            }
                                        }
                                        y++;
                                    }
                                    foreach (var files in variantData.Value.filesDic)
                                    {
                                        bool selected = false;
                                        if (m_selectedCategory == categoryData.Key && m_selectedFileName == files.Key && string.IsNullOrEmpty(m_selectedEntireName))
                                        {
                                            selected = true;
                                        }
                                        RenderRow(y, files.Value.assetId, categoryData.Value.category, variantData.Key, files.Value.fileName, files.Value.fileDependencied.Count, files.Value.bundleName, selected, files.Value.fileState);
                                        if (Event.current.type == EventType.MouseUp && files.Value.dontDamage)
                                        {
                                            if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                            {
                                                GUI.FocusControl("");
                                                m_selectedCategory = categoryData.Key;
                                                m_selectedFileName = files.Key;
                                                m_selectedEntireName = "";
                                                m_selectedVariant = variantData.Key;
                                                Repaint();
                                            }
                                        }
                                        y++;
                                    }
                                    foreach (var itse in variantData.Value.foldersDic)
                                    {
                                        foreach (var directories in itse.Value.entiresDic)
                                        {
                                            int dep = 0;
                                            foreach (var it in directories.Value.filesDic)
                                            {
                                                dep += it.Value.fileDependencied.Count;
                                            }
                                            bool selected = false;
                                            if (m_selectedCategory == categoryData.Key && m_selectedEntireName == directories.Key && string.IsNullOrEmpty(m_selectedFileName))
                                            {
                                                selected = true;
                                            }
                                            RenderRow(y, directories.Value.assetId, categoryData.Value.category, variantData.Key, directories.Value.fileName, dep, directories.Value.bundleName, selected, directories.Value.fileState);
                                            if (Event.current.type == EventType.MouseUp && directories.Value.dontDamage)
                                            {
                                                if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                                {
                                                    GUI.FocusControl("");
                                                    m_selectedCategory = categoryData.Key;
                                                    m_selectedFileName = "";
                                                    m_selectedEntireName = directories.Key;
                                                    m_selectedFolderName = itse.Key;
                                                    m_selectedVariant = variantData.Key;
                                                    Repaint();
                                                }
                                            }
                                            y++;
                                        }
                                        foreach (var files in itse.Value.filesDic)
                                        {
                                            bool selected = false;
                                            if (m_selectedCategory == categoryData.Key && m_selectedFileName == files.Key && string.IsNullOrEmpty(m_selectedEntireName))
                                            {
                                                selected = true;
                                            }
                                            RenderRow(y, files.Value.assetId, categoryData.Value.category, variantData.Key, files.Value.fileName, files.Value.fileDependencied.Count, files.Value.bundleName, selected, files.Value.fileState);
                                            if (Event.current.type == EventType.MouseUp && files.Value.dontDamage)
                                            {
                                                if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                                {
                                                    GUI.FocusControl("");
                                                    m_selectedCategory = categoryData.Key;
                                                    m_selectedFileName = files.Key;
                                                    m_selectedEntireName = "";
                                                    m_selectedFolderName = itse.Key;
                                                    m_selectedVariant = variantData.Key;
                                                    Repaint();
                                                }
                                            }
                                            y++;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var category in m_bundleBuildManager.searchedEntires)
                            {
                                foreach (var directories in category.Value)
                                {
                                    int dep = 0;
                                    foreach (var it in directories.filesDic)
                                    {
                                        dep += it.Value.fileDependencied.Count;
                                    }
                                    bool selected = false;
                                    if (!selected && m_selectedCategory == category.Key && m_selectedEntireName == directories.fileName.ToLower() && string.IsNullOrEmpty(m_selectedFileName))
                                    {
                                        selected = true;
                                    }
                                    RenderRow(y, directories.assetId, category.Key, directories.variantName, directories.fileName, dep, directories.bundleName, selected, directories.fileState);
                                    if (Event.current.type == EventType.MouseUp && directories.dontDamage)
                                    {
                                        if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                        {
                                            GUI.FocusControl("");
                                            m_selectedCategory = category.Key.ToLower();
                                            m_selectedFileName = "";
                                            m_selectedEntireName = directories.fileName.ToLower();
                                            m_selectedVariant = directories.variantName;
                                            Repaint();
                                        }
                                    }
                                    y++;
                                }
                            }
                            foreach (var category in m_bundleBuildManager.searchedFiles)
                            {
                                foreach (var files in category.Value)
                                {
                                    bool selected = false;
                                    if (!selected && m_selectedCategory == category.Key && m_selectedFileName == files.fileName.ToLower() && string.IsNullOrEmpty(m_selectedEntireName))
                                    {
                                        selected = true;
                                    }
                                    RenderRow(y, files.assetId, category.Key, files.variantName, files.fileName, files.fileDependencied.Count, files.bundleName, selected, files.fileState);
                                    if (Event.current.type == EventType.MouseUp && files.dontDamage)
                                    {
                                        if (new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight).Contains(Event.current.mousePosition))
                                        {
                                            GUI.FocusControl("");
                                            m_selectedCategory = category.Key.ToLower();
                                            m_selectedFileName = files.fileName.ToLower();
                                            m_selectedEntireName = "";
                                            m_selectedVariant = files.variantName;
                                            Repaint();
                                        }
                                    }
                                    y++;
                                }
                            }
                        }
                        m_table_maxHeight = y * m_sourceHeight;
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }
        void RenderParam()
        {
            float windowWidth = position.width;
            float windowHeight = position.height;
            m_paramsScrollPos = EditorGUILayout.BeginScrollView(m_paramsScrollPos, GUILayout.MinWidth(300), GUILayout.ExpandWidth(true), GUILayout.Height(windowHeight - 20));
            {
                EditorGUILayout.BeginVertical(GUILayout.Width(m_paramMaxWidth));
                //{
                if (!string.IsNullOrEmpty(m_selectedCategory) && m_bundleBuildManager.categoryData.ContainsKey(m_selectedCategory))
                {
                    if (string.IsNullOrEmpty(m_selectedEntireName) && string.IsNullOrEmpty(m_selectedFileName))// 单独选中分类框
                    {
                        GUILayout.Label("Params Settings", EditorStyles.boldLabel);
                        GUILayout.TextField("", GUILayout.Height(1));
                        EditorGUILayout.Separator();

                        EditorGUILayout.BeginVertical();
                        EditorGUILayout.BeginHorizontal();
                        var categoryData = m_bundleBuildManager.categoryData[m_selectedCategory];
                        if (categoryData.isBaseIdReset)
                        {
                            categoryData.oldId = EditorGUILayout.IntField("BaseID: ", categoryData.oldId);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("✔", EditorStyles.miniButton))
                            {
                                if (categoryData.oldId != categoryData.baseId)
                                {
                                    if (EditorUtility.DisplayDialog("Information", "are you sure change baseId", "Ok", "Cancel"))
                                    {
                                        if (!m_bundleBuildManager.ResetBaseId(m_selectedCategory, categoryData.oldId))
                                        {
                                            EditorUtility.DisplayDialog("Warning", "修改失败！1、id必须大于idBase\n2、id不能重复\n3、id必须是基于idBase", "Ok");
                                            categoryData.oldId = categoryData.baseId;
                                        }
                                        else
                                        {
                                            categoryData.baseId = categoryData.oldId;
                                            categoryData.isBaseIdReset = false;
                                            Repaint();
                                        }
                                    }
                                    GUI.FocusControl("");
                                }
                                else
                                {
                                    GUI.FocusControl("");
                                    categoryData.isBaseIdReset = false;
                                }
                            }
                            if (GUILayout.Button("✘", EditorStyles.miniButton))
                            {
                                categoryData.oldId = categoryData.baseId;
                                categoryData.isBaseIdReset = false;
                            }
                        }
                        else
                        {
                            GUILayout.Label("BaseID: " + categoryData.baseId);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button(" ☚ ", EditorStyles.miniButton))
                            {
                                categoryData.isBaseIdReset = true;
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                        //GUILayout.Label("TotalCount: " + (categoryData.filesDic.Count + categoryData.directoriesDic.Count));

                        EditorGUILayout.EndVertical();
                    }
                    else if (!string.IsNullOrEmpty(m_selectedEntireName) && string.IsNullOrEmpty(m_selectedFileName))//单独选中文件夹
                    {
                        GUILayout.Label("Params Settings", EditorStyles.boldLabel);
                        GUILayout.TextField("", GUILayout.Height(1));
                        EditorGUILayout.Separator();

                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.BeginHorizontal();
                        SourceEntire sourceFile = null;
                        var vari = m_bundleBuildManager.categoryData[m_selectedCategory].variantDic[m_selectedVariant];
                        if (string.IsNullOrEmpty(m_selectedFolderName))
                        {
                            sourceFile = vari.entiresDic[m_selectedEntireName];
                        }
                        else
                        {
                            sourceFile = vari.foldersDic[m_selectedFolderName].entiresDic[m_selectedEntireName];
                        }
                        if (sourceFile.isBaseIdReset)
                        {
                            sourceFile.oldAssetId = EditorGUILayout.IntField("ID: ", sourceFile.oldAssetId);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("✔", EditorStyles.miniButton))
                            {
                                if (sourceFile.oldAssetId != sourceFile.assetId)
                                {
                                    if (EditorUtility.DisplayDialog("Information", "are you sure change baseId", "Ok", "Cancel"))
                                    {
                                        if (!m_bundleBuildManager.ResetBundleId(m_selectedCategory,m_selectedVariant, m_selectedEntireName, sourceFile.oldAssetId, true))
                                        {
                                            EditorUtility.DisplayDialog("Warning", "修改失败！1、id必须大于idBase\n2、id不能重复\n3、id必须是基于idBase", "Ok");
                                            sourceFile.oldAssetId = sourceFile.assetId;
                                        }
                                        else
                                        {
                                            sourceFile.assetId = sourceFile.oldAssetId;
                                            sourceFile.isBaseIdReset = false;
                                            Repaint();
                                        }
                                        GUI.FocusControl("");
                                    }
                                }
                                else
                                {
                                    GUI.FocusControl("");
                                    sourceFile.isBaseIdReset = false;
                                }

                            }
                            if (GUILayout.Button("✘", EditorStyles.miniButton))
                            {
                                sourceFile.oldAssetId = sourceFile.assetId;
                                sourceFile.isBaseIdReset = false;
                            }
                        }
                        else
                        {
                            GUILayout.Label("ID: " + sourceFile.assetId);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button(" ☚ ", EditorStyles.miniButton))
                            {
                                sourceFile.isBaseIdReset = true;
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Label("GUID: " + sourceFile.guid);
                        if (sourceFile.bundleSize > 0)
                        {
                            GUILayout.Label("BundleSize: " + sourceFile.bundleSize + "kb");
                        }
                        EditorGUILayout.EndVertical();

                        GUILayout.Label("All Asset", EditorStyles.boldLabel);
                        GUILayout.TextField("", GUILayout.Height(1));
                        EditorGUILayout.Separator();
                        foreach (var sFile in sourceFile.filesDic)
                        {
                            var l = RenderFileHorizontal(sFile.Value.mainAssetPath, sFile.Value.mainAssetName);
                            m_paramMaxWidth = m_paramMaxWidth > l ? m_paramMaxWidth : l;
                        }
                        EditorGUILayout.Separator();
                        EditorGUILayout.Separator();
                        EditorGUILayout.LabelField("Dependency", EditorStyles.boldLabel);
                        EditorGUILayout.TextField("", GUILayout.Height(1));
                        EditorGUILayout.Separator();
                        if (sourceFile.dependenciesChain >= 0)
                        {
                            var chainId = sourceFile.dependenciesChain;
                            var chain = m_bundleBuildManager.dependChains[chainId];
                            for (int j = chain.Count - 1; j >= 0; j--)
                            {
                                for (int k = 0; k < chain[j].Count; k++)
                                {
                                    var id = chain[j][k];
                                    var baseSource = m_bundleBuildManager.allSourcesGuid[id];
                                    var m = RenderFileHorizontal(j == chain.Count - 1 ? 38 : 62, j, baseSource.bundleName, baseSource.variantName, baseSource.mainAssetPath, sourceFile.guid == id, baseSource.fileState, baseSource.dependenciedChainList.Count);
                                    m_paramMaxWidth = m_paramMaxWidth > m ? m_paramMaxWidth : m;
                                }
                            }
                        }


                        EditorGUILayout.Separator();
                        EditorGUILayout.Separator();
                        EditorGUILayout.LabelField("Dependent Chain", EditorStyles.boldLabel);
                        EditorGUILayout.TextField("", GUILayout.Height(1));
                        EditorGUILayout.Separator();
                        if (sourceFile.fileState!=ESourceState.MainBundle)
                        {
                            for (int i = 0; i < sourceFile.dependenciedChainList.Count; i++)
                            {
                                var chainId = sourceFile.dependenciedChainList[i];
                                var chain = m_bundleBuildManager.dependChains[chainId];
                                for (int j = chain.Count - 1; j >= 0; j--)
                                {
                                    for (int k = 0; k < chain[j].Count; k++)
                                    {
                                        var id = chain[j][k];
                                        var baseSource = m_bundleBuildManager.allSourcesGuid[id];
                                        var m = RenderFileHorizontal(j == chain.Count - 1 ? 38 : 62, j, baseSource.bundleName, baseSource.variantName, baseSource.mainAssetPath, sourceFile.guid == id, baseSource.fileState, baseSource.dependenciedChainList.Count);
                                        m_paramMaxWidth = m_paramMaxWidth > m ? m_paramMaxWidth : m;
                                    }
                                }
                            }
                        }
                    }
                    else//只选中文件夹中单个文件或者文件夹外单个文件
                    {
                        SourceSingle sourceFile = null;
                        var varit = m_bundleBuildManager.categoryData[m_selectedCategory].variantDic[m_selectedVariant];
                        if (string.IsNullOrEmpty(m_selectedFolderName))
                        {
                            if (string.IsNullOrEmpty(m_selectedEntireName))
                            {
                                if (varit.filesDic.ContainsKey(m_selectedFileName))
                                {
                                    sourceFile = varit.filesDic[m_selectedFileName];
                                }
                            }
                            else
                            {
                                if (varit.entiresDic.ContainsKey(m_selectedEntireName))
                                {
                                    sourceFile = varit.entiresDic[m_selectedEntireName].filesDic[m_selectedFileName];
                                }
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(m_selectedEntireName))
                            {
                                if (varit.foldersDic[m_selectedFolderName].filesDic.ContainsKey(m_selectedFileName))
                                {
                                    sourceFile = varit.foldersDic[m_selectedFolderName].filesDic[m_selectedFileName];
                                }
                            }
                            else
                            {
                                if (varit.foldersDic[m_selectedFolderName].entiresDic.ContainsKey(m_selectedEntireName))
                                {
                                    sourceFile = varit.foldersDic[m_selectedFolderName].entiresDic[m_selectedEntireName].filesDic[m_selectedFileName];
                                }
                            }
                        }
                        if (sourceFile != null)
                        {

                            GUILayout.Label("Params Settings", EditorStyles.boldLabel);
                            GUILayout.TextField("", GUILayout.Height(1));
                            EditorGUILayout.Separator();

                            EditorGUILayout.BeginVertical();
                            if (sourceFile.assetId > 0)
                            {
                                EditorGUILayout.BeginHorizontal();
                                if (sourceFile.isBaseIdReset)
                                {
                                    sourceFile.oldAssetId = EditorGUILayout.IntField("ID: ", sourceFile.oldAssetId);
                                    GUILayout.FlexibleSpace();
                                    if (GUILayout.Button("✔", EditorStyles.miniButton))
                                    {
                                        if (sourceFile.oldAssetId != sourceFile.assetId)
                                        {
                                            if (EditorUtility.DisplayDialog("Information", "are you sure change baseId", "Ok", "Cancel"))
                                            {
                                                if (!m_bundleBuildManager.ResetBundleId(m_selectedCategory,m_selectedVariant, m_selectedFileName, sourceFile.oldAssetId, false))
                                                {
                                                    EditorUtility.DisplayDialog("Warning", "修改失败！1、id必须大于idBase\n2、id不能重复\n3、id必须是基于idBase", "Ok");
                                                    sourceFile.oldAssetId = sourceFile.assetId;
                                                }
                                                else
                                                {
                                                    sourceFile.assetId = sourceFile.oldAssetId;
                                                    sourceFile.isBaseIdReset = false;
                                                    Repaint();
                                                }
                                                GUI.FocusControl("");
                                            }
                                        }
                                        else
                                        {
                                            GUI.FocusControl("");
                                            sourceFile.isBaseIdReset = false;
                                        }

                                    }
                                    if (GUILayout.Button("✘", EditorStyles.miniButton))
                                    {
                                        GUI.FocusControl("");
                                        sourceFile.oldAssetId = sourceFile.assetId;
                                        sourceFile.isBaseIdReset = false;
                                    }
                                }
                                else
                                {
                                    GUILayout.Label("ID: " + sourceFile.assetId);
                                    GUILayout.FlexibleSpace();
                                    if (GUILayout.Button(" ☚ ", EditorStyles.miniButton))
                                    {
                                        sourceFile.isBaseIdReset = true;
                                    }
                                }
                                EditorGUILayout.EndHorizontal();
                            }
                            GUILayout.Label("GUID: " + sourceFile.guid );
                            if (sourceFile.bundleSize > 0)
                            {
                                GUILayout.Label("BundleSize: " + sourceFile.bundleSize + "kb");
                            }
                            EditorGUILayout.EndVertical();

                            GUILayout.Label("Main Asset", EditorStyles.boldLabel);
                            GUILayout.TextField("", GUILayout.Height(1));
                            EditorGUILayout.Separator();
                            var l = RenderFileHorizontal(sourceFile.mainAssetPath, sourceFile.mainAssetName);
                            m_paramMaxWidth = m_paramMaxWidth > l ? m_paramMaxWidth : l;
                            EditorGUILayout.Separator();
                            EditorGUILayout.Separator();
                            EditorGUILayout.LabelField("Dependency", EditorStyles.boldLabel);
                            EditorGUILayout.TextField("", GUILayout.Height(1));
                            EditorGUILayout.Separator();
                            if (sourceFile.dependenciesChain >= 0)
                            {
                                var chainId = sourceFile.dependenciesChain;
                                var chain = m_bundleBuildManager.dependChains[chainId];
                                for (int j = chain.Count - 1; j >= 0; j--)
                                {
                                    for (int k = 0; k < chain[j].Count; k++)
                                    {
                                        var id = chain[j][k];
                                        var baseSource = m_bundleBuildManager.allSourcesGuid[id];
                                        var m = RenderFileHorizontal(j == chain.Count - 1 ? 38 : 62, j, baseSource.bundleName, baseSource.variantName, baseSource.mainAssetPath, sourceFile.guid == id, baseSource.fileState, baseSource.dependenciedChainList.Count);
                                        m_paramMaxWidth = m_paramMaxWidth > m ? m_paramMaxWidth : m;
                                    }
                                }
                            }

                            EditorGUILayout.Separator();
                            EditorGUILayout.Separator();
                            EditorGUILayout.LabelField("Dependent Chain", EditorStyles.boldLabel);
                            EditorGUILayout.TextField("", GUILayout.Height(1));
                            EditorGUILayout.Separator();

                            if (sourceFile.fileState != ESourceState.MainBundle)
                            {
                                for (int i = 0; i < sourceFile.dependenciedChainList.Count; i++)
                                {
                                    var chainId = sourceFile.dependenciedChainList[i];
                                    var chain = m_bundleBuildManager.dependChains[chainId];
                                    for (int j = chain.Count - 1; j >= 0; j--)
                                    {
                                        for (int k = 0; k < chain[j].Count; k++)
                                        {
                                            var id = chain[j][k];
                                            var baseSource = m_bundleBuildManager.allSourcesGuid[id];
                                            var m = RenderFileHorizontal(j == chain.Count - 1 ? 38 : 62, j, baseSource.bundleName, baseSource.variantName, baseSource.mainAssetPath, sourceFile.guid == id, baseSource.fileState, baseSource.dependenciedChainList.Count);
                                            m_paramMaxWidth = m_paramMaxWidth > m ? m_paramMaxWidth : m;
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
                else
                {
                }
                //}
                EditorGUILayout.EndVertical();

            }
            EditorGUILayout.EndScrollView();
        }
        float RenderFileHorizontal(string assetPath, string assetName)
        {
            float width = assetName.Length * m_textLengthAccer;
            Rect re = EditorGUILayout.BeginHorizontal();
            //{
            EditorGUIUtility.SetIconSize(new Vector2(m_singleIntervalWidth+2, m_singleIntervalWidth + 2));
            var tex = AssetDatabase.GetCachedIcon(assetPath);
            if (tex == null)
            {
                Color color = GUI.color;
                GUI.color = Color.red;
                GUILayout.Label("");
                GUI.Label(new Rect(re.x + m_singleIntervalWidth*2, re.y, width, m_sourceHeight), assetName);
                GUI.color = color;
            }
            else
            {
                GUILayout.Label(tex);
                GUI.Label(new Rect(re.x + m_singleIntervalWidth*2, re.y, width, m_sourceHeight), assetName);
                if (Event.current.type == EventType.MouseUp)
                {
                    if (re.Contains(Event.current.mousePosition))
                    {
                        PingObject(assetPath);
                    }
                }
            }
            //}
            EditorGUILayout.EndHorizontal();
            return width;
        }
        float RenderFileHorizontal(int  c,int j,string assetName,string variantName,string assetPath,bool right,ESourceState state,int chainCount)
        {
            float height = m_sourceHeight;
            string name = assetName + "." + variantName;
            var width = name.Length * m_textLengthAccer;
            Rect re = EditorGUILayout.BeginHorizontal();
            //{
            Color color = GUI.color;
            if (right)
            {
                GUI.color = Color.green;
                GUI.Label(new Rect(re.x, re.y, m_singleIntervalWidth, height), "✔");
            }
            EditorGUIUtility.SetIconSize(new Vector2(m_singleIntervalWidth + 2, m_singleIntervalWidth + 2));
            var tex = AssetDatabase.GetCachedIcon(assetPath);
            if (tex != null)
            {
                GUI.color = Color.white;
                GUI.Label(new Rect(re.x +c- m_singleIntervalWidth, re.y, m_singleIntervalWidth, height), tex);
            }
            if (chainCount>m_bundleBuildManager.config.minBundleDependencyCount)
            {
                GUI.color = Color.yellow;
                GUI.Label(new Rect(re.x + c+ m_singleIntervalWidth*2, re.y, m_singleIntervalWidth, height), chainCount.ToString());
            }
            GUI.color = Color.white;
            GUI.Label(new Rect(re.x + c + m_singleIntervalWidth/2, re.y, m_singleIntervalWidth, height), j.ToString());

            if (state==ESourceState.MainBundle)
            {
                GUI.color = m_bundleBuildManager.config.mainBundleColor;
            }
            else if (state==ESourceState.SharedBundle)
            {
                GUI.color = m_bundleBuildManager.config.sharedBundleColor;
            }
            else if (state==ESourceState.PrivateBundle)
            {
                GUI.color = m_bundleBuildManager.config.privateBundleColor;
            }
            else if (state == ESourceState.Damage)
            {
                GUI.color = m_bundleBuildManager.config.damageBundleColor;
            }
            else
            {
                GUI.color = Color.white;
            }
            GUILayout.Label("");
            GUI.Label(new Rect(re.x +m_singleIntervalWidth*3+c, re.y, width, height), name);
            GUI.color = Color.white;
            //GUI.Label(new Rect(re.x + 24 + c, re.y*j+ 15, width, 1), "", EditorStyles.textField);
            GUI.color = color;
            if (Event.current.type == EventType.MouseUp)
            {
                if (re.Contains(Event.current.mousePosition))
                {
                    PingObject(assetPath);
                }
            }
            //}
            EditorGUILayout.EndHorizontal();
            return width;
        }
        void RenderTitle(int y)
        {
            float windowWidth = position.width;
            float windowHeight = position.height;
            GUI.Box(new Rect(0, y * m_sourceHeight, 3000, m_sourceHeight), "");
            GUI.Label(new Rect(8, y * m_sourceHeight, m_table_idMaxWidth, m_sourceHeight), "ID", EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth, y * m_sourceHeight, m_table_categoryMaxWidth, m_sourceHeight), "Category", EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth , y * m_sourceHeight, m_table_variantMaxWidth, m_sourceHeight), "Variant", EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth+ m_table_variantMaxWidth, y * m_sourceHeight, m_table_fileNameMaxWidth, m_sourceHeight), "BundleName", EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth + m_table_variantMaxWidth + m_table_fileNameMaxWidth, y * m_sourceHeight, m_table_dependenciedMaxWidth, m_sourceHeight), "DependenciedCount", EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth + m_table_variantMaxWidth + m_table_fileNameMaxWidth + m_table_dependenciedMaxWidth, y * m_sourceHeight, m_table_bundlePathMaxWidth, m_sourceHeight), "BundlePath", EditorStyles.boldLabel);
        }
        void RenderRow(int y, int id, string category,string variant, string fileName, int dependenciedCount, string bundlePath, bool selected, ESourceState fileState)
        {
            float windowWidth = position.width;
            float windowHeight = position.height;
            m_table_categoryMaxWidth = m_table_categoryMaxWidth > category.Length * m_textLengthAccer ? m_table_categoryMaxWidth : category.Length * m_textLengthAccer;
            m_table_variantMaxWidth = m_table_variantMaxWidth > variant.Length * m_textLengthAccer ? m_table_variantMaxWidth : variant.Length * m_textLengthAccer;
            m_table_fileNameMaxWidth = m_table_fileNameMaxWidth > fileName.Length * m_textLengthAccer ? m_table_fileNameMaxWidth : fileName.Length * m_textLengthAccer;
            m_table_bundlePathMaxWidth = m_table_bundlePathMaxWidth > bundlePath.Length * m_textLengthAccer ? m_table_bundlePathMaxWidth : bundlePath.Length * m_textLengthAccer; GUI.Label(new Rect(0, y * m_sourceHeight, 3000, 0.5f), "", EditorStyles.textField);
            Color color = GUI.color;
            if (selected)
            {
                GUI.color = Color.yellow;
                GUI.Box(new Rect(0, y * m_sourceHeight , 3000, m_sourceHeight), "");
            }
            else
            {
                if (fileState == ESourceState.MainBundle)
                {
                    GUI.color = m_bundleBuildManager.config.mainBundleColor;
                }
                else if (fileState == ESourceState.PrivateBundle)
                {
                    GUI.color = m_bundleBuildManager.config.privateBundleColor;
                }
                else if (fileState == ESourceState.SharedBundle)
                {
                    GUI.color = m_bundleBuildManager.config.sharedBundleColor;
                }
                else if (fileState == ESourceState.Damage)
                {
                    GUI.color = m_bundleBuildManager.config.damageBundleColor;
                }
            }
            GUI.Label(new Rect(8, y * m_sourceHeight, m_table_idMaxWidth, m_sourceHeight), id.ToString(), EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth, y * m_sourceHeight, m_table_categoryMaxWidth, m_sourceHeight), category, EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth, y * m_sourceHeight, m_table_variantMaxWidth, m_sourceHeight), variant, EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth+ m_table_variantMaxWidth, y * m_sourceHeight, m_table_fileNameMaxWidth, m_sourceHeight), fileName, EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth+ m_table_variantMaxWidth + m_table_fileNameMaxWidth, y * m_sourceHeight, m_table_dependenciedMaxWidth, m_sourceHeight), dependenciedCount.ToString(), EditorStyles.boldLabel);
            GUI.Label(new Rect(m_table_idMaxWidth + m_table_categoryMaxWidth+ m_table_variantMaxWidth + m_table_fileNameMaxWidth + m_table_dependenciedMaxWidth, y * m_sourceHeight, m_table_bundlePathMaxWidth, m_sourceHeight), bundlePath, EditorStyles.boldLabel);
            GUI.color = color;
        }
    }

}