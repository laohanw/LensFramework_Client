﻿using System;
using System.Collections.Generic;

namespace SrcBuilder.Bundle
{
    public class VariantData
    {
        public string variantName;
        public bool isOpen;
        public Dictionary<string, SourceSingle> filesDic = new Dictionary<string, SourceSingle>();
        public Dictionary<string, SourceEntire> entiresDic = new Dictionary<string, SourceEntire>();
        public Dictionary<string, SourceFolder> foldersDic = new Dictionary<string, SourceFolder>();
    }
}
