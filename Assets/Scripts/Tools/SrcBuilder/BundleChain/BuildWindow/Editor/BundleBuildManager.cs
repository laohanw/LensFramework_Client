﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Text;
using System;
using ProtoBuf;
using System.Security.Cryptography;

namespace SrcBuilder.Bundle
{
    public class BundleBuildManager
    {
        public int buildTarget
        {
            get
            {
                if (m_buildTarget== BuildTarget.NoTarget)
                {
                    return config.buildTarget;
                }
                else
                {
                    return (int)m_buildTarget;
                }
            }
            set
            {
                m_buildTarget = (BuildTarget)value;
            }
        }
        public bool showTable
        {
            get
            {
                if (m_showTable == null)
                {
                    if (EditorPrefs.HasKey("BundleBuildManager_showTable"))
                    {
                        m_showTable = EditorPrefs.GetBool("BundleBuildManager_showTable");
                    }
                    else
                    {
                        m_showTable = false;
                    }
                }
                return (bool)m_showTable;
            }
            set
            {
                EditorPrefs.SetBool("BundleBuildManager_showTable", value);
                m_showTable = value;
            }
        }
        public BuildConfig config;
        
        public Dictionary<string, CategoryData> categoryData { get; private set; }
        public Dictionary<string, List<SourceEntire>> searchedEntires { get; private set; }
        public Dictionary<string, List<SourceSingle>> searchedFiles { get; private set; }
        //private System.Security.Cryptography.SHA1Managed m_sha1;

        public List<List<List<int>>> dependChains = new List<List<List<int>>>();
        public Dictionary<int, BaseSource> allSourcesGuid = new Dictionary<int, BaseSource>();
        private Dictionary<string, BaseSource> m_allSourcesPath = new Dictionary<string, BaseSource>();
        private BundlePack m_bundlePack = new BundlePack();
        private BuildAssetBundleOptions m_bundleOptions = BuildAssetBundleOptions.None;
        private BuildTarget m_buildTarget = BuildTarget.NoTarget;

        private bool? m_showTable;
        private string m_rootPath;
        private string m_bundleRootPath;
        private string m_manifestPath;
        private int m_variantMaxLength = 10;
        private Dictionary<int, BaseSource> m_infoTemp=new Dictionary<int, BaseSource>();
        private UnityEngine.AssetBundleManifest m_manifest;
        public BundleBuildManager()
        {
            var files = Directory.GetFiles(Application.dataPath, "BundleSettings.asset", SearchOption.AllDirectories);
            if (files.Length <= 0)
            {
                UnityEngine.Debug.LogError("BundleSettings.asset is not exsts");
            }
            else
            {
                string file = files[0];
                file = file.Replace("\\", "/");
                string configPath = "Assets" + file.Replace(Application.dataPath, "");
                config = (BuildConfig)AssetDatabase.LoadAssetAtPath(configPath, typeof(BuildConfig));
            }
            if (buildTarget == (int)BuildTarget.NoTarget)
            {
                buildTarget = (int)EditorUserBuildSettings.activeBuildTarget;
            }
            m_rootPath = Application.dataPath + "/" + config.rootName + "/";
            m_bundleRootPath = Path.Combine(Path.Combine(config.bundleOutputFolder, buildTarget.ToString()), config.bundleFolder);// Application.streamingAssetsPath + "/" + buildTarget.ToString() + "/" + config.bundleFolder ;
            m_manifestPath = m_bundleRootPath;// +"/" + config.bundleFolder;
            categoryData = new Dictionary<string, CategoryData>();
            searchedEntires = new Dictionary<string, List<SourceEntire>>();
            searchedFiles = new Dictionary<string, List<SourceSingle>>();
            if (File.Exists(m_manifestPath))
            {
                AssetBundle.UnloadAllAssetBundles(true);
                var manifestBundle = AssetBundle.LoadFromFile(m_manifestPath);
                m_manifest = (AssetBundleManifest)manifestBundle.LoadAsset("AssetBundleManifest");
                manifestBundle.Unload(false);
                var mainAssetBundles = m_manifest.GetAllAssetBundles();
                for (int i = 0; i < mainAssetBundles.Length; i++)
                {
                    AnalizeBundle(mainAssetBundles[i]);
                }
            }
            AnalizeFiles();
            AnalizeEditorTable();
            AnalizeSourceState();
            AnalizeDependency();
            AnalizeChain();
        }
        public void BuildPackage()
        {
            m_bundlePack.StartBuild();
            var oldPackageDic = ReadPackage(m_bundleRootPath );
            PackBundle(m_rootPath);
            PackManifestBinary();
            ClearDeleted(m_bundleRootPath);
            var newPackageDic = ReadPackage(m_bundleRootPath );
            PackEditorCacheTable();
            SavePackageConfig(oldPackageDic, newPackageDic);
            m_bundlePack.BuildOver();
            AssetDatabase.Refresh();
        }
        public void BuildTable()
        {
            //PackSourceTable();
            PackManifestBinary();
        }
        public void Clear()
        {
            string[] str=AssetDatabase.GetAllAssetBundleNames();
            for (int i = 0; i < str.Length; i++)
            {
                AssetDatabase.RemoveAssetBundleName(str[i],true);
            }
        }
        public void DeletePackge()
        {
            string outPath = Path.Combine(Path.Combine(config.bundleOutputFolder, buildTarget.ToString()), config.bundleFolder);
            outPath = outPath.Replace("\\", "/");
            AssetDatabase.DeleteAsset(outPath);
        }
        public bool ResetBaseId(string category, int newId)
        {
            if (newId / config.idBase < 0) return false;
            if (newId % config.idBase > 0) return false;
            foreach (var item in categoryData)
            {
                if (item.Value.baseId == newId)
                {
                    return false;
                }
            }
            var ites = categoryData[category];
            foreach (var ite in ites.variantDic)
            {
                foreach (var it in ite.Value.entiresDic)
                {
                    it.Value.assetId = it.Value.assetId % ites.baseId + newId;
                    it.Value.oldAssetId = it.Value.assetId;
                    it.Value.guid = it.Value.assetId * 10 + GetVariantInt(it.Value.variantName);
                }
                foreach (var it in ite.Value.filesDic)
                {
                    it.Value.assetId = it.Value.assetId % ites.baseId + newId;
                    it.Value.oldAssetId = it.Value.assetId;
                    it.Value.guid = it.Value.assetId * 10 + GetVariantInt(it.Value.variantName);
                }
                foreach (var fold in ite.Value.foldersDic)
                {
                    foreach (var it in fold.Value.entiresDic)
                    {
                        it.Value.assetId = it.Value.assetId % ites.baseId + newId;
                        it.Value.oldAssetId = it.Value.assetId;
                        it.Value.guid = it.Value.assetId * 10 + GetVariantInt(it.Value.variantName);
                    }
                    foreach (var it in fold.Value.filesDic)
                    {
                        it.Value.assetId = it.Value.assetId % ites.baseId + newId;
                        it.Value.oldAssetId = it.Value.assetId;
                        it.Value.guid = it.Value.assetId * 10 + GetVariantInt(it.Value.variantName);
                    }
                }
            }
            ites.baseId = newId;
            ites.oldId = newId;
            return true;
        }
        public bool ResetBundleId(string category,string variantName, string fileName, int newId, bool isEntires)
        {
            if (newId % config.idBase <= 0) return false;
            foreach (var item in categoryData)
            {
                foreach (var variant in item.Value.variantDic)
                {
                    foreach (var it in variant.Value.entiresDic)
                    {
                        if (it.Value.assetId == newId)
                        {
                            return false;
                        }
                    }
                    foreach (var it in variant.Value.filesDic)
                    {
                        if (it.Value.assetId == newId)
                        {
                            return false;
                        }
                    }
                    foreach (var fold in variant.Value.foldersDic)
                    {
                        foreach (var it in fold.Value.entiresDic)
                        {
                            if (it.Value.assetId == newId)
                            {
                                return false;
                            }
                        }
                        foreach (var it in fold.Value.filesDic)
                        {
                            if (it.Value.assetId == newId)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            if (isEntires)
            {
                foreach (var item in categoryData[category].variantDic)
                {
                    var sou = item.Value.entiresDic[fileName];
                    sou.assetId = newId;
                    sou.oldAssetId = newId;
                    sou.guid = sou.assetId * 10 + GetVariantInt(sou.variantName);
                }
                foreach (var ut in categoryData[category].variantDic)
                {
                    var s = ut.Value.foldersDic;
                    foreach (var item in s)
                    {
                        var sou = item.Value.entiresDic[fileName];
                        sou.assetId = newId;
                        sou.oldAssetId = newId;
                        sou.guid = sou.assetId * 10 + GetVariantInt(sou.variantName);
                    }
                }
                return true;
            }
            else
            {
                foreach (var item in categoryData[category].variantDic)
                {
                    var sou = item.Value.filesDic[fileName];
                    sou.assetId = newId;
                    sou.oldAssetId = newId;
                    sou.guid = sou.assetId * 10 + GetVariantInt(sou.variantName);
                }
                foreach (var it in categoryData[category].variantDic)
                {
                    var s = it.Value.foldersDic;
                    foreach (var item in s)
                    {
                        var sou = item.Value.filesDic[fileName];
                        sou.assetId = newId;
                        sou.oldAssetId = newId;
                        sou.guid = sou.assetId * 10 + GetVariantInt(sou.variantName);
                    }
                }
                return true;
            }
        }
        public void Search(string value, ESearchType searchType)
        {
            searchedEntires.Clear();
            searchedFiles.Clear();
            if (searchType == ESearchType.Id)
            {
                try
                {
                    int id = Convert.ToInt32(value);
                    foreach (var category in categoryData)
                    {
                        foreach (var variant in category.Value.variantDic)
                        {
                            foreach (var dirs in variant.Value.entiresDic)
                            {
                                if (dirs.Value.assetId == id)
                                {
                                    if (!searchedEntires.ContainsKey(category.Key))
                                    {
                                        searchedEntires.Add(category.Key, new List<SourceEntire>());
                                    }
                                    searchedEntires[category.Key].Add(dirs.Value);
                                }
                            }
                            foreach (var dirs in variant.Value.filesDic)
                            {
                                if (dirs.Value.assetId == id)
                                {
                                    if (!searchedFiles.ContainsKey(category.Key))
                                    {
                                        searchedFiles.Add(category.Key, new List<SourceSingle>());
                                    }
                                    searchedFiles[category.Key].Add(dirs.Value);
                                }
                            }
                            foreach (var d in variant.Value.foldersDic)
                            {
                                foreach (var dirs in d.Value.entiresDic)
                                {
                                    if (dirs.Value.assetId == id)
                                    {
                                        if (!searchedEntires.ContainsKey(category.Key))
                                        {
                                            searchedEntires.Add(category.Key, new List<SourceEntire>());
                                        }
                                        searchedEntires[category.Key].Add(dirs.Value);
                                    }
                                }
                                foreach (var dirs in d.Value.filesDic)
                                {
                                    if (dirs.Value.assetId == id)
                                    {
                                        if (!searchedFiles.ContainsKey(category.Key))
                                        {
                                            searchedFiles.Add(category.Key, new List<SourceSingle>());
                                        }
                                        searchedFiles[category.Key].Add(dirs.Value);
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }
            else if (searchType == ESearchType.Category)
            {
                try
                {
                    foreach (var category in categoryData)
                    {
                        if (category.Key == value.ToLower())
                        {
                            foreach (var variant in category.Value.variantDic)
                            {
                                foreach (var dirs in variant.Value.entiresDic)
                                {
                                    if (!searchedEntires.ContainsKey(category.Key))
                                    {
                                        searchedEntires.Add(category.Key, new List<SourceEntire>());
                                    }
                                    searchedEntires[category.Key].Add(dirs.Value);
                                }
                                foreach (var dirs in variant.Value.filesDic)
                                {
                                    if (!searchedFiles.ContainsKey(category.Key))
                                    {
                                        searchedFiles.Add(category.Key, new List<SourceSingle>());
                                    }
                                    searchedFiles[category.Key].Add(dirs.Value);
                                }
                                foreach (var d in variant.Value.foldersDic)
                                {
                                    foreach (var dirs in d.Value.entiresDic)
                                    {
                                        if (!searchedEntires.ContainsKey(category.Key))
                                        {
                                            searchedEntires.Add(category.Key, new List<SourceEntire>());
                                        }
                                        searchedEntires[category.Key].Add(dirs.Value);
                                    }
                                    foreach (var dirs in d.Value.filesDic)
                                    {
                                        if (!searchedFiles.ContainsKey(category.Key))
                                        {
                                            searchedFiles.Add(category.Key, new List<SourceSingle>());
                                        }
                                        searchedFiles[category.Key].Add(dirs.Value);
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }
            else if (searchType == ESearchType.BundleName)
            {
                try
                {
                    foreach (var category in categoryData)
                    {
                        foreach (var variant in category.Value.variantDic)
                        {
                            foreach (var dirs in variant.Value.entiresDic)
                            {
                                if (dirs.Value.fileName.ToLower().Contains(value.ToLower()))
                                {
                                    if (!searchedEntires.ContainsKey(category.Key))
                                    {
                                        searchedEntires.Add(category.Key, new List<SourceEntire>());
                                    }
                                    searchedEntires[category.Key].Add(dirs.Value);
                                }
                            }
                            foreach (var dirs in variant.Value.filesDic)
                            {
                                if (dirs.Value.fileName.ToLower().Contains(value.ToLower()))
                                {
                                    if (!searchedFiles.ContainsKey(category.Key))
                                    {
                                        searchedFiles.Add(category.Key, new List<SourceSingle>());
                                    }
                                    searchedFiles[category.Key].Add(dirs.Value);
                                }
                            }
                            foreach (var d in variant.Value.foldersDic)
                            {
                                foreach (var dirs in d.Value.entiresDic)
                                {
                                    if (dirs.Value.fileName.ToLower().Contains(value.ToLower()))
                                    {
                                        if (!searchedEntires.ContainsKey(category.Key))
                                        {
                                            searchedEntires.Add(category.Key, new List<SourceEntire>());
                                        }
                                        searchedEntires[category.Key].Add(dirs.Value);
                                    }
                                }
                                foreach (var dirs in d.Value.filesDic)
                                {
                                    if (dirs.Value.fileName.ToLower().Contains(value.ToLower()))
                                    {
                                        if (!searchedFiles.ContainsKey(category.Key))
                                        {
                                            searchedFiles.Add(category.Key, new List<SourceSingle>());
                                        }
                                        searchedFiles[category.Key].Add(dirs.Value);
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }
        public void ResetSearch()
        {
            searchedEntires.Clear();
            searchedFiles.Clear();
        }
        private void PackBundle(string folderPaths)
        {
            string outPath = Path.Combine(Path.Combine(config.bundleOutputFolder, buildTarget.ToString()), config.bundleFolder);
            outPath = outPath.Replace("\\", "/");
            foreach (var category in categoryData)
            {
                foreach (var variant in category.Value.variantDic)
                {
                    foreach (var it in variant.Value.entiresDic)
                    {
                        if (it.Value.dontDamage)
                        {
                            m_bundlePack.PackEntire(it.Value);
                        }
                        else
                        {
                            string str = outPath + "/" + it.Value.bundleName + "." + variant.Key;
                            str = str.Replace("\\", "/");
                            AssetDatabase.DeleteAsset(str);
                            AssetDatabase.DeleteAsset(str + ".manifest");
                        }
                    }
                    foreach (var it in variant.Value.filesDic)
                    {
                        if (it.Value.dontDamage)
                        {
                            m_bundlePack.PackSingle(category.Key, it.Value);
                        }
                        else
                        {
                            string str = outPath + "/" + it.Value.bundleName+"."+ variant.Key;
                            str = str.Replace("\\", "/");
                            AssetDatabase.DeleteAsset(str);
                            AssetDatabase.DeleteAsset(str + ".manifest");
                        }
                    }
                    foreach (var d in variant.Value.foldersDic)
                    {
                        foreach (var it in d.Value.entiresDic)
                        {
                            if (it.Value.dontDamage)
                            {
                                m_bundlePack.PackEntire(it.Value);
                            }
                            else
                            {
                                string str = outPath + "/" + it.Value.bundleName + "." + variant.Key;
                                str = str.Replace("\\", "/");
                                AssetDatabase.DeleteAsset(str);
                                AssetDatabase.DeleteAsset(str + ".manifest");
                            }
                        }
                        foreach (var it in d.Value.filesDic)
                        {
                            if (it.Value.dontDamage)
                            {
                                m_bundlePack.PackSingle(category.Key, it.Value);
                            }
                            else
                            {
                                string str = outPath + "/" + it.Value.bundleName + "." + variant.Key;
                                str = str.Replace("\\", "/");
                                AssetDatabase.DeleteAsset(str);
                                AssetDatabase.DeleteAsset(str + ".manifest");
                            }
                        }
                    }
                }
            }
            if (!Directory.Exists(outPath))
            {
                Directory.CreateDirectory(outPath);
            }
            BuildPipeline.BuildAssetBundles(outPath, (BuildAssetBundleOptions)config.bundleOptions, (BuildTarget)buildTarget);
        }
        private void PackManifestBinary()
        {
            string tablePath = m_bundleRootPath + "/" + config.manifestFileName;
            SourceTable sourceTable = new SourceTable();
            Dictionary<int, string> pathDic = new Dictionary<int, string>();
            Dictionary<int, List<int>> variantDic = new Dictionary<int, List<int>>();
            foreach (var item in categoryData)
            {
                foreach (var its in item.Value.variantDic)
                {
                    foreach (var it in its.Value.entiresDic)
                    {
                        if (it.Value.filesDic.Count > 0)
                        {
                            if (it.Value.fileState == ESourceState.MainBundle || it.Value.fileState == ESourceState.SharedBundle)
                            {
                                if (!pathDic.ContainsKey(it.Value.assetId))
                                {
                                    pathDic.Add(it.Value.assetId, it.Value.bundleName);
                                    variantDic.Add(it.Value.assetId, new List<int>() { GetVariantInt(it.Value.variantName) });
                                }
                                else
                                {
                                    int variantId = GetVariantInt(it.Value.variantName);
                                    if (!variantDic[it.Value.assetId].Contains(variantId))
                                        variantDic[it.Value.assetId].Add(variantId);
                                }
                            }
                        }
                    }
                    foreach (var it in its.Value.filesDic)
                    {
                        if (it.Value.fileState == ESourceState.MainBundle || it.Value.fileState == ESourceState.SharedBundle)
                        {
                            if (!pathDic.ContainsKey(it.Value.assetId))
                            {
                                pathDic.Add(it.Value.assetId, it.Value.bundleName);
                                variantDic.Add(it.Value.assetId, new List<int>() { GetVariantInt(it.Value.variantName) });
                            }
                            else
                            {
                                int variantId = GetVariantInt(it.Value.variantName);
                                if (!variantDic[it.Value.assetId].Contains(variantId))
                                    variantDic[it.Value.assetId].Add(variantId);
                            }
                        }
                    }
                    foreach (var d in its.Value.foldersDic)
                    {
                        foreach (var it in d.Value.entiresDic)
                        {
                            if (it.Value.filesDic.Count > 0)
                            {
                                if (it.Value.fileState == ESourceState.MainBundle || it.Value.fileState == ESourceState.SharedBundle)
                                {
                                    if (!pathDic.ContainsKey(it.Value.assetId))
                                    {
                                        pathDic.Add(it.Value.assetId, it.Value.bundleName);
                                        variantDic.Add(it.Value.assetId, new List<int>() { GetVariantInt(it.Value.variantName) });
                                    }
                                    else
                                    {
                                        int variantId = GetVariantInt(it.Value.variantName);
                                        if (!variantDic[it.Value.assetId].Contains(variantId))
                                            variantDic[it.Value.assetId].Add(variantId);
                                    }
                                }
                            }
                        }
                        foreach (var it in d.Value.filesDic)
                        {
                            if (it.Value.fileState == ESourceState.MainBundle || it.Value.fileState == ESourceState.SharedBundle)
                            {
                                if (!pathDic.ContainsKey(it.Value.assetId))
                                {
                                    pathDic.Add(it.Value.assetId, it.Value.bundleName);
                                    variantDic.Add(it.Value.assetId, new List<int>() { GetVariantInt(it.Value.variantName) });
                                }
                                else
                                {
                                    int variantId = GetVariantInt(it.Value.variantName);
                                    if (!variantDic[it.Value.assetId].Contains(variantId))
                                        variantDic[it.Value.assetId].Add(variantId);
                                }
                            }
                        }
                    }
                }
            }
            foreach (var item in pathDic)
            {
                var varientList = variantDic[item.Key];
                for (int i = 0; i < varientList.Count; i++)
                {
                    sourceTable.guidPaths.Add(new GuidPath()
                    {
                        guid = item.Key * m_variantMaxLength + varientList[i],
                        path = item.Value + "." + ((EVariantType)varientList[i]).ToString()
                    });
                }
            }
            for (int m = 0; m < dependChains.Count; m++)
            {
                var chains = dependChains[m];
                DependenciesChain depChain = new DependenciesChain();
                depChain.chainId = m;
                for (int i = 0; i < chains.Count; i++)
                {
                    var chain = chains[i];
                    if (chain.Count > 0)
                    {
                        Depends deps = new Depends();
                        for (int j = 0; j < chain.Count; j++)
                        {
                            int chainId = chain[j];
                            var source = allSourcesGuid[chainId];
                            if (source.fileState == ESourceState.MainBundle || source.fileState == ESourceState.SharedBundle)
                            {
                                deps.deps.Add(chainId);
                            }
                        }
                        if (deps.deps.Count > 0)
                        {
                            depChain.depends.Add(deps);
                        }
                    }
                }
                if (depChain.depends.Count > 0)
                {
                    sourceTable.dependenciesChains.Add(depChain);
                }
            }
            foreach (var item in allSourcesGuid)
            {
                if (item.Value.fileState == ESourceState.MainBundle || item.Value.fileState == ESourceState.SharedBundle)
                {
                    sourceTable.variantChains.Add(new VariantChain()
                    {
                        guid = item.Key,
                        chainId = item.Value.dependenciesChain
                    });
                }
            }
            FileInfo tableFileInfo=new FileInfo(tablePath);
            if(!tableFileInfo.Directory.Exists){
                tableFileInfo.Directory.Create();
            }
            var bytes = Serialize<SourceTable>(sourceTable);
            using (FileStream fileStream = new FileStream(tablePath, FileMode.Create))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
            using (FileStream fileStream = new FileStream(m_rootPath+config.manifestFileName, FileMode.Create))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
            
        }
        private void ClearDeleted(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                var files = Directory.GetFiles(folderPath, "*", SearchOption.AllDirectories);
                for (int i = 0; i < files.Length; i++)
                {
                    var f = files[i];
                    f = f.Replace("\\", "/");
                    FileInfo info = new FileInfo(f);
                    if (info.Name == config.packageFileName) continue;
                    if (info.Name == config.bundleFolder) continue;
                    if (info.Extension == ".manifest") continue;
                    if (config.manifestFileName.Contains(info.Name)) continue;
                    var root = m_bundleRootPath.Replace("\\", "/");
                    string p = f.Replace(root + "/", "");
                    bool has = false;
                    foreach (var item in m_allSourcesPath)
                    {
                        if (item.Value.bundleName+"."+item.Value.variantName==p)
                        {
                            has = true;
                        }
                    }
                    if (!has)
                    {
                        File.Delete(f);
                        File.Delete(f+".manifest");
                    }
                }
            }
        }
        private void PackEditorCacheTable()
        {
            string tablePath = m_bundleRootPath + "/" + config.editorCacheName;
            if (File.Exists(tablePath))
            {
                File.Delete(tablePath);
            }
            using (FileStream fileStream = new FileStream(tablePath, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fileStream))
                {
                    foreach (var item in categoryData)
                    {
                        Dictionary<string, BaseSource> sources = new Dictionary<string, BaseSource>();
                        foreach (var its in item.Value.variantDic)
                        {
                            foreach (var it in its.Value.entiresDic)
                            {
                                if (!sources.ContainsKey(it.Key))
                                {
                                    sources.Add(it.Key,it.Value);
                                }
                                sw.WriteLine(item.Key + "," + item.Value.baseId + "," + its.Key + "," + it.Value.assetId +","+it.Value.parentFolder+ ",1");
                            }
                            foreach (var it in its.Value.filesDic)
                            {
                                if (!sources.ContainsKey(it.Key))
                                {
                                    sources.Add(it.Key, it.Value);
                                }
                                sw.WriteLine(item.Key + "," + item.Value.baseId + "," + it.Key + "," + it.Value.assetId + "," + it.Value.parentFolder + ",0");
                            }
                            foreach (var d in its.Value.foldersDic)
                            {
                                foreach (var it in d.Value.entiresDic)
                                {
                                    if (!sources.ContainsKey(it.Key))
                                    {
                                        sources.Add(it.Key, it.Value);
                                    }
                                    sw.WriteLine(item.Key + "," + item.Value.baseId + "," + its.Key + "," + it.Value.assetId + "," + it.Value.parentFolder + ",1");
                                }
                                foreach (var it in d.Value.filesDic)
                                {
                                    if (!sources.ContainsKey(it.Key))
                                    {
                                        sources.Add(it.Key, it.Value);
                                    }
                                    sw.WriteLine(item.Key + "," + item.Value.baseId + "," + it.Key + "," + it.Value.assetId + "," + it.Value.parentFolder + ",0");
                                }
                            }
                        }
                        //foreach (var its in sources)
                        //{
                        //    sw.WriteLine(item.Key + "," + item.Value.baseId + "," + its.Key + "," + its.Value.assetId + ",1");
                        //}
                    }
                    sw.Flush();
                }
            }
        }
        private void AnalizeBundle(string bundlePath)
        {
            string path = m_bundleRootPath + "/" + bundlePath;
            var b = AssetBundle.LoadFromFile(path);
            if (b != null)
            {
                var fileSize = new FileInfo(path).Length ;
                var assets = b.GetAllAssetNames();
                FileInfo info = new FileInfo(path);
                string parentFolder = "";
                DirectoryInfo parent = info.Directory; 
                if (!string.IsNullOrEmpty(info.Directory.Parent.FullName.Replace("\\","/").Replace(m_bundleRootPath,"")))
                {
                    parent = info.Directory.Parent;
                    parentFolder = info.Directory.Parent.Name;
                }
                string category = parent.Name;
                string categoryLower = category.ToLower();
                string dirName = info.Name.Substring(0, info.Name.LastIndexOf('.'));
                string variantName = info.Name.LastIndexOf('.') < 0 ? null : info.Name.Substring(info.Name.LastIndexOf('.') + 1);
                variantName = string.IsNullOrEmpty(variantName) ? EVariantType.n.ToString() : variantName;
                if (!categoryData.ContainsKey(categoryLower))
                {
                    categoryData.Add(categoryLower, new CategoryData());
                }
                if (!categoryData[categoryLower].variantDic.ContainsKey(variantName))
                {
                    categoryData[categoryLower].variantDic.Add(variantName, new VariantData());
                }
                categoryData[categoryLower].category = category;
                var variant = categoryData[categoryLower].variantDic[variantName];
                variant.variantName = variantName;
                if (assets.Length > 1)
                {
                    string dirLowerName = dirName.ToLower();
                    if (string.IsNullOrEmpty(parentFolder))
                    {
                        var sourDir = new SourceEntire();
                        sourDir.fileName = dirName;
                        sourDir.bundleName = categoryLower + "/" + dirLowerName;
                        sourDir.variantName = variantName;
                        sourDir.parentFolder = parentFolder;
                        sourDir.category = category;
                        sourDir.bundleSize = fileSize;
                        sourDir.dontDamage = true;
                        try
                        {
                            sourDir.hash = m_manifest.GetAssetBundleHash(categoryLower + "/" + dirLowerName + "." + variantName);
                        }
                        catch { }

                        variant.entiresDic.Add(dirLowerName, sourDir);
                        for (int j = 0; j < assets.Length; j++)
                        {
                            var filePath = "";
                            var split = assets[j].Split('/');
                            filePath = split[0].Substring(0, 1).ToUpper() + split[0].Substring(1);
                            for (int x = 1; x < split.Length; x++)
                            {
                                filePath += "/" + split[x].Substring(0, 1).ToUpper() + split[x].Substring(1);
                            }
                            FileInfo fileInfo = new FileInfo(filePath);
                            string fileN = fileInfo.Name.Replace(fileInfo.Extension, "");
                            string fileLowerName = fileN.ToLower();
                            sourDir.filesDic.Add(fileLowerName, new SourceSingle()
                            {
                                fileName = fileN,
                                bundleName = categoryLower.ToLower() + '/' + dirName,
                                variantName = variantName,
                                parentFolder = parentFolder,
                                category = category,
                                dontDamage = true
                            });
                        }
                    }
                    else
                    {
                        SourceFolder sourceFolder = null;
                        if (!variant.foldersDic.TryGetValue(info.Directory.Name, out sourceFolder))
                        {
                            sourceFolder = new SourceFolder();
                            sourceFolder.category = category;
                            sourceFolder.variantName = variantName;
                            sourceFolder.fileName = info.Directory.Name;
                            variant.foldersDic.Add(info.Directory.Name, sourceFolder);
                        }
                        SourceEntire sourceEntire = null;
                        if (!sourceFolder.entiresDic.TryGetValue(dirLowerName,out sourceEntire))
                        {
                            sourceEntire = new SourceEntire();
                            sourceEntire.fileName = dirLowerName;
                            sourceEntire.bundleName = categoryLower + "/"+ info.Directory.Name+"/" + dirLowerName;
                            sourceEntire.variantName = variantName;
                            sourceEntire.parentFolder = info.Directory.Name;
                            sourceEntire.category = category;
                            sourceEntire.bundleSize = fileSize;
                            sourceEntire.dontDamage = true;
                        }
                        for (int j = 0; j < assets.Length; j++)
                        {
                            var split = assets[j].Split('/');
                            var filePath = split[0].Substring(0, 1)+ split[0].Substring(1);
                            for (int x = 1; x < split.Length; x++)
                            {
                                filePath += "/" + split[x].Substring(0, 1) + split[x].Substring(1);
                            }
                            FileInfo fileInfo = new FileInfo(filePath);
                            string fileN = fileInfo.Name.Replace(fileInfo.Extension, "");
                            string fileLowerName = fileN.ToLower();
                            sourceEntire.filesDic.Add(fileLowerName, new SourceSingle()
                            {
                                fileName = fileN,
                                bundleName = categoryLower.ToLower() + "/" + info.Directory.Name + '/' + dirLowerName,
                                variantName = variantName,
                                parentFolder = info.Directory.Name,
                                category = category,
                                dontDamage = true
                            });
                        }
                    }
                }
                else
                {
                    string fileName = info.Name.Substring(0, info.Name.LastIndexOf('.')).ToLower();
                    if (string.IsNullOrEmpty(parentFolder))
                    {
                        if (!categoryData[categoryLower].variantDic[variantName].filesDic.ContainsKey(fileName))
                        {
                            categoryData[categoryLower].variantDic[variantName].filesDic.Add(fileName, new SourceSingle());
                        }
                        var sourceFile = variant.filesDic[fileName];
                        sourceFile.fileName = info.Name;
                        sourceFile.bundleSize = fileSize;
                        sourceFile.bundleName = categoryLower + "/" + fileName;
                        sourceFile.variantName = variantName;
                        sourceFile.category = category;
                        sourceFile.parentFolder = parentFolder;
                        try
                        {
                            sourceFile.hash = m_manifest.GetAssetBundleHash(categoryLower + "/" + fileName + "." + variantName);
                        }
                        catch { }
                    }
                    else
                    {

                        SourceFolder sourceFolder = null;
                        if (!variant.foldersDic.TryGetValue(info.Directory.Name, out sourceFolder))
                        {
                            sourceFolder = new SourceFolder();
                            sourceFolder.category = category;
                            sourceFolder.variantName = variantName;
                            sourceFolder.fileName = info.Directory.Name;
                            variant.foldersDic.Add(info.Directory.Name, sourceFolder);
                        }
                        SourceSingle sourceEntire = null;
                        if (!sourceFolder.filesDic.TryGetValue(fileName, out sourceEntire))
                        {
                            sourceEntire = new SourceSingle();
                            sourceEntire.fileName = fileName;
                            sourceEntire.bundleName = categoryLower + "/" + info.Directory.Name + "/" + fileName;
                            sourceEntire.variantName = variantName;
                            sourceEntire.parentFolder = info.Directory.Name;
                            sourceEntire.category = category;
                            sourceEntire.bundleSize = fileSize;
                            sourceEntire.dontDamage = true;
                        }
                    }
                }
                b.Unload(true);
            }
        }
        private void AnalizeFiles()
        {
            if (!Directory.Exists(m_rootPath)) return;
            var varis= Directory.GetDirectories(m_rootPath, "*", SearchOption.TopDirectoryOnly);
            for (int m = 0; m < varis.Length; m++)
            {
                var variDirInfo = new DirectoryInfo(varis[m]);
                var variantName = variDirInfo.Name;
                if (variantName.StartsWith(config.excludePreExtension))
                {
                    continue;
                }
                var directors = Directory.GetDirectories(varis[m], "*", SearchOption.TopDirectoryOnly);
                for (int i = 0; i < directors.Length; i++)
                {
                    var dir = directors[i];
                    DirectoryInfo info = new DirectoryInfo(dir);
                    var directoryInfoArr = info.GetDirectories();
                    string categoryName = info.Name;
                    if (categoryName.StartsWith(config.excludePreExtension))
                    {
                        continue;
                    }
                    string categoryNameLower = categoryName.ToLower();
                    if (!categoryData.ContainsKey(categoryNameLower))
                    {
                        CategoryData sourceData = new CategoryData();
                        sourceData.category = categoryName;
                        sourceData.baseId = 0;
                        sourceData.oldId = 0;
                        categoryData.Add(categoryNameLower, sourceData);
                    }
                    for (int j = 0; j < directoryInfoArr.Length; j++)
                    {
                        if (directoryInfoArr[j].Extension == ".meta") continue;
                        if (directoryInfoArr[j].Extension == ".DS_Store") continue;
                        var dirInfo = directoryInfoArr[j];
                        string dirName = dirInfo.Name;
                        string dirLowerName = dirName.ToLower();
                        var category = categoryData[categoryNameLower];
                        VariantData variant = null;
                        if (category.variantDic.ContainsKey(variantName))
                        {
                            variant = category.variantDic[variantName];
                        }
                        else
                        {
                            variant = new VariantData();
                            variant.variantName = variantName;
                            category.variantDic.Add(variantName, variant);
                        }
                        if (dirName.StartsWith(config.entirePreExtension))
                        {
                            SourceEntire sourceEntire = null;
                            if (!variant.entiresDic.ContainsKey(dirLowerName))
                            {
                                sourceEntire = new SourceEntire()
                                {
                                    fileName = dirName,
                                    bundleName = categoryNameLower + "/" + dirLowerName,
                                    variantName = variantName,
                                    category = category.category,
                                    dontDamage = true
                                };
                                variant.entiresDic.Add(dirLowerName, sourceEntire);
                            }
                            else {
                                sourceEntire=variant.entiresDic[dirLowerName];
                            }

                            var childFilesInfoArr = directoryInfoArr[j].GetFiles();
                            for (int x = 0; x < childFilesInfoArr.Length; x++)
                            {
                                if (childFilesInfoArr[x].Extension == ".meta") continue;
                                if (childFilesInfoArr[x].Extension == ".cs") continue;
                                if (childFilesInfoArr[x].Extension == ".DS_Store") continue;
                                string ext = childFilesInfoArr[x].Name.Substring(childFilesInfoArr[x].Name.LastIndexOf("."));
                                string fileName = childFilesInfoArr[x].Name.Substring(0, childFilesInfoArr[x].Name.LastIndexOf("."));
                                string fileLowerName = fileName.ToLower();
                                string bundleName = info.Name.ToLower() + "/" + fileLowerName;
                                string fileAssetPath = "Assets/" + config.rootName + "/" + variantName + "/" + info.Name + "/" + dirName + "/" + fileName + ext; ;
                                string fileAssetName = info.Name + "/" + dirName + "/" + fileName + ext;
                                List<string> dependencies = new List<string>();
                                if (sourceEntire.filesDic.ContainsKey(fileLowerName))
                                {
                                    sourceEntire.filesDic[fileLowerName].fileName = fileName;
                                    sourceEntire.filesDic[fileLowerName].fileExtension = ext;
                                    sourceEntire.filesDic[fileLowerName].mainAssetPath = fileAssetPath;
                                    sourceEntire.filesDic[fileLowerName].mainAssetName = fileAssetName;
                                    sourceEntire.filesDic[fileLowerName].bundleName = bundleName;
                                    sourceEntire.filesDic[fileLowerName].dontDamage = true;
                                }
                                else
                                {
                                    sourceEntire.filesDic.Add(fileLowerName, new SourceSingle()
                                    {
                                        fileName = fileName,
                                        fileExtension = ext,
                                        mainAssetPath = fileAssetPath,
                                        mainAssetName = fileAssetName,
                                        bundleName = bundleName,
                                        dontDamage = true,
                                        variantName = variantName,
                                        category = categoryName
                                    });
                                }
                            }
                        }
                        else
                        {
                            SourceFolder sourceFolder = null;
                            if (!variant.foldersDic.ContainsKey(dirLowerName))
                            {
                                sourceFolder= new SourceFolder()
                                {
                                    fileName = dirName,
                                    variantName = variantName,
                                    category = category.category
                                };
                                variant.foldersDic.Add(dirLowerName, sourceFolder);
                            }
                            else
                            {
                                sourceFolder = variant.foldersDic[dirLowerName];
                            }
                            var childFolderInfoArr = directoryInfoArr[j].GetDirectories();
                            for (int x = 0; x < childFolderInfoArr.Length; x++)
                            {
                                if (childFolderInfoArr[x].Extension == ".meta") continue;
                                if (childFolderInfoArr[x].Extension == ".DS_Store") continue;
                                var childdirInfo = childFolderInfoArr[x];
                                string childdirName = childdirInfo.Name;
                                string childdirLowerName = childdirName.ToLower();
                                if (childdirName.StartsWith(config.entirePreExtension))
                                {
                                    var childFolderDirInfo = childFolderInfoArr[x];
                                    string childFolderDirName = childFolderDirInfo.Name;
                                    string dchildFolderDrLowerName = childFolderDirName.ToLower();

                                    SourceEntire sourceEntire = null;
                                    if (!sourceFolder.entiresDic.ContainsKey(childdirLowerName))
                                    {
                                        sourceEntire = new SourceEntire()
                                        {
                                            fileName = childdirName,
                                            bundleName = categoryNameLower + "/" + dirName + "/" + childdirLowerName,
                                            variantName = variantName,
                                            category = category.category,
                                            parentFolder = dirName,
                                            dontDamage = true
                                        };
                                        sourceFolder.entiresDic.Add(childdirLowerName, sourceEntire);
                                    }
                                    else
                                    {
                                        sourceEntire = sourceFolder.entiresDic[childdirLowerName];
                                    }
                                    var childFolderFilesInfoArr = childFolderInfoArr[x].GetFiles();
                                    for (int y = 0; y < childFolderFilesInfoArr.Length; y++)
                                    {
                                        if (childFolderFilesInfoArr[y].Extension == ".meta") continue;
                                        if (childFolderFilesInfoArr[y].Extension == ".cs") continue;
                                        if (childFolderFilesInfoArr[y].Extension == ".DS_Store") continue;
                                        string ext = childFolderFilesInfoArr[y].Name.Substring(childFolderFilesInfoArr[y].Name.LastIndexOf("."));
                                        string fileName = childFolderFilesInfoArr[y].Name.Substring(0, childFolderFilesInfoArr[y].Name.LastIndexOf("."));
                                        string fileLowerName = fileName.ToLower();
                                        string bundleName = info.Name.ToLower() + "/" + dirName + "/" + fileLowerName;
                                        string fileAssetPath = "Assets/" + config.rootName + "/" + variantName + "/" + info.Name + "/" + dirName+"/"+ childdirName + "/" + fileName + ext; ;
                                        string fileAssetName = info.Name + "/" + dirName + "/"+ childdirName+"/" + fileName + ext;
                                        List<string> dependencies = new List<string>();
                                        if (sourceEntire.filesDic.ContainsKey(fileLowerName))
                                        {
                                            sourceEntire.filesDic[fileLowerName].fileName = fileName;
                                            sourceEntire.filesDic[fileLowerName].fileExtension = ext;
                                            sourceEntire.filesDic[fileLowerName].mainAssetPath = fileAssetPath;
                                            sourceEntire.filesDic[fileLowerName].mainAssetName = fileAssetName;
                                            sourceEntire.filesDic[fileLowerName].bundleName = bundleName;
                                            sourceEntire.filesDic[fileLowerName].dontDamage = true;
                                        }
                                        else
                                        {
                                            sourceEntire.filesDic.Add(fileLowerName, new SourceSingle()
                                            {
                                                fileName = fileName,
                                                fileExtension = ext,
                                                mainAssetPath = fileAssetPath,
                                                mainAssetName = fileAssetName,
                                                parentFolder= childFolderDirName,
                                                bundleName = bundleName,
                                                dontDamage = true,
                                                variantName = variantName,
                                                category = categoryName
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    UnityEngine.Debug.LogError("Source Only support 2layer: " + childFolderInfoArr[x]);
                                }
                            }
                            var childFilesInfoArr = directoryInfoArr[j].GetFiles();
                            for (int x = 0; x < childFilesInfoArr.Length; x++)
                            {
                                if (childFilesInfoArr[x].Extension == ".meta") continue;
                                if (childFilesInfoArr[x].Extension == ".cs") continue;
                                if (childFilesInfoArr[x].Extension == ".DS_Store") continue;
                                string ext = childFilesInfoArr[x].Name.Substring(childFilesInfoArr[x].Name.LastIndexOf("."));
                                string fileName = childFilesInfoArr[x].Name.Substring(0, childFilesInfoArr[x].Name.LastIndexOf("."));
                                string fileLowerName = fileName.ToLower();
                                string bundleName = info.Name.ToLower() + "/" + dirName + "/" + fileLowerName;
                                string fileAssetPath = "Assets/" + config.rootName + "/" + variantName + "/" + info.Name + "/" + dirName + "/" + fileName + ext; ;
                                string fileAssetName = info.Name + "/" + dirName + "/" + fileName + ext;
                                List<string> dependencies = new List<string>();
                                if (sourceFolder.filesDic.ContainsKey(fileLowerName))
                                {
                                    sourceFolder.filesDic[fileLowerName].fileName = fileName;
                                    sourceFolder.filesDic[fileLowerName].fileExtension = ext;
                                    sourceFolder.filesDic[fileLowerName].mainAssetPath = fileAssetPath;
                                    sourceFolder.filesDic[fileLowerName].mainAssetName = fileAssetName;
                                    sourceFolder.filesDic[fileLowerName].bundleName = bundleName;
                                    sourceFolder.filesDic[fileLowerName].dontDamage = true;
                                }
                                else
                                {
                                    sourceFolder.filesDic.Add(fileLowerName, new SourceSingle()
                                    {
                                        fileName = fileName,
                                        fileExtension = ext,
                                        mainAssetPath = fileAssetPath,
                                        mainAssetName = fileAssetName,
                                        bundleName = bundleName,
                                        dontDamage = true,
                                        variantName = variantName,
                                        category = categoryName
                                    });
                                }
                            }
                        }
                    }
                    var filesInfoArr = info.GetFiles();
                    for (int j = 0; j < filesInfoArr.Length; j++)
                    {
                        if (filesInfoArr[j].Extension == ".meta") continue;
                        if (filesInfoArr[j].Extension == ".cs") continue;
                        if (filesInfoArr[j].Extension == ".DS_Store") continue;
                        string ext = filesInfoArr[j].Name.Substring(filesInfoArr[j].Name.LastIndexOf("."));
                        string fileName = filesInfoArr[j].Name.Substring(0, filesInfoArr[j].Name.LastIndexOf("."));
                        string fileLowerName = fileName.ToLower();
                        string bundleName = info.Name.ToLower() + "/" + fileLowerName;
                        string fileAssetPath = "Assets/" + config.rootName + "/"+variantName+"/" + info.Name + "/" + fileName + ext; ;
                        string fileAssetName = info.Name + "/" + fileName + ext;
                        List<string> dependencies = new List<string>();
                        if (categoryData.ContainsKey(categoryNameLower))
                        {
                            if (categoryData[categoryNameLower].variantDic.ContainsKey(variantName))
                            {
                                var variant = categoryData[categoryNameLower].variantDic[variantName];
                                if (variant.filesDic.ContainsKey(fileLowerName))
                                {
                                    variant.filesDic[fileLowerName].fileName = fileName;
                                    variant.filesDic[fileLowerName].fileExtension = ext;
                                    variant.filesDic[fileLowerName].mainAssetPath = fileAssetPath;
                                    variant.filesDic[fileLowerName].mainAssetName = fileAssetName;
                                    variant.filesDic[fileLowerName].bundleName = bundleName;
                                    variant.filesDic[fileLowerName].dontDamage = true;
                                }
                                else
                                {
                                    variant.filesDic.Add(fileLowerName, new SourceSingle()
                                    {
                                        fileName = fileName,
                                        fileExtension = ext,
                                        mainAssetPath = fileAssetPath,
                                        mainAssetName = fileAssetName,
                                        bundleName = bundleName,
                                        dontDamage = true,
                                        variantName=variantName,
                                        category=categoryName
                                    });
                                }
                            }
                            else
                            {
                                var variantData = new VariantData();
                                variantData.variantName = variantName;
                                variantData.filesDic.Add(fileLowerName, new SourceSingle()
                                {
                                    fileName = fileName,
                                    fileExtension = ext,
                                    mainAssetPath = fileAssetPath,
                                    mainAssetName = fileAssetName,
                                    bundleName = bundleName,
                                    dontDamage = true,
                                    assetId = 0,
                                    oldAssetId = 0,
                                    variantName=variantName,
                                    category=categoryName
                                });
                                categoryData[categoryNameLower].variantDic.Add(variantName, variantData);
                            }
                        }
                        else
                        {
                            CategoryData data = new CategoryData();
                            data.category = categoryName;
                            data.baseId = 0;
                            data.oldId = 0;
                            var variant = new VariantData();
                            variant.variantName = variantName;
                            data.variantDic.Add(variantName, variant);
                            variant.filesDic.Add(fileLowerName, new SourceSingle()
                            {
                                fileName = fileName,
                                fileExtension = ext,
                                mainAssetPath = fileAssetPath,
                                mainAssetName = fileAssetName,
                                bundleName = bundleName,
                                dontDamage = true,
                                variantName=variantName,
                                category=categoryName
                            });
                            categoryData.Add(categoryNameLower, data);
                        }

                    }

                }
            }

        }
        private void AnalizeDependency()
        {
            foreach (var sourceList in allSourcesGuid)
            {
                var source = sourceList.Value;
                if (source is SourceEntire)
                {
                    var sourceEntire = source as SourceEntire;
                    foreach (var sourceFile in sourceEntire.filesDic)
                    {
                        string fileAssetPath = sourceFile.Value.mainAssetPath;
                        var depArr = AssetDatabase.GetDependencies(fileAssetPath);
                        for (int x = 0; x < depArr.Length; x++)
                        {
                            if (depArr[x] == fileAssetPath || depArr[x].Contains(sourceEntire.category+"/"+sourceEntire.fileName)) continue;
                            if (m_allSourcesPath.ContainsKey(depArr[x]))
                            {
                                var sources = m_allSourcesPath[depArr[x]];
                                sourceFile.Value.fileDependencies.Add(sources.guid);
                                sourceEntire.fileDependencies.Add(sources.guid);
                                sources.fileDependencied.Add(sourceEntire.guid);
                            }
                            else
                            {
                                //UnityEngine.Debug.LogWarning("dont has this path: of  Source:"+ fileAssetPath+"\n" + depArr[x]);
                            }
                        }
                    }
                }
                else if (source is SourceFolder)
                {
                    var sourceFolder = source as SourceFolder;
                    foreach (var sourceFile in sourceFolder.entiresDic)
                    {
                        var sourceEntire = sourceFile.Value;
                        string fileAssetPath = sourceFile.Value.mainAssetPath;
                        var depArr = AssetDatabase.GetDependencies(fileAssetPath);
                        for (int x = 0; x < depArr.Length; x++)
                        {
                            if (depArr[x] == fileAssetPath || depArr[x].Contains(sourceEntire.category + "/" + sourceEntire.fileName)) continue;
                            if (m_allSourcesPath.ContainsKey(depArr[x]))
                            {
                                var sources = m_allSourcesPath[depArr[x]];
                                sourceFile.Value.fileDependencies.Add(sources.guid);
                                sourceEntire.fileDependencies.Add(sources.guid);
                                sources.fileDependencied.Add(sourceEntire.guid);
                            }
                            else
                            {
                                //UnityEngine.Debug.LogWarning("dont has this path: of  Source:"+ fileAssetPath+"\n" + depArr[x]);
                            }
                        }
                    }
                    foreach (var item in sourceFolder.filesDic)
                    {
                        var sourceFile = item.Value;
                        string fileAssetPath = sourceFile.mainAssetPath;
                        var depArr = AssetDatabase.GetDependencies(fileAssetPath);
                        for (int x = 0; x < depArr.Length; x++)
                        {
                            if (depArr[x] == fileAssetPath) continue;
                            if (m_allSourcesPath.ContainsKey(depArr[x]))
                            {
                                var sources = m_allSourcesPath[depArr[x]];
                                sourceFile.fileDependencies.Add(sources.guid);
                                sources.fileDependencied.Add(sourceFile.guid);
                            }
                            else
                            {
                                //UnityEngine.Debug.LogWarning("dont has this path: of  Source:" + fileAssetPath + "\n" + depArr[x]);
                            }
                        }
                    }
                }
                else
                {
                    var sourceFile = source as SourceSingle;
                    string fileAssetPath = sourceFile.mainAssetPath;
                    var depArr = AssetDatabase.GetDependencies(fileAssetPath);
                    for (int x = 0; x < depArr.Length; x++)
                    {
                        if (depArr[x] == fileAssetPath) continue;
                        if (m_allSourcesPath.ContainsKey(depArr[x]))
                        {
                            var sources = m_allSourcesPath[depArr[x]];
                            sourceFile.fileDependencies.Add(sources.guid);
                            sources.fileDependencied.Add(sourceFile.guid);
                        }
                        else
                        {
                            //UnityEngine.Debug.LogWarning("dont has this path: of  Source:" + fileAssetPath + "\n" + depArr[x]);
                        }
                    }
                }
            }
        }
        private void AnalizeChain()
        {
            foreach (var source in allSourcesGuid)
            {
                var baseSource = source.Value;
                if (baseSource is SourceEntire)
                {
                    baseSource.fileState = ESourceState.MainBundle;
                    List<int> chain = new List<int>();
                    chain.Add(source.Key);
                    dependChains.Add(new List<List<int>>() { chain});
                    int chainId = dependChains.Count-1;
                    baseSource.dependenciedChainList.Add(chainId);
                    baseSource.dependenciesChain = chainId;
                }
            }
            foreach (var source in allSourcesGuid)
            {
                var baseSource = source.Value;
                if (baseSource is SourceSingle && baseSource.fileDependencied.Count <= 0)
                {
                    baseSource.fileState = ESourceState.MainBundle;
                    List<int> chain = new List<int>();
                    chain.Add(source.Key);
                    dependChains.Add(new List<List<int>>() { chain });
                    int chainId = dependChains.Count - 1;
                    baseSource.dependenciedChainList.Add(chainId);
                    baseSource.dependenciesChain = chainId;
                    for (int j = 0; j < baseSource.fileDependencies.Count; j++)
                    {
                        var depGuid = baseSource.fileDependencies[j];
                        if (!chain.Contains(depGuid))
                        {
                            chain.Add(depGuid);
                            if (!allSourcesGuid[depGuid].dependenciedChainList.Contains(depGuid) && allSourcesGuid[depGuid] is SourceSingle)
                            {
                                allSourcesGuid[depGuid].dependenciedChainList.Add(chainId);
                                if (allSourcesGuid[depGuid].dependenciedChainList.Count > config.minBundleDependencyCount)
                                {
                                    allSourcesGuid[depGuid].fileState = ESourceState.SharedBundle;
                                }
                            }
                        }
                    }
                }
            }
            foreach (var source in allSourcesGuid)
            {
                var baseSource = source.Value;
                if (baseSource is SourceSingle && baseSource.fileState==ESourceState.SharedBundle)
                {
                    List<int> chain = new List<int>();
                    chain.Add(source.Key);
                    dependChains.Add(new List<List<int>>() { chain });
                    int chainId = dependChains.Count - 1;
                    baseSource.dependenciesChain=chainId;
                    for (int j = 0; j < baseSource.fileDependencies.Count; j++)
                    {
                        var depGuid = baseSource.fileDependencies[j];
                        if (!chain.Contains(depGuid))
                        {
                            chain.Add(depGuid);
                        }
                    }
                }
            }
            for (int i = 0; i < dependChains.Count; i++)
            {
                var chain = dependChains[i];
                var l = chain[0];
                for (int j = 1; j < l.Count; j++)
                {
                    var guid = l[j];
                    var baseSource = allSourcesGuid[guid];
                    var deps = baseSource.fileDependencies;
                    int guidLayer = 0;
                    for (int x = 0; x < deps.Count; x++)
                    {
                        var depSource = allSourcesGuid[deps[x]];
                        if (depSource.fileState == ESourceState.SharedBundle)
                        {
                            bool has = false;
                            int r = 0;
                            for (int m = 0; m < chain.Count; m++)
                            {
                                for (int n = 0; n < chain[m].Count; n++)
                                {
                                    if (m == 0 && n == 0) continue;
                                    if (chain[m][n] == guid) continue;
                                    if (chain[m][n] == deps[x])
                                    {
                                        r = m;
                                        has = true;
                                        break;
                                    }
                                }
                                if (has)
                                {
                                    break;
                                }
                            }
                            if (has)
                            {
                                int nextR = r + 1;
                                if (nextR>guidLayer)
                                {
                                    if (chain.Count <= nextR)
                                    {
                                        chain.Add(new List<int>() { guid });
                                    }
                                    else
                                    {
                                        chain[nextR].Add(guid);
                                    }
                                    chain[guidLayer].Remove(guid);
                                    guidLayer = nextR;
                                }
                                else
                                {

                                }
                                j =0;
                            }
                        }
                    }
                }
                chain.Add(new List<int>() { chain[0][0] });
                chain[0].RemoveAt(0);
                if (chain[0].Count<=0)
                {
                    chain.RemoveAt(0);
                }
            }
        }
        private void AnalizeEditorTable()
        {
            string tablePath = m_bundleRootPath + "/" + config.editorCacheName;
            if (File.Exists(tablePath))
            {
                using (FileStream fileStream = new FileStream(tablePath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fileStream))
                    {
                        string s;
                        StringBuilder str = new StringBuilder();
                        while ((s = sr.ReadLine()) != null)
                        {
                            str.Clear();
                            string categoryName = "";
                            int baseId = 0;
                            string fileName = "";
                            int id = 0;
                            string parentFolder = "";
                            bool isEntire = false;
                            for (int i = 0, j = 0; i < s.Length; i++)
                            {
                                char c = s[i];
                                if (c == ',' || i == s.Length - 1)
                                {
                                    if (j == 0)
                                    {
                                        categoryName = str.ToString();
                                    }
                                    else if (j == 1)
                                    {
                                        baseId = Convert.ToInt32(str.ToString());
                                    }
                                    else if (j == 2)
                                    {
                                        fileName = str.ToString();
                                    }
                                    else if (j == 3)
                                    {
                                        id = Convert.ToInt32(str.ToString());
                                    }
                                    else if (j == 4)
                                    {
                                        parentFolder =str.ToString();
                                    }
                                    else if (j == 5)
                                    {
                                        str.Append(c);
                                        isEntire = str.ToString() == "1";
                                    }
                                    str.Clear();
                                    j++;
                                }
                                else
                                {
                                    str.Append(c);
                                }
                            }
                            if (categoryData.ContainsKey(categoryName))
                            {
                                categoryData[categoryName].baseId = baseId;
                                categoryData[categoryName].oldId = baseId;
                                foreach (var variantItem in categoryData[categoryName].variantDic)
                                {
                                    var variant = variantItem.Value;
                                    if (string.IsNullOrEmpty(parentFolder))
                                    {
                                        if (isEntire)
                                        {
                                            if (variant.entiresDic.ContainsKey(fileName))
                                            {
                                                int guid = id * 10 + GetVariantInt(variant.variantName);
                                                var dir = variant.entiresDic[fileName];
                                                dir.assetId = id;
                                                dir.oldAssetId = id;
                                                dir.guid = guid;
                                            }
                                        }
                                        else
                                        {
                                            if (variant.filesDic.ContainsKey(fileName))
                                            {
                                                variant.filesDic[fileName].assetId = id;
                                                variant.filesDic[fileName].oldAssetId = id;
                                                variant.filesDic[fileName].guid = id * 10 + GetVariantInt(variant.variantName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var item in variantItem.Value.foldersDic)
                                        {
                                            if (isEntire)
                                            {
                                                if (item.Value.entiresDic.ContainsKey(fileName))
                                                {
                                                    int guid = id * 10 + GetVariantInt(variant.variantName);
                                                    var dir = item.Value.entiresDic[fileName];
                                                    dir.assetId = id;
                                                    dir.oldAssetId = id;
                                                    dir.guid = guid;
                                                }
                                            }
                                            else
                                            {
                                                if (item.Value.filesDic.ContainsKey(fileName))
                                                {
                                                    item.Value.filesDic[fileName].assetId = id;
                                                    item.Value.filesDic[fileName].oldAssetId = id;
                                                    item.Value.filesDic[fileName].guid = id * 10 + GetVariantInt(variant.variantName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach (var category in categoryData)
            {
                if (category.Value.baseId<=0)
                {
                    int baseId = GetNextBaseId();
                    category.Value.baseId = baseId;
                    category.Value.oldId = baseId;
                }
                Dictionary<string, List<BaseSource>> sources = new Dictionary<string, List<BaseSource>>();
                foreach (var variant in category.Value.variantDic)
                {
                    foreach (var item in variant.Value.entiresDic)
                    {
                        if (item.Value.assetId<=0)
                        {
                            if (sources.ContainsKey(item.Value.fileName))
                            {
                                sources[item.Value.fileName].Add(item.Value);
                            }
                            else
                            {
                                int id = GetNextBundleId(category.Value);
                                item.Value.assetId = id;
                                item.Value.oldAssetId = id;
                                item.Value.guid = id * 10 +GetVariantInt( variant.Value.variantName);
                                sources.Add(item.Value.fileName, new List<BaseSource>() { item.Value });
                            }
                        }
                    }
                    foreach (var item in variant.Value.filesDic)
                    {
                        if (item.Value.assetId <= 0)
                        {
                            if (sources.ContainsKey(item.Value.fileName))
                            {
                                sources[item.Value.fileName].Add(item.Value);
                            }
                            else
                            {
                                int id = GetNextBundleId(category.Value);
                                item.Value.assetId = id;
                                item.Value.oldAssetId = id;
                                item.Value.guid = id * 10 + GetVariantInt(variant.Value.variantName);
                                sources.Add(item.Value.fileName, new List<BaseSource>() { item.Value });
                            }
                        }
                    }
                    foreach (var it in variant.Value.foldersDic)
                    {
                        foreach (var item in it.Value.entiresDic)
                        {
                            if (item.Value.assetId <= 0)
                            {
                                if (sources.ContainsKey(item.Value.fileName))
                                {
                                    sources[item.Value.fileName].Add(item.Value);
                                }
                                else
                                {
                                    int id = GetNextBundleId(category.Value);
                                    item.Value.assetId = id;
                                    item.Value.oldAssetId = id;
                                    item.Value.guid = id * 10 + GetVariantInt(it.Value.variantName);
                                    sources.Add(item.Value.fileName, new List<BaseSource>() { item.Value });
                                }
                            }
                        }
                        foreach (var item in it.Value.filesDic)
                        {
                            if (item.Value.assetId <= 0)
                            {
                                if (sources.ContainsKey(item.Value.fileName))
                                {
                                    sources[item.Value.fileName].Add(item.Value);
                                }
                                else
                                {
                                    int id = GetNextBundleId(category.Value);
                                    item.Value.assetId = id;
                                    item.Value.oldAssetId = id;
                                    item.Value.guid = id * 10 + GetVariantInt(it.Value.variantName);
                                    sources.Add(item.Value.fileName, new List<BaseSource>() { item.Value });
                                }
                            }
                        }
                    }
                }
                foreach (var item in sources)
                {
                    int id = 0;
                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        if (i==0)
                        {
                            id = item.Value[0].assetId;
                        }
                        else
                        {
                            item.Value[i].assetId = id;
                            item.Value[i].oldAssetId = id;
                            item.Value[i].guid = id * 10 + GetVariantInt(item.Value[i].variantName);
                        }
                    }
                }
            }
        }
        private void AnalizeSourceState()
        {
            foreach (var category in categoryData)
            {
                int id = category.Value.baseId;
                foreach (var variant in category.Value.variantDic)
                {
                    foreach (var dir in variant.Value.entiresDic)
                    {
                        if (!dir.Value.dontDamage)
                        {
                            dir.Value.fileState = ESourceState.Damage;
                        }
                        else
                        {
                            allSourcesGuid.Add(dir.Value.guid,dir.Value);
                            foreach (var it in dir.Value.filesDic)
                            {
                                m_allSourcesPath.Add(it.Value.mainAssetPath, dir.Value);
                            }
                        }
                    }
                    foreach (var dir in variant.Value.filesDic)
                    {
                        if (!dir.Value.dontDamage)
                        {
                            dir.Value.fileState = ESourceState.Damage;
                        }
                        else
                        {
                            allSourcesGuid.Add(dir.Value.guid, dir.Value);
                            m_allSourcesPath.Add(dir.Value.mainAssetPath, dir.Value);
                        }
                    }
                    foreach (var folder in variant.Value.foldersDic)
                    {
                        foreach (var dir in folder.Value.entiresDic)
                        {
                            if (!dir.Value.dontDamage)
                            {
                                dir.Value.fileState = ESourceState.Damage;
                            }
                            else
                            {
                                allSourcesGuid.Add(dir.Value.guid, dir.Value);
                                foreach (var it in dir.Value.filesDic)
                                {
                                    m_allSourcesPath.Add(it.Value.mainAssetPath, dir.Value);
                                }
                            }
                        }
                        foreach (var dir in folder.Value.filesDic)
                        {
                            if (!dir.Value.dontDamage)
                            {
                                dir.Value.fileState = ESourceState.Damage;
                            }
                            else
                            {
                                allSourcesGuid.Add(dir.Value.guid, dir.Value);
                                m_allSourcesPath.Add(dir.Value.mainAssetPath, dir.Value);
                            }
                        }
                    }
                }
            }
        }
        private int GetNextBundleId(CategoryData data)
        {
            int baseId = data.baseId;
            int nextId = baseId++;
            List<int> m_idList = new List<int>();
            foreach (var variant in data.variantDic)
            {
                foreach (var it in variant.Value.entiresDic)
                {
                    if (it.Value.assetId>0)
                    {
                        m_idList.Add(it.Value.assetId);
                    }
                }
                foreach (var it in variant.Value.filesDic)
                {
                    if (it.Value.assetId > 0)
                    {
                        m_idList.Add(it.Value.assetId);
                    }
                }
                foreach (var fold in variant.Value.foldersDic)
                {
                    foreach (var it in fold.Value.entiresDic)
                    {
                        if (it.Value.assetId > 0)
                        {
                            m_idList.Add(it.Value.assetId);
                        }
                    }
                    foreach (var it in fold.Value.filesDic)
                    {
                        if (it.Value.assetId > 0)
                        {
                            m_idList.Add(it.Value.assetId);
                        }
                    }
                }
            }
            m_idList.Sort();
            for (int i = 0; i < m_idList.Count; i++)
            {
                if (i == 0)
                {
                    nextId = m_idList[0];
                }
                if (m_idList[i] > nextId)
                {
                    break;
                }
                nextId++;
            }

            return nextId;
        }
        private int GetNextBaseId()
        {
            int baseId = config.idBase;
            List<int> m_idList = new List<int>();
            foreach (var item in categoryData)
            {
                m_idList.Add(item.Value.baseId);
            }
            m_idList.Sort();
            for (int i = 0; i < m_idList.Count; i++)
            {
                if (i == 0)
                {
                    baseId = m_idList[0];
                }
                if (m_idList[i] > baseId)
                {
                    break;
                }
                baseId += config.idBase;
            }
            return baseId;
        }
        private int GetVariantInt(string variantName)
        {
            return (int)Enum.Parse(typeof(EVariantType), variantName);
        }
        private byte[] Serialize<T>(T instance)
        {
            byte[] bytes;
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, instance);
                bytes = ms.ToArray();// new byte[ms.Position];
                //var fullBytes = ms.GetBuffer();
                //Array.Copy(fullBytes, bytes, bytes.Length);
            }
            return bytes;
        }
        private T Deserialize<T>(object obj)
        {
            byte[] bytes = (byte[])obj;
            using (var ms = new MemoryStream(bytes))
            {
                return Serializer.Deserialize<T>(ms);
            }
        }
        string GetFileHash(string path)
        {
            var hash = SHA1.Create();
            var stream = new FileStream(path, FileMode.Open);
            byte[] hashByte = hash.ComputeHash(stream);
            stream.Close();
            return BitConverter.ToString(hashByte).Replace("-", "");
        }
        Dictionary<string,string> ReadPackage(string folderPath)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (Directory.Exists(folderPath))
            {
                var files = Directory.GetFiles(folderPath, "*", SearchOption.AllDirectories);
                for (int i = 0; i < files.Length; i++)
                {
                    var f = files[i];
                    f = f.Replace("\\", "/");
                    FileInfo info = new FileInfo(f);
                    if (info.Name == config.packageFileName) continue;
                    if (info.Name == config.bundleFolder) continue;
                    if (info.Extension == ".manifest") continue;
                    if (info.Extension == ".meta") continue;
                    if (config.manifestFileName.Contains(info.Name))continue;
                    var root = m_bundleRootPath.Replace("\\", "/");
                    dic.Add(f.Replace(root + "/", ""), GetFileHash(f));
                }
            }
            return dic;
        }
        void SavePackageConfig(Dictionary<string,string> oldDic,Dictionary<string,string> newDic)
        {
            string packPath = m_bundleRootPath + "/" + config.packageFileName;
            List<string> m_modifyfiles = new List<string>();
            List<string> m_addfiles = new List<string>();
            List<string> m_deletefiles = new List<string>();
            foreach (var src in newDic)
            {
                bool has = false;
                foreach (var dist in oldDic)
                {
                    if (src.Key == dist.Key)
                    {
                        if (src.Value != dist.Value)
                        {
                            m_modifyfiles.Add(src.Key.Replace("\\", "/"));
                        }
                        has = true;
                    }
                }
                if (!has)
                {
                    m_addfiles.Add(src.Key.Replace("\\", "/"));
                }
            }
            foreach (var dist in oldDic)
            {
                bool has = false;
                foreach (var src in newDic)
                {
                    if (src.Key == dist.Key)
                    {
                        has = true;
                    }
                }
                if (!has)
                {
                    var f = dist.Key.Replace("\\", "/");
                    if (!f.Contains(config.packageFileName))
                    {
                        m_deletefiles.Add(f);
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("add");
            for (int i = 0; i < m_addfiles.Count; i++)
            {
                sb.AppendLine(m_addfiles[i]);
            }
            sb.AppendLine("modify");
            for (int i = 0; i < m_modifyfiles.Count; i++)
            {
                sb.AppendLine(m_modifyfiles[i]);
            }
            sb.AppendLine("delete");
            for (int i = 0; i < m_deletefiles.Count; i++)
            {
                sb.AppendLine(m_deletefiles[i]);
            }
            FileInfo filI = new FileInfo(packPath);
            if (!filI.Directory.Exists)
            {
                filI.Directory.Create();
            }
            using (FileStream st = new FileStream(packPath, FileMode.Create, FileAccess.Write))
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                st.Write(bytes, 0, bytes.Length);
            }
        }
    }
}