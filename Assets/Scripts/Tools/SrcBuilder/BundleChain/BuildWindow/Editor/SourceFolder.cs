﻿using System;
using System.Collections.Generic;

namespace SrcBuilder.Bundle
{
    public class SourceFolder: BaseSource
    {
        public bool isOpen;
        public Dictionary<string, SourceSingle> filesDic = new Dictionary<string, SourceSingle>();
        public Dictionary<string, SourceEntire> entiresDic = new Dictionary<string, SourceEntire>();
    }
}
