﻿using System;
using System.Collections.Generic;

namespace SrcBuilder.Bundle
{
    public enum ESourceState
    {
        PrivateBundle,
        MainBundle,
        SharedBundle,
        Damage
    }
}
