﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SrcBuilder.Bundle
{
    public class BuildConfig:ScriptableObject
    {
        public string rootName= "ResourcesAssets";
        public string bundleOutputFolder= "Dist";
        public string bundleFolder= "Bundle";
        public string packageFileName = "package.txt";
        public string manifestFileName = "manifest.bytes";
        public string editorCacheName = "manifestCache.txt";
        public string diffZipName = "DiffBundle.7z";
        public string entireZipName = "EntireBundle.7z";
        public string excludePreExtension = "__";
        public string entirePreExtension = "e_";
        public int minBundleDependencyCount=1;
        public int bundleOptions = 0;
        public int buildTarget = -2;
        public int idBase = 10000;
        public Color mainBundleColor = Color.white;
        public Color sharedBundleColor = new Color(1, 237.0f / 255.0f, 0, 1);
        public Color privateBundleColor = new Color(161.0f / 255.0f, 161.0f / 255.0f, 161.0f / 255.0f, 1);
        public Color damageBundleColor = Color.red;
        public Color folderColor = new Color(192 / 255.0f, 36 / 255.0f, 24 / 255.0f, 1);
    }
}
