﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace SrcBuilder.Builder
{
    public static class Builder
    {
        private static BuilderConfig m_config;
        public static void BuildPackage()
        {
            InitConfig();
            new SrcBuilder.Bundle.BundleBuildManager().BuildPackage();
            new SrcBuilder.Lua.LuaPackageManager().ToPackage();
            KeyPairsGen();
            InjectAppconfig();
            UpdateVersion();
            var zipPath = new ToPackZip(m_config).PackEntire();
            File.Copy(zipPath, Path.Combine(Application.streamingAssetsPath, new FileInfo(zipPath).Name), true);
            File.Delete(zipPath);
            var dic = GetCommandArgs();
            string[] args = System.Environment.GetCommandLineArgs();
            string version = dic["version"];
            int platform = System.Convert.ToInt32(dic["platform"]);
            string outputPath = dic["outputPath"];//注意是相对路径  如  "./AutoBuild/Output/201805051202.apk"
            List<string> sceneList = new List<string>();
            EditorBuildSettingsScene[] temp = EditorBuildSettings.scenes;
            for (int i = 0, iMax = temp.Length; i < iMax; ++i)
            {
                var t = temp[i];
                if (t == null || t.enabled == false) continue;
                sceneList.Add(t.path);
            }
            if (dic.ContainsKey("currentLevel") && !string.IsNullOrEmpty(dic["currentLevel"]))
                QualitySettings.SetQualityLevel(System.Convert.ToInt32(dic["currentLevel"]));
            if (dic.ContainsKey("development") && !string.IsNullOrEmpty(dic["development"]))
                EditorUserBuildSettings.development = System.Convert.ToBoolean(dic["development"]);
            if (dic.ContainsKey("connectProfiler") && !string.IsNullOrEmpty(dic["connectProfiler"]))
                EditorUserBuildSettings.connectProfiler = System.Convert.ToBoolean(dic["connectProfiler"]);
            if (dic.ContainsKey("buildScriptsOnly") && !string.IsNullOrEmpty(dic["buildScriptsOnly"]))
                EditorUserBuildSettings.buildScriptsOnly = System.Convert.ToBoolean(dic["buildScriptsOnly"]);
            if (dic.ContainsKey("allowDebugging") && !string.IsNullOrEmpty(dic["allowDebugging"]))
                EditorUserBuildSettings.allowDebugging = System.Convert.ToBoolean(dic["allowDebugging"]);
            if (dic.ContainsKey("compressFilesInPackage") && !string.IsNullOrEmpty(dic["compressFilesInPackage"]))
                EditorUserBuildSettings.compressFilesInPackage = System.Convert.ToBoolean(dic["compressFilesInPackage"]);
            if (dic.ContainsKey("compressWithPsArc") && !string.IsNullOrEmpty(dic["compressWithPsArc"]))
                EditorUserBuildSettings.compressWithPsArc = System.Convert.ToBoolean(dic["compressWithPsArc"]);
            if (dic.ContainsKey("enableHeadlessMode") && !string.IsNullOrEmpty(dic["enableHeadlessMode"]))
                EditorUserBuildSettings.enableHeadlessMode = System.Convert.ToBoolean(dic["enableHeadlessMode"]);
            if (dic.ContainsKey("explicitDivideByZeroChecks") && !string.IsNullOrEmpty(dic["explicitDivideByZeroChecks"]))
                EditorUserBuildSettings.explicitDivideByZeroChecks = System.Convert.ToBoolean(dic["explicitDivideByZeroChecks"]);
            if (dic.ContainsKey("explicitNullChecks") && !string.IsNullOrEmpty(dic["explicitNullChecks"]))
                EditorUserBuildSettings.explicitNullChecks = System.Convert.ToBoolean(dic["explicitNullChecks"]);
            if (dic.ContainsKey("androidBuildSystem") && !string.IsNullOrEmpty(dic["androidBuildSystem"]))
                EditorUserBuildSettings.androidBuildSystem = (AndroidBuildSystem)System.Convert.ToInt32(dic["androidBuildSystem"]);
            if (dic.ContainsKey("androidBuildSubtarget") && !string.IsNullOrEmpty(dic["androidBuildSubtarget"]))
                EditorUserBuildSettings.androidBuildSubtarget = (MobileTextureSubtarget)System.Convert.ToInt32(dic["androidBuildSubtarget"]);
            if (dic.ContainsKey("androidDebugMinification") && !string.IsNullOrEmpty(dic["androidDebugMinification"]))
                EditorUserBuildSettings.androidDebugMinification = (AndroidMinification)System.Convert.ToInt32(dic["androidDebugMinification"]);
            if (dic.ContainsKey("androidReleaseMinification") && !string.IsNullOrEmpty(dic["androidReleaseMinification"]))
                EditorUserBuildSettings.androidReleaseMinification = (AndroidMinification)System.Convert.ToInt32(dic["androidReleaseMinification"]);
            // if (dic.ContainsKey("androidDeviceSocketAddress") && !string.IsNullOrEmpty(dic["androidDeviceSocketAddress"]))
            //     EditorUserBuildSettings.androidDeviceSocketAddress = dic["androidDeviceSocketAddress"];
            if (dic.ContainsKey("iOSBuildConfigType") && !string.IsNullOrEmpty(dic["iOSBuildConfigType"]))
                EditorUserBuildSettings.iOSBuildConfigType = (iOSBuildType)System.Convert.ToInt32(dic["iOSBuildConfigType"]);

            if (dic.ContainsKey("companyName") && !string.IsNullOrEmpty(dic["companyName"]))
                PlayerSettings.companyName = dic["companyName"];
            if (dic.ContainsKey("productName") && !string.IsNullOrEmpty(dic["productName"]))
                PlayerSettings.productName = dic["productName"];
            if (dic.ContainsKey("applicationIdentifier") && !string.IsNullOrEmpty(dic["applicationIdentifier"]))
                PlayerSettings.applicationIdentifier = dic["applicationIdentifier"];
            if (dic.ContainsKey("colorSpace") && !string.IsNullOrEmpty(dic["colorSpace"]))
                PlayerSettings.colorSpace = (ColorSpace)System.Convert.ToInt32(dic["colorSpace"]);
            if (dic.ContainsKey("gpuSkinning") && !string.IsNullOrEmpty(dic["gpuSkinning"]))
                PlayerSettings.gpuSkinning = System.Convert.ToBoolean(dic["gpuSkinning"]);
            if (dic.ContainsKey("graphicsJobs") && !string.IsNullOrEmpty(dic["graphicsJobs"]))
                PlayerSettings.graphicsJobs = System.Convert.ToBoolean(dic["graphicsJobs"]);
            if (dic.ContainsKey("muteOtherAudioSources") && !string.IsNullOrEmpty(dic["muteOtherAudioSources"]))
                PlayerSettings.muteOtherAudioSources = System.Convert.ToBoolean(dic["muteOtherAudioSources"]);
            if (dic.ContainsKey("runInBackground") && !string.IsNullOrEmpty(dic["runInBackground"]))
                PlayerSettings.runInBackground = System.Convert.ToBoolean(dic["runInBackground"]);
            if (dic.ContainsKey("stripEngineCode") && !string.IsNullOrEmpty(dic["stripEngineCode"]))
                PlayerSettings.stripEngineCode = System.Convert.ToBoolean(dic["stripEngineCode"]);
            if (dic.ContainsKey("strippingLevel") && !string.IsNullOrEmpty(dic["strippingLevel"]))
                PlayerSettings.strippingLevel = (StrippingLevel)System.Convert.ToInt32(dic["strippingLevel"]);
            if (platform == (int)BuildTarget.Android)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
                if (dic.ContainsKey("android.androidIsGame") && !string.IsNullOrEmpty(dic["android.androidIsGame"]))
                    PlayerSettings.Android.androidIsGame = System.Convert.ToBoolean(dic["android.androidIsGame"]);
                if (dic.ContainsKey("android.androidTVCompatibility") && !string.IsNullOrEmpty(dic["android.androidTVCompatibility"]))
                    PlayerSettings.Android.androidTVCompatibility = System.Convert.ToBoolean(dic["android.androidTVCompatibility"]);
                if (dic.ContainsKey("android.blitType") && !string.IsNullOrEmpty(dic["android.blitType"]))
                    PlayerSettings.Android.blitType = (AndroidBlitType)System.Convert.ToInt32(dic["android.blitType"]);
                if (dic.ContainsKey("android.bundleVersionCode") && !string.IsNullOrEmpty(dic["android.bundleVersionCode"]))
                    PlayerSettings.Android.bundleVersionCode = System.Convert.ToInt32(dic["android.bundleVersionCode"]);
                if (dic.ContainsKey("android.disableDepthAndStencilBuffers") && !string.IsNullOrEmpty(dic["android.disableDepthAndStencilBuffers"]))
                    PlayerSettings.Android.disableDepthAndStencilBuffers = System.Convert.ToBoolean(dic["android.disableDepthAndStencilBuffers"]);
                if (dic.ContainsKey("android.forceInternetPermission") && !string.IsNullOrEmpty(dic["android.forceInternetPermission"]))
                    PlayerSettings.Android.forceInternetPermission = System.Convert.ToBoolean(dic["android.forceInternetPermission"]);
                if (dic.ContainsKey("android.forceSDCardPermission") && !string.IsNullOrEmpty(dic["android.forceSDCardPermission"]))
                    PlayerSettings.Android.forceSDCardPermission = System.Convert.ToBoolean(dic["android.forceSDCardPermission"]);
                if (dic.ContainsKey("android.keystoreName") && !string.IsNullOrEmpty(dic["android.keystoreName"]))
                    PlayerSettings.Android.keystoreName = Path.Combine(Application.dataPath, dic["android.keystoreName"]);
                if (dic.ContainsKey("android.keystorePass") && !string.IsNullOrEmpty(dic["android.keystorePass"]))
                    PlayerSettings.Android.keystorePass = dic["android.keystorePass"];
                if (dic.ContainsKey("android.keyaliasName") && !string.IsNullOrEmpty(dic["android.keyaliasName"]))
                    PlayerSettings.Android.keyaliasName = dic["android.keyaliasName"];
                if (dic.ContainsKey("android.keyaliasPass") && !string.IsNullOrEmpty(dic["android.keyaliasPass"]))
                    PlayerSettings.Android.keyaliasPass = dic["android.keyaliasPass"];
                if (dic.ContainsKey("android.maxAspectRatio") && !string.IsNullOrEmpty(dic["android.maxAspectRatio"]))
                    PlayerSettings.Android.maxAspectRatio = System.Convert.ToSingle(dic["android.maxAspectRatio"]);
                if (dic.ContainsKey("android.minSdkVersion") && !string.IsNullOrEmpty(dic["android.minSdkVersion"]))
                    PlayerSettings.Android.minSdkVersion = (AndroidSdkVersions)System.Convert.ToInt32(dic["android.minSdkVersion"]);
                if (dic.ContainsKey("android.preferredInstallLocation") && !string.IsNullOrEmpty(dic["android.preferredInstallLocation"]))
                    PlayerSettings.Android.preferredInstallLocation = (AndroidPreferredInstallLocation)System.Convert.ToInt32(dic["android.preferredInstallLocation"]);
                if (dic.ContainsKey("android.showActivityIndicatorOnLoading") && !string.IsNullOrEmpty(dic["android.showActivityIndicatorOnLoading"]))
                    PlayerSettings.Android.showActivityIndicatorOnLoading = (AndroidShowActivityIndicatorOnLoading)System.Convert.ToInt32(dic["android.showActivityIndicatorOnLoading"]);
                if (dic.ContainsKey("android.splashScreenScale") && !string.IsNullOrEmpty(dic["android.splashScreenScale"]))
                    PlayerSettings.Android.splashScreenScale = (AndroidSplashScreenScale)System.Convert.ToInt32(dic["android.splashScreenScale"]);
                if (dic.ContainsKey("android.targetDevice") && !string.IsNullOrEmpty(dic["android.targetDevice"]))
                    PlayerSettings.Android.targetDevice = (AndroidTargetDevice)System.Convert.ToInt32(dic["android.targetDevice"]);
                if (dic.ContainsKey("android.targetSdkVersion") && !string.IsNullOrEmpty(dic["android.targetSdkVersion"]))
                    PlayerSettings.Android.targetSdkVersion = (AndroidSdkVersions)System.Convert.ToInt32(dic["android.targetSdkVersion"]);
                if (dic.ContainsKey("android.useAPKExpansionFiles") && !string.IsNullOrEmpty(dic["android.useAPKExpansionFiles"]))
                    PlayerSettings.Android.useAPKExpansionFiles = System.Convert.ToBoolean(dic["android.useAPKExpansionFiles"]);
                UnityEngine.Debug.Log(outputPath);
                BuildPipeline.BuildPlayer(sceneList.ToArray(), outputPath, BuildTarget.Android, BuildOptions.None);
            }
            else if (platform == (int)BuildTarget.iOS)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);

                if (dic.ContainsKey("ios.allowHTTPDownload") && !string.IsNullOrEmpty(dic["ios.allowHTTPDownload"]))
                    PlayerSettings.iOS.allowHTTPDownload = System.Convert.ToBoolean(dic["ios.allowHTTPDownload"]);
                if (dic.ContainsKey("ios.appInBackgroundBehavior") && !string.IsNullOrEmpty(dic["ios.appInBackgroundBehavior"]))
                    PlayerSettings.iOS.appInBackgroundBehavior = (iOSAppInBackgroundBehavior)System.Convert.ToInt32(dic["ios.appInBackgroundBehavior"]);
                if (dic.ContainsKey("ios.appleDeveloperTeamID") && !string.IsNullOrEmpty(dic["ios.appleDeveloperTeamID"]))
                    PlayerSettings.iOS.appleDeveloperTeamID = dic["ios.appleDeveloperTeamID"];
                if (dic.ContainsKey("ios.appleEnableAutomaticSigning") && !string.IsNullOrEmpty(dic["ios.appleEnableAutomaticSigning"]))
                    PlayerSettings.iOS.appleEnableAutomaticSigning = System.Convert.ToBoolean(dic["ios.appleEnableAutomaticSigning"]);
                if (dic.ContainsKey("ios.applicationDisplayName") && !string.IsNullOrEmpty(dic["ios.applicationDisplayName"]))
                    PlayerSettings.iOS.applicationDisplayName = dic["ios.applicationDisplayName"];
                if (dic.ContainsKey("ios.backgroundModes") && !string.IsNullOrEmpty(dic["ios.backgroundModes"]))
                    PlayerSettings.iOS.backgroundModes = (iOSBackgroundMode)System.Convert.ToInt32(dic["ios.backgroundModes"]);
                if (dic.ContainsKey("ios.buildNumber") && !string.IsNullOrEmpty(dic["ios.buildNumber"]))
                    PlayerSettings.iOS.buildNumber = dic["ios.buildNumber"];
                if (dic.ContainsKey("ios.cameraUsageDescription") && !string.IsNullOrEmpty(dic["ios.cameraUsageDescription"]))
                    PlayerSettings.iOS.cameraUsageDescription = dic["ios.cameraUsageDescription"];
                if (dic.ContainsKey("ios.forceHardShadowsOnMetal") && !string.IsNullOrEmpty(dic["ios.forceHardShadowsOnMetal"]))
                    PlayerSettings.iOS.forceHardShadowsOnMetal = System.Convert.ToBoolean(dic["ios.forceHardShadowsOnMetal"]);
                if (dic.ContainsKey("ios.iOSManualProvisioningProfileID") && !string.IsNullOrEmpty(dic["ios.iOSManualProvisioningProfileID"]))
                    PlayerSettings.iOS.iOSManualProvisioningProfileID = dic["ios.iOSManualProvisioningProfileID"];
                if (dic.ContainsKey("ios.locationUsageDescription") && !string.IsNullOrEmpty(dic["ios.locationUsageDescription"]))
                    PlayerSettings.iOS.locationUsageDescription = dic["ios.locationUsageDescription"];
                if (dic.ContainsKey("ios.microphoneUsageDescription") && !string.IsNullOrEmpty(dic["ios.microphoneUsageDescription"]))
                    PlayerSettings.iOS.microphoneUsageDescription = dic["ios.microphoneUsageDescription"];
                if (dic.ContainsKey("ios.prerenderedIcon") && !string.IsNullOrEmpty(dic["ios.prerenderedIcon"]))
                    PlayerSettings.iOS.prerenderedIcon = System.Convert.ToBoolean(dic["ios.prerenderedIcon"]);
                if (dic.ContainsKey("ios.requiresFullScreen") && !string.IsNullOrEmpty(dic["ios.requiresFullScreen"]))
                    PlayerSettings.iOS.requiresFullScreen = System.Convert.ToBoolean(dic["ios.requiresFullScreen"]);
                if (dic.ContainsKey("ios.requiresPersistentWiFi") && !string.IsNullOrEmpty(dic["ios.requiresPersistentWiFi"]))
                    PlayerSettings.iOS.requiresPersistentWiFi = System.Convert.ToBoolean(dic["ios.requiresPersistentWiFi"]);
                if (dic.ContainsKey("ios.scriptCallOptimization") && !string.IsNullOrEmpty(dic["ios.scriptCallOptimization"]))
                    PlayerSettings.iOS.scriptCallOptimization = (ScriptCallOptimizationLevel)System.Convert.ToInt32(dic["ios.scriptCallOptimization"]);
                if (dic.ContainsKey("ios.sdkVersion") && !string.IsNullOrEmpty(dic["ios.sdkVersion"]))
                    PlayerSettings.iOS.sdkVersion = (iOSSdkVersion)System.Convert.ToInt32(dic["ios.sdkVersion"]);
                if (dic.ContainsKey("ios.showActivityIndicatorOnLoading") && !string.IsNullOrEmpty(dic["ios.showActivityIndicatorOnLoading"]))
                    PlayerSettings.iOS.showActivityIndicatorOnLoading = (iOSShowActivityIndicatorOnLoading)System.Convert.ToInt32(dic["ios.showActivityIndicatorOnLoading"]);
                if (dic.ContainsKey("ios.statusBarStyle") && !string.IsNullOrEmpty(dic["ios.statusBarStyle"]))
                    PlayerSettings.iOS.statusBarStyle = (iOSStatusBarStyle)System.Convert.ToInt32(dic["ios.statusBarStyle"]);
                if (dic.ContainsKey("ios.targetDevice") && !string.IsNullOrEmpty(dic["ios.targetDevice"]))
                    PlayerSettings.iOS.targetDevice = (iOSTargetDevice)System.Convert.ToInt32(dic["ios.targetDevice"]);
                if (dic.ContainsKey("ios.targetOSVersionString") && !string.IsNullOrEmpty(dic["ios.targetOSVersionString"]))
                    PlayerSettings.iOS.targetOSVersionString = dic["ios.targetOSVersionString"];

                string filepath = outputPath;
                FileInfo fileInfo = new FileInfo(filepath);
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                BuildPipeline.BuildPlayer(sceneList.ToArray(), filepath, BuildTarget.iOS, BuildOptions.None);
            }
            else if (platform == (int)BuildTarget.StandaloneWindows64)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
                string filepath = outputPath + ".exe";
                FileInfo fileInfo = new FileInfo(filepath);
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                BuildPipeline.BuildPlayer(sceneList.ToArray(), filepath, BuildTarget.StandaloneWindows64, BuildOptions.None);
            }
            else if (platform == (int)BuildTarget.StandaloneOSXIntel64)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSXIntel64);
                string filepath = outputPath + ".exe";
                FileInfo fileInfo = new FileInfo(filepath);
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                BuildPipeline.BuildPlayer(sceneList.ToArray(), filepath, BuildTarget.StandaloneOSXIntel64, BuildOptions.None);
            }
        }
        public static void Check()
        {
            var dic = GetCommandArgs();
            var records = CodeStage.Maintainer.Issues.IssuesFinder.StartSearch(false);
            using (StreamWriter sr = File.CreateText(dic["maintainer_filePath"]))
            {
                sr.Write(CodeStage.Maintainer.ReportsBuilder.GenerateReport(CodeStage.Maintainer.Issues.IssuesFinder.MODULE_NAME, records));
            }
        }
        public static void BuildDiffSource()
        {
            new SrcBuilder.Bundle.BundleBuildManager().BuildPackage();
            new SrcBuilder.Lua.LuaPackageManager().ToPackage();
            KeyPairsGen();
            var version = UpdateVersion();
            new ToPackZip(m_config).PackDiff(version);
        }
        private static void KeyPairsGen()
        {
            var keyPath = Path.Combine(Application.dataPath, "../" + m_config.sourceKeyFileName);
            var publicKeyPath = Path.Combine(Application.dataPath, "../" + m_config.sourceKeyFileName) + ".pub";
            if (File.Exists(keyPath) || File.Exists(publicKeyPath))
            {
                UnityEngine.Debug.Log("key pairs existed!");
                return;
            }
            var rsa = new RSACryptoServiceProvider();
            File.WriteAllText(keyPath, rsa.ToXmlString(true));
            File.WriteAllText(publicKeyPath, Convert.ToBase64String(rsa.ExportCspBlob(false)));
        }
        private static string UpdateVersion()
        {
            var dic = GetCommandArgs();
            string[] args = System.Environment.GetCommandLineArgs();
            string versionMode = dic["mode"];
            string platform = dic["platform"];
            string branch = dic["branch"];
            string channel = dic["channel"];
            string url = dic["url"];
            int major = 0;
            int milestone = 0;
            int weekly = 0;
            int revision = 0;
            string versionPath = Path.Combine(Application.dataPath, "../Dist/" + platform + "/version");
            if (File.Exists(versionPath))
            {
                var lines = File.ReadAllLines(versionPath);
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i == 2)
                    {
                        var vsplit = lines[i].Split('.');
                        major = Convert.ToInt32(vsplit[0]);
                        milestone = Convert.ToInt32(vsplit[1]);
                        weekly = Convert.ToInt32(vsplit[2]);
                        revision = Convert.ToInt32(vsplit[3]);
                        break;
                    }
                }
                if (versionMode.ToLower() == "major")
                {
                    major++;
                    milestone = 0;
                    weekly = 0;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "milestone")
                {
                    milestone++;
                    weekly = 0;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "weekly")
                {
                    weekly++;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "revision")
                {
                    revision++;
                }
                else
                {
                    UnityEngine.Debug.LogError("Dont support versionMode:" + versionMode);
                }
            }
            else
            {
                if (versionMode.ToLower() == "major")
                {
                    major = 0;
                    milestone = 0;
                    weekly = 0;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "milestone")
                {
                    major = 0;
                    milestone = 1;
                    weekly = 0;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "weekly")
                {
                    major = 0;
                    milestone = 1;
                    weekly = 1;
                    revision = 0;
                }
                else if (versionMode.ToLower() == "revision")
                {
                    major = 0;
                    milestone = 1;
                    weekly = 0;
                    revision = 1;
                }
                else
                {
                    UnityEngine.Debug.LogError("Dont support versionMode:" + versionMode);
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(branch);
            sb.AppendLine(channel);
            string version = major + "." + milestone + "." + weekly + "." + revision;
            sb.AppendLine(version);
            int ind = url.LastIndexOf("/");
            sb.AppendLine(ind == url.Length - 1 ? url + platform : url + "/" + platform);
            File.WriteAllText(versionPath, sb.ToString());
            return version;
        }
        private static void InjectAppconfig()
        {
            string appconfigPath = Application.streamingAssetsPath+"/appconfig";
            var dic = GetCommandArgs();
            string[] args = System.Environment.GetCommandLineArgs();
            string branch = dic["branch"];
            string channel = dic["channel"];
            string url = dic["url"];
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(branch);
            sb.AppendLine(channel);
            sb.AppendLine(url);
            File.WriteAllText(appconfigPath, sb.ToString());
        }
        private static Dictionary<string, string> GetCommandArgs()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string[] args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Contains("="))
                {
                    var split = args[i].Split('=');
                    if (dic.ContainsKey(split[0]))
                    {
                        UnityEngine.Debug.LogError("An item with the same key has already been added.   " + split[0]);
                    }
                    else
                    {
                        dic.Add(split[0], split[1]);
                    }
                }
            }
            return dic;
        }
        private static void InitConfig()
        {
            if(m_config!=null)return;
            var files = Directory.GetFiles(Application.dataPath, "BuilderSettings.asset", SearchOption.AllDirectories);
            if (files.Length <= 0)
            {
                UnityEngine.Debug.LogError("BundleSettings.asset is not exsts");
            }
            else
            {
                string file = files[0];
                file = file.Replace("\\", "/");
                string configPath = "Assets" + file.Replace(Application.dataPath, "");
                m_config = (BuilderConfig)AssetDatabase.LoadAssetAtPath(configPath, typeof(BuilderConfig));
            }
        }
    }
}
