﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace SrcBuilder.Builder
{
    public class ToPackZip
    {
        private BuilderConfig m_config;
        private string m_bundlePath;
        private string m_luaPath;
        private string m_rootPath;
        private bool m_hasKey;
        private string m_keyPath;
        private SHA1 m_sha;
        private RSACryptoServiceProvider m_rsa;

        public ToPackZip(BuilderConfig config)
        {
            m_config=config;
            m_bundlePath = Path.Combine(Application.dataPath, string.Format(m_config.bundlePath, (int)EditorUserBuildSettings.activeBuildTarget));
            m_luaPath = Path.Combine(Application.dataPath, string.Format(m_config.luaPath, (int)EditorUserBuildSettings.activeBuildTarget));
            m_rootPath = Path.Combine(Application.dataPath, string.Format(m_config.zipPath, (int)EditorUserBuildSettings.activeBuildTarget));
            m_keyPath = Path.Combine(Application.dataPath, "../" + m_config.sourceKeyFileName);
            if (File.Exists(m_keyPath))
            {
                m_hasKey = true;
                m_sha = new SHA1CryptoServiceProvider();
                m_rsa = new RSACryptoServiceProvider();
                m_rsa.FromXmlString(File.ReadAllText(m_keyPath));
            }
        }
        public string PackEntire()
        {
            string zipFolder = Path.Combine(m_rootPath, "__compress__");
            CreateDirectory(zipFolder);

            CopyEntireToFolder(m_luaPath, m_bundlePath, zipFolder);

            Lens.Framework.Utility.Zip7Utility.CompressFolder(zipFolder, Path.Combine(m_rootPath, m_config.entireZipName));
            DirectoryInfo dir = new DirectoryInfo(zipFolder);
            dir.Delete(true);
            return Path.Combine(m_rootPath, m_config.entireZipName); 
        }
        public void PackDiff(string zipName)
        {
            if (string.IsNullOrEmpty(zipName))
            {
                zipName = m_config.diffZipName;
            }
            string zipFolder = Path.Combine(m_rootPath, "__compress__");
            string bundleDistFolder = Path.Combine(zipFolder, "Bundle");
            CreateDirectory(bundleDistFolder);

            CopyDiffToFolder(m_bundlePath, m_config.packageFileName, bundleDistFolder);
            File.Copy(Path.Combine(m_bundlePath, m_config.bundleManifestName), Path.Combine(bundleDistFolder, m_config.bundleManifestName));

            string luaDistFolder = Path.Combine(zipFolder, "Lua");
            CreateDirectory(luaDistFolder);
            CopyDiffToFolder(m_luaPath, m_config.packageFileName, luaDistFolder);

            Lens.Framework.Utility.Zip7Utility.CompressFolder(zipFolder, Path.Combine(m_rootPath, zipName));
            DirectoryInfo dir = new DirectoryInfo(zipFolder);
            dir.Delete(true);
        }
        private void CopyDiffToFolder(string sourceFolder, string packageFileName, string distFolder)
        {
            var sourcePackagePath = Path.Combine(sourceFolder, packageFileName);
            var distPackagePath = Path.Combine(distFolder, packageFileName);
            List<string> copFiles = new List<string>();
            using (FileStream fileStream = new FileStream(sourcePackagePath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader sr = new StreamReader(fileStream))
                {
                    string s;
                    StringBuilder str = new StringBuilder();
                    int i = 0;
                    while ((s = sr.ReadLine()) != null)
                    {
                        str.Clear();
                        if (s == "add")
                        {
                            i = 1;
                            continue;
                        }
                        else if (s == "modify")
                        {
                            i = 2;
                            continue;
                        }
                        else if (s == "delete")
                        {
                            i = 3;
                            continue;
                        }
                        if (i == 1 || i == 2)
                        {
                            copFiles.Add(s.ToString());
                        }
                    }
                }
            }
            for (int i = 0; i < copFiles.Count; i++)
            {
                string sourceFile = Path.Combine(sourceFolder, copFiles[i]);
                string distFile = Path.Combine(distFolder, copFiles[i]);
                FileInfo distFileInfo = new FileInfo(distFile);
                if (!distFileInfo.Directory.Exists)
                {
                    distFileInfo.Directory.Create();
                }
                if (m_hasKey)
                {
                    CopyFileSignature(sourceFile, distFile);
                }
                else
                {
                    File.Copy(sourceFile, distFile);
                }
            }
            if (m_hasKey)
            {
                CopyFileSignature(sourcePackagePath, distPackagePath);
            }
            else
            {
                File.Copy(sourcePackagePath, distPackagePath);
            }
        }
        private void CopyEntireToFolder(string luaSourceFolder, string bundleSourceFolder, string distFolder)
        {
            var bundleFiles = Directory.GetFiles(bundleSourceFolder, "*", SearchOption.AllDirectories);
            for (int i = 0; i < bundleFiles.Length; i++)
            {
                CopyTo(bundleFiles[i], distFolder);
            }
            var luaFiles = Directory.GetFiles(luaSourceFolder, "*", SearchOption.AllDirectories);
            for (int i = 0; i < luaFiles.Length; i++)
            {
                CopyTo(luaFiles[i], distFolder);
            }
        }
        private void CopyTo(string f, string distFolder)
        {
            f = f.Replace("\\", "/");
            FileInfo info = new FileInfo(f);
            if (info.Extension == ".manifest") return;
            if (info.Extension == ".meta") return;

            string a = m_rootPath.Replace("\\", "/");
            string tar = Path.Combine(distFolder, f.Replace(a + "/", ""));
            FileInfo tarInfo = new FileInfo(tar);
            if (!tarInfo.Directory.Exists)
            {
                tarInfo.Directory.Create();
            }
            if (m_hasKey)
            {
                CopyFileSignature(f, tar);
            }
            else
            {
                File.Copy(f, tar);
            }
        }
        private void CreateDirectory(string folder)
        {
            if (Directory.Exists(folder))
            {
                Directory.Delete(folder, true);
            }
            Directory.CreateDirectory(folder);
        }
        private void CopyFileSignature(string sourceFile,string distFile)
        {
            byte[] filecontent;
            using (FileStream fs = new FileStream(sourceFile,FileMode.Open,FileAccess.Read))
            {
                filecontent = new byte[fs.Length];
                fs.Read(filecontent, 0, filecontent.Length);
            }
            byte[] sig = m_rsa.SignData(filecontent, m_sha);
            using (FileStream fs = new FileStream(distFile, FileMode.Create))
            {
                fs.Write(sig, 0, sig.Length);
                fs.Write(filecontent, 0, filecontent.Length);
            }
        }
    }
}