﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SrcBuilder.Builder
{
    public class BuilderConfig : ScriptableObject
    {
        public string sourceKeyFileName = "key_ras_source";
        public string bundlePath = "../Dist/{0}/Bundle";
        public string luaPath = "../Dist/{0}/Lua";
        public string zipPath = "../Dist/{0}";
        public string bundleManifestName = "manifest.bytes";
        public string packageFileName = "package";
        public string entireZipName = "entirePack.7z";
        public string diffZipName = "diffPack.7z";
    }
}