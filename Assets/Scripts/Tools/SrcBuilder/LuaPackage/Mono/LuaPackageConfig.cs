﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SrcBuilder.Lua
{
    public class LuaPackageConfig : ScriptableObject
    {
        public string luaPath = "../Lua";
        public string srcFolder = "src";
        public string luaModulesFolder = "lua_modules";
        public string distPath = "../Dist";
        public string distFolderName = "Lua";
        public string diffFileName = "package";
    }
}