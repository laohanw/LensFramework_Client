﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace SrcBuilder.Lua
{
    public class LuaPackageManager
    {
        public LuaPackageConfig config;

        private string m_luaFolder;
        private string m_distFolder;
        private string m_srcFolder;
        private string m_luaModulesFolder;
        private string m_diffFilePath;
        private Dictionary<string, string> m_srcPackConfig;
        private Dictionary<string, string> m_srcModulesPackConfig;
        private Dictionary<string, string> m_distPackConfig;
        private List<string> m_addfiles = new List<string>();
        private List<string> m_deletefiles = new List<string>();
        private List<string> m_modifyfiles = new List<string>();
        
        public LuaPackageManager()
        {
            var files = Directory.GetFiles(Application.dataPath, "LuaPackageSettings.asset", SearchOption.AllDirectories);
            if (files.Length <= 0)
            {
                UnityEngine.Debug.LogError("LuaPackageConfig.asset is not exsts");
            }
            else
            {
                string file = files[0];
                file = file.Replace("\\", "/");
                string configPath = "Assets" + file.Replace(Application.dataPath, "");
                config = (LuaPackageConfig)AssetDatabase.LoadAssetAtPath(configPath, typeof(LuaPackageConfig));
            }
            m_luaFolder= Path.Combine(Application.dataPath, config.luaPath).Replace("\\", "/"); ;
            m_srcFolder = Path.Combine(Application.dataPath, config.luaPath+"/"+config.srcFolder).Replace("\\", "/");
            m_luaModulesFolder = Path.Combine(Path.Combine(Application.dataPath, config.luaPath + "/" + config.luaModulesFolder).Replace("\\", "/"));
            m_distFolder = Path.Combine(Application.dataPath, config.distPath, ((int)EditorUserBuildSettings.activeBuildTarget).ToString(), config.distFolderName).Replace("\\", "/");
            m_diffFilePath = Path.Combine(Application.dataPath, config.distPath, ((int)EditorUserBuildSettings.activeBuildTarget).ToString(), config.distFolderName, config.diffFileName).Replace("\\", "/");
        }
        public void ToPackage()
        {
            m_srcPackConfig = ReadPackageConfig(m_srcFolder);
            m_srcModulesPackConfig = ReadPackageConfig(m_luaModulesFolder, "lua_modules/");
            m_distPackConfig = ReadPackageConfig(m_distFolder);
            CompareDiff();
            DoPackage(m_srcFolder, m_distFolder);
            for (int i = 0; i < m_deletefiles.Count; i++)
            {
                string fil = Path.Combine(m_distFolder, m_deletefiles[i]);
                if (File.Exists(fil))
                {
                    File.Delete(fil);
                }
            }
            SaveToPackageInfo();
        }
        void DoPackage(string from, string to)
        {
            if (!Directory.Exists(to))
            {
                Directory.CreateDirectory(to);
            }
            var files = Directory.GetFiles(from, "*");
            foreach (var filename in files)
            {
                var fil = filename.Replace("\\", "/");
                var f = fil.Replace(m_srcFolder + "/", "");
                if (m_addfiles.Contains(f) || m_modifyfiles.Contains(f))
                {
                    File.Copy(fil, to + "/" + Path.GetFileName(fil), true);
                }
                else
                {
                    var p = fil.Replace(m_luaFolder + "/", "");
                    if (m_addfiles.Contains(p) || m_modifyfiles.Contains(p))
                    {
                        File.Copy(fil, to + "/" + Path.GetFileName(fil), true);
                    }
                }
            }
            foreach (var dir in Directory.GetDirectories(from))
            {
                DoPackage(dir, to + "/" + new DirectoryInfo(dir).Name);
            }
        }
        Dictionary<string, string> ReadPackageConfig(string folder,string preEx="")
        {
            var dic = new Dictionary<string, string>();
            if (Directory.Exists(folder))
            {
                var files = Directory.GetFiles(folder, "*", SearchOption.AllDirectories);
                for (int i = 0; i < files.Length; i++)
                {
                    var f = files[i];
                    f = f.Replace("\\", "/");
                    dic.Add(preEx+f.Replace(folder + "/", ""), GetFileHash(f));
                }
            }
            return dic;
        }
        string GetFileHash(string path)
        {
            var hash = SHA1.Create();
            string h = null;
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                byte[] hashByte = hash.ComputeHash(stream);
                h = BitConverter.ToString(hashByte).Replace("-", "");
            }
            return h;
        }
        void CompareDiff()
        {
            foreach (var src in m_srcPackConfig)
            {
                bool has = false;
                foreach (var dist in m_distPackConfig)
                {
                    if (src.Key == dist.Key)
                    {
                        if (src.Value != dist.Value)
                        {
                            m_modifyfiles.Add(src.Key.Replace("\\", "/"));
                        }
                        has = true;
                    }
                }
                if (!has)
                {
                    m_addfiles.Add(src.Key.Replace("\\", "/"));
                }
            }
            foreach (var src in m_srcModulesPackConfig)
            {
                if (src.Key.Contains("package.lua") || src.Key.Contains("README.md"))
                {
                    continue;
                }
                bool has = false;
                foreach (var dist in m_distPackConfig)
                {
                    if (dist.Key.Contains(src.Key))
                    {
                        if (src.Value != dist.Value)
                        {
                            m_modifyfiles.Add(src.Key.Replace("\\", "/"));
                        }
                        has = true;
                    }
                }
                if (!has)
                {
                    m_addfiles.Add(src.Key.Replace("\\", "/"));
                }
            }
            foreach (var dist in m_distPackConfig)
            {
                bool has = false;
                foreach (var src in m_srcPackConfig)
                {
                    if (src.Key == dist.Key)
                    {
                        has = true;
                    }
                }
                foreach (var src in m_srcModulesPackConfig)
                {
                    if (src.Key == dist.Key)
                    {
                        has = true;
                    }
                }
                if (!has)
                {
                    var f = dist.Key.Replace("\\", "/");
                    if (!f.Contains(config.diffFileName))
                    {
                        m_deletefiles.Add(f);
                    }
                }
            }
        }
        void SaveToPackageInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("add");
            for (int i = 0; i < m_addfiles.Count; i++)
            {
                sb.AppendLine(m_addfiles[i]);
            }
            sb.AppendLine("modify");
            for (int i = 0; i < m_modifyfiles.Count; i++)
            {
                sb.AppendLine(m_modifyfiles[i]);
            }
            sb.AppendLine("delete");
            for (int i = 0; i < m_deletefiles.Count; i++)
            {
                sb.AppendLine(m_deletefiles[i]);
            }
            FileInfo filI = new FileInfo(m_diffFilePath);
            if (!filI.Directory.Exists)
            {
                filI.Directory.Create();
            }
            using (FileStream st = new FileStream(m_diffFilePath, FileMode.Create, FileAccess.Write))
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                st.Write(bytes, 0, bytes.Length);
            }
        }
    }
}