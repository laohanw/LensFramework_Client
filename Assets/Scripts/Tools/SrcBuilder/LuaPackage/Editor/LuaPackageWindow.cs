﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.IO;
using System.Text;
using System;
using System.Security.Cryptography;

namespace SrcBuilder.Lua
{
    public class LuaPackageWindow : EditorWindow
    {
        private static LuaPackageWindow m_instance;
        private string m_keyPath;
        private string m_publicKeyPath;
        [MenuItem("Tools/SrcBuilder/Lua/Package Window")]
        static void Init()
        {
            m_instance = EditorWindow.GetWindow<LuaPackageWindow>("LuaPackageWindow");
            m_instance.Initialize();
            m_instance.Show();
        }
        [MenuItem("Tools/SrcBuilder/Lua/To Package")]
        static void ToPackage()
        {
            new LuaPackageManager().ToPackage();
            EditorUtility.DisplayDialog("Information", "Success", "Ok");
        }
        void Initialize()
        {
        }
        void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Settings",EditorStyles.toolbarButton))
                {
                    PingObject(new LuaPackageManager().config);
                }
            }EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("ToPackage"))
            {
                new LuaPackageManager().ToPackage();
                EditorUtility.DisplayDialog("Information", "Success", "Ok");
            }
        }
        void PingObject(UnityEngine.Object obj)
        {
            int instanceID = obj.GetInstanceID();
            EditorGUIUtility.PingObject(instanceID);
            Selection.activeObject = obj;
        }
    }
}