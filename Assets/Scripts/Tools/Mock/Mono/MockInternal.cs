﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ProtocolSimulation
{
    public static class MockInternal
    {
        public static void SendMessage(int msgId,object msg)
        {
            UnityEngine.Debug.Log("发送协议到服务器:"+msgId+"   "+msg);
        }
        public static void PushMessage(int msgId, object msg)
        {
            UnityEngine.Debug.Log("服务器发送协议客户端："+msgId+"   "+msg);
        }
    }
}
