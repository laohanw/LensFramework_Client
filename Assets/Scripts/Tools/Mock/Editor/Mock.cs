﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ProtocolSimulation
{
    public class Mock
    {
        public static string CONFIG_ASSET_PATH = "";
        public MockConfig config { get; private set; }
        public List<MockMessage> list { get; private set; }
        
        public Mock()
        {
            if (!File.Exists(CONFIG_ASSET_PATH))
            {
                string[] files = Directory.GetFiles(Application.dataPath, "protocolSimulationSettings.asset", SearchOption.AllDirectories);
                CONFIG_ASSET_PATH ="Assets"+files[0].Replace("\\","/").Replace(Application.dataPath,"");
            }
            config = (MockConfig)AssetDatabase.LoadAssetAtPath(CONFIG_ASSET_PATH, typeof(ScriptableObject));
            list = new List<MockMessage>();
            var assemblys = AppDomain.CurrentDomain.GetAssemblies();
            for (int i = 0; i < assemblys.Length; i++)
            {
                var assembly = assemblys[i];
                if (assembly.GetName().Name==config.assembly)
                {
                    var types = assembly.GetTypes();
                    for (int j = 0; j < types.Length; j++)
                    {
                        var type = types[j];
                        if (type.Namespace==config.nameSpace)
                        {
                            list.Add(new MockMessage(type));
                        }
                    }
                }
            }
        }
        public void ClientToServer(int msgId,MockMessage msg)
        {
            MockInternal.SendMessage(msgId, msg.Serialize());
        }
        public void ServerToClient(int msgId, MockMessage msg)
        {
            MockInternal.PushMessage(msgId, msg.Serialize());
        }
    }
}