﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProtocolSimulation
{
    public class MockMember
    {
        public string name;
        public object value;
        public Type type;
        public Type genericType;
        private PropertyInfo m_proInfo;
        public MockMember(PropertyInfo prop)
        {
            m_proInfo = prop;
            this.name = prop.Name;
            this.type = prop.PropertyType;
            if (type.IsGenericType)
            {
                var paramType=type.GetGenericArguments()[0];
                genericType = paramType;
                if (IsClass(paramType))
                {
                    value = new List<MockMessage>();
                }
                else
                {
                    value = new List<MockMember>();
                }
            }
            else if(IsClass(type))
            {
                value = new MockMessage(type);
            }
        }
        public MockMember(Type type)
        {
            this.type = type;
        }
        public void SetValue(object obj)
        {
            if (type.IsGenericType)
            {
                var prop=obj.GetType().GetProperty(name,BindingFlags.Instance | BindingFlags.Public);
                var methodInfo= prop.PropertyType.GetMethod("Add", BindingFlags.Instance | BindingFlags.Public);
                if (IsClass(genericType))
                {
                    var msgList = (List<MockMessage>)value;
                    for (int i = 0; i < msgList.Count; i++)
                    {
                        var o = msgList[i].Serialize();
                        methodInfo.Invoke(prop.GetValue(obj,null), new object[] { o });
                    }
                }
                else
                {
                    var msgList = (List<MockMember>)value;
                    for (int i = 0; i < msgList.Count; i++)
                    {
                        methodInfo.Invoke(prop.GetValue(obj, null), new object[] { msgList[i].value });
                    }
                }
            }
            else if (IsClass(type))
            {
                var l = (MockMessage)value;
                var v = l.Serialize();
                m_proInfo.SetValue(obj, v, null);
            }
            else
            {
                m_proInfo.SetValue(obj, value, null);
            }
        }
        bool IsClass(Type paramType)
        {
            if (paramType == typeof(int) ||
                paramType == typeof(uint) ||
                paramType == typeof(long) ||
                paramType == typeof(ulong) ||
                paramType == typeof(string) ||
                paramType == typeof(float) ||
                paramType == typeof(double) ||
                paramType == typeof(bool))
            {
                return false;
            }
            else if (paramType.IsEnum)
            {
                return false;
            }
            else if (paramType.IsGenericType)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
