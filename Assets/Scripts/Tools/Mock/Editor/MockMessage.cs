﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolSimulation
{
    public class MockMessage
    {
        public string name;
        public string nameSpace;
        public List<MockMember> members;
        public Type type;
        public MockMessage(Type type)
        {
            this.type = type;
            name = type.Name;
            nameSpace = type.Namespace;
            members = new List<MockMember>();
            var props=type.GetProperties();
            for (int i = 0; i < props.Length; i++)
            {
                var prop = props[i];
                members.Add(new MockMember(prop));
            }
        }
        public object Serialize()
        {
            var cls = Activator.CreateInstance(type);
            for (int i = 0; i < members.Count; i++)
            {
                members[i].SetValue(cls);
            }
            return cls;
        }
    }
}