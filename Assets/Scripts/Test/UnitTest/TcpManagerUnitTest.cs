﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using Framework.Managers;
    using ProtoBuf;
    using System.Net;

    public class TcpManagerUnitTest : MonoBehaviour
    {
        public string mainLua= "test/unitTest/tcpUnitTest";
        private TcpManager m_tcpManager;

        LuaManager m_luaManager;
        // Use this for initialization
        void Start()
        {
            m_tcpManager = TcpManager.GetInstance();
            m_tcpManager.Initialize();
            TcpManager.AddListener(EMsgType.Login, OnLogin);
            m_luaManager = LuaManager.GetInstance();
            m_luaManager.Initialize(new Framework.Managers.LuaManager.MyOptions(){
                luaUrl=Gameplay.Core.Define.luaUrl
            });
            m_luaManager.Preload();
            m_luaManager.Start(mainLua);
            var luaNetTable = LuaManager.GetTable<L_Net>("L_Net");
            TcpManager.AddListener(ENetState.Connected, luaNetTable.C_OnConnected);
            TcpManager.AddListener(ENetState.Disconnected, luaNetTable.C_OnDisconnect);
            TcpManager.AddListenAll(EPackType.LuaProto, luaNetTable.C_OnReceive);
            TcpManager.AddListenAll(EPackType.Bytes, luaNetTable.C_OnReceive);
        }

        // Update is called once per frame
        void Update()
        {
            m_luaManager.Update();
            m_tcpManager.Update();
        }
        void OnGUI()
        {
            if (GUILayout.Button("Connect"))
            {
                TcpManager.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 18001));
            }
            if (GUILayout.Button("ReConnect"))
            {
                TcpManager.Reconnect();
            }
            if (GUILayout.Button("Send"))
            {
                TcpManager.Send((int)EMsgType.Login, new byte[] { 1, 1, 1 ,1});
            }
            if (GUILayout.Button("Disconnect"))
            {
                TcpManager.Disconnect();
            }
        }
        void OnDestroy()
        {
            m_luaManager.Dispose();
            m_tcpManager.Dispose();
        }
        void OnLogin(IExtensible obj)
        {
            UnityEngine.Debug.Log("Login:" + obj);
        }
    }
}