﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

namespace Lens.UnitTest
{
	public class TempUnitTest : MonoBehaviour {
        public string sourceFile;
        public string distPath;
        public string sourceHash;
        public string distHash;
		// Use this for initialization
		void Start () {
            
		}
		
		// Update is called once per frame
		void Update () {

		}
        void OnGUI()
        {
            if (GUILayout.Button("Has Equal"))
            {
                sourceHash = GetFileHash(sourceFile);
                distHash = GetFileHash(distPath);
            }
        }

        string GetFileHash(string path)
        {
            var hash = SHA1.Create();
            string h = null;
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                byte[] hashByte = hash.ComputeHash(stream);
                h = BitConverter.ToString(hashByte).Replace("-", "");
            }
            return h;
        }

    }
}