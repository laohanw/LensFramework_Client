﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using Framework.Utility;
    public class Zip7UnitTest : MonoBehaviour
    {
        public string filePath = "";
        public string zipPath = "";
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnGUI()
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 210, 200, 140, 80), "LMZA异步压缩文件"))
            {
               Zip7Utility.CompressFileAsync(Application.dataPath + "/"+ filePath, Application.dataPath + "/"+ zipPath, null);
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 210, 320, 140, 80), "LMZA异步解压文件"))
            {
                Zip7Utility.DeCompressFileAsync(Application.dataPath + "/" + zipPath, Application.dataPath + "/" + filePath, null);
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 210, 440, 140, 80), "LMZA同步压缩文件"))
            {
                Zip7Utility.CompressFile(Application.dataPath + "/" + filePath, Application.dataPath + "/" + zipPath);
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 210, 560, 140, 80), "LMZA同步解压文件"))
            {
                Zip7Utility.DeCompressFile(Application.dataPath + "/" + zipPath, Application.dataPath + "/" + filePath);
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 210, 680, 140, 80), "LMZA同步压缩文件夹"))
            {
                Zip7Utility.CompressFolder( filePath, zipPath );
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 210, 800, 140, 80), "LMZA同步解压文件夹"))
            {
                Zip7Utility.DecompressFolder(zipPath, filePath   );
            }
        }
    }
}