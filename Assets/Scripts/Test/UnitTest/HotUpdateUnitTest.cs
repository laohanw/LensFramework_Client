﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Lens.UnitTest
{
    using Framework.Managers;
    using Framework.Core;
    public class HotUpdateUnitTest : MonoBehaviour
    {
        private class MyListener :UpdateManager.IListener{
            /// <summary>
            /// 首包解压缩
            /// </summary>
            void UpdateManager.IListener.ToInstallNew(){

            }
            /// <summary>
            /// 首包解压缩
            /// </summary>
            void UpdateManager.IListener.OnDecompressing(UpdateManager.DecompressingInfo info){

            }
            /// <summary>
            /// 差异包下载
            /// </summary>
            void UpdateManager.IListener.OnDownloading(UpdateManager.DownloadingInfo info){
                
            }
            /// <summary>
            /// 热更状态
            /// </summary>
            void UpdateManager.IListener.OnStatus(UpdateManager.EStatus status){
                
            }
            /// <summary>
            /// 热更错误
            /// </summary>
            void UpdateManager.IListener.OnError(UpdateManager.EErrorType error, string msg){
                
            }
            /// <summary>
            /// 下载确认框
            /// </summary>
            void UpdateManager.IListener.ToDownloadDialog(Action ok,Action cancel){
                ok();
            }
        }
        UpdateManager m_manager;
        // Use this for initialization
        void Start()
        {
#if RELEASE
            m_manager = UpdateManager.GetInstance();
            m_manager.setLogHandler(Log.i);
            m_manager.AddListener(new MyListener());
            m_manager.Initialize(new UpdateManager.MyOptions(){
                sourceUrl=GamePlay.Core.Define.sourceUrl,
                tempUrl=GamePlay.Core.Define.tempUrl
            });
            m_manager.Start();
#endif
        }

        // Update is called once per frame
        void Update()
        {
#if RELEASE
            m_manager.Update();
#endif
        }
        void OnDestroy()
        {
#if RELEASE
            m_manager.Dispose();
#endif
        }
    }
}