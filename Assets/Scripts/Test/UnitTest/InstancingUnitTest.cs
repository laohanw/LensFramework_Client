﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using System;
    using Framework.Libs;
    using System.Collections;

    public class InstancingUnitTest : MonoBehaviour
    {
        public InstancingComponent m_com;
        public int count;
        public EAnimationType animName;
        public List<InstancingComponent> m_list = new List<InstancingComponent>();
        public Animator sourceAnim;
        public int index;
        private InstancingCluster m_manager;
        void Start()
        {
            m_manager = InstancingCluster.GetInstance();
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    var obj = UnityEngine.Object.Instantiate(m_com.gameObject);
                    obj.transform.position = new Vector3(i,0,j)*1.8f;
                    obj.transform.eulerAngles = Vector3.zero;
                    m_list.Add(obj.GetComponent<InstancingComponent>());
                    var com = obj.GetComponent<InstancingComponent>();
                    com.Initialize(0, Color.white);
                }
            }
            StartCoroutine(RandomPlay());
        }
        void Update()
        {
            m_manager.Update();
        }
        void OnDestroy() {
            m_manager.Dispose();
        }
        void OnGUI()
        {
            if (GUILayout.Button("ShowShadow"))
            {
                m_list[index].ShowShadow();
            }
            if (GUILayout.Button("HideShadow"))
            {
                m_list[index].HideShadow();
            }
        }
        IEnumerator RandomPlay()
        {
            while (true)
            {
                yield return new UnityEngine.WaitForSeconds(UnityEngine.Random.Range(0, 2));
                for (int i = 0; i < m_list.Count; i++)
                {
                    m_list[i].PlayFusion(UnityEngine.Random.Range(0, 10));
                }
            }
        }
    }
}
