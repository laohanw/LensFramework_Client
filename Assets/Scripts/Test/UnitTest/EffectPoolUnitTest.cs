﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using System;
    using Framework.Libs;
    using Framework.Core;
    using Framework.Managers;

    public class EffectPoolUnitTest : MonoBehaviour
    {
        public List<GameObject> list = new List<GameObject>();
        public List<GameObject> listDelay = new List<GameObject>();
        EffectManager m_effectPool;
        private List<IEffect> usedTest = new List<IEffect>();
        private List<IEffect> usedDelayTest = new List<IEffect>();
        // Use this for initialization
        void Start()
        {
            m_effectPool = EffectManager.GetInstance();
            m_effectPool.loadHandler = OnLoad;
            m_effectPool.destroyHandler = ONDestroy;
            EffectManager.AddGroup(1);
            Store(()=> {
                InvokeRepeating("RandomAdd", 0.2f, 0.2f);
                InvokeRepeating("RandomAddDelay", 1.2f, 0.8f);
                InvokeRepeating("RandomRemove", 2f, 1.3f);
            });
        }

        // Update is called once per frame
        void Update()
        {
            m_effectPool.Update();
        }
        void OnDestroy()
        {
            m_effectPool.Dispose();
        }
        void Store(Framework.Core.EventHandler onStoreOver)
        {
            EffectStoreData[] arr = new EffectStoreData[list.Count+listDelay.Count];
            for (int i = 0; i < list.Count; i++)
            {
                EffectStoreData data = new EffectStoreData();
                data.count = 5;
                data.index = i;
                data.group = 0;
                arr[i] = data;
            }
            for (int i = 0; i < listDelay.Count; i++)
            {
                EffectStoreData data = new EffectStoreData();
                data.count = 5;
                data.index = i+list.Count;
                data.group = 1;
                arr[i + list.Count] = data;
            }
            EffectManager.Store(arr, onStoreOver);
        }
        void RandomAdd()
        {
            for (int m = 0; m < list.Count; m++)
            {
                usedTest.Add(EffectManager.Get(m,null,Vector3.zero,Quaternion.identity));
            }
        }
        void RandomAddDelay()
        {
            for (int m = 0; m < listDelay.Count; m++)
            {
                usedDelayTest.Add(EffectManager.Get(m+list.Count, null, Vector3.zero, Quaternion.identity, 1));
            }
        }
        void RandomRemove()
        {
            for (int i = 0; i < usedTest.Count; i++)
            {
                EffectManager.Free(usedTest[i]);
            }
            for (int i = 0; i < usedDelayTest.Count; i++)
            {
                EffectManager.Free(usedDelayTest[i],1000,1);
            }
        }
        void OnLoad(int id,DataHandler<UnityEngine.Object> handler)
        {
            if (id<list.Count)
            {
                GameObject obj = Instantiate(list[id]);
                handler(obj);
            }
            else
            {
                GameObject obj = Instantiate(listDelay[id-list.Count]);
                handler(obj);
            }
        }
        void ONDestroy(int id)
        {
            if (id < list.Count)
            {
                Destroy(list[id]);
            }
            else
            {
                Destroy(listDelay[id - list.Count]);
            }
        }
    }
}