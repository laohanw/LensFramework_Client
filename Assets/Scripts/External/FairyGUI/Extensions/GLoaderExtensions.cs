﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FairyGUI
{
    using Lens.Framework.Managers;
    public class GLoaderExtensions : GLoader
    {
        private static Dictionary<string, NTexture> texturesDic = new Dictionary<string, NTexture>();
        private static Dictionary<string, int> texturesCount = new Dictionary<string, int>();
        private static Dictionary<int, string> texturesUrl = new Dictionary<int, string>();
        private static Dictionary<string, UrlLoader> urlLoaderInfoDic = new Dictionary<string, UrlLoader>();
        
        public delegate void OnUrlTextureLoadFinish(string url);
        
        public static event OnUrlTextureLoadFinish OnUrlTextureLoadFinishEvent;

        public static void AddTextureCount(string url, int addNum)
        {
            if (texturesCount.ContainsKey(url))
            {
                texturesCount[url] += addNum;
            }
        }

        protected bool isDisposed = false;
        private UrlLoader m_urlLoader = null;
        private class UrlLoader
        {
            private static List<UrlLoader> pool = new List<UrlLoader>();
            public static UrlLoader GetLoadUnit()
            {
                UrlLoader pUrlLoader = popLoadUnit();
                if (null == pUrlLoader)
                {
                    pUrlLoader = new UrlLoader();
                }
                return pUrlLoader;
            }
            public static void pushLoadUnit(UrlLoader pUrlLoader)
            {
                pUrlLoader.Reset();
                pool.Add(pUrlLoader);
            }
            public static UrlLoader popLoadUnit()
            {
                if (pool.Count <= 0)
                {
                    return null;
                }
                UrlLoader pUrlLoader = pool[0];
                pool.RemoveAt(0);
                return pUrlLoader;
            }

            public int reCount = 0;
            public string httpUrl = null;
            public delegate void LoadCompeleteDelegate(NTexture pNTexture, string httpUrl);
            public event LoadCompeleteDelegate OnLoadCompelete;
            public UrlLoader()
            {
                this.httpUrl = null;
                reCount = 0;
                OnLoadCompelete = null;
            }

            public void Reset()
            {
                reCount = 0;
                this.httpUrl = null;
                OnLoadCompelete = null;
            }

            /// <summary>
            /// 加载成功
            /// </summary>
            /// <param name="pTexture2D"></param>
            /// <param name="checkUnUse"></param>
            public void LoadCompelete(Texture2D pTexture2D, DestroyMethod destroyMethod, bool checkUnUse)
            {
                urlLoaderInfoDic.Remove(httpUrl);
                NTexture pNTexture = null;
                int hasCode = 0;
                if (null != pTexture2D)
                {
                    hasCode = pTexture2D.GetHashCode();
                    if (!texturesUrl.ContainsKey(hasCode))
                    {
                        pNTexture = new NTexture(pTexture2D);
                        pNTexture.destroyMethod = destroyMethod;
                        texturesCount.Add(httpUrl, 0);
                        if (OnUrlTextureLoadFinishEvent != null)
                        {
                            OnUrlTextureLoadFinishEvent(httpUrl);
                        }
                        texturesDic.Add(httpUrl, pNTexture);
                        texturesUrl.Add(hasCode, httpUrl);
                    }
                    else
                    {
                        pNTexture = texturesDic[httpUrl];
                    }
                }
                if (null != OnLoadCompelete)
                {
                    OnLoadCompelete.Invoke(pNTexture, this.httpUrl);
                }
                pushLoadUnit(this);
                if (checkUnUse) // 预加载不检测
                {
                    if (null != pNTexture)
                    {
                        if (texturesUrl.ContainsKey(hasCode))
                        {
                            string url = texturesUrl[hasCode];
                            int count = texturesCount[url];
                            if (count <= 0)
                            {
                                texturesCount.Remove(url);
                                texturesDic.Remove(url);
                                texturesUrl.Remove(hasCode);
                                urlLoaderInfoDic.Remove(url);
                                pNTexture.Dispose();
                            }
                        }
                    }
                }
            }
        }

        public static void PreLoadExternal(string url,int assetId,string assetName, System.Action onReadFinished = null)
        {
            NTexture nTexture;
            if (texturesDic.TryGetValue(url, out nTexture))
            {
                if (null != onReadFinished)
                {
                    onReadFinished();
                }
                return;
            }
            UrlLoader pUrlLoader = null;
            if (urlLoaderInfoDic.TryGetValue(url, out pUrlLoader))
            {
                if (null != onReadFinished)
                {
                    pUrlLoader.OnLoadCompelete += (NTexture pNTexture, string preUrl) => { onReadFinished(); };
                }
                return;
            }
            pUrlLoader = UrlLoader.GetLoadUnit();
            pUrlLoader.httpUrl = url;
            urlLoaderInfoDic[url] = pUrlLoader;
            if (null != onReadFinished)
            {
                pUrlLoader.OnLoadCompelete += (NTexture pNTexture, string preUrl) => { onReadFinished(); };
            }
            UnityEngine.Object obj = ResourcesManager.LoadAsset(assetId, assetName);
            pUrlLoader.LoadCompelete(obj as Texture2D, DestroyMethod.Unload, false);
        }
        protected override void LoadExternal()
        {
            if (null != m_urlLoader)
            {
                m_urlLoader.OnLoadCompelete -= onUrlLoadTextureComplete;
            }
            NTexture nTexture;
            string httpUrl = url;
            if (texturesDic.TryGetValue(httpUrl, out nTexture))
            {
                setTexture(nTexture);
                return;
            }
            UrlLoader pUrlLoader = null;
            if (urlLoaderInfoDic.TryGetValue(httpUrl, out pUrlLoader))
            {
                m_urlLoader = pUrlLoader;
                pUrlLoader.OnLoadCompelete -= onUrlLoadTextureComplete;
                pUrlLoader.OnLoadCompelete += onUrlLoadTextureComplete;
                return;
            }
            var assetId = ResourcesManager.GetAssetId(url);
            if (assetId<0)
            {
                UnityEngine.Debug.LogError("GLoaderExtensino Dont has this url: "+ httpUrl);
                return;
            }
            pUrlLoader = UrlLoader.GetLoadUnit();
            pUrlLoader.httpUrl = httpUrl;
            urlLoaderInfoDic[httpUrl] = pUrlLoader;
            m_urlLoader = pUrlLoader;
            pUrlLoader.OnLoadCompelete += onUrlLoadTextureComplete;
            UnityEngine.Object obj = ResourcesManager.LoadAsset(assetId, "");
            pUrlLoader.LoadCompelete(obj as Texture2D, DestroyMethod.Unload, true);
        }
        protected override void LoadContent()
        {
            base.ClearContent();
            string ur = url;
            if (string.IsNullOrEmpty(ur))
                return;

            if (ur.StartsWith(UIPackage.URL_PREFIX))
                LoadFromPackage(ur);
            else if (ur.StartsWith(UIPackage.URL_PREFIX_HTTP) || ur.StartsWith(UIPackage.URL_PREFIX_FILE) || ur.StartsWith(UIPackage.URL_PREFIX_HTTPS))
            {
                Lens.Gameplay.Managers.CoroutineManager.Start(LoadWebRequest());
            }
            else
                LoadExternal();
        }
        //GLoader扩展
        protected IEnumerator LoadWebRequest()
        {
            if (null != m_urlLoader)
            {
                m_urlLoader.OnLoadCompelete -= onUrlLoadTextureComplete;
            }
            NTexture texture;
            UrlLoader pUrlLoader = null;
            string httpUrl = this.url;
            if (texturesDic.TryGetValue(httpUrl, out texture))
            {
                setTexture(texture);
                yield return null;
            }
            else
            {
                if (urlLoaderInfoDic.TryGetValue(httpUrl, out pUrlLoader))
                {
                    m_urlLoader = pUrlLoader;
                    pUrlLoader.OnLoadCompelete -= onUrlLoadTextureComplete;
                    pUrlLoader.OnLoadCompelete += onUrlLoadTextureComplete;
                    yield return null;
                }
                else
                {
                    pUrlLoader = UrlLoader.GetLoadUnit();
                    m_urlLoader = pUrlLoader;
                    urlLoaderInfoDic[httpUrl] = pUrlLoader;
                    pUrlLoader.httpUrl = httpUrl;
                    pUrlLoader.OnLoadCompelete += onUrlLoadTextureComplete;
                    string fullUrl = httpUrl;
                    if (httpUrl.StartsWith(UIPackage.URL_PREFIX_HTTP) || httpUrl.StartsWith(UIPackage.URL_PREFIX_HTTPS))
                    {
                        fullUrl += "?" + DateTime.Now.ToString();
                    }
                    using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(fullUrl))
                    {
                        yield return uwr.SendWebRequest();
                        if (uwr.isNetworkError || uwr.isHttpError)
                        {
                            Lens.Gameplay.Managers.CoroutineManager.Start(ReLoadWebRequest(pUrlLoader));
                        }
                        else
                        {
                            Debug.Log("LoadWebRequest Ok");
                            Texture2D pTexture2D = DownloadHandlerTexture.GetContent(uwr);
                            pUrlLoader.LoadCompelete(pTexture2D, DestroyMethod.Destroy, true);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 加载 5次
        /// </summary>
        /// <param name="pUrlLoader"></param>
        /// <returns></returns>
        private static IEnumerator ReLoadWebRequest(UrlLoader pUrlLoader)
        {
            string httpUrl = pUrlLoader.httpUrl;
            string fullUrl = httpUrl;
            if (httpUrl.StartsWith(UIPackage.URL_PREFIX_HTTP) || httpUrl.StartsWith(UIPackage.URL_PREFIX_HTTPS))
            {
                fullUrl += "?" + DateTime.Now.ToString();
            }
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(fullUrl))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    if (pUrlLoader.reCount >= 5)
                    {
                        Debug.Log(uwr.error);
                        pUrlLoader.LoadCompelete(null, DestroyMethod.Destroy, false);
                    }
                    else
                    {
                        pUrlLoader.reCount = pUrlLoader.reCount + 1;
                        Lens.Gameplay.Managers.CoroutineManager.Start(ReLoadWebRequest(pUrlLoader));
                    }
                }
                else
                {
                    Debug.Log("LoadWebRequest Ok");
                    Texture2D pTexture2D = DownloadHandlerTexture.GetContent(uwr);
                    pUrlLoader.LoadCompelete(pTexture2D, DestroyMethod.Destroy, true);
                }
            }
        }

        /// <summary>
        /// 本地IO加载或www加载成功
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="strTextTure"></param>
        private void onUrlLoadTextureComplete(NTexture texture, string strTextTure)
        {
            if (isDisposed || (strTextTure != url))
            {
                return;
            }
            if (texture == null)
            {
                onExternalLoadFailed();
                return;
            }
            setTexture(texture);
        }

        protected void setTexture(NTexture texture)
        {
            texturesCount[url]++;
            onExternalLoadSuccess(texture);
        }

        public override void Dispose()
        {
            isDisposed = true;
            if (null != m_urlLoader)
            {
                m_urlLoader.OnLoadCompelete -= onUrlLoadTextureComplete;
            }
            m_urlLoader = null;
            base.Dispose();
        }

        protected override void FreeExternal(NTexture texture)
        {
            if (texture.nativeTexture == null)
            {
                Debug.LogWarning("FreeExternal texture.nativeTexture is nil.");
                return;
            }
            int hasCode = texture.nativeTexture.GetHashCode();
            if (texturesUrl.ContainsKey(hasCode))
            {
                string url = texturesUrl[hasCode];
                int count = --texturesCount[url];
                if (count <= 0)
                {
                    texturesCount.Remove(url);
                    texturesDic.Remove(url);
                    texturesUrl.Remove(hasCode);
                    urlLoaderInfoDic.Remove(url);
                    texture.Dispose();
                }
            }
        }
    }
}