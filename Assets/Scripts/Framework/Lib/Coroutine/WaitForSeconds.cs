﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{
    public class WaitForSeconds: YieldInstruction
    {
        public float time;
    }
}
