﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Animations;
using System.IO;
using System.Text;
using System;
namespace Lens.Framework.Libs
{
    using Core;
    public class CharacterInstancing
    {
        public class DualQuaternion
        {
            public Quaternion m_real;
            public Quaternion m_dual;
            public static Quaternion Quaternion_Normalize(Quaternion q)
            {
                float mod = Mathf.Sqrt(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
                return new Quaternion(q.x / mod, q.y / mod, q.z / mod, q.w / mod);
            }

            public static Quaternion Quaternion_Multiply(Quaternion a, Quaternion b)
            {
                Quaternion ret = new Quaternion();
                ret.w = (a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z);
                ret.x = (a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y);
                ret.y = (a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x);
                ret.z = (a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w);
                return ret;
            }

            public static Quaternion Quaternion_Multiply(Quaternion a, float b)
            {
                return new Quaternion(a.x * b, a.y * b, a.z * b, a.w * b);
            }

            public static Quaternion Quaternion_Conjugate(Quaternion a)
            {
                return new Quaternion(-a.x, -a.y, -a.z, a.w);
            }

            public static Quaternion Quaternion_Add(Quaternion a, Quaternion b)
            {
                Quaternion ret = new Quaternion(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
                return ret;
            }

            public DualQuaternion()
            {
                m_real = new Quaternion(0, 0, 0, 1);
                m_dual = new Quaternion(0, 0, 0, 0);
            }
            public DualQuaternion(Quaternion r, Quaternion d)
            {
                m_real = Quaternion_Normalize(r);
                m_dual = d;
            }
            public DualQuaternion(Quaternion r, Vector3 t)
            {
                m_real = Quaternion_Normalize(r);
                m_dual = Quaternion_Multiply(Quaternion_Multiply(new Quaternion(t.x, t.y, t.z, 0), m_real), 0.5f);
            }
            public static float Dot(DualQuaternion a, DualQuaternion b)
            {
                return Quaternion.Dot(a.m_real, b.m_real);
            }
            public static DualQuaternion operator *(DualQuaternion q, float scale)
            {
                DualQuaternion ret = q;
                ret.m_real = Quaternion_Multiply(ret.m_real, scale);
                ret.m_dual = Quaternion_Multiply(ret.m_dual, scale);
                return ret;
            }
            public static DualQuaternion Normalize(DualQuaternion q)
            {
                float mag = Quaternion.Dot(q.m_real, q.m_real);
                DualQuaternion ret = q;
                ret.m_real = Quaternion_Multiply(ret.m_real, 1.0f / mag);
                ret.m_dual = Quaternion_Multiply(ret.m_dual, 1.0f / mag);
                return ret;
            }

            public static DualQuaternion operator +(DualQuaternion lhs, DualQuaternion rhs)
            {
                return new DualQuaternion(Quaternion_Add(lhs.m_real, rhs.m_real), Quaternion_Add(lhs.m_dual, rhs.m_dual));
            }
            public static DualQuaternion operator *(DualQuaternion lhs, DualQuaternion rhs)
            {
                return new DualQuaternion(Quaternion_Multiply(rhs.m_real, lhs.m_real), Quaternion_Add(Quaternion_Multiply(rhs.m_dual, lhs.m_real), Quaternion_Multiply(rhs.m_real, lhs.m_dual)));
            }
            public static DualQuaternion Conjugate(DualQuaternion q)
            {
                return new DualQuaternion(Quaternion_Conjugate(q.m_real), Quaternion_Conjugate(q.m_dual));
            }
            public static Quaternion GetRotation(DualQuaternion q)
            {
                return q.m_real;
            }
            public static Vector3 GetTranslation(DualQuaternion q)
            {
                Quaternion t = Quaternion_Multiply(Quaternion_Multiply(q.m_dual, 2.0f), Quaternion_Conjugate(q.m_real));
                return new Vector3(t.x, t.y, t.z);
            }
            public static Matrix4x4 DualQuaternionToMatrix(DualQuaternion q)
            {
                q = DualQuaternion.Normalize(q);

                Matrix4x4 M = Matrix4x4.identity;
                float w = q.m_real.w;
                float x = q.m_real.x;
                float y = q.m_real.y;
                float z = q.m_real.z;

                // Extract rotational information   
                M.m00 = w * w + x * x - y * y - z * z;
                M.m10 = 2 * x * y + 2 * w * z;
                M.m20 = 2 * x * z - 2 * w * y;
                M.m01 = 2 * x * y - 2 * w * z;
                M.m11 = w * w + y * y - x * x - z * z;
                M.m21 = 2 * y * z + 2 * w * x;

                M.m02 = 2 * x * z + 2 * w * y;
                M.m12 = 2 * y * z - 2 * w * x;
                M.m22 = w * w + z * z - x * x - y * y;

                // Extract translation information   
                Quaternion t = Quaternion_Multiply(Quaternion_Multiply(q.m_dual, 2.0f), Quaternion_Conjugate(q.m_real));
                M.m03 = t.x;
                M.m13 = t.y;
                M.m23 = t.z;
                return M;
            }
        }
        public class InstancingBone
        {
            public bool isTopNode = false;
            public bool hasNotFoundParentTopNode = false;
            public Transform transform = null;
            public Matrix4x4 bindpose;
            public InstancingBone parent = null;
            public InstancingBone[] children = null;
        }

        private static string m_fileAssetName;
        private static string m_assetName;
        private static string m_assetPath;
        private static string m_assetPrefabDirectory;
        private static string m_assetDirectory;
        private static string m_parentFolder = Application.dataPath.Replace("Assets", "");
        private static string m_prefabDirectory;
        
        private static ModelImporter m_importer;
        private static List<AnimationClip> m_clipList = new List<AnimationClip>();
        private static Avatar m_roleSourceAvatar;
        private static string gpuPrefabFolderPath = "Assets/ResourcesAssets/Prefabs/Character/Soldier/";
        private static string bakerPefabFolderPath = "Assets/ResourcesAssets/Prefabs/Character/Soldier/";

        private static string m_materialDirectory;
        private static string m_textureDirectory;

        private static int m_bakeFrame = 30;
        private  string prefabFolderPath;
        private Vector2 m_listScrollPos;
        private static InstancingData m_allBoneAnimation = null;
        private class MatInformation
        {
            public string matName;
            public string shaderName;
            public List<string> modelNames;
        }
        [MenuItem("Tools/Character/Instancing")]
        static void OptimizeAnimations()
        {
            GameObject[] objs = Selection.gameObjects;
            for (int i = 0; i < objs.Length; i++)
            {
                ExportFBXModel(objs[i]);
            }
        }
        private static void ExportFBXModel(GameObject target)
        {
            m_assetPath = AssetDatabase.GetAssetPath(target);

            if (!m_assetPath.EndsWith(".FBX"))
            {
                Debug.LogError(m_assetPath + "---->please choose .FBX file");
                return;
            }

            m_fileAssetName = target.name;
            if (!m_fileAssetName.StartsWith("fbx_"))
            {
                Debug.LogError(m_fileAssetName + "---->please name start with fbx_");
                return;
            }

            m_assetName = m_fileAssetName.Replace("fbx_", "");
            if (!IslegalString(m_assetName))
            {
                Debug.LogWarning(string.Format("命名错误: {0}", m_assetName));
                return;
            }
            
            string toLowerName = m_assetName.ToLower();
            m_assetDirectory = Path.GetDirectoryName(m_assetPath);
            m_assetPrefabDirectory = Path.Combine(Path.GetDirectoryName(m_assetDirectory), "Prefabs");

            if (!Directory.Exists(m_assetPrefabDirectory))
            {
                Directory.CreateDirectory(m_assetPrefabDirectory);
            }

            m_materialDirectory = Path.Combine(m_assetDirectory, "Materials/").Replace("\\", "/");
            if (!Directory.Exists(m_materialDirectory))
            {
                Directory.CreateDirectory(m_materialDirectory);
            }

            m_textureDirectory = Path.Combine(Path.GetDirectoryName(m_assetDirectory), "Textures/").Replace("\\", "/");

            string sourceAnimatorControllerPath = string.Empty;
            
            sourceAnimatorControllerPath = "Assets/Art/Character/Role/Common/controller/battle_normal_soldier.controller";
            m_prefabDirectory = "Assets/ResourcesAssets/Prefabs/Character/Soldier";// + m_assetName + ".prefab";
            
            m_importer = AssetImporter.GetAtPath(m_assetPath) as ModelImporter;
            m_importer.optimizeMesh = true;
            m_importer.meshCompression = ModelImporterMeshCompression.Medium;
            m_importer.weldVertices = true;
            m_importer.importMaterials = false;
            m_importer.optimizeGameObjects = false;
            m_importer.isReadable = false;

            m_importer.importNormals = ModelImporterNormals.Import;
            m_importer.importTangents = ModelImporterTangents.None;
            
            m_importer.extraExposedTransformPaths = null;
            m_importer.sourceAvatar = null;

            m_importer.importAnimation = true;
            m_importer.resampleCurves = true;
            m_importer.animationType = ModelImporterAnimationType.Generic;
            m_importer.animationCompression = ModelImporterAnimationCompression.Optimal;
            m_importer.animationPositionError = 0.1f;
            m_importer.animationRotationError = 0.1f;
            m_importer.animationScaleError = 0.1f;

            m_importer.SaveAndReimport();
            

            if (!GetAvatar(m_assetPath))
                return;

            GetAnimClips(m_assetPath);
            
            ExportCharacterPrefab(target);
            
            m_roleSourceAvatar = null;
            m_assetPath = null;
            m_assetDirectory = null;
            
            m_clipList.Clear();
            Selection.activeObject = null;
            AssetDatabase.Refresh();
            m_importer = null;
        }
        private static void ExportCharacterPrefab(GameObject obj)
        {
            SkinnedMeshRenderer[] smr = obj.GetComponentsInChildren<SkinnedMeshRenderer>();
            string rootBoneName = smr[0].rootBone.name;
            for (int i = 1; smr != null && i < smr.Length; i++)
            {
                if (!rootBoneName.Equals(smr[i].rootBone.name))
                {
                    Debug.LogWarning( "Root不相同:" + smr[i].name);
                    return;
                }
            }

            List<KeyValuePair<Mesh, Mesh>> rebindpose = new List<KeyValuePair<Mesh, Mesh>>();
            List<KeyValuePair<Transform, int>> remapBones = ResortBone(obj);
            for (int s = 0; smr != null && s < smr.Length; s++)
            {
                if (!IslegalString(smr[s].name))
                {
                    string strLog = string.Format("Mesh  命名错误: {0} -> {1}", obj.name, smr[s].name);
                    Debug.LogWarning( strLog);
                    continue;
                }

                SkinnedMeshRenderer sm = smr[s];
                string meshName = sm.sharedMesh.name;
                Mesh targetMesh = null;

                string meshPath = Path.Combine(Path.GetDirectoryName(m_assetPath), meshName) + "_mesh.asset";
                if (File.Exists(m_parentFolder + meshPath.Replace("\\", "/")))
                {
                    CheckOut(m_parentFolder + meshPath.Replace("\\", "/"));
                    AssetDatabase.DeleteAsset(meshPath);
                }

                if (sm.sharedMesh.subMeshCount > 1)
                {
                    Debug.LogError("Not subMeshCount > 1:" + meshName);
                    continue;
                }

                targetMesh = UnityEngine.Object.Instantiate<Mesh>(sm.sharedMesh);
                targetMesh.uv2 = null;
                targetMesh.uv3 = null;
                AssetDatabase.CreateAsset(targetMesh, meshPath);
                
                RebulidMeshBindPose(obj, sm, targetMesh, remapBones);

                EditorUtility.SetDirty(targetMesh);

                rebindpose.Add(new KeyValuePair<Mesh, Mesh>(sm.sharedMesh, targetMesh));
            }

            InstancingData gpuAnim = null;
            if (remapBones != null)
            {
                ToGenerate(m_assetPath, m_assetName, obj, m_clipList, ref remapBones);
                gpuAnim = m_allBoneAnimation;
                if (gpuAnim == null)
                {
                    Debug.LogError("GPU Anim error:" + m_assetName);
                    return;
                }
            }
            float offset = 0.0f;
            for (int s = 0; smr != null && s < smr.Length; s++)
            {
                string assetName = smr[s].gameObject.name;

                SkinnedMeshRenderer sm = smr[s];
                string prefabName = sm.name;

                GameObject cloneTarget = UnityEngine.Object.Instantiate<GameObject>(obj);
                cloneTarget.name = assetName;

                Mesh targetMesh = null;

                SkinnedMeshRenderer[] targetSmr = cloneTarget.GetComponentsInChildren<SkinnedMeshRenderer>();
                for (int d = 0; targetSmr != null && d < targetSmr.Length; d++)
                {
                    if (!targetSmr[d].name.Equals(prefabName))
                    {
                        UnityEngine.Object.DestroyImmediate(targetSmr[d].gameObject);
                    }
                    else
                    {
                        targetMesh = rebindpose.Find(x => x.Key == targetSmr[d].sharedMesh).Value;

                        targetSmr[d].sharedMesh = targetMesh;
                        targetSmr[d].gameObject.layer = LayerMask.NameToLayer("Char");
                    }
                }

                string matName = assetName;
                matName = matName.Substring(0, matName.LastIndexOf("_"));
                
                MatInformation matInformation = new MatInformation();
                matInformation.matName = matName;
                matInformation.shaderName = "Mobile/Model/TextureMask";
                Material shadowPlane_mat = AssetDatabase.LoadAssetAtPath<Material>("Assets/Art/Character/Role/soldier/slg/shadow_plane.mat");

                SkinnedMeshRenderer renderer = cloneTarget.GetComponentInChildren<SkinnedMeshRenderer>();
                renderer.sharedMaterials = new Material[] { AddMaterial(matInformation), shadowPlane_mat };
                
                cloneTarget.transform.position = new Vector3(offset, 0, 0);
                offset += sm.bounds.size.x;
                
                string objName = prefabName + "_inst";
                string prefabPath = gpuPrefabFolderPath + objName + ".prefab";
                UnityEngine.Object oldPrefab;
                if (File.Exists(prefabPath))
                {
                    //P4Utils.P4CheckOut(m_parentFolder + prefabPath);
                    oldPrefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
                }
                else
                {
                    oldPrefab = PrefabUtility.CreateEmptyPrefab(prefabPath);
                    P4Add(m_parentFolder + prefabPath);
                }

                GameObject newPrefab = new GameObject(objName);

                Material instanceMat = null;
                Material shadowMat = null;
                GetInstanceMaterial(renderer.sharedMaterials[0], out instanceMat, out shadowMat);

                var meshVertices = newPrefab.AddComponent<InstancingComponent>();
                meshVertices.material = instanceMat;
                meshVertices.shadowMaterial = shadowMat;
                meshVertices.meshLods = new List<Mesh>() { targetMesh };
                meshVertices.animationInfo = gpuAnim;

                PrefabUtility.ReplacePrefab(newPrefab, oldPrefab);
                UnityEngine.Object.DestroyImmediate(newPrefab);
                UnityEngine.Object.DestroyImmediate(cloneTarget);
            }
        }
        /// <summary>
        /// 获取所有配置骨骼的全路径
        /// </summary>
        /// <param name="target"></param>
        /// <param name="paths"></param>
        /// <returns></returns>
        private static string[] GetFullPaths(Transform target, List<string> paths)
        {
            List<string> fullPaths = new List<string>();
            for (int i = 0; i < paths.Count; i++)
            {
                string fullPath = GetFullPath(target, paths[i]);
                if (fullPath == null)
                {
                    Debug.LogError(string.Format("fullPath is error:{0} -> {1}", m_assetName, paths[i]));
                }
                else
                {
                    fullPaths.Add(fullPath);
                }
            }
            return fullPaths.ToArray();
        }
        private static string GetFullPath(Transform target, string path)
        {
            Transform obj = Find(target, path);
            if (obj != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Insert(0, obj.name);
                RecursivePath(target, obj, ref sb);
                return sb.ToString();
            }
            return null;
        }
        private static void RecursivePath(Transform target, Transform root, ref StringBuilder sb)
        {
            Transform parent = root.parent;
            if (!parent.name.Equals(target.name))
            {
                sb.Insert(0, parent.name + "/");
                RecursivePath(target, parent, ref sb);
            }
        }
        private static Transform Find(Transform target, string name)
        {
            foreach (Transform child in target.GetComponentsInChildren<Transform>())
            {
                if (child.name == name)
                {
                    return child;
                }
            }
            return null;
        }
        /// <summary>
        /// 浮点数精度压缩到f3
        /// </summary>
        /// <param name="newClip"></param>
        private static void OptimizeFloatingPoint(AnimationClip newClip)
        {
            Keyframe key;
            Keyframe[] keyFrames;
            AnimationClipCurveData[] curves = AnimationUtility.GetAllCurves(newClip);
            for (int i = 0; i < curves.Length; ++i)
            {
                AnimationClipCurveData curveData = curves[i];
                if (curveData.curve == null || curveData.curve.keys == null)
                {
                    continue;
                }
                keyFrames = curveData.curve.keys;
                for (int j = 0; j < keyFrames.Length; j++)
                {
                    key = keyFrames[j];
                    key.value = float.Parse(key.value.ToString("f3"));
                    key.inTangent = float.Parse(key.inTangent.ToString("f3"));
                    key.outTangent = float.Parse(key.outTangent.ToString("f3"));
                    keyFrames[j] = key;
                }
                curveData.curve.keys = keyFrames;
                newClip.SetCurve(curveData.path, curveData.type, curveData.propertyName, curveData.curve);
            }
        }
        private static List<KeyValuePair<Transform, int>> ResortBone(GameObject target)
        {
            SkinnedMeshRenderer[] smr = target.GetComponentsInChildren<SkinnedMeshRenderer>();
            if (smr.Length == 0)
            {
                Debug.LogError("Don't has SkinnedMeshRenderer component  " + target.name);
                return null;
            }

            Transform root = smr[0].rootBone;
            if (root == null)
            {
                Debug.LogError("Don't has Root: " + target.name);
                return null;
            }

            List<KeyValuePair<Transform, int>> mappedIdx = new List<KeyValuePair<Transform, int>>();
            foreach (var sm in smr)
            {
                if (root != sm.rootBone)
                {
                    Debug.LogError(sm.name + ":Root bone error:" + sm.rootBone.name);
                    continue;
                }

                Transform[] smBones = sm.bones;
                foreach (var b in smBones)
                {
                    if (mappedIdx.FindIndex(x => x.Key == b) == -1)
                    {
                        mappedIdx.Add(new KeyValuePair<Transform, int>(b, mappedIdx.Count));
                    }
                }
            }

            return mappedIdx;
        }
        private static void RebulidMeshBindPose(GameObject obj, SkinnedMeshRenderer sm, Mesh targetMesh, List<KeyValuePair<Transform, int>> inRemapBones)
        {
            // Init Bones
            Transform[] aBones = sm.bones;
            int numBones = aBones.Length;

            int[] remapIdx = new int[numBones];
            for (int i = 0; i < numBones; ++i)
                remapIdx[i] = i;

            if (inRemapBones != null && inRemapBones.Count > 0)
            {
                for (int i = 0; i < numBones; ++i)
                {
                    if (inRemapBones.FindIndex(x => x.Key == sm.bones[i]) == -1)
                    {
                        Debug.LogError(targetMesh.name + ":在 mappedIdx 没有找到 Transform:" + sm.bones[i].name);
                        continue;
                    }
                    KeyValuePair<Transform, int> idx = inRemapBones.Find(x => x.Key == sm.bones[i]);
                    remapIdx[i] = idx.Value;
                }
            }

            Vector3[] aNormals = targetMesh.normals;
            Vector3[] aVertices = targetMesh.vertices;
            List<Vector3> tangents = new List<Vector3>();
            Matrix4x4[] aBindPoses = targetMesh.bindposes;
            BoneWeight[] aBoneWeights = targetMesh.boneWeights;
            for (int i = 0; i < targetMesh.vertexCount; ++i)
            {
                Vector3 tangent = Vector3.zero;
                Vector3 v3Normal = Vector3.zero;
                Vector3 v3World = Vector3.zero;
                BoneWeight bw = aBoneWeights[i];
                if (Mathf.Abs(bw.weight0) > 0.00001f)
                {
                    Transform bone = (aBones == null || aBones.Length == 0) ? obj.transform : aBones[bw.boneIndex0].transform;
                    v3World = (bone.localToWorldMatrix * aBindPoses[bw.boneIndex0]).MultiplyPoint3x4(aVertices[i]) * bw.weight0;
                    v3Normal = (bone.localToWorldMatrix * aBindPoses[bw.boneIndex0]).MultiplyVector(aNormals[i]) * bw.weight0;
                    tangent.x = remapIdx[bw.boneIndex0];
                    tangent.y = bw.weight0;
                }
                else
                {
                    Debug.LogError(targetMesh + " Idx:" + i + ": Bone 0 weight == 0.0f.");
                }

                if (Mathf.Abs(bw.weight1) > 0.00001f)
                {
                    Transform bone = (aBones == null || aBones.Length == 0) ? obj.transform : aBones[bw.boneIndex1].transform;
                    v3World += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex1]).MultiplyPoint3x4(aVertices[i]) * bw.weight1;
                    v3Normal += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex1]).MultiplyVector(aNormals[i]) * bw.weight1;

                    tangent.z = remapIdx[bw.boneIndex1];
                    //tangent.w = bw.weight1;
                }
                else
                {
                    tangent.y = 1.0f;
                    tangent.z = tangent.x;
                    //tangent.w = 0.0f;
                }

                if (Mathf.Abs(bw.weight2) > 0.00001f)
                {
                    Transform bone = (aBones == null || aBones.Length == 0) ? obj.transform : aBones[bw.boneIndex2].transform;
                    v3World += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex2]).MultiplyPoint3x4(aVertices[i]) * bw.weight2;
                    v3Normal += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex2]).MultiplyVector(aNormals[i]) * bw.weight2;

                    Debug.LogError(targetMesh + " Idx:" + i + ": Bone weight > 2.");
                }
                if (Mathf.Abs(bw.weight3) > 0.00001f)
                {
                    Transform bone = (aBones == null || aBones.Length == 0) ? obj.transform : aBones[bw.boneIndex3].transform;
                    v3World += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex3]).MultiplyPoint3x4(aVertices[i]) * bw.weight3;
                    v3Normal += (bone.localToWorldMatrix * aBindPoses[bw.boneIndex3]).MultiplyVector(aNormals[i]) * bw.weight3;

                    Debug.LogError(targetMesh + " Idx:" + i + ": Bone weight > 3.");
                }
                aVertices[i] = v3World;
                aNormals[i] = v3Normal;
                tangents.Add(tangent);
            }

            for (int bpIdx = 0; bpIdx < aBindPoses.Length; bpIdx++)
            {
                aBindPoses[bpIdx] = aBones[bpIdx].worldToLocalMatrix * obj.transform.localToWorldMatrix;
            }

            targetMesh.vertices = aVertices;
            targetMesh.normals = aNormals;
            targetMesh.bindposes = aBindPoses;
            
            targetMesh.SetUVs(1, tangents);
        }
        private static Material AddMaterial(MatInformation matInformation)
        {
            string materialPath = m_materialDirectory + matInformation.matName + ".mat";
            Material mat = null;
            Texture mainTexture = null;
            Texture maskTexture = null;
            if (File.Exists(materialPath))
            {
                mat = AssetDatabase.LoadAssetAtPath<Material>(materialPath);
            }
            else
            {
                string mainTexturePath = m_textureDirectory + matInformation.matName + ".tga";
                if (!File.Exists(mainTexturePath))
                {
                    mainTexturePath = m_textureDirectory + matInformation.matName + ".png";
                }

                string maskTexturePath = m_textureDirectory + matInformation.matName + "_m.tga";
                if (!File.Exists(maskTexturePath))
                {
                    maskTexturePath = m_textureDirectory + matInformation.matName + "_m.png";
                    if (!File.Exists(maskTexturePath))
                    {
                        maskTexturePath = null;
                    }
                }

                mainTexture = AssetDatabase.LoadAssetAtPath<Texture>(mainTexturePath);
                mat.SetTexture(Uniforms._MainTex, mainTexture);
                if (maskTexturePath != null)
                {
                    maskTexture = AssetDatabase.LoadAssetAtPath<Texture>(maskTexturePath);
                    mat.SetTexture(Uniforms._MaskTex, maskTexture);
                }
                CheckOut(m_parentFolder + materialPath);
                AssetDatabase.CreateAsset(mat, materialPath);
            }
            return mat;
        }
        private static void GetInstanceMaterial(Material srcMat, out Material instanceMat, out Material instanceShadowMat)
        {
            string materialPath = m_materialDirectory + srcMat.name + "_instance.mat";
            string shadowMaterialPath = m_materialDirectory + srcMat.name + "_instance_shadow.mat";
            CheckOut(m_parentFolder + shadowMaterialPath);

            Shader shader = Shader.Find("Mobile/Model/Instancing");
            if (File.Exists(materialPath))
            {
                CheckOut(m_parentFolder + materialPath);
                instanceMat = AssetDatabase.LoadAssetAtPath<Material>(materialPath);
                instanceMat.CopyPropertiesFromMaterial(srcMat);
            }
            else
            {
                instanceMat = Material.Instantiate(srcMat);
                AssetDatabase.CreateAsset(instanceMat, materialPath);
                P4Add(m_parentFolder + materialPath);
            }

            instanceMat.shader = shader;
            instanceMat.enableInstancing = true;

            Shader shadowShader = Shader.Find("Mobile/Model/InstancingShadow");
            if (File.Exists(shadowMaterialPath))
            {
                CheckOut(m_parentFolder + shadowMaterialPath);
                instanceShadowMat = AssetDatabase.LoadAssetAtPath<Material>(shadowMaterialPath);
            }
            else
            {
                instanceShadowMat = new Material(shadowShader);
                AssetDatabase.CreateAsset(instanceShadowMat, shadowMaterialPath);
                P4Add(m_parentFolder + shadowMaterialPath);
            }

            instanceShadowMat.shader = shadowShader;
            instanceShadowMat.enableInstancing = true;

        }
        private static bool GetAvatar(string assetPath)
        {
            UnityEngine.Object[] objs = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            m_clipList.Clear();
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i] is Avatar)
                {
                    m_roleSourceAvatar = objs[i] as Avatar;
                }
            }
            if (m_roleSourceAvatar == null)
            {
                Debug.LogError("没有找到 Avatar." + assetPath);
                return false;
            }
            string avatarPath = Path.Combine(Path.GetDirectoryName(m_assetPath), m_assetName) + "_avatar.asset";
            m_roleSourceAvatar = UnityEngine.Object.Instantiate<Avatar>(m_roleSourceAvatar);
            AssetDatabase.CreateAsset(m_roleSourceAvatar, avatarPath);

            return true;
        }
        private static void GetAnimClips(string assetPath)
        {
            UnityEngine.Object[] objs = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            m_clipList.Clear();
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i] is AnimationClip)
                {
                    if (objs[i].hideFlags == (HideFlags.HideInHierarchy | HideFlags.NotEditable))
                        continue;

                    if (!IslegalString(objs[i].name))
                    {
                        string strLog = string.Format("AnimationCLip  命名错误: {0} -> {1}", assetPath, objs[i].name);
                        Debug.LogWarning( strLog);
                    }

                    m_clipList.Add(objs[i] as AnimationClip);
                }
            }
        }
        private static void P4Add(string path)
        {

        }
        private static void CheckOut(string path)
        {
        }
        private static bool IsChina(string CString)
        {
            bool BoolValue = false;
            for (int i = 0; i < CString.Length; i++)
            {
                if (Convert.ToInt32(Convert.ToChar(CString.Substring(i, 1))) > Convert.ToInt32(Convert.ToChar(128)))
                {
                    BoolValue = true;
                }

            }
            return BoolValue;
        }
        private static bool IslegalString(string str)
        {
            if (str.IndexOf(" ") > -1)
                return false;

            if (IsChina(str))
                return false;

            return true;
        }

        private static void ToGenerate(string assetPath, string assetName, GameObject obj, List<AnimationClip> clips, ref List<KeyValuePair<Transform, int>> mappedIdx)
        {
            string animInstancePath = Path.Combine(Path.GetDirectoryName(assetPath), assetName) + ".asset";
            Matrix4x4 local2World = obj.transform.localToWorldMatrix;
            SkinnedMeshRenderer[] smr = obj.GetComponentsInChildren<SkinnedMeshRenderer>();
            Transform root = smr[0].rootBone;
            if (root == null)
            {
                Debug.LogError("Don't has Root: " + obj.name);
                return;
            }

            // Init Bones
            int numBones = mappedIdx.Count;
            var bones = new InstancingBone[numBones];
            for (int i = 0; i < numBones; ++i)
            {
                InstancingBone bone = new InstancingBone();
                bones[i] = bone;
                bone.transform = mappedIdx[i].Key;
                bone.bindpose = mappedIdx[i].Key.transform.worldToLocalMatrix * local2World;
            }

            var rootBoneIndex = 0;
            // Construct Bones' Hierarchy
            for (int i = 0; i < numBones; ++i)
            {
                if (bones[i].transform == root)
                {
                    rootBoneIndex = i;
                    break;
                }
            }

            m_allBoneAnimation = ScriptableObject.CreateInstance<InstancingData>();
            m_allBoneAnimation.boneCount = bones.Length;
            List<InstancingBone> newBones = new List<InstancingBone>(bones);
            AddBones(newBones, obj.transform.GetChild(0)/*root*/, false, false);
            bones = newBones.ToArray();
            int targetRootBoneIndex = 0;
            for (int i = 0; i < bones.Length; i++)
            {
                if (bones[i].transform == obj.transform.GetChild(0)/*root*/)
                {
                    targetRootBoneIndex = i;
                    break;
                }
            }
            rootBoneIndex = targetRootBoneIndex;

            System.Action<InstancingBone> CollectChildren = null;
            CollectChildren = (currentBone) =>
            {
                List<InstancingBone> children = new List<InstancingBone>();
                for (int j = 0; j < currentBone.transform.childCount; ++j)
                {
                    Transform childTransform = currentBone.transform.GetChild(j);
                    InstancingBone childBone = null;
                    foreach (InstancingBone bone in bones)
                    {
                        if (bone.transform == childTransform)
                        {
                            childBone = bone;
                            break;
                        }
                    }
                    if (childBone != null)
                    {
                        childBone.parent = currentBone;
                        children.Add(childBone);
                        CollectChildren(childBone);
                    }
                }
                currentBone.children = children.ToArray();
            };
            CollectChildren(bones[rootBoneIndex]);

            List<InstancingAnimation> animationClips = new List<InstancingAnimation>();
            foreach (AnimationClip animClip in clips)
            {
                int clipId = -1;
                try
                {
                    clipId = (int)((EAnimationType)System.Enum.Parse(typeof(EAnimationType), animClip.name));//.Substring(animClip.name.LastIndexOf('_')+1);
                }
                catch
                {
                    continue;
                }
                if (animationClips.Find(x => x.animType == clipId) != null)
                    continue;

                InstancingAnimation boneAnimation = new InstancingAnimation();
                boneAnimation.fps = m_bakeFrame;
                boneAnimation.animType = clipId;//.Substring(animClip.name.LastIndexOf('_')+1);
                boneAnimation.animName = animClip.name;
                boneAnimation.frames = new InstancingFrame[(int)(animClip.length * boneAnimation.fps)];
                boneAnimation.length = animClip.length;
                boneAnimation.isLooping = animClip.isLooping;
                animationClips.Add(boneAnimation);
                for (int frameIndex = 0; frameIndex < boneAnimation.frames.Length; ++frameIndex)
                {
                    InstancingFrame frame = new InstancingFrame();
                    boneAnimation.frames[frameIndex] = frame;
                    List<Matrix4x4> matrices = new List<Matrix4x4>();
                    animClip.SampleAnimation(obj, (float)frameIndex / boneAnimation.fps);
                    for (int i = 0; i < numBones; i++)
                    {
                        var remapBone = mappedIdx[i];

                        int idx = System.Array.FindIndex(bones, (x => x.transform == remapBone.Key));
                        if (idx == -1)
                        {
                            Debug.LogError("Not Found bone:" + remapBone.Key.name);
                            continue;
                        }

                        var bone = bones[idx];
                        Matrix4x4 matrixBip001 = bone.transform.localToWorldMatrix;
                        matrices.Add(matrixBip001);
                    }
                    frame.matrices = matrices.ToArray();
                }
            }
            m_allBoneAnimation.boneAnimations = animationClips.ToArray();

            for (int i = 0; i < m_allBoneAnimation.boneAnimations.Length; i++)
            {
                m_allBoneAnimation.boneAnimations[i].frameStartIndex = m_allBoneAnimation.totalAnimationLength;
                m_allBoneAnimation.totalAnimationLength += m_allBoneAnimation.boneAnimations[i].frames.Length;
            }

            var tex2D = new Texture2D(m_allBoneAnimation.totalAnimationLength, numBones * 2, TextureFormat.RGBAHalf, false);
            tex2D.filterMode = FilterMode.Point;
            int currentFrameIndex = 0;
            int animationIndex = 0;
            foreach (var boneAnimation in m_allBoneAnimation.boneAnimations)
            {
                for (int frameIndex = 0; frameIndex < boneAnimation.frames.Length; frameIndex++)
                {
                    for (int boneIndex = 0; boneIndex < numBones; boneIndex++)
                    {
                        var remapBone = mappedIdx[boneIndex];
                        int idx = System.Array.FindIndex(bones, (x => x.transform == remapBone.Key));
                        if (idx == -1)
                        {
                            Debug.LogError("Not Found bone:" + remapBone.Key.name);
                            continue;
                        }

                        Matrix4x4 matrix = (Matrix4x4)boneAnimation.frames[frameIndex].matrices[boneIndex] * bones[idx].bindpose;

                        Vector3 scale = matrix.lossyScale;
                        float sx = Mathf.Floor(scale.x * 100.0f);
                        float sy = Mathf.Floor(scale.y * 100.0f);
                        float sz = Mathf.Floor(scale.z * 100.0f);
                        if ((sx - sy) > 5.0f
                            || (sx - sz) > 5.0f
                            || (sy - sz) > 5.0f)
                        {
                            string strLog = string.Format("AnimClip scale X Y Z not equal: {0} -> {1} {2}", obj.name, boneAnimation.animName, bones[idx].transform.name);
                            UnityEngine.Debug.Log("AnimClip scale" + strLog);
                        }

                        Quaternion rotation = ToQuaternion(matrix);
                        tex2D.SetPixel(currentFrameIndex, boneIndex * 2, new Color(rotation.x, rotation.y, rotation.z, rotation.w));
                        tex2D.SetPixel(currentFrameIndex, boneIndex * 2 + 1, new Color(matrix.GetColumn(3).x, matrix.GetColumn(3).y, matrix.GetColumn(3).z, Mathf.Clamp01(matrix.lossyScale.magnitude)));
                    }
                    currentFrameIndex++;
                }
                animationIndex++;
            }
            tex2D.Apply();
            byte[] bytes = tex2D.GetRawTextureData();
            m_allBoneAnimation.tex2D = bytes;

            string parentFolder = Application.dataPath.Replace("Assets", "");
            UnityEditor.AssetDatabase.CreateAsset(m_allBoneAnimation, animInstancePath);
            UnityEngine.Object.DestroyImmediate(tex2D);
            UnityEditor.AssetDatabase.Refresh();
        }
        static void AddBones(List<InstancingBone> bones, Transform transform, bool parentIsFound, bool parentIsTopNode)
        {
            bool found = false;
            InstancingBone targetBone = null;
            foreach (InstancingBone bone in bones)
            {
                if (bone.transform == transform)
                {
                    found = true;
                    targetBone = bone;
                    break;
                }
            }
            if (!found)
            {
                InstancingBone newBone = new InstancingBone();
                newBone.transform = transform;
                newBone.hasNotFoundParentTopNode = !parentIsTopNode;
                bones.Add(newBone);
            }
            else if (parentIsFound == false)
            {
                targetBone.isTopNode = true;
                parentIsTopNode = true;
                targetBone.hasNotFoundParentTopNode = !parentIsTopNode;
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                AddBones(bones, transform.GetChild(i), found, parentIsTopNode);
            }
        }
        static bool CompareApproximately(float f0, float f1, float epsilon = 0.000001F)
        {
            float dist = (f0 - f1);
            dist = Mathf.Abs(dist);
            return dist < epsilon;
        }
        static Quaternion ToQuaternion(Matrix4x4 mat)
        {
            float det = mat.determinant;
            if (!CompareApproximately(det, 1.0F, .005f))
                return Quaternion.identity;

            Quaternion quat = Quaternion.identity;
            float tr = mat.m00 + mat.m11 + mat.m22;

            // check the diagonal
            if (tr > 0.0f)
            {
                float fRoot = Mathf.Sqrt(tr + 1.0f);  // 2w
                quat.w = 0.5f * fRoot;
                fRoot = 0.5f / fRoot;  // 1/(4w)
                quat.x = (mat[2, 1] - mat[1, 2]) * fRoot;
                quat.y = (mat[0, 2] - mat[2, 0]) * fRoot;
                quat.z = (mat[1, 0] - mat[0, 1]) * fRoot;
            }
            else
            {
                // |w| <= 1/2
                int[] s_iNext = { 1, 2, 0 };
                int i = 0;
                if (mat.m11 > mat.m00)
                    i = 1;
                if (mat.m22 > mat[i, i])
                    i = 2;
                int j = s_iNext[i];
                int k = s_iNext[j];

                float fRoot = Mathf.Sqrt(mat[i, i] - mat[j, j] - mat[k, k] + 1.0f);
                if (fRoot < float.Epsilon)
                    return Quaternion.identity;

                quat[i] = 0.5f * fRoot;
                fRoot = 0.5f / fRoot;
                quat.w = (mat[k, j] - mat[j, k]) * fRoot;
                quat[j] = (mat[j, i] + mat[i, j]) * fRoot;
                quat[k] = (mat[k, i] + mat[i, k]) * fRoot;
            }

            return QuaternionNormalize(quat);

        }
        static Quaternion QuaternionNormalize(Quaternion quat)
        {
            float scale = new Vector4(quat.x, quat.y, quat.z, quat.w).magnitude;
            scale = 1.0f / scale;

            return new Quaternion(scale * quat.x, scale * quat.y, scale * quat.z, scale * quat.w);
        }
    }
}