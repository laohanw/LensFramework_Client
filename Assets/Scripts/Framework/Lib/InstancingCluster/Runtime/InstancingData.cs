﻿using UnityEngine;
using System.Collections;

namespace Lens.Framework.Libs
{
    [System.Serializable]
    public class InstancingData : ScriptableObject
    {
        public InstancingAnimation[] boneAnimations = null;
        public int totalAnimationLength = 0;
        public int boneCount = 0;
        [UnityEngine.HideInInspector]
        public byte[] tex2D;
        private Texture2D m_texture;
        public Texture2D GetTexture()
        {
            m_texture = new Texture2D(totalAnimationLength, boneCount * 2, TextureFormat.RGBAHalf, false);
            m_texture.filterMode = FilterMode.Point;
            m_texture.LoadRawTextureData(tex2D);
            m_texture.Apply();
            return m_texture;
        }
        public void Dispose()
        {
            if (m_texture!=null)
            {
                UnityEngine.Object.DestroyImmediate(m_texture);
            }
        }
    }
    [System.Serializable]
    public class InstancingFrame
    {
        public Matrix4x4[] matrices = null;
    }

    [System.Serializable]
    public class InstancingAnimation
    {
        public int animType=-1;
        public string animName;
        public InstancingFrame[] frames = null;
        public float length = 0;
        public int fps = 0;
        public bool isLooping;
        public int frameStartIndex = 0;
        public int framesCount { get { return Mathf.RoundToInt(fps * length); } }
    }
}