﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Lens.Framework.Libs
{
    public class InstancingCluster 
    {
        private static InstancingCluster m_instance;
        private bool m_isPause;
        private Dictionary<int, int> m_dic = new Dictionary<int, int>();
        private Dictionary<int, InstancingGroup> m_instacing = new Dictionary<int, InstancingGroup>();
        public static InstancingCluster GetInstance(){
            if(m_instance!=null)return null;
            return m_instance=new InstancingCluster();
        }
        InstancingCluster()
        {
            
        }
        public void Dispose()
        {
            foreach (var item in m_instacing)
            {
                item.Value.Dispose();
            }
            m_instance = null;
        }
        public void Update()
        {
            if (m_isPause) return;
            foreach (var item in m_instacing)
            {
                item.Value.Update();
            }
        }
        public void Pause()
        {
            m_isPause = true;
        }
        public void Resume()
        {
            m_isPause = false;
        }
        public static int AddInstancingConfig(int id,InstancingData data, Material mat, Material shadowMat,Color campColor,bool fusion,bool hasCamp)
        {
            if (m_instance.m_dic.ContainsKey(id))
            {
                return m_instance.m_dic[id];
            }
            else
            {
                int index = m_instance.m_dic.Count;
                m_instance.m_dic.Add(id, m_instance.m_dic.Count);
                var ue = new InstancingGroup();
                m_instance.m_instacing.Add(index,ue);
                ue.Initialize(data, mat, shadowMat,campColor,fusion, hasCamp);
                return index;
            }
        }
        public static int AddSpawnObject(int instancingId, InstancingComponent trans)
        {
            return m_instance.m_instacing[instancingId].AddSpawnObject(trans); 
        }
        public static void RemoveSpawnObject(int instancingId,int spawnId)
        {
            m_instance.m_instacing[instancingId].RemoveSpawnObject(spawnId);
        }
        public static void ShowShadow(int instancingId,int spawnId)
        {
            if (m_instance == null) return;
            m_instance.m_instacing[instancingId].ShowShadow(spawnId);
        }
        public static void HideShadow(int instancingId, int spawnId)
        {
            if (m_instance == null) return;
            m_instance.m_instacing[instancingId].HideShadow(spawnId);
        }
        public static void SetLayer(int instancingId,int layer)
        {
            if (m_instance == null) return;
            m_instance.m_instacing[instancingId].SetLayer(layer);
        }
        public static void SetShadowLayer(int instancingId,int layer)
        {
            if (m_instance == null) return;
            m_instance.m_instacing[instancingId].SetShadowLayer(layer);
        }
        public static void SetLod(int instancingId, int lod)
        {
            if (m_instance == null) return;
            m_instance.m_instacing[instancingId].SetLod(lod);
        }
    }

}