﻿#define INSTANCE_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Lens.Framework.Libs
{
    public class InstancingGroup
    {
        private List<InstancingComponent> spawnObjects = new List<InstancingComponent>();
        private InstancingData m_data = null;
        private Material m_material;
        private Material m_shadowMaterial;
        public Texture2D m_tex2D;
        private List<Matrix4x4> matrices = new List<Matrix4x4>(20);
        private List<Matrix4x4> matricesShadow = new List<Matrix4x4>(20);
        private List<float> frameIndexes = new List<float>(20);
        private List<float> frameIndexFusion = new List<float>(20);
        private List<float> fusionProgress = new List<float>(20);

        private MaterialPropertyBlock properties = new MaterialPropertyBlock();
        private float lastTime = -1;
        private int m_layer;
        private int m_shadowLayer;
        private int m_index;
        private bool m_fusion;
        private int m_lod;
        
        public void Initialize(InstancingData data, Material mat, Material shadowMat,Color campColor,bool fusion,bool hasCamp)
        {
            m_data = data;
            m_material = Material.Instantiate(mat);
            m_material.SetColor(Core.Uniforms._MaskColor, campColor);
            m_tex2D = m_data.GetTexture();
            m_material.SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
            m_material.SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
            m_shadowMaterial = Material.Instantiate(shadowMat);
            m_shadowMaterial.SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
            m_shadowMaterial.SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
            m_fusion = fusion;
            if (fusion)
            {
                m_material.EnableKeyword("FUSION_ON");
                m_shadowMaterial.EnableKeyword("FUSION_ON");
            }
            else
            {
                m_material.DisableKeyword("FUSION_ON");
                m_shadowMaterial.DisableKeyword("FUSION_ON");
            }
            if (hasCamp)
            {
                m_material.EnableKeyword("MASK_COLOR_ON");
                m_shadowMaterial.EnableKeyword("MASK_COLOR_ON");
            }
            else
            {
                m_material.DisableKeyword("MASK_COLOR_ON");
                m_shadowMaterial.DisableKeyword("MASK_COLOR_ON");
            }

        }
        public void Dispose()
        {
            if (m_material!=null)
            {
                UnityEngine.Object.Destroy(m_material);
            }
            if (m_shadowMaterial!=null)
            {
                UnityEngine.Object.Destroy(m_shadowMaterial);
            }
            if(m_data!=null)
                m_data.Dispose();
        }
        public void Update()
        {
            float dTime = Time.smoothDeltaTime;
            if (lastTime > 0.0f)
                dTime = Time.time - lastTime;
            lastTime = Time.time;
            if (spawnObjects[0].useApiDraw)
            {
                for (int i = 0; i < spawnObjects.Count; i++)
                {
                    var spawn = spawnObjects[i];
                    spawn.ToUpdate(dTime);
                    matrices[i] = spawn.transform.localToWorldMatrix;
                    if (spawnObjects[i].shownShadow)
                    {
                        matricesShadow[i] =matrices[i];
                    }
                    else
                    {
                        matricesShadow[i] = new Matrix4x4();
                    }
                    frameIndexes[i] = spawn.frameIndex;
                    frameIndexFusion[i] = spawn.frameIndexFusion;
                    fusionProgress[i] = spawn.progress;
                }

                properties.SetFloatArray(Core.Uniforms._FrameIndex, frameIndexes);
                if (m_fusion)
                {
                    properties.SetFloatArray(Core.Uniforms._FrameIndexFusion, frameIndexFusion);
                    properties.SetFloatArray(Core.Uniforms._FusionProgress, fusionProgress);
                }

                if (spawnObjects.Count <= 0) return;
                if (spawnObjects[0].meshLods.Count <= 0) return;
                Mesh lodMesh = spawnObjects[0].meshLods[m_lod];
                if (lodMesh == null)
                    return;
                if (matrices.Count > 0)
                {
                    Graphics.DrawMeshInstanced(lodMesh, 0, m_material, matrices, properties, UnityEngine.Rendering.ShadowCastingMode.Off, false, m_layer);
                }
                if (spawnObjects[0].useShadow)
                {
                    Graphics.DrawMeshInstanced(lodMesh, lodMesh.subMeshCount > 1?1:0, m_shadowMaterial, matricesShadow, properties, UnityEngine.Rendering.ShadowCastingMode.Off, false, m_shadowLayer);
                }
            }
            else
            {
                if (spawnObjects.Count <= 0) return;
                if (spawnObjects[0].meshLods.Count <= 0) return;
                Mesh lodMesh = spawnObjects[0].meshLods[m_lod];
                for (int i = 0; i < spawnObjects.Count; i++)
                {
                    var spawn = spawnObjects[i];
                    spawn.ToUpdate(dTime);
                    properties.SetFloat(Core.Uniforms._FrameIndex, spawn.frameIndex);
                    if (m_fusion)
                    {
                        properties.SetFloat(Core.Uniforms._FrameIndexFusion, spawn.frameIndexFusion);
                        properties.SetFloat(Core.Uniforms._FusionProgress, spawn.progress);
                    }
                    
                    spawn.filter.sharedMesh = lodMesh;
                    spawn.render.SetPropertyBlock(properties);

                    if (spawn.useShadow)
                    {
                        if (spawn.render.sharedMaterials == null || spawn.render.sharedMaterials.Length != 2)
                        {
                            spawn.render.sharedMaterials = new Material[2] { m_material, m_shadowMaterial };
                        }
                    }
                    else
                    {
                        if (spawn.render.sharedMaterials == null || spawn.render.sharedMaterials.Length != 1 || spawn.render.sharedMaterials[0]==null)
                        {
                            spawn.render.sharedMaterials = new Material[] { m_material };
                        }
                    }
                }
            }
        }
        public void SetLod(int lod)
        {
            m_lod = lod;
        }
        public int AddSpawnObject(InstancingComponent spawnObject)
        {
            spawnObjects.Add(spawnObject);
            matrices.Add(spawnObject.transform.localToWorldMatrix);
            matricesShadow.Add(spawnObject.transform.localToWorldMatrix);
            frameIndexes.Add(spawnObject.frameIndex);
            frameIndexFusion.Add(spawnObject.frameIndexFusion);
            fusionProgress.Add(spawnObject.progress);
            m_index++;
            m_layer = spawnObject.gameObject.layer;
            m_shadowLayer = m_layer;
            return m_index;
        }
        public void RemoveSpawnObject(int spawnId)
        {
            for (int i = 0; i < spawnObjects.Count; i++)
            {
                var spawnObject = spawnObjects[i];
                if (spawnObject.spawnId==spawnId)
                {
                    spawnObjects.RemoveAt(i);
                    matrices.RemoveAt(matrices.Count - 1);
                    matricesShadow.RemoveAt(matricesShadow.Count - 1);
                    frameIndexes.RemoveAt(frameIndexes.Count - 1);
                    frameIndexFusion.RemoveAt(frameIndexFusion.Count - 1);
                    fusionProgress.RemoveAt(fusionProgress.Count - 1);
                    return;
                }
            }
        }
        public void ShowShadow(int spawnId)
        {
            for (int i = 0; i < spawnObjects.Count; i++)
            {
                var spawnObj = spawnObjects[i];
                if (spawnObj.spawnId== spawnId && !spawnObj.shownShadow)
                {
                    spawnObj.ShowShadow();
                    return;
                }
            }
        }
        public void HideShadow(int spawnId)
        {
            for (int i = 0; i < spawnObjects.Count; i++)
            {
                var spawnObj = spawnObjects[i];
                if (spawnObj.spawnId == spawnId)
                {
                    spawnObj.HideShadow();
                    return;
                }
            }
        }
        public void SetLayer(int layer)
        {
            m_layer = layer;
        }
        public void SetShadowLayer(int layer)
        {
            m_shadowLayer = layer;
        }
    }
}