﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{
    public enum ETaskType
    {
        Leaf = 1,
        Composite = 2,
        Decorator=3
    }
}
