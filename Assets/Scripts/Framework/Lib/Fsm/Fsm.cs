﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Libs
{
    /// <summary>
    /// FSMSystem class represents the Finite State Machine class.
    ///  It has a List with the States the NPC has and methods to add,
    ///  delete a state, and to change the current state the Machine is on.
    /// </summary>
    public class Fsm
    {
        private List<FsmState> m_states;
        private object m_owner;
        // The only way one can change the state of the FSM is by performing a transition
        // Don't change the CurrentState directly
        private int m_currentStateID;
        public int CurrentStateID { get { return m_currentStateID; } }
        private FsmState m_currentState;
        public FsmState CurrentState { get { return m_currentState; } }

        public Fsm(object owner)
        {
            m_owner = owner;
            m_states = new List<FsmState>();
        }
        public void Update()
        {
            if (m_currentState != null)
            {
                if (m_currentState.status == EStateStatus.Execute)
                {
                    m_currentState.Execute();
                }
            }
        }
        /// <summary>
        /// This method places new states inside the FSM,
        /// or prints an ERROR message if the state was already inside the List.
        /// First state added is also the initial state.
        /// </summary>
        public void AddState(FsmState s)
        {
            // Check for Null reference before deleting
            if (s == null)
            {
                Debug.LogError("FSM ERROR: Null reference is not allowed");
            }
            // First State inserted is also the Initial state,
            //   the state the machine is in when the simulation begins
            if (m_states.Count == 0)
            {
                s.owner = m_owner;
                m_states.Add(s);
                m_currentState = s;
                m_currentStateID = s.id;
                return;
            }

            // Add the state to the List if it's not inside it
            foreach (FsmState state in m_states)
            {
                if (state.id == s.id)
                {
                    Debug.LogError("FSM ERROR: Impossible to add state " + s.id.ToString() +
                                   " because state has already been added");
                    return;
                }
            }
            s.owner = m_owner;
            m_states.Add(s);
        }

        /// <summary>
        /// This method delete a state from the FSM List if it exists, 
        ///   or prints an ERROR message if the state was not on the List.
        /// </summary>
        public void DeleteState(int id)
        {
            // Check for NullState before deleting
            if (id == -1)
            {
                Debug.LogError("FSM ERROR: NullStateID is not allowed for a real state");
                return;
            }

            // Search the List and delete the state if it's inside it
            foreach (FsmState state in m_states)
            {
                if (state.id == id)
                {
                    m_states.Remove(state);
                    return;
                }
            }
            Debug.LogError("FSM ERROR: Impossible to delete state " + id.ToString() +
                           ". It was not on the list of states");
        }

        /// <summary>
        /// This method tries to change the state the FSM is in based on
        /// the current state and the transition passed. If current state
        ///  doesn't have a target state for the transition passed, 
        /// an ERROR message is printed.
        /// </summary>
        public void PerformTransition(int trans)
        {
            // Check for NullTransition before changing the current state
            if (trans == -1)
            {
                Debug.LogError("FSM ERROR: NullTransition is not allowed for a real transition");
                return;
            }

            // Check if the currentState has the transition passed as argument
            int id = m_currentState.GetOutputState(trans);
            if (id == -1)
            {
                Debug.LogError("FSM ERROR: State " + m_currentStateID.ToString() + " does not have a target state " +
                               " for transition " + trans.ToString());
                return;
            }

            // Update the currentStateID and currentState		
            m_currentStateID = id;
            foreach (FsmState state in m_states)
            {
                if (state.id == m_currentStateID)
                {
                    m_currentState.status = EStateStatus.leave;
                    // Do the post processing of the state before setting the new one
                    m_currentState.DoBeforeLeaving();

                    m_currentState = state;

                    m_currentState.status = EStateStatus.Enter;
                    // Reset the state to its desired condition before it can reason or act
                    m_currentState.DoBeforeEntering();
                    m_currentState.status = EStateStatus.Execute;
                    break;
                }
            }

        }

    }
}