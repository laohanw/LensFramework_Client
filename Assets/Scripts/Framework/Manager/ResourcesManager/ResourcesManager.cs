﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace Lens.Framework.Managers
{
    using Core;
    public class ResourcesManager:Singleton<ResourcesManager>
    {
        private SourceOptions m_options;
        public static ISource source { get; private set; }
        public override void Initialize(Options options)
        {
#if DEVELOPMENT
            source=new LocalSource();
#else
            source = new BundleSource();
#endif
            m_options=options as SourceOptions;
            source.Initialize(m_options);
        }
        public override void Update()
        {
            source.Update();
        }
        public void LateUpdate()
        {
            source.LateUpdate();
        }
        public override void Dispose()
        {
            source.Dispose();
            source = null;
            base.Dispose();
        }
        public static int GetAssetId(string assetPath)
        {
            return source.GetAssetId(assetPath);
        }
        public static void LoadAsset(int assetId, DataHandler<UnityEngine.Object> onLoadOver,EVariantType variant=EVariantType.n,bool toAsync=true)
        {
            source.Load(assetId, onLoadOver, variant, toAsync);
        }
        public static void LoadAsset(int assetId, DataHandler<UnityEngine.Object[]> onLoadOver, EVariantType variant = EVariantType.n,bool toAsync=true)
        {
            source.Load(assetId, onLoadOver, variant, toAsync);
        }
        public static UnityEngine.Object LoadAsset(int assetId,string assetName, EVariantType variant = EVariantType.n)
        {
            return source.Load(assetId, assetName, variant);
        }
        public static UnityEngine.Object[] LoadAsset(int assetId, EVariantType variant = EVariantType.n)
        {
            return source.Load(assetId, variant);
        }
        public static string LoadFile(string path)
        {
            var filePath =Path.Combine( p_instance.m_options.sourceUrl ,path);
            string file = null;
            using (FileStream fileStream=new FileStream(filePath,FileMode.Open,FileAccess.Read))
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
                file = System.Text.Encoding.UTF8.GetString(bytes);
            }
            return file;
        }
        public static string LoadAbsoluteFile(string filePath)
        {
            string file = null;
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
                file = System.Text.Encoding.UTF8.GetString(bytes);
            }
            return file;
        }
        public static byte[] LoadBytes(string path)
        {
            var filePath =Path.Combine( p_instance.m_options.bundleUrl ,path);
            byte[] bytes = null;
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
            }
            return bytes;
        }
        public static bool Exists(string path)
        {
            return File.Exists(Path.Combine(p_instance.m_options.sourceUrl, path));
        }
        public static FileStream LoadStream(string path)
        {
            var filePath = p_instance.m_options.bundleUrl+"/" + path;
            return new FileStream(filePath, FileMode.Open, FileAccess.Read);
        }
        public static FileStream LoadAbsoluteStream(string filePath)
        {
            return new FileStream(filePath, FileMode.Open, FileAccess.Read);
        }
        public static void Destroy(int assetId,EVariantType variant=EVariantType.n)
        {
            source.Destroy(assetId);
        }
        public static void Unload(int assetId,EVariantType variant = EVariantType.n)
        {
            source.UnLoad(assetId, variant);
        }
        public static void UnloadUnusedAsset()
        {
            source.UnloadUnusedAsset();
        }
    }
}
