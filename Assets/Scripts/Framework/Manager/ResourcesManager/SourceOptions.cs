
namespace Lens.Framework.Managers
{
  using Core;
  public class SourceOptions:Singleton<ResourcesManager>.Options
  {
        /// <summary>
        /// 同时有多少个bundle文件加载
        /// </summary>
        public int maxBundleLoading = 10;
        /// <summary>
        /// 资源变体的最大个数（必须是10的倍数）
        /// </summary>
        public int variantMaxLength = 10;
        /// <summary>
        /// 依赖优化用的最多有多少个依赖层 最多99个
        /// </summary>
        public int dependLayerDigit = 100;
        public string bundleManifestUrl="";
        public string bundleUrl="";
        public string localSourceUrl="";
        public string sourceUrl="";
  }
}