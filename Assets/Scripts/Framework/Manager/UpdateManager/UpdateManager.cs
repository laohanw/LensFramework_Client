﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using Utility;
    using Core;
    /// <summary>
    /// 流程：判断是否首包解压--》检查版本是否一致，并获得需要下载的大小--》下载，解压，合并--》完成
    /// </summary>
    public class UpdateManager:Singleton<UpdateManager>
    {
        public const string tags="UpdateManager";
        public class MyOptions:Options{
            public string versionFileName="version";
            public string packageInfoFileName="info";
            public string entireZipName="entirePack.7z";

            public string sourceUrl;
            public string tempUrl;
            public int platform=
#if UNITY_EDITOR
        (int)UnityEditor.EditorUserBuildSettings.activeBuildTarget;
#else
        (int)Application.platform;
#endif
        }
        public interface IListener{
            void ToInstallNew();
            /// <summary>
            /// 首包解压缩
            /// </summary>
            void OnDecompressing(DecompressingInfo info);
            /// <summary>
            /// 差异包下载
            /// </summary>
            void OnDownloading(DownloadingInfo info);
            /// <summary>
            /// 热更状态
            /// </summary>
            void OnStatus(EStatus status);
            /// <summary>
            /// 热更错误
            /// </summary>
            void OnError(EErrorType error, string msg);
            /// <summary>
            /// 下载确认框
            /// </summary>
            void ToDownloadDialog(Action ok,Action cancel);
        }
        public enum EStatus
        {
            None,
            CheckingApp,
            Decompressing,
            Decompressed,
            CheckingVersion,
            Checked,
            Downloading,
            Downloaded,
            Combining,
            Over,
            Error
        }
        public enum EErrorType
        {
            Version,
            PackageInfo,
            Download
        }
        public class PackageInfo
        {
            public List<string> packages = new List<string>();
            /// <summary>
            /// byte
            /// </summary>
            public List<long> sizes = new List<long>();
            /// <summary>
            /// 已下载了的大小
            /// </summary>
            public List<long> sized = new List<long>();
            public List<UnityWebRequest> request=new List<UnityWebRequest>();
            public int downloadCount;
            public PackageInfo()
            {

            }
            public PackageInfo(string content)
            {
                var split = content.Split('\n');
                for (int i = 0; i < split.Length; i++)
                {
                    var s = split[i].Split(',');
                    packages.Add(s[0].Trim());
                    sizes.Add(Convert.ToInt64(s[1]));
                }
            }
            public PackageInfo(string[] files)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    FileInfo info = new FileInfo(files[i]);
                    packages.Add(info.Name);
                    sizes.Add(info.Length);
                }
            }
            public override string ToString()
            {
                string str = "";
                for (int i = 0; i < packages.Count; i++)
                {
                    str += packages[i] + "," + sizes[i] + "\n"; 
                }
                return str;
            }
        }
        public class Version
        {
            /// <summary>
            /// 主版本号
            /// </summary>
            public int major;
            /// <summary>
            /// 里程碑
            /// </summary>
            public int milestone;
            /// <summary>
            /// 周版本
            /// </summary>
            public int weekly;
            /// <summary>
            /// 流水号
            /// </summary>
            public int revision;
            /// <summary>
            /// 资源Info信息
            /// </summary>
            public string assetInfo;
            /// <summary>
            /// 资源的url地址
            /// </summary>
            public string assetUrl;
            public Version()
            {

            }
            public Version(string content)
            {
                var split = content.TrimEnd().Split('\n');
                var vSplit = split[0].Split('.');

                major = Convert.ToInt32(vSplit[0]);
                milestone = Convert.ToInt32(vSplit[1]);
                weekly = Convert.ToInt32(vSplit[2]);
                revision = Convert.ToInt32(vSplit[3]);

                assetInfo = split[0];
                assetUrl = split[1].Trim()+ split[0]; 
            }
            public void SaveTo(string filePath)
            {
                using (FileStream fs=new FileStream(filePath,FileMode.Create,FileAccess.Write))
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ToString());
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            public override string ToString()
            {
                return major + "." + milestone + "." + weekly + "." + revision+"\n" + assetUrl;
            }
            public override bool Equals(object obj)
            {
                var t = obj as Version;
                if (major == t.major && milestone == t.milestone && weekly == t.weekly && revision == t.revision && assetInfo==t.assetInfo)
                {
                    return true;
                }
                else
                    return false;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
        public class DownloadingInfo
        {
            /// <summary>
            /// 总下载数量
            /// </summary>
            public long totalSize;
            /// <summary>
            /// 下载了的文件大小
            /// </summary>
            public double downloadedSize;
            /// <summary>
            /// 下载进度
            /// </summary>
            public float progress;
            public override string ToString()
            {
                return "totalSize:" + totalSize + "\ndownloadedSize:" + downloadedSize + "\nprogress:" + progress;
            }
        }
        public class DecompressingInfo
        {
            /// <summary>
            /// 解压文件大小
            /// </summary>
            public long fileSize=-1;
            /// <summary>
            /// 解压了多少
            /// </summary>
            public long processSize=-1;
            public bool IsOver()
            {
                return fileSize == processSize;
            }
            public void Clear()
            {
                fileSize = -1;
                processSize = -1;
            }
            public override string ToString()
            {
                return "fileSize:"+fileSize+ "  processSize："+ processSize;
            }
        }
        public EStatus Status { get;private set; }
        /// <summary>
        /// 本地缓存目录的版本信息
        /// </summary>
        private Version m_cacheVersion;
        /// <summary>
        /// 本地包内目录的版本信息 StreamingAssets目录
        /// </summary>
        private Version m_localVersion;
        /// <summary>
        /// 远程版本信息
        /// </summary>
        private Version m_remoteVersion;
        /// <summary>
        /// 远程差异包的info
        /// </summary>
        private PackageInfo m_remotePkgInfo;
        /// <summary>
        /// 本地已经下载过的info
        /// </summary>
        private PackageInfo m_localPkgInfo;
        /// <summary>
        /// 远程待下载的url列表
        /// </summary>
        private PackageInfo m_waitDownloadInfo;
        /// <summary>
        /// 下载中的信息数据
        /// </summary>
        private DownloadingInfo m_downloadingInfo=new DownloadingInfo();
        /// <summary>
        /// 首包解压进度信息
        /// </summary>
        private DecompressingInfo m_decompressingInfo = new DecompressingInfo();
        /// <summary>
        /// 是否下载完成
        /// </summary>
        private bool m_downloadingError;
        /// <summary>
        /// 重连下载次数
        /// </summary>
        private int m_reDownloadCount=3;
        /// <summary>
        /// 分支
        /// </summary>
        private string m_branch;
        /// <summary>
        /// 渠道
        /// </summary>
        private int m_channel;
        /// <summary>
        /// 远程资源url地址
        /// </summary>
        private string m_assetUrl;
        private IListener m_listener;
        private Action m_overHandler;
        private MyOptions m_options;
        private Action<string,string> m_logHandler;
        public override void Initialize(Options options = null)
        {
            if(m_options!=null){
                m_options=options as MyOptions;
            }
            else {
                m_options=new MyOptions();
            }
        }
        public void AddListener(IListener listener){
            m_listener=listener;
        }
        public void SetLogHandler(Action<string,string> callback){
            m_logHandler=callback;
        }
        public void Start(Action onOverHandler=null)
        {
            m_overHandler=onOverHandler;
            if (!ReadAppconfig())
            {
                UnityEngine.Debug.LogError("Dont has appconfig in StreamingAssets");
                return;
            }
            GetRemoteVersion();
        }
        public void Update()
        {
            if (Status == EStatus.Decompressing &&  m_decompressingInfo.fileSize>0)
            {
                if(m_listener!=null){
                    m_listener.OnDecompressing(m_decompressingInfo);
                }
                if (m_decompressingInfo.IsOver())
                {
                    Status = EStatus.Decompressed;
                    if(m_listener!=null){
                        m_listener.OnStatus(EStatus.Decompressed);
                    }
                    m_decompressingInfo.Clear();
                    CheckVersion();
                }
            }
            else if (Status==EStatus.Downloading &&  m_waitDownloadInfo != null)
            {
                double s = 0;
                float p = 0;
                for (int i = 0; i < m_waitDownloadInfo.request.Count; i++)
                {
                    var size = m_waitDownloadInfo.sizes[i];
                    var request = m_waitDownloadInfo.request[i];
                    if (request != null)
                    {
                        if (request.isDone)
                        {
                            if (request.isNetworkError || !string.IsNullOrEmpty(request.error) || request.responseCode != 200)
                            {
                                m_waitDownloadInfo.downloadCount++;
                                ((AssetDownloadHandler)request.downloadHandler).Release();
                                request.Dispose();
                                request = null;
                                m_downloadingError = true;
                            }
                            else
                            {
                                s += request.downloadedBytes;
                                p += request.downloadProgress;
                                if (request.downloadProgress >= 1)
                                {
                                    m_waitDownloadInfo.downloadCount++;
                                    ((AssetDownloadHandler)request.downloadHandler).Release();
                                    request.Dispose();
                                    request = null;
                                }
                            }
                        }
                    }
                }
                m_downloadingInfo.downloadedSize = s;
                m_downloadingInfo.progress = p / (float)m_waitDownloadInfo.request.Count;
                if(m_listener!=null){
                    m_listener.OnDownloading(m_downloadingInfo);
                }
                if (m_waitDownloadInfo.downloadCount >= m_waitDownloadInfo.packages.Count)
                {
                    Status = EStatus.Downloaded;
                    if(m_listener!=null)
                    {
                        m_listener.OnStatus(EStatus.Downloaded);
                    }
                    IsDownloadOver();
                }
            }
        }
        private void Download()
        {
            m_downloadingError = false;
            Status = EStatus.Downloading;
            if(m_listener!=null)
            {
                m_listener.OnStatus(EStatus.Downloading);
            }
            GenWaitDownloadInfo();
            if (m_waitDownloadInfo.packages.Count <= 0)
            {
                Status = EStatus.Over;
                if(m_listener!=null)
                {
                    m_listener.OnStatus(EStatus.Over);
                }
                if(m_overHandler!=null)
                {
                    m_overHandler();
                }
            }
            else
            {
                string tempPath =Path.Combine(m_options.tempUrl, "download/package");
                DirectoryInfo folderInfo = new DirectoryInfo(tempPath);
                if (!folderInfo.Exists)
                    folderInfo.Create();
                for (int i = 0; i < m_waitDownloadInfo.packages.Count; i++)
                {
                    string url = m_remoteVersion.assetUrl + "/" + m_waitDownloadInfo.packages[i];
                    UnityWebRequest www = UnityWebRequest.Get(url);
                    www.disposeDownloadHandlerOnDispose = true;
                    www.chunkedTransfer = false;
                    long length = m_waitDownloadInfo.sized[i];
                    www.SetRequestHeader("Range", string.Format("bytes={0}-", length));
                    www.downloadHandler = new AssetDownloadHandler(tempPath + "/" + m_waitDownloadInfo.packages[i], length);
                    m_waitDownloadInfo.request.Add(www); 
                    www.SendWebRequest();
                }
            }
        }
        private void IsDownloadOver()
        {
            if (m_downloadingError)
            {
                m_reDownloadCount--;
                if (m_reDownloadCount>0)
                {
                    Download();
                }
                else
                {
                    if(m_listener!=null){
                        m_listener.OnError(EErrorType.Download, "");
                    }
                }
            }
            else
            {
                Combine();
            }
        }
        private void Combine()
        {
            Status = EStatus.Combining;
            if(m_listener!=null)
            {
                m_listener.OnStatus(EStatus.Combining);
            }
            string tempPackageFolder = Path.Combine(m_options.tempUrl, "download/package");
            for (int i = 0; i < m_waitDownloadInfo.packages.Count; i++)
            {
                string tempPackagePath= Path.Combine(tempPackageFolder, m_waitDownloadInfo.packages[i]);
                string tempDecompressPath = Path.Combine(m_options.tempUrl, "decompress");
                if (Directory.Exists(tempDecompressPath))
                {
                    Directory.Delete(tempDecompressPath, true);
                    Directory.CreateDirectory(tempDecompressPath);
                }
                Zip7Utility.DecompressFolder(tempPackagePath, tempDecompressPath+"/");
                List<string> addList = new List<string>();
                List<string> modifyList = new List<string>();
                List<string> deleteList = new List<string>();
                string tempBundleFolder = Path.Combine(tempDecompressPath, "Bundle");
                string tempBundlePackageInfo = Path.Combine(tempBundleFolder, "package");
                string distBundleFolder = Path.Combine(m_options.sourceUrl, "Bundle");
                GetDiffList(tempBundlePackageInfo, ref addList, ref modifyList, ref deleteList);
                for (int j = 0; j < addList.Count; j++)
                {
                    string srcPath = Path.Combine(tempBundleFolder, addList[j]);
                    if (File.Exists(srcPath))
                    {
                        string distPath = Path.Combine(distBundleFolder, addList[j]);
                        FileUtility.CopyFile(srcPath, distPath, true);
                        File.Delete(srcPath);
                    }
                }
                for (int j = 0; j < modifyList.Count; j++)
                {
                    string srcPath = Path.Combine(tempBundleFolder, modifyList[j]);
                    if (File.Exists(srcPath))
                    {
                        string distPath = Path.Combine(distBundleFolder, modifyList[j]);
                        FileUtility.CopyFile(srcPath, distPath, true);
                        File.Delete(srcPath);
                    }
                }
                for (int j = 0; j < deleteList.Count; j++)
                {
                    string distPath = Path.Combine(distBundleFolder, deleteList[j]);
                    if (File.Exists(distPath))
                    {
                        File.Delete(distPath);
                    }
                }
                addList.Clear();
                modifyList.Clear();
                deleteList.Clear();
                string tempLuaFolder=Path.Combine(tempDecompressPath,"Lua");
                string tempLuaPackageInfo = Path.Combine(tempLuaFolder, "package");
                string distLuaFolder = Path.Combine(m_options.sourceUrl, "Lua");
                GetDiffList(tempLuaPackageInfo, ref addList, ref modifyList, ref deleteList);
                for (int j = 0; j < addList.Count; j++)
                {
                    string srcPath = Path.Combine(tempLuaFolder, addList[j]);
                    if (File.Exists(srcPath))
                    {
                        string distPath = Path.Combine(distLuaFolder, addList[j]);
                        FileUtility.CopyFile(srcPath, distPath, true);
                        File.Delete(srcPath);
                    }
                }
                for (int j = 0; j < modifyList.Count; j++)
                {
                    string srcPath = Path.Combine(tempLuaFolder, modifyList[j]);
                    if (File.Exists(srcPath))
                    {
                        string distPath = Path.Combine(distLuaFolder, modifyList[j]);
                        FileUtility.CopyFile(srcPath, distPath, true);
                        File.Delete(srcPath);
                    }
                }
                for (int j = 0; j < deleteList.Count; j++)
                {
                    string distPath = Path.Combine(distLuaFolder, deleteList[j]);
                    if (File.Exists(distPath))
                    {
                        File.Delete(distPath);
                    }
                }
                Directory.Delete(tempDecompressPath, true);
            }
            Directory.Delete(tempPackageFolder, true);
            Over();
        }
        private void Over()
        {
            m_remoteVersion.SaveTo(Path.Combine(m_options.sourceUrl, m_options.versionFileName));
            Status = EStatus.Over;
            if(m_listener!=null)
            {
                m_listener.OnStatus(EStatus.Over);
            }
            if(m_overHandler!=null)
            {
                m_overHandler();
            }
        }
        private void CheckVersion()
        {
            Status = EStatus.CheckingVersion;
            if(m_listener!=null)
            {
                m_listener.OnStatus(EStatus.CheckingVersion);
            }
            string versionPath =Application.streamingAssetsPath;
            var filePath =Path.Combine(m_options.sourceUrl ,m_options.versionFileName);
            var versionStr = LoadFile(filePath);
            m_cacheVersion = new Version(versionStr);
            if(m_logHandler!=null){
                m_logHandler(tags,"localVersion:" + m_cacheVersion);
            }
            else{
                UnityEngine.Debug.Log("localVersion:" + m_cacheVersion);
            }
            if (m_cacheVersion.Equals(m_remoteVersion))
            {
                Status = EStatus.Over;
                if(m_listener!=null)
                {
                    m_listener.OnStatus(EStatus.Over);
                }
                if(m_overHandler!=null)
                {
                    m_overHandler();
                }
            }
            else
            {
                GetPackageInfo(m_remoteVersion.assetUrl +"/"+ m_options.packageInfoFileName);
            }
        }
        private void GetRemoteVersion(){
            var remoteVersionUrl = m_assetUrl + "/" + m_options.platform + "/" + m_branch + "/" + m_channel + "/" + m_options.versionFileName;

            var www = UnityWebRequest.Get(remoteVersionUrl);
            www.disposeDownloadHandlerOnDispose = true;
            www.chunkedTransfer = true;
            var request = www.SendWebRequest();
            request.completed += OnRemoteVersion;
        }
        private void OnRemoteVersion(UnityEngine.AsyncOperation obj)
        {
            var async = obj as UnityWebRequestAsyncOperation;
            var w = async.webRequest;
            if (w.isNetworkError)
            {
                if(m_listener!=null){
                    m_listener.OnError(EErrorType.Version, w.error+"\n"+w.url);
                }
                Status = EStatus.Error;
                if(m_listener!=null)
                {
                    m_listener.OnStatus(EStatus.Error);
                }
                w.Dispose();
            }
            else
            {
                if (w.responseCode==200)
                {
                    byte[] result = w.downloadHandler.data;
                    var vStr = System.Text.Encoding.UTF8.GetString(result);
                    m_remoteVersion = new Version(vStr);
                    if(m_logHandler!=null){
                        m_logHandler(tags,"remoteVersion:" + m_remoteVersion);
                    }
                    else{
                        UnityEngine.Debug.Log("remoteVersion:" + m_remoteVersion);
                    }
                    string localVersionPath = Path.Combine(Application.streamingAssetsPath, m_options.versionFileName);
                    if (File.Exists(localVersionPath))
                    {
                        var localVersionStr=LoadFile(localVersionPath);
                        m_localVersion=new Version(localVersionStr);
                        if (m_localVersion.major==m_remoteVersion.major)
                        {
                            if (!File.Exists(Path.Combine(m_options.sourceUrl, m_options.versionFileName)))
                            {
                                string zipPath = Path.Combine(Application.streamingAssetsPath, m_options.entireZipName);
                                if (File.Exists(zipPath))
                                {
                                    Status = EStatus.Decompressing;
                                    if(m_listener!=null){
                                        m_listener.OnStatus(EStatus.Decompressing);
                                    }
                                    string internalVersionPath = Path.Combine(Application.streamingAssetsPath,m_options.versionFileName);
                                    var cacheVersionPath = Path.Combine(m_options.sourceUrl, m_options.versionFileName);
                                    File.Copy(internalVersionPath, cacheVersionPath,true);
                                    Zip7Utility.DecompressFolderAsync(zipPath, m_options.sourceUrl+"/",OnDecompressAsync);
                                }
                                else {
                                    Status = EStatus.Over;
                                    if(m_logHandler!=null){
                                        m_logHandler(tags,"must has first package");
                                    }
                                    else{
                                        UnityEngine.Debug.Log("must has first package");
                                    }
                                }
                            }
                            else
                            {
                                CheckVersion();
                            }
                        }
                        else {
                            if(m_listener!=null){
                                m_listener.ToInstallNew();
                            }
                        }
                    }
                    else {
                        Status = EStatus.Over;
                        if(m_logHandler!=null){
                            m_logHandler(tags,"must has first package");
                        }
                        else{
                            UnityEngine.Debug.Log("must has first package");
                        }
                    }
                    w.Dispose();
                }
                else
                {
                    if(m_listener!=null){
                        m_listener.OnError(EErrorType.Version, w.responseCode.ToString()+"\n"+w.url);
                    }
                    Status = EStatus.Error;
                    if(m_listener!=null)
                    {
                        m_listener.OnStatus(EStatus.Error);
                    }
                    w.Dispose();
                }
            }
        }
        private void GetPackageInfo(string url)
        {
            var www = UnityWebRequest.Get(url);
            www.disposeDownloadHandlerOnDispose = true;
            www.chunkedTransfer = true;
            var request = www.SendWebRequest();
            request.completed += OnRemotePackageInfo;
        }
        private void OnRemotePackageInfo(UnityEngine.AsyncOperation obj)
        {
            var async = obj as UnityWebRequestAsyncOperation;
            var w = async.webRequest;
            if (w.isNetworkError)
            {
                if(m_listener!=null){
                    m_listener.OnError(EErrorType.PackageInfo, w.error+"\n"+w.url);
                }
                Status = EStatus.Error;
                if(m_listener!=null)
                {
                    m_listener.OnStatus(EStatus.Error);
                }
                w.Dispose();
            }
            else
            {
                byte[] result = w.downloadHandler.data;
                var packageInfoStr = System.Text.Encoding.UTF8.GetString(result);
                if (w.responseCode == 200)
                {
                    m_remotePkgInfo = new PackageInfo(packageInfoStr);
                    GenWaitDownloadInfo();
                    if(m_logHandler!=null){
                        m_logHandler(tags,"packageInfo:" + m_remotePkgInfo);
                    }
                    else{
                        UnityEngine.Debug.Log("packageInfo:" + m_remotePkgInfo);
                    }
                    Status = EStatus.Checked;
                    if(m_listener!=null)
                    {
                        m_listener.OnStatus(EStatus.Checked);
                    }
                    if(m_listener!=null){
                        m_listener.ToDownloadDialog(Download,()=>{
                            Status = EStatus.Over;
                            if(m_overHandler!=null)
                            {
                                m_overHandler();
                            }
                        });
                    }
                    else{
                        Download();
                    }
                    w.Dispose();
                }
                else
                {
                    if(m_listener!=null){
                        m_listener.OnError(EErrorType.PackageInfo, packageInfoStr + "\n" + w.url);
                    }
                    Status = EStatus.Error;
                    if(m_listener!=null)
                    {
                        m_listener.OnStatus(EStatus.Error);
                    }
                    w.Dispose();
                }

            }
        }
        private void OnDecompressAsync(long fileSize,long processSize)
        {
            m_decompressingInfo.fileSize = fileSize;
            m_decompressingInfo.processSize = processSize;
        }
        private void GenWaitDownloadInfo()
        {
            m_downloadingInfo = new DownloadingInfo();
            string tempPath = Path.Combine(m_options.tempUrl, "download/package");
            m_waitDownloadInfo = new PackageInfo();
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            m_localPkgInfo = new PackageInfo(Directory.GetFiles(tempPath, "*", SearchOption.TopDirectoryOnly));
            //跳过首包 所以 i=1
            for (int i = 1; i < m_remotePkgInfo.packages.Count; i++)
            {
                bool has = false;
                for (int j = 0; j < m_localPkgInfo.packages.Count; j++)
                {
                    if (m_localPkgInfo.packages[j] == m_remotePkgInfo.packages[i] && m_localPkgInfo.sizes[j] != m_remotePkgInfo.sizes[i])
                    {
                        has = true;
                        m_waitDownloadInfo.packages.Add(m_localPkgInfo.packages[j]);
                        m_waitDownloadInfo.sizes.Add(m_remotePkgInfo.sizes[j]);
                        m_waitDownloadInfo.sized.Add(m_localPkgInfo.sizes[j]);
                        m_downloadingInfo.totalSize += m_remotePkgInfo.sizes[j];
                    }
                }
                if (!has)
                {
                    m_waitDownloadInfo.packages.Add(m_remotePkgInfo.packages[i]);
                    m_waitDownloadInfo.sizes.Add(m_remotePkgInfo.sizes[i]);
                    m_waitDownloadInfo.sized.Add(0);
                    m_downloadingInfo.totalSize += m_remotePkgInfo.sizes[i];
                }
            }
        }
        private void GetDiffList(string tempBundlePackageInfo,ref List<string> addList,ref List<string> modifyList,ref List<string> deleteList)
        {
            using (FileStream st = new FileStream(tempBundlePackageInfo, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader sr = new StreamReader(st))
                {
                    string s;
                    int type = 0;
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (s == "add")
                        {
                            type = 1;
                        }
                        else if (s == "modify")
                        {
                            type = 2;
                        }
                        else if (s == "delete")
                        {
                            type = 3;
                        }
                        else
                        {
                            if (type == 1)
                            {
                                addList.Add(s);
                            }
                            else if (type == 2)
                            {
                                modifyList.Add(s);
                            }
                            else if (type == 3)
                            {
                                deleteList.Add(s);
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
        }
        private bool ReadAppconfig()
        {
            string appconfigPath = Path.Combine(Application.streamingAssetsPath, "appconfig");
            if (File.Exists(appconfigPath))
            {
                var lines = File.ReadAllLines(appconfigPath);
                m_branch = lines[0];
                m_channel =Convert.ToInt32(lines[1]);
                m_assetUrl = lines[2];
                return true;
            }
            else
            {
                return false;
            }
        }

        private string LoadFile(string filePath)
        {
            string file = null;
            using (FileStream fileStream=new FileStream(filePath,FileMode.Open,FileAccess.Read))
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
                file = System.Text.Encoding.UTF8.GetString(bytes);
            }
            return file;
        }
    }
}