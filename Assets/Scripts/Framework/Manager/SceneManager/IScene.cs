﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public interface IScene
    {
        void Initialize();
        void Load();
        void Unload();
        void Dispose();
    }
}
