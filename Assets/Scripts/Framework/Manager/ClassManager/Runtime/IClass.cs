﻿using System.Collections;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public interface IClass
    {
        /// <summary>
        /// 类对象重置
        /// </summary>
        void OnReset();
    }
}