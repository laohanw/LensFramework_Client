﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class EffectStoreData
    {
        public int index;
        public int count;
        public int group=0;
        public int capacity;
    }
}
