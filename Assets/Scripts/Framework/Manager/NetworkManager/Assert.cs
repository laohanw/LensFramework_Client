﻿using System;

namespace Lens.Framework.Managers
{
    public static class Assert
    {
        
        public static void IsTrue(bool args)
        {
            if (!args)
            {
                throw new Exception("args is false");
            }
        }
        
        public static void IsTrue(bool args, string info)
        {
            if (!args)
            {
                throw new Exception("args is false" + FormatInfo(info));
            }
        }
        
        public static void IsFalse(bool args)
        {
            if (args)
            {
                throw new Exception("args is true");
            }
        }
        
        public static void IsFalse(bool args, string info)
        {
            if (args)
            {
                throw new Exception("args is true" + FormatInfo(info));
            }
        }
        
        public static void AreEqual(object args1, object args2)
        {
            if (args1 != args2)
            {
                throw new Exception("they are not equal");
            }
        }
        
        public static void AreEqual(object args1, object args2, string info)
        {
            if (args1 != args2)
            {
                throw new Exception("they are not equal" + FormatInfo(info));
            }
        }
        
        public static void AreNotEqual(object args1, object args2)
        {
            if (args1 == args2)
            {
                throw new Exception("they are equal");
            }
        }
        
        public static void AreNotEqual(object args1, object args2, string info)
        {
            if (args1 == args2)
            {
                throw new Exception("they are equal" + FormatInfo(info));
            }
        }
        
        public static void IsNotNull(object args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args is null");
            }
        }
        
        public static void IsNotNull(object args, string info)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args is null" + FormatInfo(info));
            }
        }
        
        public static void IsNotNullOrEmpty(string args)
        {
            if (string.IsNullOrEmpty(args))
            {
                throw new Exception("string is null or empty");
            }
        }
        
        public static void IsNotNullOrEmpty(string args, string info)
        {
            if (string.IsNullOrEmpty(args))
            {
                throw new Exception("string is null or empty" + FormatInfo(info));
            }
        }
        
        public static void IsNull(object args)
        {
            if (args != null)
            {
                throw new Exception("args is not null");
            }
        }
        
        public static void IsNull(object args, string info)
        {
            if (args != null)
            {
                throw new Exception("args is not null" + FormatInfo(info));
            }
        }
        
        public static void IsNullOrEmpty(string args)
        {
            if (!string.IsNullOrEmpty(args))
            {
                throw new Exception("args is not null or empty");
            }
        }
        
        public static void IsNullOrEmpty(string args, string info)
        {
            if (!string.IsNullOrEmpty(args))
            {
                throw new Exception("args is not null or empty" + FormatInfo(info));
            }
        }
        
        public static void Fail(string args)
        {
            throw new Exception(args);
        }
        private static string FormatInfo(string info)
        {
            if (info == null)
            {
                info = string.Empty;
            }
            return string.Format("[{0}]", info);
        }
    }
}