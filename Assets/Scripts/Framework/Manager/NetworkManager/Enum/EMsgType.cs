﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public enum EMsgType
    {
        Login=60001,
        /// <summary>
        /// 心跳包
        /// </summary>
        Heart=60002
    }
}
