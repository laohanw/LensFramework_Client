﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    /// <summary>
    /// 需要余服务器商定类型
    /// </summary>
    public enum EPackType
    {
        /// <summary>
        /// C#端使用的protobuf
        /// </summary>
        CSharpProto=0,
        /// <summary>
        /// lua使用的protobuf
        /// </summary>
        LuaProto=1,
        /// <summary>
        /// 纯byte拼接的类型
        /// </summary>
        Bytes=2
    }
}
