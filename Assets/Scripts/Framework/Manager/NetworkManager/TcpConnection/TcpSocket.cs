﻿using System;
using System.Net;
using System.Net.Sockets;
using Lens.Debugger;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class TcpSocket
    {
        /// <summary>
        /// Get socket and modify it(for example: set timeout)
        /// </summary>
        public System.Net.Sockets.Socket Socket { get; private set; }
        public IPEndPoint IPPoint { get; private set; }
        /// <summary>
        /// if connected(should use heart beat check if server disconnect)
        /// </summary>
        public bool IsConnected
        {
            get { return Socket != null && Socket.Connected; }
        }
        /// <summary>
        /// connected count
        /// </summary>
        public int ConnectedCount {get;private set;}

        /// <summary>
        /// trigger when connecting
        /// </summary>
        public event Action connectingHandler;

        /// <summary>
        /// trigger when connected
        /// </summary>
        public event Action connectedHandler;

        /// <summary>
        /// Trigger when disconnected
        /// </summary>
        public event Action disconnectedHandler;
        /// <summary>
        /// trigger connect failure
        /// </summary>
        public event Action<string> connectFailureHandler;
        /// <summary>
        /// trigger send failure
        /// </summary>
        public event Action<string> sendFailureHandler;
        /// <summary>
        /// triger receive failure
        /// </summary>
        public event Action<string> receiveFailureHandler;
        /// <summary>
        /// trigger when get message from server, it havent unpacked
        /// use .net socket api
        /// </summary>
        public event Action<byte[]> receivedHandler;

        /// <summary>
        /// trigger when send message to server, it already packed
        /// use .net socket api
        /// </summary>
        public event Action<byte[]> sendedHandler;

        /// <summary>
        /// Send buffer
        /// If disconnect, user can operate the remain data
        /// </summary>
        public IBlockBuffer<byte> SendBuffer { get; private set; }

        /// <summary>
        /// Receive buffer
        /// </summary>
        public IBlockBuffer<byte> ReceiveBuffer { get; private set; }
        
        private readonly object m_locker = new object();
        private Queue<byte[]> m_waitSendQueue = new Queue<byte[]>();
        private Dictionary<int, INetPlugin> m_plugins = new Dictionary<int, INetPlugin>();
        private Dictionary<int, INetMessage> m_messages = new Dictionary<int, INetMessage>();
        private Dictionary<int, INetPackage> m_packages = new Dictionary<int, INetPackage>();
        /// <summary>
        /// The default buffer is 1<<16, if small will automatically add buffer block
        /// </summary>
        /// <param name="bufferSize"></param>
        public TcpSocket(int bufferSize = 1 << 16)
        {
            SendBuffer = new BlockBuffer<byte>(bufferSize);
            ReceiveBuffer = new BlockBuffer<byte>(bufferSize);
        }
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            lock (m_locker)
            {
                Disconnect();
                Socket = null;
                connectingHandler = null;
                connectedHandler = null;
                disconnectedHandler = null;
                receivedHandler = null;
                sendedHandler = null;
                SendBuffer.Dispose();
                ReceiveBuffer.Dispose();
            }
        }
        public void Update()
        {
            foreach (var item in m_plugins)
            {
                item.Value.Update();
            }
            foreach (var item in m_messages)
            {
                item.Value.Update();
            }
            if (IsConnected)
            {
                while (m_waitSendQueue.Count > 0)
                {
                    var s = m_waitSendQueue.Dequeue();
                    Send(s);
                }
            }
        }
        public void Send(int msgId, byte[] bytes)
        {
            var x = m_packages[GetPackType(msgId)].Pack(msgId, bytes);
            Send(x);
        }
        public void AddPackage(EPackType packageType, INetPackage package)
        {
            m_packages.Add((int)packageType, package);
        }
        /// <summary>
        /// Add plugin to extend logic
        /// </summary>
        /// <param name="plugin"></param>
        public void AddPlugin(ENetPluginType pluginType, INetPlugin plugin)
        {
            plugin.TcpSocket = this;
            plugin.Initialize();
            m_plugins.Add((int)pluginType, plugin);
        }
        /// <summary>
        /// Get plugin by name
        /// </summary>
        /// <param name="name">plugin's name</param>
        /// <returns>plugin</returns>
        public INetPlugin GetPlugin(ENetPluginType pluginType)
        {
            return m_plugins[(int)pluginType];
        }
        public void AddMessage(EPackType packType, INetMessage message)
        {
            m_messages.Add((int)packType, message);
        }
        public INetMessage GetMessage(int packType)
        {
            return m_messages[(int)packType];
        }
        public void ClearCache()
        {
            m_waitSendQueue.Clear();
        }
        public void Reconnect()
        {
            if (IPPoint!=null && !IsConnected)
            {
                lock (m_locker)
                {
                    if (IsConnected)
                    {
                        try
                        {
                            Socket.Shutdown(SocketShutdown.Both);
                            Socket.Close();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.ToString());
                        }
                    }
                }
                Connect(IPPoint);
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        public void Connect(IPEndPoint iep)
        {
            IPPoint = iep;
            lock (m_locker)
            {
                Assert.IsFalse(IsConnected, "Already Connected");
                Assert.IsNotNull(iep, "iep is null");
                if (connectingHandler != null)
                {
                    connectingHandler();
                }
                try
                {
                    Socket = new System.Net.Sockets.Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    ConnectedCount++;
                }
                catch (Exception e)
                {
                    if (connectFailureHandler != null)
                    {
                        connectFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
                try
                {
                    Socket.BeginConnect(iep, delegate (IAsyncResult ar)
                    {
                        try
                        {
                            var socket = ar.AsyncState as System.Net.Sockets.Socket;
                            Assert.IsNotNull(socket, "Socket is null when end connect");
                            socket.EndConnect(ar);
                            if (IsConnected)
                            {
                                if (connectedHandler != null)
                                {
                                    connectedHandler();
                                }
                                ToReceive();
                            }
                            else
                            {
                                if (connectFailureHandler!=null)
                                {
                                    connectFailureHandler("Connect faild");
                                }
                                Assert.Fail("Connect faild");
                            }
                        }
                        catch (Exception e)
                        {
                            if (connectFailureHandler != null)
                            {
                                connectFailureHandler(e.ToString());
                            }
                            throw new Exception(e.ToString());
                        }

                    }, Socket);
                }
                catch (Exception e)
                {
                    if (connectFailureHandler != null)
                    {
                        connectFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        /// <param name="ip">ipv4/ipv6</param>
        /// <param name="port"></param>
        public void Connect(string ip, int port)
        {
            lock (m_locker)
            {
                Assert.IsNotNullOrEmpty(ip, "ip is null or empty");
                var iep = new IPEndPoint(IPAddress.Parse(ip), port);
                Connect(iep);
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public void Connect(IPAddress ip, int port)
        {
            lock (m_locker)
            {
                Assert.IsNotNull(ip, "ip is null");
                var iep = new IPEndPoint(ip, port);
                Connect(iep);
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        /// <param name="www"></param>
        /// <param name="port"></param>
        public void ConnectWWW(string www, int port)
        {
            var hostEntry = Dns.GetHostEntry(www);

            foreach (IPAddress address in hostEntry.AddressList)
            {
                IPEndPoint ipe = new IPEndPoint(address, port);
                Connect(ipe);
            }
        }
        public void Disconnect()
        {
            lock (m_locker)
            {
                if (IsConnected)
                {
                    ConnectedCount = 0;
                    IPPoint = null;
                    try
                    {
                        if (disconnectedHandler != null)
                        {
                            disconnectedHandler();
                        }
                    }
                    catch { }
                    try
                    {
                        Socket.Shutdown(SocketShutdown.Both);
                        Socket.Close();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.ToString());
                    }
                }
            }
        }
        public int GetPackType(int msgId)
        {
            int packageType = 0;
            if (msgId < 10000)
            {
                packageType = (int)EPackType.CSharpProto;
            }
            else if (msgId >= 10000 && msgId < 50000)
            {
                packageType = (int)EPackType.LuaProto;
            }
            else
            {
                packageType = (int)EPackType.Bytes;
            }
            return packageType;
        }
        /// <summary>
        /// Send bytes to server
        /// </summary>
        /// <param name="bytes"></param>
        private void Send(byte[] bytes)
        {
            if (IsConnected)
            {
                lock (SendBuffer)
                {
                    int index = 0;
                    int length = bytes.Length;
                    Assert.IsTrue(IsConnected, "From send : disconnected");
                    SendBuffer.Write(bytes, index, length);
                    try
                    {
                        Socket.BeginSend(bytes, index, length, SocketFlags.None, EndSend, Socket);
                    }
                    catch (Exception e)
                    {
                        if (sendFailureHandler!=null)
                        {
                            sendFailureHandler(e.ToString());
                        }
                        m_waitSendQueue.Enqueue(bytes);
                        throw new Exception(e.ToString());
                    }
                }
            }
            else
            {
                m_waitSendQueue.Enqueue(bytes);
            }
        }
        private void EndSend(IAsyncResult ar)
        {
            lock (SendBuffer)
            {
                if (IsConnected)//User disconnect tcpConnection proactively
                {
                    int length = 0;
                    try
                    {
                        var socket = ar.AsyncState as System.Net.Sockets.Socket;
                        Assert.IsNotNull(socket, "Socket is null when end send");
                        length = socket.EndSend(ar);
                    }
                    catch (Exception e)
                    {
                        if (sendFailureHandler != null)
                        {
                            sendFailureHandler(e.ToString());
                        }
                        throw new Exception(e.ToString());
                    }
                    byte[] sendBytes = SendBuffer.Read(length);
                    SendBuffer.ResetIndex();
                    if (sendedHandler != null)
                    {
                        sendedHandler(sendBytes);
                    }
                }
            }
        }
        private void ToReceive()
        {
            lock (ReceiveBuffer)
            {
                try
                {
                    var count = ReceiveBuffer.Size - ReceiveBuffer.WritePosition;
                    Socket.BeginReceive(ReceiveBuffer.Buffer, ReceiveBuffer.WritePosition, count, SocketFlags.None,
                        EndReceive, Socket);
                }
                catch (Exception e)
                {
                    if (receiveFailureHandler != null)
                    {
                        receiveFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
            }
        }
        private void EndReceive(IAsyncResult ar)
        {
            lock (ReceiveBuffer)
            {
                int length = 0;
                try
                {
                    var socket = ar.AsyncState as System.Net.Sockets.Socket;
                    Assert.IsNotNull(socket, "Socket is null when end receive");
                    length = socket.EndReceive(ar);
                }
                catch (Exception e)
                {
                    if (receiveFailureHandler != null)
                    {
                        receiveFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
                UnityEngine.Debug.Log("ReceiveBuffer:"+ length+"   " + ReceiveBuffer.WritePosition + "   " + ReceiveBuffer.ReadPosition);
                int curLength = 0;
                while (curLength < length)
                {
                    try
                    {
                        ReceiveBuffer.MoveWritePosition(2);
                        var msgId = EndianBitConverter.GetBigEndian(BitConverter.ToUInt16(ReceiveBuffer.Read(2), 0));
                        ReceiveBuffer.MoveWritePosition(2);
                        var bodyLength = EndianBitConverter.GetBigEndian(BitConverter.ToUInt16(ReceiveBuffer.Read(2), 0));

                        curLength += bodyLength + 2+2;
                        ReceiveBuffer.MoveWritePosition(bodyLength);
                        var bytes = ReceiveBuffer.Read(bodyLength);

                        var result = ToPack(msgId, bytes);

                        if (receivedHandler != null)
                        {
                            receivedHandler(bytes);
                        }
                        Received(msgId, result);
                        UnityEngine.Debug.Log(msgId+"   "+curLength + "   "+ bodyLength+"   "+ ReceiveBuffer.WritePosition + "   " + ReceiveBuffer.ReadPosition);
                    }
                    catch (Exception exc)
                    {
                        if (receiveFailureHandler != null)
                        {
                            receiveFailureHandler(exc.ToString());
                        }
                    }
                }
                ReceiveBuffer.ResetIndex();
                UnityEngine.Debug.Log("ResetIndex:" + ReceiveBuffer.WritePosition+"   "+ ReceiveBuffer.ReadPosition);
                if (length > 0)
                {
                    UnityEngine.Debug.Log("ToReceive");
                    ToReceive();
                }
                else
                {
                    if (disconnectedHandler != null)
                    {
                        disconnectedHandler();
                    }
                }
            }
        }
        private byte[] ToPack(int msgId, byte[] bytes)
        {
            var packageType = GetPackType(msgId);
            return m_packages[packageType].Unpack(bytes);
        }
        private void Received(int msgId, byte[] bytes)
        {
            var packageType = GetPackType(msgId);
            m_messages[packageType].ToQueue(msgId, bytes);
        }
    }
}