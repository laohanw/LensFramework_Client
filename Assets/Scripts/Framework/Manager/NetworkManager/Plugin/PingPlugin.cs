﻿/***************************************************************
 * Description:   
 * bug: if you use this logic with unity(.net3.5)will occur a bug, but unity(.net4.6)will fine.
 * 
 * Documents: https://github.com/hiramtan/HiSocket
 * Author: hiramtan@live.com
***************************************************************/

using System.Net;
using System.Net.NetworkInformation;

namespace Lens.Framework.Managers
{
    public sealed class PingPlugin : NetPluginBase
    {
        public long Ping;
        private Ping m_ping;
        private float m_time;
        public PingPlugin() 
        {
            PingLogic();
            m_ping = new Ping();
        }

        public override void Update()
        {
            PingLogic();
        }

        void PingLogic()
        {
            if (TcpSocket!=null && TcpSocket.IsConnected)
            {
                if (UnityEngine.Time.time - m_time > 2)
                {
                    m_time = UnityEngine.Time.time;
                    var ip = (TcpSocket as TcpSocket).Socket.RemoteEndPoint as IPEndPoint;
                    var reply = m_ping.Send(ip.Address);
                    if (reply.Status == IPStatus.Success)
                    {
                        Ping = reply.RoundtripTime;
                    }
                }
            }
        }
    }
}
