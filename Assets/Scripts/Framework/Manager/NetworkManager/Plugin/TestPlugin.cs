﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    class TestPlugin:NetPluginBase
    {
        public override void Initialize()
        {
            base.Initialize();

            TcpSocket.receivedHandler += OnReceive;
            TcpSocket.sendedHandler += OnSended; ;
            TcpSocket.sendFailureHandler += OnSendFailure;
            TcpSocket.connectFailureHandler += OnConnectFailure;
            TcpSocket.disconnectedHandler += OnDsiconnected;
            TcpSocket.connectedHandler += OnConnected;
        }

        private void OnConnected()
        {
            UnityEngine.Debug.Log("OnConnected ");
        }

        private void OnDsiconnected()
        {
            UnityEngine.Debug.Log("OnDsiconnected ");
        }

        private void OnSended(byte[] obj)
        {
            string s = "";
            for (int i = 0; i < obj.Length; i++)
            {
                s += "  "+obj[i].ToString();
            }
            UnityEngine.Debug.Log("OnSended obj:" + s);
        }

        private void OnReceive(byte[] obj)
        {
            //string s = "";
            //for (int i = 0; i < obj.Length; i++)
            //{
            //    s += "  " + obj[i].ToString();
            //}
            UnityEngine.Debug.Log("Received obj:" );
        }

        private void OnConnectFailure(string obj)
        {
            UnityEngine.Debug.Log("OnConnectFailure: " + obj);
        }

        private void OnSendFailure(string obj)
        {
            UnityEngine.Debug.Log("OnSendFailure: " + obj);
        }
        public override void Update()
        {
            base.Update();
        }

    }
}
