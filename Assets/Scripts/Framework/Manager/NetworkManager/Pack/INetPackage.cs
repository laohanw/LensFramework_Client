﻿using System;

namespace Lens.Framework.Managers
{
    public interface INetPackage
    {
        /// <summary>
        /// Handle data receive from server
        /// </summary>
        /// <param name="bytes"></param>
        byte[] Unpack(byte[] source);

        /// <summary>
        /// handle data will send to server
        /// </summary>
        /// <param name="bytes"></param>
        byte[] Pack(int msgId,byte[] source);
    }
}
