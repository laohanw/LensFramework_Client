﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class BytePackage : PackageBase
    {
        protected override byte[] Pack(int msgId,BlockBuffer<byte> bytes)
        {
            //Use int as header
            int length = bytes.WritePosition;
            var msgIdBytes= BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)msgId));
            var bodyLengthBytes = BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)length));
            var newBytes = new BlockBuffer<byte>(2 + bodyLengthBytes.Length + length );
            //Write header and body to buffer
            newBytes.Write(msgIdBytes);
            newBytes.Write(bodyLengthBytes);
            if (length>0)
            {
                newBytes.Write(bytes.Buffer);
            }
            //Notice pack funished
            return newBytes.Buffer;
        }

        protected override byte[] Unpack(BlockBuffer<byte> bytes)
        {
            var data = bytes.Read(bytes.WritePosition);
            bytes.ResetIndex();
            return data;
        }
    }
}
