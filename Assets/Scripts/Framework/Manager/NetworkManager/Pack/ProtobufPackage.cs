﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class ProtobufPackage : PackageBase
    {
        protected override byte[] Pack(int msgId,BlockBuffer<byte> bytes)
        {
            //Use int as header
            int length = bytes.WritePosition;
            var msgIdByte= BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)msgId));
            var lengthByte = BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)length));
            var newBytes = new BlockBuffer<byte>(msgIdByte.Length+length + lengthByte.Length);
            //Write header and body to buffer
            newBytes.Write(msgIdByte);
            newBytes.Write(lengthByte);
            newBytes.Write(bytes.Buffer);
            UnityEngine.Debug.Log("Pack" + length + "   " + lengthByte.Length + "   " + newBytes.Buffer.Length);
            //Notice pack funished
            return newBytes.Buffer;
        }

        protected override byte[] Unpack(BlockBuffer<byte> bytes)
        {
            var data = bytes.Read(bytes.Buffer.Length);
            bytes.ResetIndex();
            UnityEngine.Debug.Log("Unpack: " + bytes.WritePosition + "  " + bytes.Buffer.Length);
            return data;
        }
    }
}
