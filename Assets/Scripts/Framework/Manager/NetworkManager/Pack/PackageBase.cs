﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public abstract class PackageBase : INetPackage
    {
        /// <summary>
        /// Unpack
        /// </summary>
        /// <param name="source"></param>
        /// <param name="onUnpacked"></param>
        byte[] INetPackage.Unpack(byte[] source)
        {
            using (BlockBuffer<byte> buffer = new BlockBuffer<byte>(source))
            {
                return Unpack(buffer);
            }
        }

        /// <summary>
        /// Pack
        /// </summary>
        /// <param name="source"></param>
        /// <param name="onPacked"></param>
        byte[] INetPackage.Pack(int msgId,byte[] source)
        {
            using (BlockBuffer<byte> buffer = new BlockBuffer<byte>(source))
            {
                return Pack(msgId,buffer);
            }
        }

        /// <summary>
        /// Sub class achive this methond
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="onUnpacked"></param>
        protected abstract byte[] Unpack(BlockBuffer<byte> bytes);

        /// <summary>
        /// Sub class achive this methond
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="onPacked"></param>
        protected abstract byte[] Pack(int msgId,BlockBuffer<byte> bytes);
    }
}
