﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Lens.Framework.Managers
{
    public abstract class NetMessageBase :INetMessage
    {
        public Dictionary<int, List<Action<IExtensible>>> p_dic = new Dictionary<int, List<Action<IExtensible>>>();
        public List<Action<int, IExtensible>> p_listenAlls = new List<Action<int, IExtensible>>();
        public Queue<byte[]> p_bytes = new Queue<byte[]>();
        public Queue<int> p_msgs = new Queue<int>();
        public string Name { get; set; }
        void INetMessage.AddListener(int msgId, Action<IExtensible> callback)
        {
            List<Action<IExtensible>> list;
            if (p_dic.TryGetValue(msgId, out list))
            {
                list.Add(callback);
            }
            else
            {
                list = new List<Action<IExtensible>>();
                list.Add(callback);
                p_dic.Add(msgId, list);
            }
        }
        void INetMessage.RemoveListener(int msgId, Action<IExtensible> callback)
        {
            List<Action<IExtensible>> list;
            if (p_dic.TryGetValue(msgId, out list))
            {
                list.Remove(callback);
            }
        }
        void INetMessage.ToQueue(int msgId, byte[] bytes)
        {
            p_msgs.Enqueue(msgId);
            p_bytes.Enqueue(bytes);
        }
        void INetMessage.AddListenAll(Action<int, IExtensible> callback)
        {
            p_listenAlls.Add(callback);
        }
        void INetMessage.RemoveListenAll(Action<int, IExtensible> callback)
        {
            p_listenAlls.Remove(callback);
        }

        public abstract void Update();

        
    }
}
