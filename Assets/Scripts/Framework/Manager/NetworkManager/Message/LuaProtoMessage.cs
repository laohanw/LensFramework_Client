﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class LuaProtoMessage:NetMessageBase
    {
        public override void Update()
        {
            if (p_bytes.Count > 0)
            {
                var bytes = p_bytes.Dequeue();
                var msgId = p_msgs.Dequeue();
                var ext = new LuaExtensible(bytes);
                List<Action<IExtensible>> list;
                if (p_dic.TryGetValue(msgId, out list))
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i](ext);
                    }
                    p_dic.Remove(msgId);
                }
                for (int i = 0; i < p_listenAlls.Count; i++)
                {
                    p_listenAlls[i](msgId, ext);
                }
            }
        }
    }
}
