﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Lens.Framework.Managers
{
    public interface INetMessage
    {
        void AddListener(int msgId, Action<IExtensible> callback);
        void RemoveListener(int msgId, Action<IExtensible> callback);
        void AddListenAll(Action<int,IExtensible> callback);
        void RemoveListenAll(Action<int,IExtensible> callback);
        void ToQueue(int msgId, byte[] bytes);
        void Update();
    }
}
