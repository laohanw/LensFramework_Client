﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lens.Framework.Managers
{
    public class ByteExtensible : IExtensible
    {
        public byte[] bytes;
        public ByteExtensible(byte[] bytes)
        {
            this.bytes = bytes;
        }
        public ByteExtensible()
        {

        }
        public virtual byte[] Serialize<T>(T t) where T :IExtensible
        {
            return null;
        }
        public virtual T Deserialize<T>(byte[] bytes) where T :IExtensible
        {
            return default(T);
        }
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
            return null;
        }
    }
}
