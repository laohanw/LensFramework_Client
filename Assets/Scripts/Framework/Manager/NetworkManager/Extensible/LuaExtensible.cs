﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lens.Framework.Managers
{

    public class LuaExtensible : IExtensible
    {
        public byte[] bytes;
        public LuaExtensible(byte[] bytes)
        {
            this.bytes = bytes;
        }
        public virtual byte[] Serialize()
        {
            return bytes;
        }
        public virtual void Deserialize(byte[] bytes)
        {

        }
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
            return null;
        }
    }
}
