﻿using ProtoBuf;
using System;
using System.IO;
using System.Net;

namespace Lens.Framework.Managers
{
    using Core;
    public class TcpManager:Singleton<TcpManager>
    {
        private TcpSocket m_tcpSocket;
        public override void Initialize(Options options=null)
        {
            p_instance.m_tcpSocket = new TcpSocket();
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Ping, new PingPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Statistical, new PingPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Reconnect, new ReconnectPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Heart, new HeartPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Test, new TestPlugin());

            p_instance.m_tcpSocket.AddPackage(EPackType.CSharpProto, new ProtobufPackage());
            p_instance.m_tcpSocket.AddPackage(EPackType.LuaProto, new ProtobufPackage());
            p_instance.m_tcpSocket.AddPackage(EPackType.Bytes, new BytePackage());

            p_instance.m_tcpSocket.AddMessage(EPackType.CSharpProto, new ProtoMessage());
            p_instance.m_tcpSocket.AddMessage(EPackType.LuaProto, new LuaProtoMessage());
            p_instance.m_tcpSocket.AddMessage(EPackType.Bytes, new ByteMessage());
        }
        public override void Dispose()
        {
            Disconnect();
            base.Dispose();
        }
        public override void Update()
        {
            p_instance.m_tcpSocket.Update();
        }
        public static void Connect(IPEndPoint point)
        {
            p_instance.m_tcpSocket.Connect(point);
        }
        public static void Reconnect()
        {
            p_instance.m_tcpSocket.Reconnect();
        }
        public static void Disconnect()
        {
            p_instance.m_tcpSocket.Disconnect();
        }
        public static void Send(int msgId,byte[] bytes)
        {
            p_instance.m_tcpSocket.Send(msgId, bytes);
        }
        public static void Send(int msgId, byte[] bytes,Action<IExtensible> callback)
        {
            var packType = p_instance.m_tcpSocket.GetPackType(msgId);
            if (callback!=null)
            {
                p_instance.m_tcpSocket.GetMessage(packType).AddListener(msgId, callback);
            }
            p_instance.m_tcpSocket.Send(msgId, bytes);
        }
        public static void Send(int msgId,MemoryStream mm)
        {
            byte[] bytes = new byte[mm.Length];
            mm.Position = 0;
            mm.Read(bytes, 0, (int)mm.Length);
            Send(msgId, bytes);
            mm.Close();
            mm.Dispose();
        }
        public static void Send(int msgId, MemoryStream mm, Action<IExtensible> callback)
        {
            byte[] bytes = new byte[mm.Length];
            mm.Position = 0;
            mm.Read(bytes, 0, (int)mm.Length);
            Send(msgId, bytes, callback);
            mm.Close();
            mm.Dispose();
        }
        public static void AddListener(EMsgType msgType,Action<IExtensible> callback)
        {
            if (p_instance == null) return;
            var packType = p_instance.m_tcpSocket.GetPackType((int)msgType);
            p_instance.m_tcpSocket.GetMessage(packType).AddListener((int)msgType, callback);
        }
        public static void RemoveListener(EMsgType msgType, Action<IExtensible> callback)
        {
            if (p_instance == null) return;
            var packType = p_instance.m_tcpSocket.GetPackType((int)msgType);
            p_instance.m_tcpSocket.GetMessage(packType).RemoveListener((int)msgType, callback);
        }
        public static void AddListenAll(EPackType packType,Action<int,IExtensible> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.GetMessage((int)packType).AddListenAll(callback);
        }
        public static void RemoveListenAll(EPackType packType, Action<int, IExtensible> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.GetMessage((int)packType).RemoveListenAll(callback);
        }
        public static void AddListener(ENetState netState,Action callback)
        {
            if (p_instance == null) return;
            switch (netState)
            {
                case ENetState.Connected:
                    p_instance.m_tcpSocket.connectedHandler += callback;
                    break;
                case ENetState.Connecting:
                    p_instance.m_tcpSocket.connectingHandler += callback;
                    break;
                case ENetState.Disconnected:
                    p_instance.m_tcpSocket.disconnectedHandler += callback;
                    break;
                case ENetState.ReconnectFailure:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectFailureHandler += callback;
                    break;
                case ENetState.Reconnecting:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectingHandler += callback;
                    break;
                case ENetState.Reconnected:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectedHandler += callback;
                    break;
                default:
                    break;
            }
        }
        public static void RemoveListener(ENetState netState, Action callback)
        {
            if (p_instance == null) return;
            switch (netState)
            {
                case ENetState.Connected:
                    p_instance.m_tcpSocket.connectedHandler -= callback;
                    break;
                case ENetState.Connecting:
                    p_instance.m_tcpSocket.connectingHandler -= callback;
                    break;
                case ENetState.Disconnected:
                    p_instance.m_tcpSocket.disconnectedHandler -= callback;
                    break;
                case ENetState.ReconnectFailure:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectFailureHandler -= callback;
                    break;
                case ENetState.Reconnecting:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectingHandler -= callback;
                    break;
                case ENetState.Reconnected:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectedHandler -= callback;
                    break;
                default:
                    break;
            }
        }
    }
}
