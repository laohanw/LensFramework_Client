﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    [XLua.CSharpCallLua]
    public interface L_Net
    {
        void C_OnConnected();
        void C_OnDisconnect();
        void C_OnReceive(int msgId, IExtensible extensible);
    }
}
