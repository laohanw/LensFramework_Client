﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using Core;
    using System.IO;
    using XLua;
    public class LuaManager : Singleton<LuaManager>
    {
        public class MyOptions:Options{
            public string luaUrl="";
        }
        private string publicKey = "BgIAAACkAABSU0ExAAQAAAEAAQDh4S7SN5LYj/GFA2ZVhhgptP9qQ1BIHIdGAbJsbdUhye/nI2ls5r2VM8r2tksxwUmnOwnn7xzEmU5aAH1KPjGO46SgP6DSKkz8nkzFQSnQ1jxMz/vyO/e1p7Hxd1CezFx0LILdKJBXzxDmdfDTTuhY9puWpQTpd/nKBS+oAIe8mQ==";
        private LuaEnv m_env;
        private bool m_started;
        private MyOptions m_options;
        private Dictionary<string, byte[]> m_luaFiles = new Dictionary<string, byte[]>();
        public override void Initialize(Options options)
        {
            m_options=options as MyOptions;
            base.Initialize(options);
            m_env = new LuaEnv();
        }
        public void Preload()
        {
            string luaPath;
            #if RELEASE
                luaPath = Path.Combine(Application.dataPath, "../Lua/src/");
            #else
                luaPath = m_options.luaUrl;
            #endif
            var files = Directory.GetFiles(luaPath, "*.lua", SearchOption.AllDirectories);
            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i];
                var replateName = Path.GetFileNameWithoutExtension(file); //file.Replace(luaPath, "");
                byte[] bytes = null;
                if (!m_luaFiles.TryGetValue(replateName, out bytes))
                {
                    using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                    {
                        bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, bytes.Length);
                        m_luaFiles.Add(replateName, bytes);
                    }
                }
            }
        }
        public void Start(string mainLua)
        {
            m_env.AddBuildin("rapidjson", XLua.LuaDLL.Lua.LoadRapidJson);
            m_env.AddBuildin("lpeg", XLua.LuaDLL.Lua.LoadLpeg);
            m_env.AddBuildin("pb", XLua.LuaDLL.Lua.LoadLuaProfobuf);
            m_env.AddBuildin("ffi", XLua.LuaDLL.Lua.LoadFFI);
            #if DEVELOPMENT
                m_env.AddLoader(OnLoader);
            #else
                m_env.AddLoader(new SignatureLoader(publicKey, OnLoader));
            #endif
            m_env.DoString("require('" + mainLua + "')", mainLua);
            m_started = true;
        }
        public static bool isStarted()
        {
            if (p_instance==null)
            {
                return false;
            }
            return p_instance.m_started;
        }
        [LuaCallCSharp]
        public static string LoadFileString(string path)
        {
            byte[] bytes = LoadFileBytes(path);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
        [LuaCallCSharp]
        public static byte[] LoadFileBytes(string path)
        {
            string filePath;
            #if DEVELOPMENT
                filePath = Path.Combine(Application.dataPath, "../Lua/src/", path);
            #else
                filePath = Path.Combine(Core.Define.luaUrl, path);
            #endif
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
                return bytes;
            }
        }
        public static T GetTable<T>(string name)
        {
            return p_instance.m_env.Global.Get<T>(name);
        }
        public static void DoString(string str)
        {
            p_instance.m_env.DoString(str);
        }
        public override void Update()
        {
            if (m_env == null) return;
            m_env.Tick();
        }
        public override void Dispose()
        {
            m_env.Dispose();
            base.Dispose();
        }

        private byte[] OnLoader(ref string filename)
        {
            byte[] bytes;
            if (m_luaFiles.TryGetValue(filename, out bytes))
            {
                return bytes;
            }
            else
            {
                string luaPath;
                string n = filename.Contains(".lua") ?filename:filename + ".lua";
                #if DEVELOPMENT
                    luaPath = Path.Combine(Application.dataPath, "../Lua/src/", n);
                #else
                    luaPath = Path.Combine(Core.Define.luaUrl, n);
                #endif
                if (File.Exists(luaPath))
                {
                    using (FileStream stream = new FileStream(luaPath, FileMode.Open, FileAccess.Read))
                    {
                        bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, bytes.Length);
                        return bytes;
                    }
                }
                else
                {
                    #if DEVELOPMENT
                        luaPath = Path.Combine(Application.dataPath, "../Lua/lua_modules", filename + "/index.lua");
                    #else
                        luaPath = Path.Combine(Core.Define.luaUrl, "lua_modules", filename + "/index.lua");
                    #endif

                    if (File.Exists(luaPath))
                    {
                        using (FileStream stream = new FileStream(luaPath, FileMode.Open, FileAccess.Read))
                        {
                            bytes = new byte[stream.Length];
                            stream.Read(bytes, 0, bytes.Length);
                            return bytes;
                        }
                    }
                    else
                    {
                        #if RELEASE
                            luaPath = Path.Combine(Application.dataPath, "../Lua/lua_modules", n);
                        #else
                            luaPath = Path.Combine(m_options.luaUrl, "lua_modules", n);
                        #endif
                        if (File.Exists(luaPath))
                        {
                            using (FileStream stream = new FileStream(luaPath, FileMode.Open, FileAccess.Read))
                            {
                                bytes = new byte[stream.Length];
                                stream.Read(bytes, 0, bytes.Length);
                                return bytes;
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.LogError( "Dont has this lua：  " + luaPath);
                            return null;
                        }

                    }
                }
            }
        }
    }
}