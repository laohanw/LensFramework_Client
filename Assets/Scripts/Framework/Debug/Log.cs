﻿using System;
using System.Collections.Generic;

namespace Lens.Debugger
{
    public class Log
    {
        private static Dictionary<string,string> m_dics=new Dictionary<string,string>();
        private static List<string> m_marks=new List<string>();
        static Log(){
            m_dics.Add("Info","#000000");
        }
        public static void SetTags(Dictionary<string,string> dic){
            foreach (var item in dic)
            {
                if(m_dics.ContainsKey(item.Key)){
                    m_dics[item.Key]=item.Value;
                }
                else{
                    m_dics.Add(item.Key,item.Value);
                }
            }
        }
        public static void SetMarks(List<string> marks){
            for (int i = 0; i < marks.Count ; i++)
            {
                m_marks.Add(marks[i]);
            }
        }
        public static void i(string value)
        {
            i("Info", value);
        }
        public static void i(string type,string value)
        {
            string result;
            if(m_dics.TryGetValue(type,out result)){
                UnityEngine.Debug.Log("<color>"+result+"</color>"+value);
            }
            else{
                UnityEngine.Debug.Log(value);
            }
        }
        public static void f(string value, string mark)
        {
            f("info", value, mark);
        }
        public static void f(string type,string value,string mark)
        {
            if(m_marks.Contains(mark)){
                i(type,value);
            }
        }
    }
}
