﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lens.Framework.Core
{ 
    public abstract class Singleton<T> where T : class, new()
    {
        public class Options{

        }
        protected static T p_instance;
        public static T GetInstance()
        {
            if (Singleton<T>.p_instance != null) return null;
            p_instance = Activator.CreateInstance<T>();
            return p_instance;
        }
        public virtual void Initialize(Options options = null) { }
        public virtual void Update(){}
        public virtual void Dispose()
        {
            if (Singleton<T>.p_instance != null)
            {
                Singleton<T>.p_instance = (T)((object)null);
            }
        }
    }
}