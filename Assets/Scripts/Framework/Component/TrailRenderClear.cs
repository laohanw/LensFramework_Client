﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Lens.Framework.Components
{
    [ExecuteInEditMode]
    public class TrailRenderClear : MonoBehaviour
    {
        private TrailRenderer[] m_list;
        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < m_list.Length; i++)
            {
                m_list[i].enabled = false;
            }
        }
        void OnDisable()
        {
            if (Application.isPlaying)
            {
                if (m_list != null)
                {
                    for (int i = 0; i < m_list.Length; i++)
                    {
                        m_list[i].Clear();
                    }
                }
            }
        }

        void OnEnable()
        {
            if (m_list == null)
            {
                m_list = GetComponentsInChildren<TrailRenderer>();
            }
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                m_list = GetComponentsInChildren<TrailRenderer>();
            }
#endif
        }
    }
}