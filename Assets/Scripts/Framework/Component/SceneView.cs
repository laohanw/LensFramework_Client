﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Lens.Framework.Components
{
    public class SceneView : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                var view = UnityEditor.SceneView.lastActiveSceneView;
                if (view == null) return;
                view.orthographic = false;
                view.LookAtDirect(transform.position, transform.rotation, 0.001f);
            }
#endif
        }
    }
}