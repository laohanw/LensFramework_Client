local FairyGUI = CS.FairyGUI
G_LuaBehavior = require("core/ui/luaBehavior")
G_UIBase = require("core/ui/uiBase")
local fgui = require("core/ui/fairyGUI")
local uiConfig = require("config/uiConfig")
return class(
    {
        tag = "uiManager",
        ctor = function(self)
            for i, v in pairs(uiConfig.extensions) do
                fgui.register_extension(FairyGUI.UIPackage.GetItemURL(v.pkgName, v.resName), require(v.luaPath))
            end
            for i, v in pairs(uiConfig.layer) do
                self.data.layerDic[v] = self:_createLayerItem(v)
            end
        end,
        data = {
            layerDic = {},
            modalDic = {},
            uiQueue = {},
            uiDic = {},
            orderUIDic={},
            orderUIQueue = {},
            currentUIName = nil,
            currentOrderUIName = nil
        },
        ------------------
        -- public methods
        ------------------
        initialize = function(self)
            if uiConfig.defaultUI then
                self:showUI(uiConfig.defaultUI)
            end
        end,
        -- 普通的UI界面
        showUI = function(self, uiName, intent)
            if uiName == nil then
                printf.error("showUI must has uiName")
            end
            local ui = table.tryGetValue(self.data.uiDic, uiName)
            if ui then
                local uiIndex = table.indexOf(self.data.uiQueue, uiName)
                if uiIndex then
                    if uiIndex == 1 then
                        printf.warning("ui is showing  " .. uiName)
                        return
                    else
                        table.remove(self.data.uiQueue, uiIndex)
                        if intent and intent.replace then
                            table.remove(self.data.uiQueue, 1)
                        end
                        table.insert(self.data.uiQueue, 1, uiName)
                    end
                else
                    if intent and intent.replace then
                        table.remove(self.data.uiQueue, 1)
                    end
                    table.insert(self.data.uiQueue, 1, uiName)
                end
                if ui.input and intent then
                    ui.input(intent)
                end
                if ui.super.input and intent then
                    ui.super.input(intent)
                end
                if ui.visible ~= nil then
                    ui:visible()
                end
                ui.super:visible()
                self.data.currentUIName=uiName
            else
                local config = table.tryGetValue(uiConfig.generalUI, uiName)
                if config then
                    local cls = self:_createUIBase(config)
                    self.data.uiDic[config.uiName] = cls
                    if intent and intent.replace and #self.data.uiQueue > 1 then
                        table.remove(self.data.uiQueue, 1)
                    end
                    table.insert(self.data.uiQueue, 1, uiName)
                    self.data.currentUIName=uiName
                else
                    printf.error("UI show ui dont has this uiName  " .. uiName)
                end
            end
        end,
        backUI = function(self, isDispose, count)
            count = count or 1
            if #self.data.uiQueue > 1 then
                for i = 1, count, 1 do
                    local uiName = self.data.uiQueue[i]
                    local ui = table.tryGetValue(self.data.uiDic, uiName)
                    if ui then
                        local config = table.tryGetValue(uiConfig.generalUI, uiName)
                        if config then
                            self:_destroyUIBase(config, ui, isDispose)
                            if isDispose then
                                self.data.uiDic[uiName] = nil
                            end
                            table.remove(self.data.uiQueue, i)
                            self:showUI(self.data.uiQueue[1])
                        else
                            printf.error("UI show ui dont has this uiName  " .. uiName)
                        end
                    else
                        printf.error("UI dont has this ui  " .. uiName)
                    end
                end
            else
                printf.error("dont backUI is the last ui")
            end
        end,
        -- 有排序要求的UI
        showOrderUI = function(self,uiName,intent)
            if uiName == nil then
                printf.error("show OrderUI must has uiName")
            end
            local ui = table.tryGetValue(self.data.orderUIDic, uiName)
            if ui then
                if ui.input and intent then
                    ui.input(intent)
                end
                if ui.super.input and intent then
                    ui.super.input(intent)
                end
                if ui.visible ~= nil then
                    ui:visible()
                end
                ui.super:visible()
                table.insert(self.data.orderUIQueue, 1, uiName)
            else
                local config = table.tryGetValue(uiConfig.orderUI, uiName)
                if config then
                    local cls = self:_createUIBase(config)
                    self.data.orderUIDic[config.uiName] = cls
                    table.insert(self.data.orderUIQueue, 1, uiName)
                else
                    printf.error("UI show orderui dont has this uiName  " .. uiName)
                end
            end
        end,
        hideOrderUI = function(self,uiName)
        end,
        showModal = function(self, uiName)
            local modal = table.tryGetValue(self.data.modalDic, uiName)
            if modal then
                if modal.visible ~= nil then
                    modal:visible()
                end
                modal.super:visible()
            else
                local config = table.tryGetValue(uiConfig.modal, uiName)
                printf.warning("woaso")
                if config then
                    local cls = self:_createUIBase(config)
                    self.data.modalDic[config.uiName] = cls
                else
                    printf.error("UI show modal dont has this uiName  " .. uiName)
                end
            end
        end,
        hideModal = function(self, uiName, isDispose)
            local modal = table.tryGetValue(self.data.modalDic, uiName)
            if modal then
                local config = table.tryGetValue(uiConfig, uiName)
                if config then
                    self:_destroyUIBase(config, modal, isDispose)
                    self.data.modalDic[uiName] = nil
                else
                    printf.error("UI show modal dont has this uiName  " .. uiName)
                end
            else
                printf.error("UI dont has this modal  " .. uiName)
            end
        end,
        -- 模块隔离的根节点layer
        hideLayer = function(self, layer)
            for k, v in pairs(self.data.layerDic) do
                if nil ~= v and v == layer then
                    v.visible = false
                end
            end
        end,
        showLayer = function(self, layer)
            for k, v in pairs(self.data.uiCache) do
                if nil ~= v and v.layer <= layer then
                    v.visible = true
                end
            end
        end,
        showBanner = function(self)
        end,
        ------------------
        -- private methods
        ------------------
        _createLayerItem = function(self, name, index)
            local layerui = FairyGUI.GRoot.inst:GetChild(name)
            if layerui ~= nil then
                layerui.visible = true
                return layerui
            end
            layerui = FairyGUI.GComponent()
            layerui.gameObjectName = name
            layerui.name = name
            if index == nil then
                FairyGUI.GRoot.inst:AddChild(layerui)
            else
                FairyGUI.GRoot.inst:AddChildAt(layerui, index)
            end
            layerui:SetPosition(0, 0, 0)
            layerui:SetSize(FairyGUI.GRoot.inst.width, FairyGUI.GRoot.inst.height)
            return layerui
        end,
        _getUIConfigByName = function(uiName)
            local config = uiConfig.library[uiName]
            if config ~= nil then
                config.uiName = uiName
            end
            return uiConfig
        end,
        _createUIBase = function(self, config)
            C_UIManager.Load(config.pkgName)
            local req = require(config.luaPath)
            local cls = class(req)
            local ui = FairyGUI.UIPackage.CreateObject(config.pkgName, config.resName)
            cls.super.view = ui
            if cls.view == nil then
                cls.view = ui
            end
            if cls.created ~= nil then
                cls:created()
            end
            cls.super:created()
            self.data.layerDic[config.layer]:AddChildAt(ui, 0)
            if cls.bind ~= nil then
                cls:bind()
            end
            cls.super.bind()
            if cls.visible ~= nil then
                cls:visible()
            end
            cls.super:visible()
            return cls
        end,
        _destroyUIBase = function(self, config, cls, isDispose)
            if cls.unbind ~= nil then
                cls:unbind()
            end
            cls.super:unbind()
            if cls.invisible ~= nil then
                cls:invisible()
            end
            cls.super.invisible()
            self.data.layerDic[config.layer]:RemoveChild(cls.super.view)
            if isDispose ~= nil or isDispose == false then
                if cls.dispose ~= nil then
                    cls:dispose()
                end
                cls.super:dispose()
                C_UIManager.Unload(config.pkgName)
            end
        end
    }
)
