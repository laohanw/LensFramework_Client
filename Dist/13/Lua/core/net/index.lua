return class({
    tag="network",
    ctor=function(self)
        self.data.proto=class(require("proto/index"))
    end,
    data={
        proto,
        listeners={},
        connecteds={},
        disconnecteds={}
    },
    initialize=function( )
    end,
    connectedHandler=function(self, callback ,single)
        single=single or false
        table.insert( self.data.connecteds,1,{["single"]=single,["callback"]=callback});
    end,
    disconnectedHandler=function( self,callback,single )
        single=single or false
        table.insert( self.data.disconnecteds,1,{["single"]=single,["callback"]=callback} );
    end,
    addListener=function(self, cmd,callback )
        self.data.listeners[cmd]=self.data.listeners[cmd] or {}
        table.insert(self.data.listeners[cmd],1,callback)
    end,
    removeListener=function(self, cmd,callback )
        local list=self.data.listeners[cmd]
        if list==nil then
            return;
        end
        for k,v in pairs(list) do
            printf.log("removeListener  "..k)
            if v==callback then
                table.remove(list,k)
            end
        end
    end,
    sendProto=function(self,cmd,data )
        local bytes,msgId=self.data.proto:encode("cs",cmd,data)
        C_TcpManager.Send(msgId,bytes)
    end,
    sendBytes=function(self,msgId,bytes )
        C_TcpManager.Send(msgId,bytes)
    end,
    C_OnReceive=function(self, msgId,data )
        local cmd=self.data.proto:getSCCmd(msgId)
        if cmd then
            printf.log("msgId:  "..msgId.." cmd: "..cmd)
            local pro=self.data.proto:decode("sc",cmd,data.bytes)
            for k,v in pairs(self.data.listeners) do
                if k==cmd then
                    for i,m in pairs(v) do
                        m(pro)
                    end
                end
            end
        else
            printf.warning("msgId is dont exists  "..msgId)
        end
    end,
    C_OnConnected=function(self)
        printf.log("C_OnConnected")
        for k,v in pairs(self.data.connecteds) do
            v.callback()
            if v["single"]==true then
                self.data.connecteds[k]=nil;
            end
        end
    end,
    C_OnDisconnect=function(self)
        printf.log("C_OnDisconnect")
        for k,v in pairs(self.data.disconnecteds) do
            v.callback()
            if v["single"]==true then
                self.data.disconnecteds[k]=nil;
            end
        end
    end
})