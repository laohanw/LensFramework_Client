local gameobject = CS.UnityEngine.GameObject('Lua_Coroutine')
CS.UnityEngine.Object.DontDestroyOnLoad(gameobject)
local cs_coroutine_runner = gameobject:AddComponent(typeof(CS.lhFramework.Infrastructure.Managers.LuaCoroutine))

local move_end = {}
local generator_mt = {
    __index = {
        MoveNext = function(self)
            self.Current = self.co()
            if self.Current == move_end then
                self.Current = nil
                return false
            else
                return true
            end
        end;
        Reset = function(self)
            self.co = coroutine.wrap(self.w_func)
        end
    }
}
local function cs_generator(func, ...)
    local params = {...}
    local generator = setmetatable({
        w_func = function()
            func(table.unpack(params))
            return move_end
        end
    }, generator_mt)
    generator:Reset()
    return generator
end
coroutine.start=function( ... )
    return cs_coroutine_runner:StartCoroutine(cs_generator(...))
end
coroutine.stop=function()
    cs_coroutine_runner:StopCoroutine(coroutine)
end
coroutine.stopAll=function( )
    cs_coroutine_runner:StopAllCoroutine()
end
coroutine.waitForSeconds=function( value )
    return CS.UnityEngine.WaitForSeconds(value)
end