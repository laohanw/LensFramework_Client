local moduleList = {
    ["info"] = "#ffffff",
    ["main"] = "#0AFFCF"
}
printf = {
    log = function(value, module)
        local s = ""
        local info = debug.getinfo(2, "Sl")
        module = module or "info"
        local m = moduleList[module]
        if type(value)=="table" then
            value=table.print(value)
        elseif type(value)=="function" then
            value="function"
        end
        s =
            info.source ..
            ":" .. info.currentline .. ": " .. "<color=" .. m .. ">[ " .. module .. " ]</color> " .. value .. "\n"
        s = s .. "stack traceback:\n"
        for i = 3, 4, 1 do
            local inf = debug.getinfo(i, "Sl")
            if inf then
                s = s .. "                " .. inf.source .. ":" .. inf.currentline .. "\n"
            end
        end
        print(s)
    end,
    warning = function(value, module)
        local s = ""
        local info = debug.getinfo(2, "Sl")
        module = module or "info"
        local m = moduleList[module]
        if type(value)=="table" then
            value=table.print(value)
        elseif type(value)=="function" then
            value="function"
        end
        s =
            info.source ..
            ":" .. info.currentline .. ": " .. "<color=" .. m .. ">[ " .. module .. " ]</color> " .. value .. "\n"
        s = s .. "stack traceback:\n"
        for i = 3, 10, 1 do
            local inf = debug.getinfo(i, "Sl")
            if inf then
                s = s .. "                " .. inf.source .. ":" .. inf.currentline .. "\n"
            end
        end
        warning(s)
    end,
    error = function(value, module)
        module = module or "info"
        local m = moduleList[module]
        if type(value)=="table" then
            value=table.print(value)
        elseif type(value)=="function" then
            value="function"
        end
        error("<color=" .. m .. ">[ " .. module .. " ]</color> " .. value)
    end
}