
function checknumber(value, base)
  return tonumber(value, base) or 0
end

function checkint(value)
  return math.round(checknumber(value))
end

function checkbool(value)
  return (value ~= nil and value ~= false)
end

function checktable(value)
  if type(value) ~= "table" then value = {} end
  return value
end
