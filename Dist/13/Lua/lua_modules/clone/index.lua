
function clone(object)
  local lookup_table = {}
  local function _copy(obj)
      if type(obj) ~= "table" then
          return obj
      elseif lookup_table[obj] then
          return lookup_table[obj]
      end
      local newObject = {}
      lookup_table[obj] = newObject
      for key, value in pairs(obj) do
          newObject[_copy(key)] = _copy(value)
      end
      return setmetatable(newObject, getmetatable(obj))
  end
  return _copy(object)
end