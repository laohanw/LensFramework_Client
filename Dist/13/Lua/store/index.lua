local glo = require("store/global/index")
return class(
    {
        tag = "store",
        ctor = function(self)
            self.data.dic.global = class(glo)
        end,
        initialize = function()
        end,
        data = {
            dic = {}
        },
        dispatch = function(self, module, actions, params)
            for k, v in pairs(self.data.dic) do
                if k == module then
                    v:dispatch(actions, params)
                    return
                end
            end
            error("dont has this module dispatch  " .. module)
        end,
        commit = function(self, module, commits, params)
            for k, v in pairs(self.data.dic) do
                if k == module then
                    v:commit(commits, params)
                    return
                end
            end
            error("dont has this module dispatch" .. module)
        end,
        getter = function(self, module, params)
            for k, v in pairs(self.data.dic) do
                if k == module then
                    return v:getter(params)
                end
            end
            error("dont has this module getter" .. module)
        end,
        mapActions = function(self, module, maps)
        end,
        mapStates = function(self, module, states)
        end,
        mapMutations = function(self, module, mutations)
        end
    }
)

--[[ test:

  L_Store:dispatch("global","testActions",2);
  L_Store:commit("global","SET_PLAYERID",3);
  local playerId=L_Store:getter("global","playerId");
  print(playerId);

--]]
