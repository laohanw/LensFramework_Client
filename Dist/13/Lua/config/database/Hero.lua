local __rt_1 = {
}
local Hero = 
{
	{
		attributes = {
			attack = {
				canCrit = true,
				hitRate = 0.9,
				magic = 100,
				physical = 20,
				ult = {
					{
						cd = 5,
						name = "CoupDeGrace",
						params = {
							"circle",
							1,
							0,
							true
						}
					}
				}
			},
			defence = {
				magic = 60,
				physical = 10
			},
			hp = 200,
			modelSize = {
				4,
				5,
				10.5
			},
			petPhrase = "I will kill you!"
		},
		isOpen = true,
		name = "@73209"
	},
	{
		heroId = 2,
		isOpen = true,
		name = "@435339",
		type = 2
	},
	{
		heroId = 3,
		name = "@452332",
		type = 3
	},
	{
		attributes = {
			attack = {
				canCrit = true,
				hitRate = 0.9,
				magic = 0,
				physical = 140,
				ult = {
					{
						cd = 5,
						name = "CoupDeGrace",
						params = {
							"sector",
							150,
							0,
							true
						}
					}
				}
			},
			defence = {
				magic = 0,
				physical = 40
			},
			hp = 150,
			modelSize = {
				3.5,
				4,
				8
			},
			petPhrase = "Death to all who oppose me!"
		},
		heroId = 4,
		type = 4
	}
}
local __default_values = {
	attributes = __rt_1,
	defaultStar = 1,
	heroId = 1,
	isOpen = false,
	name = "@362396",
	rare = 11,
	type = 1
}
do
	local base = { __index = __default_values, __newindex = function() error( "Attempt to modify read-only table" ) end }
	for k, v in pairs( Hero ) do
		setmetatable( v, base )
	end
	base.__metatable = false
end

return Hero
