local HeroEquipmentDict = 
{
	{
		[10] = {
			{
				equipRank = 1,
				propId = 200001
			},
			{
				equipRank = 1,
				propId = 200002
			},
			{
				equipRank = 1,
				propId = 200003
			},
			{
				equipRank = 1,
				propId = 200004
			}
		},
		[20] = {
			{
				equipRank = 5,
				propId = 200005
			},
			{
				equipRank = 5,
				propId = 200006
			},
			{
				equipRank = 5,
				propId = 200007
			},
			{
				equipRank = 5,
				propId = 200008
			}
		},
		[21] = {
			{
				equipRank = 10,
				propId = 200001
			},
			{
				equipRank = 10,
				propId = 200002
			},
			{
				equipRank = 10,
				propId = 200003
			},
			{
				equipRank = 10,
				propId = 200004
			}
		},
		[30] = {
			{
				equipRank = 20,
				propId = 200005
			},
			{
				equipRank = 20,
				propId = 200006
			},
			{
				equipRank = 20,
				propId = 200007
			},
			{
				equipRank = 20,
				propId = 200008
			}
		},
		[31] = {
			{
				equipRank = 25,
				propId = 200001
			},
			{
				equipRank = 25,
				propId = 200002
			},
			{
				equipRank = 25,
				propId = 200003
			},
			{
				equipRank = 25,
				propId = 200004
			}
		},
		[32] = {
			{
				equipRank = 30,
				propId = 200005
			},
			{
				equipRank = 30,
				propId = 200006
			},
			{
				equipRank = 30,
				propId = 200007
			},
			{
				equipRank = 30,
				propId = 200008
			}
		},
		[40] = {
			{
				equipRank = 40,
				propId = 200001
			},
			{
				equipRank = 40,
				propId = 200002
			},
			{
				equipRank = 40,
				propId = 200003
			},
			{
				equipRank = 40,
				propId = 200004
			}
		},
		[41] = {
			{
				equipRank = 45,
				propId = 200005
			},
			{
				equipRank = 45,
				propId = 200006
			},
			{
				equipRank = 45,
				propId = 200007
			},
			{
				equipRank = 45,
				propId = 200008
			}
		},
		[42] = {
			{
				equipRank = 50,
				propId = 200001
			},
			{
				equipRank = 50,
				propId = 200002
			},
			{
				equipRank = 50,
				propId = 200003
			},
			{
				equipRank = 50,
				propId = 200004
			}
		},
		[43] = {
			{
				equipRank = 55,
				propId = 200005
			},
			{
				equipRank = 55,
				propId = 200006
			},
			{
				equipRank = 55,
				propId = 200007
			},
			{
				equipRank = 55,
				propId = 200008
			}
		}
	}
}

return HeroEquipmentDict
