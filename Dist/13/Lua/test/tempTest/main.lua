local rapidjson = require 'rapidjson' 
local t = rapidjson.decode('{"a":123}')
print(t.a)
t.a = 456
local s = rapidjson.encode(t)
print('json', s)
------------------------------------
local lpeg = require 'lpeg'
print(lpeg.match(lpeg.R '09','123'))
------------------------------------
local pb = require 'pb'
local protoc = require 'plugins/protoc'

assert(protoc:load [[
message Phone {
    optional string name        = 1;
    optional int64  phonenumber = 2;
}
message Person {
    optional string name     = 1;
    optional int32  age      = 2;
    optional string address  = 3;
    repeated Phone  contacts = 4;
} ]])

local data = {
name = 'ilse',
age  = 18,
    contacts = {
        { name = 'alice', phonenumber = 12312341234 },
        { name = 'bob',   phonenumber = 45645674567 }
    }
}

local bytes = assert(pb.encode('Person', data))
print(pb.tohex(bytes))

local data2 = assert(pb.decode('Person', bytes))
print(data2.name)
print(data2.age)
print(data2.address)
print(data2.contacts[1].name)
print(data2.contacts[1].phonenumber)
print(data2.contacts[2].name)
print(data2.contacts[2].phonenumber)
---------------------------------
local ffi = require('ffi')
ffi.cdef [[
    typedef struct {int fake_id;unsigned int len;} CSSHeader;
]]
ffi.cdef [[
    typedef struct {
        CSSHeader header;
        float x;
        float y;
        float z;
    } Vector3;
]]

local Vector3Native = ffi.typeof('Vector3 *')
local v = CS.UnityEngine.Vector3(1, 2, 3)
print("vector3",v)
local vn = ffi.cast(Vector3Native, v)
print(vn)
if vn.header.fake_id == -1 then
    print('vector { ', vn.x, vn.y, vn.z, '}')
else
    print('please gen code')
end

------------------------------------------

local LuaManager=CS.lhFramework.Infrastructure.Managers.LuaManager

local pb = require 'pb'
local bytes=LuaManager.LoadFileBytes("proto/test.pb")
print(bytes)
local ret,pos=assert(pb.load(bytes))
print(ret,pos)
local data = {
name = 'ilse',
age  = 18,
    contacts = {
        { name = 'alice', phonenumber = 12312341234 },
        { name = 'bob',   phonenumber = 45645674567 }
    }
}
local by=assert(pb.encode("com.youzu.got.protocol.Person",data))
print(by)
local deco=assert(pb.decode("com.youzu.got.protocol.Person",by))
print(deco.age)
