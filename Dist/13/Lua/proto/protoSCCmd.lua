--【协议号文件】由脚本自动产生，避免手动修改内容

--协议包名
local package = "com.lh.demo.protocol";
return {
  ["SC_LoginRequest"] = { id = 30001, value = package..".LoginRequest"},
  ["SC_ProtoTestSturct"] = { id = 40005, value = package..".ProtoTestSturct"},
  parse = function(self, id)
    for k in pairs(self) do
       if self[k]~=nil and type(self[k])=="table" and  self[k].id == id then
         return k,self[k].value;
       end
    end
    return nil;
 end
}